//documentReady
$(function () {
    //ox-quiz 생성
    $("*[data-ui='ox-quiz']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).oxQuiz(option);
    });
});

/*
 *	OxQuiz
 */
(function ($) {
    "use strict";

    var OxQuiz =
        OxQuiz ||
        (function () {
            function initcont() {
                var owner = this;

                this.element.find(".ox-cont .btn").on("click", function (e) {
                    if (!owner.isPlaying) return;

                    $(".main").trigger("click_sound");
                    var pageX = e.pageX;
                    var pageY = e.pageY;
                    if (e.originalEvent.changedTouches) {
                        pageX = e.originalEvent.changedTouches[0].clientX;
                        pageY = e.originalEvent.changedTouches[0].clientY;
                    }
                    var point = {};
                    point.x =
                        (pageX - $("#container").position().left) /
                        window.scale;
                    point.y = pageY / window.scale;

                    check.call(owner, $(this).index() + 1);
                    npcMove.call(owner, exportRoot.npc.me, point, 0);
                });

                this.element.find(".btn-again").on("click", function () {
                    $(".main").trigger("click_sound");
                    owner.restart();
                });

                this.element.find(".btn-point").on("click", function () {
                    $(".main").trigger("click_sound");
                    owner.showPopupPoint();
                });
            }

            function quizStart() {
                var owner = this;
                console.log("quizStart");

                gtag("event", "quiz-start", {});

                exportRoot.timeMc.visible = true;
                exportRoot.npc.visible = true;

                for (var i = 1; i < 9; i++) {
                    var target = exportRoot.npc["npc" + i];
                    target.visible = true;
                    target.right = false;
                    target.type = "";
                    npcGoto.call(owner, target, "base");

                    target = exportRoot.ending["npc" + i];
                    npcGoto.call(owner, target, "base");
                    target.y = -100;
                }

                this.element
                    .find(".popup-point .flag")
                    .on("click", function () {
                        $(".main").trigger("click_sound");
                        $(this).addClass("active");
                        openPoint.call(owner, $(this).index());
                    });

                this.element
                    .find(".popup-point .btn-close")
                    .on("click", function () {
                        $(".main").trigger("click_sound");
                        closePoint.call(owner);
                    });

                if (this.options.skipQuiz != "Y") {
                    playQuiz.call(owner);
                } else {
                    // 엔딩 팝업 테스트
                    this.showPopupPoint();
                }
            }

            function timeover() {
                var owner = this;
                // console.log("타이머 종료");

                exportRoot.timeMc.stop();
                this.isPlaying = false;
                this.timerAudio.stop();

                this.timer = setTimeout(function () {
                    checkRight.call(owner);
                }, 500);
            }

            function timestart() {
                var owner = this;
                // console.log("타이머 시작");

                this.timerAudio.stop();
                this.timerAudio.play();
                exportRoot.timeMc.gotoAndPlay(1);
            }

            function check(value) {
                var owner = this;

                if (value == this.options.answer[this.currentQuiz])
                    this.isRight = true;
                else this.isRight = false;
            }

            function checkRight() {
                var owner = this;

                npcCheck.call(owner);

                if (this.isRight) {
                    this.clearAudio.play();
                    npcGoto.call(owner, exportRoot.npc.me, "right");
                } else {
                    this.failedAudio.play();
                    npcGoto.call(owner, exportRoot.npc.me, "wrong");
                }

                const npcRight = [];
                const npcWrong = [];
                for (let i = 1; i < 9; i++) {
                    const npc = exportRoot.npc["npc" + i];
                    if (npc.type === "right") {
                        npcRight.push(npc);
                    } else if (npc.type === "wrong") {
                        npcWrong.push(npc);
                    }
                }
                const quizText =
                    this.element.find(".question")[owner.currentQuiz].innerText;

                if (owner.isRight) {
                    gtag("event", "quiz-correct", {
                        quizText,
                        quizNum: owner.currentQuiz,
                        npcRightCount: npcRight.length,
                        npcWrongCount: npcWrong.length,
                    });
                } else {
                    gtag("event", "quiz-wrong", {
                        quizText,
                        quizNum: owner.currentQuiz,
                        npcRightCount: npcRight.length,
                        npcWrongCount: npcWrong.length,
                    });
                }

                setTimeout(function () {
                    owner.currentQuiz++;
                    if (
                        owner.currentQuiz ==
                        owner.element.find(".question").length
                    ) {
                        // console.log("5문제 모두 완료");
                        // owner.checkAnswer();
                        owner.showPopupEnd();

                        gtag("event", "quizEnd", {});
                    } else {
                        // console.log("다음 문제 시작");
                        playQuiz.call(owner);
                    }
                }, 2000);
            }

            function playQuiz() {
                var owner = this;

                this.npcRightList = [];
                this.isRight = false;
                this.isPlaying = true;
                this.element.find(".question").hide();
                this.element.find(".question").eq(this.currentQuiz).show();
                timestart.call(owner);

                // npc reset
                exportRoot.npc.me.x = this.meBase.x;
                exportRoot.npc.me.y = this.meBase.y;
                npcGoto.call(owner, exportRoot.npc.me, "base");

                var target = null;
                var point = { x: 0, y: 0 };
                var wait = 0;

                // pointList 셋팅
                for (var i = 0; i < 8; i++) {
                    target = exportRoot.npc["npc" + (i + 1)];
                    target.right = false;
                    if (target.type != "wrong") {
                        target.right = Math.floor(Math.random() * 3)
                            ? true
                            : false;
                    }
                    this.pointList[i] = getPoint.call(owner, target);
                }

                // npc 캐릭터 셋팅
                for (var i = 1; i < 9; i++) {
                    target = exportRoot.npc["npc" + i];
                    npcGoto.call(owner, target, "base");

                    // console.log(i, target.right);

                    if (target.type == "wrong") {
                        target.visible = false;
                        target.right = false;
                    } else {
                        point = this.pointList[i - 1];
                        point = checkPoint.call(owner, target, point);
                        this.pointList[i - 1] = point;
                        wait = 300 + Math.floor(Math.random() * 4000);
                        npcMove.call(owner, target, point, wait);
                    }
                }
            }

            // npc
            function npcMove(target, point, wait) {
                var owner = this;

                target.timer = setTimeout(function () {
                    npcGoto.call(owner, target, "move");
                    createjs.Tween.removeTweens(target);
                    createjs.Tween.get(target)
                        .to(
                            { x: point.x, y: point.y },
                            600,
                            createjs.Ease.sineInOut
                        )
                        .call(function () {
                            target.gotoAndStop(0);
                            npcGoto.call(owner, target, "base");
                            // console.log("움직임 끝");
                        });
                }, wait);
            }

            function npcGoto(target, type) {
                createjs.Tween.removeTweens(target);
                switch (type) {
                    case "":
                    case "base":
                        target.gotoAndStop(0);
                        break;
                    case "move":
                        target.gotoAndStop(1);
                        break;
                    case "right":
                        target.gotoAndStop(2);
                        break;
                    case "wrong":
                        target.gotoAndStop(3);
                        break;
                }
            }

            function npcCheck() {
                var owner = this;

                var target = null;
                var type = "wrong";
                for (var i = 1; i < 9; i++) {
                    target = exportRoot.npc["npc" + i];
                    if (target.x < 640) {
                        if (this.options.answer[this.currentQuiz] == 1)
                            type = "right";
                        else type = "wrong";
                    } else {
                        if (this.options.answer[this.currentQuiz] == 2)
                            type = "right";
                        else type = "wrong";
                    }
                    target.type = type;
                    npcGoto.call(owner, target, type);
                }
            }

            function getPoint(npc) {
                var owner = this;

                var point = { x: 0, y: 0 };
                var target = this.element.find(".btn-o");
                if (Math.floor(Math.random() * 2) == 1)
                    target = this.element.find(".btn-x");

                if (npc) {
                    if (npc.right == true) {
                        if (this.npcRightList.indexOf(npc.name) == -1)
                            this.npcRightList.push(npc.name);
                        target =
                            this.options.answer[this.currentQuiz] == 1
                                ? this.element.find(".btn-o")
                                : this.element.find(".btn-x");
                    }
                }

                point.x = Math.floor(
                    target.position().left / window.scale +
                        Math.random() * target.width()
                );
                point.y = Math.floor(
                    target.position().top / window.scale +
                        Math.random() * target.height()
                );
                return point;
            }

            function checkPoint(target, point) {
                var owner = this;

                var re_point = point;
                var n = target.name.substr(3, 1);
                var loop = true;
                var count = 0;

                while (loop) {
                    loop = false;
                    count++;
                    if (count > 50) {
                        console.log("count over!!");
                        loop = false;
                    }

                    for (var i = 0; i < 8; i++) {
                        if (i + 1 != n) {
                            var gx = Math.abs(this.pointList[i].x - re_point.x);
                            var gy = Math.abs(this.pointList[i].y - re_point.y);
                            if (gx < 80 && gy < 80) {
                                re_point = getPoint.call(owner, target);
                                loop = true;
                            }
                        }
                    }
                }

                return re_point;
            }

            function openPoint(idx) {
                this.element.find(".point-cont .point").hide();
                this.element.find(".point-cont").removeClass("hide");
                this.element.find(".point-cont .point").eq(idx).show();
                this.checkAnswer();
            }

            function closePoint() {
                this.element.find(".point-cont").addClass("hide");
                this.element.find(".point-cont .point").hide();
            }

            return IPattern.extend({
                init: function (element, options) {
                    this._super(element, options);
                    this.clearAudio = null;
                    this.failedAudio = null;
                    this.finishAudio = null;
                    this.timerAudio = null;
                    this.nowAudio = null;
                    this.timer = null;
                    this.clear = false;

                    this.currentQuiz = 0;
                    this.isPlaying = false;
                    this.meBase = { x: 640, y: 692 };
                    this.isRight = false;
                    this.pointList = [];
                    this.npcRightList = [];

                    var owner = this;
                    if (this.options.failedAudio)
                        this.failedAudio = new AudioControl(
                            this.options.failedAudio
                        );
                    if (this.options.clearAudio)
                        this.clearAudio = new AudioControl(
                            this.options.clearAudio
                        );
                    if (this.options.finishAudio)
                        this.finishAudio = new AudioControl(
                            this.options.finishAudio
                        );
                    if (this.options.timerAudio)
                        this.timerAudio = new AudioControl(
                            this.options.timerAudio,
                            {
                                onFinish: function () {
                                    owner.timerAudio.play();
                                },
                            }
                        );

                    initcont.call(this);
                },

                start: function () {
                    var owner = this;

                    exportRoot.npc.me.x = this.meBase.x;
                    exportRoot.npc.me.y = this.meBase.y;

                    exportRoot.timeMc.onFinish = function () {
                        timeover.call(owner);
                    };

                    quizStart.call(this);
                },

                showPopupEnd: function () {
                    clearTimeout(this.timer);

                    this.element.find(".popup-end").removeClass("hide");
                },

                showPopupPoint: function () {
                    // this.checkAnswer();

                    for (var i = 0; i < PageData.endingData.length; i++) {
                        var npc =
                            exportRoot.ending[
                                "npc" + PageData.endingData[i].npc
                            ];
                        npc.x = PageData.endingData[i].x;
                        npc.y = PageData.endingData[i].y;
                    }

                    exportRoot.npc.visible = false;
                    exportRoot.timeMc.visible = false;
                    exportRoot.ending.visible = true;
                    exportRoot.ending.gotoAndStop(1);
                    this.element.find(".quiz-cont").addClass("hide");
                    this.element.find(".ox-cont").addClass("hide");
                    this.element.find(".popup-end").addClass("hide");
                    this.element.find(".popup-point").removeClass("hide");
                },

                restart: function () {
                    this.reset();

                    quizStart.call(this);
                },

                checkAnswer: function () {
                    if (
                        this.element.find(".popup-point .flag").length !=
                        this.element.find(".popup-point .flag.active").length
                    )
                        return;

                    if (!this.clear) {
                        this.clear = true;
                        this.element.data("clear", true);
                        $(".section").trigger("checkClear");
                    }
                },

                reset: function () {
                    this.currentQuiz = 0;

                    clearTimeout(this.timer);
                    exportRoot.npc.visible = true;
                    exportRoot.timeMc.visible = true;
                    exportRoot.ending.visible = false;
                    exportRoot.ending.gotoAndStop(0);

                    this.element.find(".quiz-cont").removeClass("hide");
                    this.element.find(".ox-cont").removeClass("hide");
                    this.element.find(".popup-end").addClass("hide");
                    this.element.find(".popup-point").addClass("hide");
                },

                dispose: function () {},
            });
        })();

    // 메인 기본 옵션
    OxQuiz.DEFAULT = {
        answer: [],
        failedAudio: "",
        clearAudio: "",
        finishAudio: "",
        timerAudio: "",
        customLength: null,
    };

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.oxQuiz");
            var options = $.extend(
                {},
                OxQuiz.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data("ui.oxQuiz", (data = new OxQuiz($this, options)));
            if (typeof option == "string") data[option](params);
            $this.data("quizType", "oxQuiz");
        });
    }

    window.OxQuiz = OxQuiz;

    $.fn.oxQuiz = Plugin;
    $.fn.oxQuiz.Constructor = OxQuiz;
})(jQuery);
