//documentReady
$(function ()
{
	//quizShow 생성
	$("*[data-ui='quizShow']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).quizShow(option);
	});

});

/*
*	QuizShow
*/
(function ($){
	'use strict';

	var QuizShow = QuizShow || (function ()
	{
		function initCont()
		{
			
			var owner = this;

			setEvent.call(this);
			setAudio.call(this);
		}

		function setAudio()
		{
			var owner = this;

			if(this.options.introAudio) this.introAudio = new AudioControl( this.options.introAudio, {});

			var audio;
			/*
			for(var i = 0; i < PageData.narration.length; i++)
			{
				audio = new AudioControl(PageData.narration[i], {onFinish:function(audioDom){
				}});
				this.audioList.push(audio);
			}
			*/
		}

		function playAudio(audio)
		{
			if(this.nowAudio != null && this.nowAudio != audio)
			{
				this.nowAudio.stop();
			} 

			if(audio == null) return;

			this.nowAudio = audio;
			this.nowAudio.play();
		}

		function setEvent()
		{
			var owner = this;
			
			// 완강 체크;
			this.element.on("checkClear",function(){
				if(owner.element.find("#ox-quiz").data("clear"))
				{
					console.log("완강 처리 ");
					$(".main").trigger("studyFinish");
				}
			});
		}

		function startQuizShow()
		{
			var owner = this;
			
			if(this.options.skipIntro != "Y")
			{
				// 1. 인트로
				exportRoot.intro.visible = true;
				setTimeout(function(){
					owner.introAudio.play();
					exportRoot.intro.gotoAndPlay(1);
				}, 100);
				exportRoot.intro.onFinish=function()
				{
					console.log("인트로 끝");
					exportRoot.intro.visible = false;
					owner.element.find("#ox-quiz").removeClass("hide");
					owner.element.find("#ox-quiz").oxQuiz("start");
				}
			}
			else
			{
				// 2. oxQuiz
				owner.element.find("#ox-quiz").removeClass("hide");
				owner.element.find("#ox-quiz").oxQuiz("start");
			}
		}

		return ISection.extend({

			init : function (element, options)
			{
				this._super(element, options);
				this.currentGame = 0;
				this.audioList = [];
				this.nowAudio = null;
				this.introAudio = null;

				initCont.call(this);
			},

			start : function ()
			{
				startQuizShow.call(this);
			},

			reset : function()
			{
				this.nowAudio = null;
			},

			dispose : function ()
			{
				
			}	
		});

	})();

	// 기본 옵션
	QuizShow.DEFAULT = {};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.quizShow');
            var options =  $.extend({}, QuizShow.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.quizShow', (data = new QuizShow($this, options)));
			if (typeof option == 'string') data[option](params);
        });
    }

	window.QuizShow = QuizShow;

    $.fn.quizShow = Plugin;
    $.fn.quizShow.Constructor = QuizShow;

})(jQuery);

var exportRoot;