"use strict";

var PageData = {
	// 오디오
	narration :[
		"./audio/k_01.mp3", 
    ],
    
    subtitleData :
    [
        // 캐릭터 번호 object로 입력 가능. {ch:[1,6],ch_subtitle:6}
        // 0:Jiho, 1:Eric, 2:Yuri, 3:Hailey, 4:Jayden
        // 장면 1
        [
            3.913, 4.763, 0,
            "Hello?",
            "여보세요?",
        ],
        [
            4.983, 6.014, 1,
            "Hello?",
            "여보세요?",
        ],
        [
            6.477, 9.799, 0,
            "Eric, let’s play soccer this Saturday.",
            "에릭, 이번주 토요일에 축구 하자.",
        ],
        [
            10.201, 14.236, 1,
            "Sorry, but I take a cooking class on Saturdays.",
            "미안하지만, 나는 토요일마다 요리 수업이 있어.",
        ],
        [
            14.653, 16.685, 0,
            "How about this Sunday?",
            "이번주 일요일은 어때?",
        ],
        [
            17.004, 19.014, 1,
            "Oh, okay!",
            "오, 좋아!",
        ],
        // 장면 2
        [
            21.441, 24.201, 2,
            "What do you do on weekends?",
            "너는 주말마다 무엇을 하니?",
        ],
        [
            24.603, 27.167, 3,
            "I usually watch TV.",
            "나는 보통 TV를 봐.",
        ],
        [
            28.517, 31.361, 2,
            "Let’s watch a movie this Saturday.",
            "이번주 토요일에 영화 보자.",
        ],
        [
            31.626, 33.371, 3,
            "Sounds good!",
            "좋은 생각이야!",
        ],
        [
            33.947, 38.346, 4,
            "Sorry, but I have baseball practice on Saturdays.",
            "미안하지만, 나는 토요일마다 야구 연습을 해.",
        ],
        [
            38.981, 40.232, 2,
            "Okay.",
            "알았어.",
        ]
       
    ]
}


