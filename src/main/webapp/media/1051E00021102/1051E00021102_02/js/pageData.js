"use strict";

var PageData = {
	// 오디오
	narration :[
		"./audio/k_01_01.mp3", //친구들이 인사를 나누고 있어~ 대화를 들어볼까?
		"./audio/k_02_01.mp3" //친구들이 안부를 묻고 답하고 있어! 대화를 들어볼까?
	],
	narration_02 :[
		"./audio/k_01_02.mp3", //궁금한 메시지를 눌러 봐!
		"./audio/k_02_02.mp3" //궁금한 메시지를 눌러 봐!
	],
	previewTalk_01 :[
		["",""],
		["",""],
		["./audio/e_01_01.mp3"],
		["./audio/e_01_02.mp3"],
		["./audio/e_01_03.mp3"],
		["./audio/e_01_04.mp3"],
		["./audio/e_01_05.mp3"],
		["./audio/e_01_06.mp3"]
	],
	previewTalk_02 :[
		["",""],
		["",""],
		["./audio/e_02_01.mp3"],
		["./audio/e_02_02.mp3"],
		["./audio/e_02_03.mp3"],
		["./audio/e_02_04.mp3"],
		["./audio/e_02_05.mp3"],
		["./audio/e_02_06.mp3"]
	],
	previewWord_word:["practice","bake","draw","free","time","listen","music","exercise","vegetable","kite","feed","cook"],
	previewWord_word_ko:["연습하다","굽다","그리다","자유의","시간","듣다","음악","운동, 연습<br/>운동하다","채소","연","먹이를 주다","요리하다"],
	previewWord_word_mp3:["./audio/e_03_01.mp3","./audio/e_03_02.mp3","./audio/e_03_03.mp3","./audio/e_03_04.mp3",
						"./audio/e_03_05.mp3","./audio/e_03_06.mp3","./audio/e_03_07.mp3","./audio/e_03_08.mp3",
						"./audio/e_03_09.mp3","./audio/e_03_10.mp3","./audio/e_03_11.mp3","./audio/e_03_12.mp3"],
}
