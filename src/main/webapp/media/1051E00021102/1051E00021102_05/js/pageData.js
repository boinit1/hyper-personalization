"use strict";

var PageData = {
	// 나레이션 퀴즈1,2 타이틀
	na_mp3 : [
		"./audio/k_01_01.mp3",
		"./audio/k_02_01.mp3"
	],
	// 정오 효과음, 카드 성공 실패 효과음, 엔딩 효과음
	eff_mp3 : [
		"./audio/quiz_o.mp3",
		"./audio/quiz_x.mp3",
		"./audio/card_o.mp3",
		"./audio/card_x.mp3",
		"./audio/goodending.mp3",
		"./audio/badending.mp3",
		"./audio/good.mp3"
	],
	// 타이머 효과음
	timer_mp3 : "./audio/timer.mp3",
	// game-drag
	sentence_mp3 : [
		"./audio/e_01_01.mp3",
		"./audio/e_01_02.mp3",
		"./audio/e_01_03.mp3",
		"./audio/e_01_04.mp3",
		"./audio/e_01_05.mp3",

	]
}


