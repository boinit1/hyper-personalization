//documentReady
$(function () {
    //preview 생성
    $("*[data-ui='preview']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).preview(option);
    });
});

/*
 *	Preview
 */
(function ($) {
    "use strict";

    var Preview =
        Preview ||
        (function () {
            function initCont() {
                var owner = this;

                setAudio.call(this);
                setTalkEvent.call(this);
                setWordEvent.call(this);

                // ============== 테스트
                // setTimeout(function (){owner.start();}, 200);

                // tab 버튼
                this.element.find(".tab .num").each(function (i) {
                    $(this)
                        .off("click")
                        .on("click", $.proxy(onChangeTalk, owner, i));
                });

                if (this.totalTalk < 2) this.element.find(".tab").hide();

                //word 버튼
                this.element
                    .find(".btn-word")
                    .on("click", $.proxy(startWord, owner));

                // go 버튼
                this.element.find(".na_box .btn").on("click", function () {
                    if ($(this).is(".on")) return;
                    $(this).addClass("on").removeClass("effect");

                    stopAudio.call(owner);
                    $(".main").trigger("click_sound");
                    $(".main").removeClass("disable");
                    TweenLite.to(
                        owner.element
                            .find(".na_container")
                            .eq(owner.currentTalk),
                        1,
                        {
                            autoAlpha: 0,
                            y: -20,
                            ease: Sine.easeIn,
                            onComplete: function () {
                                startTalk.call(owner);
                            },
                        }
                    );
                });
            }

            function setAudio() {
                var owner = this;

                var audio;
                for (var i = 0; i < PageData.narration.length; i++) {
                    audio = new AudioControl(PageData.narration[i], {
                        onFinish: function (audioDom) {
                            $(".main").removeClass("disable");
                            if (
                                !owner.element
                                    .find(".na_container")
                                    .eq(owner.currentTalk)
                                    .find(".na_box .btn")
                                    .is(".on")
                            ) {
                                owner.element
                                    .find(".na_container")
                                    .eq(owner.currentTalk)
                                    .find(".na_box .btn")
                                    .addClass("effect");
                                /*
						owner.element.find(".na_container").eq(owner.currentTalk).find(".na_box .btn").addClass("on");
						TweenLite.to(owner.element.find(".na_container").eq(owner.currentTalk), 1, {autoAlpha:0, y:-20, ease:Sine.easeIn, onComplete:function(){
							startTalk.call(owner);
						}})
						*/
                            }
                        },
                    });
                    this.audioList.push(audio);
                }
            }

            function playAudio(audio) {
                if (this.nowAudio != null && this.nowAudio != audio) {
                    stopAudio.call(this);
                }

                if (audio == null) return;

                this.nowAudio = audio;
                this.nowAudio.play();

                gtag &&
                    gtag("event", "audio-play", {
                        id: audio.source,
                        timer: audio.timer,
                    });
            }

            function stopAudio() {
                if (this.nowAudio != null) {
                    gtag &&
                        gtag("event", "audio-stop", {
                            id: this.nowAudio.source,
                            timer: this.nowAudio.timer,
                        });

                    this.nowAudio.stop();
                    this.nowAudio = null;
                }
            }

            function onChangeTalk(idx) {
                if (idx == this.currentTalk) return;

                $(".main").trigger("click_sound");

                this.element
                    .find(".preview-talk")
                    .eq(this.currentTalk)
                    .previewTalk("stop");

                this.currentTalk = idx;
                this.start();

                this.element.find(".tab .num").removeClass("on");
                this.element
                    .find(".tab .num")
                    .eq(this.currentTalk)
                    .addClass("on");
            }

            function startPreview() {
                console.log("111 startPreview");
                var owner = this;

                resetCCBtn.call(owner);

                $(".main").addClass("disable");
                this.element
                    .find(".na_container")
                    .eq(this.currentTalk)
                    .find(".na_box .btn")
                    .addClass("on");
                this.element
                    .find(".na_container")
                    .eq(this.currentTalk)
                    .removeClass("hide");

                TweenLite.set(
                    this.element.find(".na_container").eq(this.currentTalk),
                    { opacity: 0, y: 20 }
                );
                TweenLite.to(
                    this.element.find(".na_container").eq(this.currentTalk),
                    1,
                    {
                        autoAlpha: 1,
                        y: 0,
                        ease: Cubic.easeOut,
                        onComplete: function () {
                            // 나래이션 재생 완료 후 창 닫기;
                            console.log(
                                "Na: 친구들이 인사를 나누고 있어~ 대화를 들어볼까?"
                            );
                            var audio = owner.audioList[owner.currentTalk];
                            console.log(audio.source);
                            playAudio.call(owner, audio);

                            // Go 버튼 활성화
                            owner.element
                                .find(".na_container")
                                .eq(owner.currentTalk)
                                .find(".na_box .btn")
                                .removeClass("on");
                        },
                    }
                );
                // 썸네일 변경
                if (this.element.find(".avata_thumbnail").length > 1) {
                    this.element.find(".avata_thumbnail").addClass("hide");
                    this.element
                        .find(".avata_thumbnail")
                        .eq(this.currentTalk)
                        .removeClass("hide");
                }
            }

            function setTalkEvent() {
                var owner = this;

                // this.element.find(".preview-talk-group .preview-talk").on("talkStart", $.proxy(resetCCBtn, owner));

                this.element
                    .find(".preview-talk-group .preview-talk")
                    .on("talkEnd", function () {
                        console.log("Na: 궁금한 메시지를 눌러 봐!");
                        console.log(PageData.narration_02[owner.currentTalk]);
                        var audio = new AudioControl(
                            PageData.narration_02[owner.currentTalk],
                            {}
                        );
                        owner.nowAudio = audio;
                        audio.play();
                    });

                this.element
                    .find(".preview-talk-group .preview-talk")
                    .on("playEffect", function () {
                        console.log(
                            "on.playEffect: ",
                            owner.currentTalk,
                            " / ",
                            owner.totalTalk
                        );
                        if (owner.currentTalk == owner.totalTalk - 1)
                            exportRoot.word.gotoAndStop(1);
                        else {
                            if (owner.currentTalk == 0) {
                                exportRoot.num2.visible = true;
                                exportRoot.num2.gotoAndStop(1);
                            }
                        }
                    });

                this.element
                    .find(".preview-talk-group .preview-talk")
                    .on("textClickClear", function () {
                        console.log(
                            "textBox 클릭 완료 : num2, word 버튼 cc 애니 효과 실행"
                        );
                        if (owner.currentTalk == 0) {
                            exportRoot.num2.visible = true;
                            exportRoot.num2.gotoAndStop(1);
                        }
                    });
            }

            function setWordEvent() {
                var owner = this;
                this.element.find(".preview-word").on("wordStart", function () {
                    resetCCBtn.call(owner);
                });
                this.element
                    .find(".preview-word")
                    .on("wordEnd", $.proxy(endWord, owner));
                this.element.find(".preview-word").on("wordClear", function () {
                    $(".main").trigger("studyFinish");
                });
            }

            function startTalk() {
                var owner = this;

                this.element
                    .find(".preview-talk-group .preview-talk")
                    .eq(this.currentTalk)
                    .previewTalk("start");
            }

            function startWord() {
                gtag('event', 'start-word');

                $(".main").trigger("click_sound");
                $(".main").trigger("stop_bgm");
                this.element.find(".btn-word").addClass("disable");
                this.element
                    .find(".preview-talk-group .preview-talk")
                    .eq(this.currentTalk)
                    .previewTalk("stop");
                this.element.find(".preview-word").previewWord("start");
            }
            function endWord() {
                $(".main").trigger("start_bgm");
                this.element.find(".btn-word").removeClass("disable");
                this.element
                    .find(".preview-talk-group .preview-talk")
                    .eq(this.currentTalk)
                    .previewTalk("start");
            }
            function resetCCBtn() {
                exportRoot.num2.gotoAndStop(0);
                exportRoot.word.gotoAndStop(0);
            }

            return ISection.extend({
                init: function (element, options) {
                    this._super(element, options);
                    this.currentTalk = 0;
                    this.totalTalk = this.element.find(".preview-talk").length;
                    this.audioList = [];
                    this.nowAudio = null;
                    this.isReady = false;

                    initCont.call(this);
                },

                start: function () {
                    startPreview.call(this);
                },

                reset: function () {
                    this.nowAudio = null;
                },

                dispose: function () {},
            });
        })();

    // 기본 옵션
    Preview.DEFAULT = {};

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.preview");
            var options = $.extend(
                {},
                Preview.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data("ui.preview", (data = new Preview($this, options)));
            if (typeof option == "string") data[option](params);
        });
    }

    window.Preview = Preview;

    $.fn.preview = Plugin;
    $.fn.preview.Constructor = Preview;
})(jQuery);
