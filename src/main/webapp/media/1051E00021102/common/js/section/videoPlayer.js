//documentReady
$(function ()
{
	//video-player 생성
	$("*[data-ui='videoPlayer']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).videoPlayer(option);
	});

});

/*
*	VideoPlayer
*/
(function ($){
	'use strict';

	var VideoPlayer = VideoPlayer || (function ()
	{
		function initVideo()
		{
			var owner = this;

			if(owner.options.rollplay){ owner.element.find(".effect").hide(); }

			var audio = new AudioControl(PageData.narration[0], {});
			this.nowAudio = audio;
			audio.play();

			this.element.find(".subtitle-cont").removeClass("hide");

			var cdnURL = "http://app.wisecamp1.gscdn.com/contents/";

			if(this.options.isLocal == "true") cdnURL = "";
			this.videoControl = new VideoControl( this.element.find(".video-cont"), cdnURL + this.options.source, {
				onUpdate:$.proxy(onUpdate, this),
				onFinish:$.proxy(onFinish, this)
			});

			this.element.find(".btn-playpause").on("click", function(){
				if(owner.videoControl.video.paused)
				{
					$(this).find(".effect").hide();
					if(owner.nowAudio) owner.nowAudio.stop();
					if(owner.videoControl.video.currentTime >= owner.videoControl.video.duration-0.5)
					{
						if(owner.options.rollplay)
						{
							owner.element.find(".btn-roll").removeClass("on");
							owner.element.find(".roll-popup").removeClass("hide");
						}
						else
						{
							$(this).addClass("on");
							owner.videoControl.play(0.001);
						}
					}
					else
					{
						$(this).addClass("on");
						owner.videoControl.play();
						owner.element.find(".btn-roll").addClass("on");
					}
					owner.element.find(".btn-replay").addClass("hide");
				}
				else
				{
					$(this).removeClass("on");
					owner.videoControl.pause();
				}
			});
			this.element.find(".btn-stop").on("click", function(){
				owner.element.find(".btn-playpause").removeClass("on");
				owner.videoControl.stop();
				owner.reset();
				if(owner.options.rollplay)
				{
					owner.element.find(".btn-roll").removeClass("on");
					owner.element.find(".roll-popup").removeClass("hide");
				}
			});
			this.element.find(".btn-subtitle").on("click", function(){
				if(!$(this).is(".on"))
				{
					$(this).addClass("on");
					owner.element.find(".subtitle .text.en").addClass("remove");
					owner.element.find(".subtitle .text.ko").removeClass("remove");
				}
				else
				{
					if($(this).is(".off"))
					{
						$(this).removeClass("on off");
						owner.element.find(".subtitle .text.en").removeClass("remove");
						owner.element.find(".subtitle .text.ko").addClass("remove");
					}
					else
					{
						$(this).addClass("off");
						owner.element.find(".subtitle .text.en").addClass("remove");
						owner.element.find(".subtitle .text.ko").addClass("remove");
					}
				}
			});
			this.element.find(".btn-roll").on("click", function(){
				if(!$(this).is(".on"))
				{
					if(!owner.videoControl.video.paused)
					{
						$(this).removeClass("on");
						owner.videoControl.stop();
						owner.reset();
					}

					$(this).removeClass("on");
					owner.element.find(".roll-popup").removeClass("hide");
				}
				else
				{
					// 기능 없음
					/*
					owner.videoControl.stop();
					owner.reset();

					$(this).removeClass("on");
					owner.element.find(".roll-popup").removeClass("hide");
					*/
				}
			});
			this.element.find(".roll-cont .ch").on("click", function(){
				$(".main").trigger("click_sound");
				owner.element.find(".roll-cont .characters").removeClass("effect");
				if(!$(this).is(".on"))
				{
					$(this).addClass("on");
				}
				else
				{
					$(this).removeClass("on");
				}

				if(owner.element.find(".roll-cont .ch.on").length > 0)
				{
					owner.element.find(".btn-roll").addClass("on");
					owner.element.find(".roll-cont .btn-play").removeClass("dimmed");
				}
				else
				{
					owner.element.find(".btn-roll").removeClass("on");
					owner.element.find(".roll-cont .btn-play").addClass("dimmed");
				}
			});
			this.element.find(".roll-cont .btn-play").on("click", function(){
				console.log("롤플레이 시작");
                const clickedCharacters = owner.element.find(".roll-cont .characters .ch.on .name").map((idx, a) => a.innerText).toArray();
                gtag('event', 'roleplay-start', {
                    selectedCharacters: clickedCharacters.join(",")
                })

				if(owner.nowAudio) owner.nowAudio.stop();
				$(".main").trigger("click_sound");
				owner.element.find(".roll-popup").addClass("hide");
				owner.isRollPlay = true;
				owner.element.find(".btn-playpause").addClass("on");
				owner.videoControl.play(0.001);
				owner.element.find(".btn-replay").addClass("hide");

			});
			this.element.find(".btn-replay").on("click", function(){
				if(owner.options.rollplay)
				{
					owner.element.find(".btn-roll").removeClass("on");
					owner.element.find(".roll-popup").removeClass("hide");
				}
				else
				{
					owner.element.find(".btn-playpause").addClass("on");
					owner.videoControl.play(0.001);
					owner.element.find(".btn-replay").addClass("hide");
					owner.element.find(".btn-roll").addClass("on");
				}
			});

			if(this.options.rollplay)
			{
				this.element.find(".btn-roll").removeClass("remove");
				this.element.find(".btn-roll").trigger("click");
			}
			else
			{
				this.element.find(".btn-subtitle").removeClass("remove");
			}

		}
		function onUpdate(video, per)
		{
			var owner = this;

			var currentTime = video.currentTime;

			// 자막, 롤플레이
			if(this.subtitleData)
			{
				var index = null;
				var st = 0;
				var et = 0;
				var ch = null;
				var ch_arr = [];
				var eng = "";
				var kor = "";
				var muted = false;

				for(var i = 0; i < this.subtitleData.length; ++i)
				{
					st = this.subtitleData[i][0];
					et = this.subtitleData[i][1];
					if(currentTime >= st && currentTime < et)
					{
						index = i;
						eng = this.subtitleData[i][3];
						kor = this.subtitleData[i][4];
						ch = this.subtitleData[i][2];
						break;
					}
				}

				if(this.subtitleIndex != index)
				{
					this.subtitleIndex = index;
					if(index != null)
					{
						if(typeof ch == "number")
						{
							ch_arr.push(ch);
						}
						else
						{
							ch_arr = ch.ch;
							ch = ch.ch_subtitle;
						}
						this.element.find(".subtitle .text.en").html(eng);
						this.element.find(".subtitle .text.ko").html(kor);
						this.element.find(".subtitle-cont .character img").css("visibility", "hidden");
						this.element.find(".subtitle-cont .character img").eq(ch).css("visibility", "visible");

						// console.log(this.subtitle.find(".en").innerWidth());
						// console.log(this.subtitle.width());

						if(this.subtitle.find(".en").innerWidth() > (this.subtitle.width()-30))
						{
							var scale = (this.subtitle.width()-30) / this.subtitle.find(".en").innerWidth();
							TweenMax.set(this.subtitle.find(".en"),{scaleX:scale, scaleY:scale});
						}

						// 롤플레이
						if(ch != null)
						{
							for(var i = 0; i < ch_arr.length; i++)
							{
								if(this.element.find(".roll-cont .characters .ch").eq(ch_arr[i]).is(".on"))
								{
									muted = true;
								}
							}
						}
					}
					else
					{
						this.element.find(".subtitle .text.en").html("");
						this.element.find(".subtitle .text.ko").html("");
						TweenMax.set(this.subtitle.find(".en"),{scaleX:1,scaleY:1});

						this.element.find(".subtitle-cont .character img").css("visibility", "hidden");
					}

					if(video.muted != muted) video.muted = muted;

					// 따라말하기 팝업
					if(video.muted)
					{
						this.element.find(".repeat-cont").removeClass("hide");
						if(this.rollAudio) this.rollAudio.stop();
						if(this.rollAudio) this.rollAudio.play();
						// this.videoControl.pause();
						// $(".main").addClass("disable");
					}
					else this.element.find(".repeat-cont").addClass("hide");
				}

				// 따라말하기 바
				var bar = this.element.find(".repeat-cont .repear-bar");
				per = (this.subtitleIndex == null) ? 0 : 1 - (et-currentTime)/(et-st);
				TweenLite.set(bar, {x:(per*180)-180});
			}
		}
		function onFinish(video)
		{
			var owner = this;

			console.log("finish");
			// console.log(this.options.rollplay);
			video.currentTime = video.duration;
			this.element.find(".btn-replay").removeClass("hide");
			this.element.find(".btn-playpause").removeClass("on");
			owner.element.find(".btn-roll").removeClass("on");
			owner.reset();
			owner.end();
		}

		return ISection.extend({

			init : function (element, options)
			{
				this._super(element, options);

				this.videoControl = null;
				this.isRollPlay = false;
				this.clear = false;
				this.subtitleData = PageData.subtitleData || null;
				this.subtitleIndex = null;
				this.subtitle = this.element.find(".subtitle");
				this.nowAudio = null;

				var owner = this;
				this.rollAudio = null;
				if(this.options.rollAudio) this.rollAudio = new AudioControl(this.options.rollAudio, {onFinish:function(){
					// owner.videoControl.play();
					// $(".main").removeClass("disable");
				}});

				initVideo.call(this);
			},

			start : function ()
			{
				console.log("start");
			},

			reset : function ()
			{
				this.isRollPlay = false;
				this.element.find(".subtitle .text.en").html("");
				this.element.find(".subtitle .text.ko").html("");
				this.videoControl.video.muted = false;
				this.element.find(".roll-cont .characters .ch").removeClass("on");
				this.element.find(".roll-cont .btn-play").addClass("dimmed");
				this.element.find(".btn-roll").removeClass("on");
				this.element.find(".repeat-cont").addClass("hide");
			},

			end : function ()
			{
				if(!this.clear)
				{
					console.log("완강처리");
					this.clear = true;
					this.element.data("clear", true);
					$(".main").trigger("studyFinish");
				}
			},

			dispose : function ()
			{

			}
		});

	})();

	// 기본 옵션
	VideoPlayer.DEFAULT = {source:"",rollplay:false};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.videoPlayer');
            var options =  $.extend({}, VideoPlayer.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.videoPlayer', (data = new VideoPlayer($this, options)));
			if (typeof option == 'string') data[option](params);
			$this.data("quizType", 'videoPlayer');
        });
    }

	window.VideoPlayer = VideoPlayer;

    $.fn.videoPlayer = Plugin;
    $.fn.videoPlayer.Constructor = VideoPlayer;

})(jQuery);

