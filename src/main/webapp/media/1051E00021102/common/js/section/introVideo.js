//documentReady
$(function ()
{
	//intro-video 생성
	$("*[data-ui='introVideo']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).introVideo(option);
	});

});

/*
*	IntroVideo
*/
(function ($){
	'use strict';

	var IntroVideo = IntroVideo || (function ()
	{
		function initVideo()
		{
			this.videoControl = new VideoControl( this.element.find(".video-cont"), this.options.source, {
				onUpdate:$.proxy(onUpdate, this),
				onFinish:$.proxy(onFinish, this)
			});

			this.element.find(".btn-start").on("click", function(){
				console.log("start click");
				$(".main").trigger("studyFinish");
			});

			// this.start();
		}
		function onUpdate(video, per)
		{
			var owner = this;
			
			// console.log("per", per);
			if(this.options.startBtn == "Y")
			{
				if(per > 0.6)
				{
					if(this.element.find(".btn-start").css("display") == "none")
					{
						showBtn.call(owner);
					};
				}
			}
		}
		function onFinish(video)
		{
			// console.log("finish");
			video.currentTime = video.duration;
			if(this.options.startBtn != "Y")
			{
				$(".main").trigger("studyFinish");
			}
		}

		function showBtn()
		{
			this.element.find(".btn-start").show();
			TweenLite.set(this.element.find(".btn-start"), {x:180, y:180});
			TweenLite.to(this.element.find(".btn-start"), 1.5, {x:0, y:0, ease:Elastic.easeOut});
		}

		return ISection.extend({

			init : function (element, options)
			{
				this._super(element, options);

				this.videoControl = null;

				initVideo.call(this);
			},

			start : function ()
			{
				this.videoControl.play();
			},

			dispose : function ()
			{
				
			}	
		});

	})();

	// 기본 옵션
	IntroVideo.DEFAULT = {source:""};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.introVideo');
            var options =  $.extend({}, IntroVideo.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.introVideo', (data = new IntroVideo($this, options)));
			if (typeof option == 'string') data[option](params);
			$this.data("quizType", 'introVideo');
        });
    }

	window.IntroVideo = IntroVideo;

    $.fn.introVideo = Plugin;
    $.fn.introVideo.Constructor = IntroVideo;

})(jQuery);

