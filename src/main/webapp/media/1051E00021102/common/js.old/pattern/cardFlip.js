//documentReady
$(function () {
    //card-flip 생성
    $("*[data-ui='card-flip']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).cardFlip(option);
    });
});

/*
 *	CardFlip
 */
(function ($) {
    "use strict";

    var CardFlip =
        CardFlip ||
        (function () {
            function initCont() {
                var owner = this;

                setWord.call(this);
                setButton.call(this);
                setAudio.call(this);

                // 테스트
                // setTimeout(function(){owner.start.call(owner);},500);
            }

            function setWord() {
                var owner = this;

                this.random_en = $.makeRandom(6, 6);
                this.random_ko = $.makeRandom(6, 6);
                var card;
                for (var i = 0; i < PageData.card_word.length; i++) {
                    this.element
                        .find(".card-item .cardWrapper.en")
                        .eq(i)
                        .find(".text")
                        .html(PageData.card_word[owner.random_en[i]]);
                    this.element
                        .find(".card-item .cardWrapper.en")
                        .eq(i)
                        .parent()
                        .data("idx", owner.random_en[i])
                        .removeClass("eq-0 eq-1 eq-2 eq-3 eq-4 eq-5")
                        .addClass("eq-" + owner.random_en[i]);
                    this.element
                        .find(".card-item .cardWrapper.ko")
                        .eq(i)
                        .find(".text")
                        .html(PageData.card_word_ko[owner.random_ko[i]]);
                    this.element
                        .find(".card-item .cardWrapper.ko")
                        .eq(i)
                        .parent()
                        .data("idx", owner.random_ko[i])
                        .removeClass("eq-0 eq-1 eq-2 eq-3 eq-4 eq-5")
                        .addClass("eq-" + owner.random_ko[i]);
                }
            }

            function setButton() {
                var owner = this;

                this.element.find(".card-item").each(function (i) {
                    $(this).data("ani", null);
                    $(this).addClass("disable");
                    $(this)
                        .off("click")
                        .on(
                            "click",
                            $.proxy(onClickCard, owner, { target: $(this) })
                        );

                    if (owner.isIE) {
                        $(this)
                            .find(".back")
                            .css("backface-visibility", "visible");
                        $(this).find(".back").css("transform", "rotateY(0deg)");
                        $(this).find(".back").addClass("ie");
                    }
                });

                this.element
                    .find(".popup .btn-restart")
                    .on("click", function () {
                        $(".main").trigger("click_sound");
                        owner.restart();
                    });
            }

            function setAudio() {
                var owner = this;

                var audio;

                // 나레이션, Good / Nice
                for (var i = 0; i < PageData.na_mp3.length; i++) {
                    audio = new AudioControl(PageData.na_mp3[i], {});
                    this.naAudioList.push(audio);
                }
                // 정오 효과음
                for (var i = 0; i < PageData.eff_mp3.length; i++) {
                    audio = new AudioControl(PageData.eff_mp3[i], {});
                    this.effAudioList.push(audio);
                }
                // 타이머
                this.timerAudio = new AudioControl(PageData.timer_mp3, {
                    onFinish: function () {
                        console.log("타이머 사운드 종료");
                    },
                });
            }

            function playAudio(audio) {
                if (this.nowAudio != null && this.nowAudio != audio) {
                    this.nowAudio.stop();
                }

                if (audio == null) return;

                this.nowAudio = audio;
                if (this.isPlaying) this.nowAudio.play();

                gtag("event", "audio-play", {
                    id: audio.source,
                    timer: audio.timer,
                });
            }

            function startGame() {
                var owner = this;

                console.log(">> == >> startGame >>");
                // console.log(this.naAudioList);
                this.naAudioList[0].play();

                var delay = 0;
                var complete = null;
                this.element.find(".card-item").removeClass("on");

                owner.element.find(".card-item").each(function (i) {
                    var card = $(this);
                    setTimeout(function () {
                        if (i + 1 == owner.random_en.length * 2)
                            complete = flipClose;
                        flipCard.call(owner, card, complete);
                    }, delay);
                    delay += 100;
                });
            }

            function onRandomAni() {
                var owner = this;

                // if(this.isPlaying && owner.ani == null)
                if (this.isPlaying && owner.ani_length < 2) {
                    this.ani_count++;
                    if (this.ani_count > this.ani_limit) {
                        this.ani_limit = 2 + Math.floor(Math.random() * 10);
                        this.ani_count = 0;
                        var cardList = this.element
                            .find(".card-item")
                            .not(".on")
                            .not(".answer")
                            .not(".disable")
                            .not(".ani");
                        if (cardList.length < 1) return;

                        var ran_idx = $.makeRandom(
                            cardList.length,
                            cardList.length
                        );
                        var card = cardList.eq(ran_idx[0]);
                        var top =
                            card.find(".cardWrapper")[0].offsetTop +
                            card.find(".cardWrapper")[0].offsetParent.offsetTop;
                        var left =
                            card.find(".cardWrapper")[0].offsetLeft +
                            card.find(".cardWrapper")[0].offsetParent
                                .offsetLeft;

                        // cc 애니 사용
                        var ani = card.find(".cardWrapper").is(".en")
                            ? exportRoot.dogMc
                            : exportRoot.catMc;
                        if (ani.card != null)
                            ani = card.find(".cardWrapper").is(".en")
                                ? exportRoot.dogMc1
                                : exportRoot.catMc1;
                        ani.eq = card.index();
                        ani.card = card;
                        card.data("ani", ani);
                        card.addClass("ani");
                        ani.x = left + 80;
                        ani.y = top + 137.5;
                        ani.gotoAndPlay(2);
                        ani.onFinish = function () {
                            owner.ani_length--;
                            this.y = -1000;
                            this.card.data("ani", null);
                            this.card.removeClass("ani");
                            this.card = null;
                            this.eq = null;
                        };
                        owner.ani_length++;
                    }
                }
            }

            function flipClose() {
                var owner = this;

                var delay = 3000;
                var complete = null;
                owner.element.find(".card-item").each(function (i) {
                    var card = $(this);
                    setTimeout(function () {
                        if (i + 1 == owner.random_en.length * 2)
                            complete = flipCloseComplete;
                        flipCard.call(owner, card, complete);
                    }, delay);
                    delay += 100;
                });
            }

            function flipCloseComplete() {
                var owner = this;

                exportRoot.timeMc.onFinish = function () {
                    timeover.call(owner);
                };
                this.timerAudio.stop();
                this.timerAudio.play();
                exportRoot.timeMc.gotoAndPlay(1);

                enableCard.call(this);
                this.ani_timer = setInterval(
                    $.proxy(onRandomAni, owner),
                    1000 / 30
                );
            }

            function onClickCard(e) {
                var owner = this;

                var card = e.target;
                if (card.is(".on")) return;

                if (this.card1 != null) {
                    this.card2 = card;
                    disableCard.call(owner);
                } else {
                    this.card1 = card;
                }
                flipCard.call(owner, card);

                if (card.data("ani") != null) {
                    var ani = card.data("ani");
                    if (ani.eq == card.index()) {
                        resetAni.call(owner, card);
                    }
                }

                $(".main").trigger("click_sound");
            }

            function resetAni(card) {
                var owner = this;

                var ani = card.data("ani");
                ani.y = -1000;
                ani.gotoAndStop(0);
                ani.eq = null;
                ani.card = null;
                card.data("ani", null);
                card.removeClass("ani");

                owner.ani_length--;
            }

            function resetAniAll() {
                var owner = this;

                this.element.find(".card-item").each(function () {
                    var ani = $(this).data("ani");
                    if (ani != null) resetAni.call(owner, $(this));
                });
            }

            function disableCard() {
                this.element.find(".card-item").addClass("disable");
            }

            function enableCard() {
                this.element
                    .find(".card-item")
                    .not(".answer")
                    .removeClass("disable");
            }

            /**
             * @param {DOM} card : .cardWrapper
             * @param {function} complete : flip ani complete
             */
            function flipCard(card, complete) {
                var owner = this;

                if (card.is(".on")) {
                    if (this.isIE) {
                        TweenLite.to(card.find(".card"), 0.3, {
                            scaleX: 0,
                            ease: Back.easeIn,
                            onComplete: function () {
                                card.find(".front").css(
                                    "visibility",
                                    "visible"
                                );
                                card.find(".back").css("visibility", "hidden");
                            },
                        });
                        TweenLite.to(card.find(".card"), 0.3, {
                            delay: 0.3,
                            scaleX: 1,
                            ease: Back.easeOut,
                            onComplete: function () {
                                card.removeClass("on");
                            },
                        });
                    } else {
                        TweenLite.to(card.find(".card"), 0.8, {
                            rotationY: 0,
                            ease: Back.easeOut,
                            onComplete: function () {
                                card.removeClass("on");
                            },
                        });
                    }
                } else {
                    card.addClass("on");
                    if (this.isIE) {
                        TweenLite.to(card.find(".card"), 0.3, {
                            scaleX: 0,
                            ease: Back.easeIn,
                            onComplete: function () {
                                card.find(".front").css("visibility", "hidden");
                                card.find(".back").css("visibility", "visible");
                            },
                        });
                        TweenLite.to(card.find(".card"), 0.3, {
                            delay: 0.3,
                            scaleX: 1,
                            ease: Back.easeOut,
                        });
                    } else {
                        TweenLite.to(card.find(".card"), 0.8, {
                            rotationY: 180,
                            ease: Back.easeOut,
                        });
                    }

                    setTimeout(function () {
                        flipCardComplite.call(owner, card);
                    }, 800);
                }
                setTimeout(function () {
                    if (complete) complete.call(owner);
                }, 800);
            }

            function flipCardComplite(card) {
                var owner = this;

                if (this.card2 == null) return;
                if (card.data("idx") != this.card2.data("idx")) return;
                if (this.card2 != card) return;

                gtag("event", "flip-card-complete", {
                    card1: this.card1[0].outerText,
                    card2: this.card2[0].outerText,
                    result: {
                        completion: false,
                        success:
                            this.card1.data("idx") == this.card2.data("idx"),
                    },
                });

                if (this.card1.data("idx") == this.card2.data("idx")) {
                    console.log("정답");
                    playAudio.call(owner, owner.effAudioList[2]);
                    this.card1.addClass("answer disable");
                    this.card2.addClass("answer disable");
                    owner.card1 = null;
                    owner.card2 = null;
                    enableCard.call(owner);
                    if (owner.checkEnd()) {
                        console.log("낱말 액티비티 완료");
                        owner.stop();
                    }
                } else {
                    console.log("오답");
                    playAudio.call(owner, owner.effAudioList[3]);
                    setTimeout(function () {
                        flipCard.call(owner, owner.card1);
                        flipCard.call(owner, owner.card2);
                        owner.card1 = null;
                        owner.card2 = null;
                        enableCard.call(owner);
                    }, 300);
                }
            }

            function timeover() {
                var owner = this;
                console.log("타이머 종료");

                this.timerAudio.stop();

                playAudio.call(owner, owner.effAudioList[5]);

                this.isPlaying = false;
                this.element.find(".popup").removeClass("hide");
                resetAniAll.call(owner);
                this.element.trigger("End");
            }

            return IPattern.extend({
                init: function (element, options) {
                    this._super(element, options);
                    this.random_en;
                    this.random_ko;
                    this.naAudioList = [];
                    this.effAudioList = [];
                    this.audioList = [];
                    this.timerAudio = null;
                    this.nowAudio = null;
                    this.isPlaying = false;
                    this.card1 = null;
                    this.card2 = null;
                    this.timer = null;
                    this.ani_length = 0;
                    this.ani_timer = null;
                    this.ani_count = 0;
                    this.ani_limit = 30;
                    this.isIE = $.checkBroswerIE();
                    // console.log("IE? ", this.isIE);

                    initCont.call(this);
                },

                restart: function () {
                    var owner = this;

                    exportRoot.timeMc.gotoAndStop(0);
                    this.element.find(".popup").addClass("hide");

                    this.element.find(".card-item").each(function (i) {
                        if ($(this).is(".on")) {
                            flipCard.call(owner, $(this));
                        }
                        $(this).removeClass("answer on").addClass("disable");
                    });

                    this.timer = setTimeout(function () {
                        setWord.call(owner);
                        owner.start();
                    }, 800);
                },

                start: function () {
                    this.reset();

                    this.element.removeClass("hide");
                    this.element.trigger("Start");

                    exportRoot.timeMc.visible = true;
                    this.isPlaying = true;
                    this.element.find(".popup").addClass("hide");
                    startGame.call(this);
                },

                stop: function () {
                    var owner = this;

                    this.isPlaying = false;
                    this.element.trigger("End");
                    exportRoot.timeMc.stop();
                    exportRoot.timeMc.visible = false;
                    this.timerAudio.stop();

                    this.timer = setTimeout(function () {
                        exportRoot.goodMc.gotoAndStop(1);
                        owner.effAudioList[6].play();
                        owner.element.find(".popup.white").removeClass("hide");

                        setTimeout(function () {
                            owner.effAudioList[4].play();
                        }, 500);
                    }, 1000);
                },

                reset: function () {
                    clearTimeout(this.timer);
                    clearTimeout(this.ani_timer);
                    this.timer = null;
                    this.nowAudio = null;
                    this.isPlaying = false;
                    this.ani_count = 0;
                    this.ani_length = 0;
                    this.card1 = null;
                    this.card2 = null;
                },

                checkEnd: function () {
                    if (
                        this.element.find(".card-item.answer").length ==
                        this.element.find(".card-item").length
                    ) {
                        return true;
                    }
                    return false;
                },

                dispose: function () {},
            });
        })();

    // 기본 옵션
    CardFlip.DEFAULT = {};

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.cardFlip");
            var options = $.extend(
                {},
                CardFlip.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data(
                    "ui.cardFlip",
                    (data = new CardFlip($this, options))
                );
            if (typeof option == "string") data[option](params);
            $this.data("patternType", "cardFlip");
        });
    }

    window.CardFlip = CardFlip;

    $.fn.cardFlip = Plugin;
    $.fn.cardFlip.Constructor = CardFlip;
})(jQuery);
