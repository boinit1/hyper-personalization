//documentReady
$(function ()
{
	//preview-talk 생성
	$("*[data-ui='preview-talk']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).previewTalk(option);
	});

});

/*
*	PreviewTalk
*/
(function ($){
	'use strict';

	var PreviewTalk = PreviewTalk || (function ()
	{
		function initCont()
		{
			var owner = this;

			setAudio.call(this);
			setScroll.call(this);

			this.cueList = this.element.find(".cue").each(function(i){
				// textList;
				if($(this).find(".text-box").length > 0) owner.textList.push($(this).find(".text-box"));

				// text-box click;
				$(this).find(".text-box").off("click").on("click", function(){
					var audio;
					if($(this).is(".ko")) 
					{
						$(this).removeClass("ko");
						audio = owner.audioListEn[i];
						playAudio.call(owner, audio);
					}
					else
					{
						$(this).addClass("ko");
						// audio = owner.audioListKo[i];
						// playAudio.call(owner, audio);
						$(".main").trigger("click_sound");
					}					

					if($(this).data("effect") == true)
					{
						removeEffect.call(owner, $(this));
						clickEffect.call(owner);
					}
					exportRoot.word.gotoAndStop(0);
				});
			});

			this.maxY = this.container.height();
			this.contentY = this.maxY;
			TweenLite.set(this.content, {y:this.contentY});
		}

		function setScroll()
		{
			
            this.container.mCustomScrollbar({
                theme:"light-thick",
                scrollInertia:500,
                alwaysShowScrollbar:1,
                scrollbarPosition:"outside",
                // documentTouchScroll:true,
                scrollButtons:{
                    // enable:true
                    // scrollType:"stepless",
                },
                mouseWheel:{
                    // enable:false
                },
                callbacks:{
                    alwaysTriggerOffsets:false
                }
			});
		}

		function startCue()
		{
			var owner = this;

			if(!this.isPlaying) return;

			if(this.cueIndex == this.cueList.length)
			{
				TweenLite.to(this.content, 1, {y:0, ease:Sine.easeOut});
				this.container.removeClass("disable");
				this.element.trigger("talkEnd");
				console.log("톡 재생 완료");
				clickEffect.call(owner);
				this.element.trigger("playEffect");
				return ;
			} 

			// var top = this.cueList.eq(this.cueIndex).position().top;// jQuery로 top스케일 조정값이 반영되어 계산상 오차 발생.
			var top = this.cueList.eq(this.cueIndex)[0].offsetTop;
			this.contentY = this.maxY - ( top + this.cueList.eq(this.cueIndex).height()+5);
			// console.log("top: ", top);
			//console.log("height: ", this.cueList.eq(this.cueIndex).height());
			
			TweenLite.killTweensOf(this.content);
			TweenLite.to(this.content, 1, {y:this.contentY, ease:Sine.easeInOut});

			var textBox = this.cueList.eq(this.cueIndex).find(".text-box");
			if(textBox != undefined)
			{
				this.timer = setTimeout(function(){
					textBox.addClass("ani-scaleEaseBack");
				},700);
				// ======= 버벅거려서 CSS 애니메이션을 사용함
				// TweenLite.set(textBox, {scale:0.01});
				// TweenLite.to(textBox, 0.7, {delay:0.8, scale:1, ease:Back.easeOut});
			}
			
			this.timer = setTimeout(function(){
				startAudio.call(owner);
			},1000);
		}

		function startAudio()
		{
			var owner = this;
			// 영어 음원 재생
			if(this.audioList[this.cueIndex][0])
			{
				var audio = new AudioControl(this.audioList[this.cueIndex][0], {onFinish:function(e){
					// console.log("오디오 재생 완료");

					if(owner.isPlaying)
					{
						this.timer = setTimeout(function(){
							startCue.call(owner);
						}, 200);
					}
				}});
				playAudio.call(owner, audio);
			}
			else
			{
				// console.log("오디오 없음");
				this.timer = setTimeout(function(){
					startCue.call(owner);
				}, 200);
			}
			this.cueIndex++;
		}

		function setAudio()
		{
			var audio;
			for(var i = 0; i < this.audioList.length; i++)
			{
				audio = new AudioControl(this.audioList[i][0]);
				this.audioListEn.push(audio);
				// audio = new AudioControl(this.audioList[i][1]);
				// this.audioListKo.push(audio);
			}
		}

		function playAudio(audio)
		{
			if(this.nowAudio != null && this.nowAudio != audio)
			{
				this.nowAudio.stop();
			} 

			if(audio == null) return;

			this.nowAudio = audio;
			this.nowAudio.play();

		}

		function clickEffect()
		{
			var owner = this;
			if(this.textClickCount >= this.textList.length)
			{ 
				if(!this.textClickClear) {
					// this.element.trigger("textClickClear");
				}
				this.textClickClear = true;
				return;
			}

			this.textList[this.textClickCount].data("effect", true);
			this.textList[this.textClickCount].find(".effect").addClass("ani-textBoxEffect");
			this.textList[this.textClickCount].find(".effect2").addClass("ani-textBoxEffect2");

			if(this.textClickCount != 0)
			{

				// var top = this.textList[this.textClickCount].parent().parent().position().top;
				var top = this.textList[this.textClickCount].parent().parent()[0].offsetTop;
				var tarY = this.textList[this.textClickCount].height() + top - 470;
				if(tarY > 0) goScrollTo.call(owner, tarY);
			}

			this.textClickCount ++;
		}

		function goScrollTo(tarY)
		{
			this.container.mCustomScrollbar("scrollTo", tarY);
			// this.container.animate({scrollTop: tarY}, 300);
 		}

		function removeEffect(el)
		{
			el.data("effect", false);
			el.find(".effect").removeClass("ani-textBoxEffect");
			el.find(".effect2").removeClass("ani-textBoxEffect2");
		}

		return IPattern.extend({

			init : function (element, options)
			{
				this._super(element, options);
				this.container = this.element.find(".container");
				this.content = this.element.find(".content");
				this.contentY = 0;
				this.nowAudio = null;
				this.cueList = [];
				this.audioList = PageData[this.options.audioList];
				this.audioListEn = [];
				this.audioListKo = [];
				this.cueIndex = 0;
				this.textList = [];
				this.textClickCount = 0;
				this.textClickClear = false;
				this.maxY = 558;
				this.isPlaying = false;
				this.timer = null;

				var owner = this;
				$(window).one("load",function(){
					initCont.call(owner);
				});
			},

			start : function ()
			{
				this.reset();

				this.element.removeClass("hide");
				this.element.trigger("talkStart");
				this.isPlaying = true;
				startCue.call(this);
			},

			stop : function ()
			{
				this.element.addClass("hide");
				this.isPlaying = false;
				clearTimeout(this.timer);
				playAudio.call(this, null);
				this.nowAudio = null;

				TweenLite.killTweensOf(this.content);
				this.contentY = this.maxY;
				TweenLite.set(this.content, {y:this.contentY});
				// this.container.scrollTop(0);
				this.container.mCustomScrollbar("scrollTo", "top", {scrollInertia:0});
				this.container.addClass("disable");

				this.reset();
			},

			reset : function()
			{
				clearTimeout(this.timer);
				this.cueIndex = 0;
				this.textClickCount = 0;

				for(var i = 0; i < this.textList.length; i++)
				{
					if(this.textList[i].is(".ko")) 
					{
						this.textList[i].removeClass("ko");
					}
					removeEffect(this.textList[i]);
				}
			},

			dispose : function ()
			{
				
			}	
		});

	})();

	// 기본 옵션
	PreviewTalk.DEFAULT = {audioList:[]};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.previewTalk');
            var options =  $.extend({}, PreviewTalk.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.previewTalk', (data = new PreviewTalk($this, options)));
			if (typeof option == 'string') data[option](params);
			$this.data("patternType", 'previewTalk');
        });
    }

	window.PreviewTalk = PreviewTalk;

    $.fn.previewTalk = Plugin;
    $.fn.previewTalk.Constructor = PreviewTalk;

})(jQuery);

