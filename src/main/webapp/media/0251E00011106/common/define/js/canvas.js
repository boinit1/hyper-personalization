/**
 * canvas.js
 */
window.addEventListener("load", () => {
  console.log('%c ::: canvas.js load complete 🚀 :::', 'background: #222; color: #bada55');

  // 캔버스 초기화
  initCanvas();

  // 임시저장 버튼 이번트리스너
  btnTemporary?.addEventListener(drawStart, saveTemporary);
  // 다했어요 버튼 이벤트리스너
  btnSave.addEventListener(drawStart, saveDone);

  // 인트로 사운드 플레이
  introSoundPlay();
});

/**
 * 전역 변수
 */
const mainCanvas = $("#canvas");                      // 메인 캔버스    
const mainCtx = mainCanvas.getContext("2d");          // 메인 캔버스 콘텍스트
const offCanvas = new OffscreenCanvas(                // 렌더링 캔버스 (성능향상)
  mainCanvas.width,
  mainCanvas.height
);
const offCtx = offCanvas.getContext('2d');            // 렌더링 캔버스 콘텍스트
const drawCanvas = $("#drawCanvas");                  // 그리는 이벤트 캔버스
const drawCtx = drawCanvas.getContext("2d");          // 그리는 이벤트 캔버스 콘텍스트
const saveCanvas = document.createElement('canvas');  // 저장할 캔버스
const saveCtx = saveCanvas.getContext('2d');          // 저장 캔버스 콘텍스트 

// 이벤트 명
const drawStart = 'pointerdown';
const drawEnd = 'pointerup';
const drawMove = 'pointermove';

// 저장 관련
let saveLength = 0; // 저장 수
let saveList = [];  // 저장 목록
const $$bogiBox = $$(".bogi_box"); // 저장된 보기
const savePopup = $(".save_popup");
let saveImgList = [];  // 다 했어요. 목록 최대 3개

// 버튼
const btnTemporary = $("[data-temporary]");
const btnSave = $("[data-save]");

/**
 * 캔버스 초기화
 */
const initCanvas = () => {
  // 타이틀 설정
  setTitle();
  // 캔버스 크기 조절
  mainCanvas.width *= devicePixelRatio;
  mainCanvas.height *= devicePixelRatio;
  mainCtx.scale(devicePixelRatio, devicePixelRatio);
  // 임시저장 불러오기
  loadTemporary();
  // 저장목록 불러오기
  getSaveList()
}

/**
 * 타이틀 설정
 */
const setTitle = () => {
  $('#define_title').classList.remove('define');
  $('#define_title').classList.remove('quiz');

  switch (option.title) {
    case 'D':
      $('#define_title').classList.remove('quiz');
      $('#define_title').classList.add('define');
      break;

    case 'Q':
      $('#define_title').classList.remove('define');
      $('#define_title').classList.add('quiz');
      break;
  }
}

/**
 * 인트로 사운드
 */
const introSoundPlay = () => {
  const playSound = option.title == 'D' ? 'intro_define' : 'intro_quiz';

  let timeout = setTimeout(() => {
    effectFunction(playSound);
    clearTimeout(timeout);
  }, 100)
}

/**
 * 임시저장 불러오기
 */
const loadTemporary = () => {
  try {
    swcontobj.DataTypeImageCall();
  } catch (e) {
    console.warn(e);
    dataTypeImageCallCallback();
  }
}

/**
 * 임시저장
 */
const saveTemporary = () => {
  try {
    swcontobj.DataTypeImageUpload(mainCanvas.toDataURL("Image/png"));
  } catch (e) {
    console.warn(e);
    dataTypeImageUploadCallback();
  }
}


/**
 * 저장(다했어요) 목록 불러오기
 */
const getSaveList = async () => {
  try {
    saveImgList = await swcontobj.UploadImageListCallV2();
  } catch (e) {
    console.warn(e);
    saveImgList = null;
  }
  let data = JSON.parse(saveImgList);
  if (!data) return;
  saveList = data[0].generalFileInfo;
  saveLength = saveList.length;
  saveList = saveList.sort((a, b) => a.seq - b.seq);
  for (let i = 0; i < 3; i++) {
    if (saveList[i]) {
      $$bogiBox[i].classList.add('on');
      $$('.bogi_box .save_img')[i].style.backgroundImage = `url(${saveList[i].url})`;
    } else {
      $$bogiBox[i].classList.remove('on');
      $$('.bogi_box .save_img')[i].style.backgroundImage = '';
    }
  }
}

// 저장
const saveDone = async () => {
  let data = JSON.parse(saveImgList);
  if (data) saveLength = data[0].generalFileInfo.length;

  if (saveLength > 2) {
    // 꽉 찼어요
    savePopup.classList.add("full");
    savePopup.classList.remove("hide");
  } else {
    // 저장 시작

    // 사이즈 조절
    // pop_select_img 다 했어요. 보기 배경 크기 1260px * 735px
    const width = saveCanvas.width = mainCanvas.width / devicePixelRatio;
    const height = saveCanvas.height = 735; // mainCanvas.height / devicePixelRatio;

    // 캔버스 정리
    saveCtx.clearRect(0, 0, saveCanvas.width, saveCanvas.height);
    console.log(saveCanvas.width, saveCanvas.height)
    // 컨텍스트 리셋
    saveCtx.beginPath();

    // 배경 저장
    saveCtx.drawImage(
      preloadObj['../common/define/images/pop_select_img.png'],
      0,
      0,
      preloadObj['../common/define/images/pop_select_img.png'].width,
      preloadObj['../common/define/images/pop_select_img.png'].height
    );

    // 밑그림 추가
    if (sketchImg.hasAttribute('src') && sketchImg.style.opacity != '0') {
      // 밑그림 컨텍스트에 추가
      saveCtx.drawImage(
        sketchImg,
        (width - sketchImg.width)/2,
        (height - sketchImg.height)/2,
        sketchImg.width,
        sketchImg.height
      );
    }

    // 메인 캔버스 복사
    const mainHeight = mainCanvas.height / devicePixelRatio;
    saveCtx.drawImage(
      mainCanvas, 
      0, 
      (height - mainHeight) / 2, 
      width, 
      mainCanvas.height / devicePixelRatio
    );
    saveCtx.fillStyle = "rgba(255, 165, 0, 1)";

    // 개발용 로컬 저장
    // saveLocalImageFile();

    // 캔버스값 base64로 컨버팅
    var base64ImageData = saveCanvas.toDataURL();
    // json으로 감싸기
    var json = { fileUploadType: "Image", imageData: base64ImageData };
    var jsonToString = JSON.stringify(json);
    // 이미지 업로드 API 호출 => imageFileUploadCallback() 콜백

    try {
      swcontobj.UploadImageFileRequest(jsonToString);
    } catch (e) {
      console.warn(e);
      imageFileUploadCallback();
    }
  }
}