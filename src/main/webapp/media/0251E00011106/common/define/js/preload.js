/**
 * preload.js
 */
window.addEventListener("load", () => {
  console.log('%c ::: preload.js load complete 🚀 :::', 'background: #222; color: #bada55');  
});

const imageList = [
  '../common/define/images/bg.png',
  '../common/define/images/bottom.png',
  '../common/define/images/title_define.png',
  '../common/define/images/title_quiz.png',
  '../common/define/images/btn_next.png',
  '../common/define/images/btn_pre.png',
  '../common/define/images/color.png',
  '../common/define/images/thickness.png',
  '../common/define/images/alert_delete.png',
  '../common/define/images/alert_save.png',
  '../common/define/images/btn_delete.png',
  '../common/define/images/msg_save_complet.png',
  '../common/define/images/msg_save_full.png',
  '../common/define/images/select_color.png',
  '../common/define/images/pop_select_img.png',
];

let preloadImageCnt = 0;
let preloadObj = {};
const preloadImage = () => {
  imageList.map(url => {
    const preImg = new Image();
    preloadImageCnt++;
    preImg.onload = (e) => {
      preloadImageCnt--;
      preloadObj[url] = preImg;
      // console.log(preloadImageCnt, url)
    }
    preImg.error = (e) => {
      preloadImageCnt--;
      // console.log(preloadImageCnt, url)
    }
    preImg.src = url;
  });
}

// 즉시 실행
preloadImage();