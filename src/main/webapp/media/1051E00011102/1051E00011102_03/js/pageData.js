"use strict";

var PageData = {
	// 오디오
	narration :[
		"./audio/k_01.mp3", 
    ],
    
    subtitleData :
    [
        // 캐릭터 번호 object로 입력 가능. {ch:[1,6],ch_subtitle:6}
        // 0:Jiho, 1:Eric, 2:선생님, 3:Jayden, 4:Yuri, 5:Hailey, 6:Nora, 
        // 장면 1
        [
            1.692, 5.416, 0,
            "Hi, I’m Jiho. What’s your name?",
            "안녕, 나는 지호야. 네 이름이 뭐니?",
        ],
        [
            5.807, 8.827, 1,
            "Hi, Jiho. My name is Eric.",
            "안녕 지호야. 내 이름은 에릭이야.",
        ],
        [
            9.174, 11.333, 0,
            "How do you spell your name?",
            "이름의 철자가 어떻게 되니?",
        ],
        [
            12.277, 14.725, 1,
            "E-R-I-C.",
            "E-R-I-C.",
        ],
        [
            15.448, 17.310, 0,
            "Where are you from?",
            "너는 어느 나라 출신이니?",
        ],
        [
            18.109, 19.869, 1,
            "I’m from Kenya.",
            "나는 케냐 출신이야.",
        ],
        // 장면 2
        [
            25.565, 28.073, 2,
            "Everyone stand up, please.",
            "모두 일어나 주세요.",
        ],
        [
            33.157, 34.841, 3,
            "Where are you from?",
            "너는 어느 나라 출신이니?",
        ],
        [
            35.274, 37.026, 4,
            "I’m from Korea.",
            "나는 한국 출신이야.",
        ],
        [
            38.242, 40.129, 3,
            "I’m from Canada.",
            "나는 캐나다 출신이야.",
        ],
        [
            41.107, 43.020, 5,
            "I’m from the U.S.",
            "나는 미국 출신이야.",
        ],
        [
            44.258, 46.120, {ch:[1,6],ch_subtitle:6},
            "I’m from Kenya.",
            "나는 케냐 출신이야.",
        ]
    ]
}


