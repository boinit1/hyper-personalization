"use strict";

var PageData = {
	// 오디오
	narration :[
		"./audio/k_01_01.mp3", //친구들이 인사를 나누고 있어~ 대화를 들어볼까?
		"./audio/k_02_01.mp3" //친구들이 안부를 묻고 답하고 있어! 대화를 들어볼까?
	],
	narration_02 :[
		"./audio/k_01_02.mp3", //궁금한 메시지를 눌러 봐!
		"./audio/k_02_02.mp3" //궁금한 메시지를 눌러 봐!
	],
	previewTalk_01 :[
		["",""],
		["",""],
		["./audio/e_01_01.mp3"],
		["./audio/e_01_02.mp3"],
		["./audio/e_01_03.mp3"],
		["./audio/e_01_04.mp3"]
	],
	previewTalk_02 :[
		["",""],
		["",""],
		["./audio/e_02_01.mp3"],
		["./audio/e_02_02.mp3"],
		["./audio/e_02_03.mp3"],
		["./audio/e_02_04.mp3"]
	],
	previewWord_word:["Australia","China","the U.K.","Kenya ","France","Vietnam","from","spell"],
	previewWord_word_ko:["오스트레일리아","중국","영국","케냐","프랑스","베트남","~에서, ~출신의","철자를 말하다[쓰다]"],
	previewWord_word_mp3:["./audio/e_03_01.mp3","./audio/e_03_02.mp3","./audio/e_03_03.mp3","./audio/e_03_04.mp3",
						"./audio/e_03_05.mp3","./audio/e_03_06.mp3","./audio/e_03_07.mp3","./audio/e_03_08.mp3"],
	
}



