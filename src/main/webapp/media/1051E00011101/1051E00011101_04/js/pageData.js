"use strict";

var PageData = {
	// 오디오
	narration :[
		"./audio/k_01.mp3", 
    ],
    
    subtitleData :
    [
        // 캐릭터 번호 object로 입력 가능. {ch:[1,6],ch_subtitle:6}
        // 0:Jiho, 1:Hailey, 2:Jayden, 3:Nora, 4:Yuri
        // 장면 1
        [
            8.196, 10.185, 0,
            "Good morning, Hailey.",
            "안녕, 헤일리.",
        ],
        [
            10.642, 12.458, 1,
            "Good morning, Jiho.",
            "안녕, 지호.",
        ],
        [
            16.462, 18.328, 2,
            "Good morning, Nora.",
            "안녕, 노라.",
        ],
        [
            18.945, 20.774, 3,
            "Good morning, Jayden.",
            "안녕, 제이든.",
        ],
        // 장면 2
        [
            23.900, 26.705, 0,
            "Hi, Yuri. How’s it going?",
            "안녕, 유리. 어떻게 지내니?",
        ],
        [
            27.928, 30.993, 4,
            "I’m fine. How about you?",
            "잘 지내. 너는 어떠니?",
        ],
        [
            31.413, 32.871, 0,
            "Not bad.",
            "나쁘지 않아.",
        ],
        [
            36.318, 40.865, 2,
            "Hi, I’m Jayden. This is my friend, Yuri.",
            "안녕, 나는 제이든이야. 얘는 내 친구 유리야.",
        ],
        [
            41.347, 43.819, 1,
            "Hi! I’m Hailey.",
            "안녕! 나는 헤일리야.",
        ],
        [
            44.894, 46.636, 4,
            "How’s it going?",
            "어떻게 지내니?",
        ],
        [
            47.958, 50.837, 1,
            "Not bad. How about you?",
            "나쁘지 않아. 너는 어떠니?",
        ],
        [
            51.764, 53.160, 4,
            "Very well.",
            "아주 좋아.",
        ]
    ]
}


