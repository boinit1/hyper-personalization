//documentReady
$(function () {
    //activity-draggable 생성
    $("*[data-ui='activity-draggable']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).activityDraggable(option);
    });
});

/*
 *	ActivityDraggable
 */
(function ($) {
    "use strict";

    var ActivityDraggable =
        ActivityDraggable ||
        (function () {
            function setDraggable() {
                var owner = this;
                this.element.find(".drag-item").each(function (i) {
                    $(this).data("idx", i + 1);
                    $(this)
                        .find(".area")
                        .on(touchstart, $.proxy(onTouchStart, owner));
                });
                this.element.find(".drag-area").each(function (i) {
                    $(this).data("idx", i + 1);
                    $(this)
                        .find(".area")
                        .on(touchstart, $.proxy(onTouchStart, owner));
                });

                $(window).on(touchmove, $.proxy(onTouchMove, this));
                $(window).on(touchend, $.proxy(onTouchEnd, this));
            }

            function onTouchStart(e) {
                if (!this.isDrag) return;

                var pageX = e.pageX;
                var pageY = e.pageY;

                if (e.originalEvent.changedTouches) {
                    pageX = e.originalEvent.changedTouches[0].clientX;
                    pageY = e.originalEvent.changedTouches[0].clientY;
                }

                var target = $(e.currentTarget).parent();
                if (!target.data("area")) {
                    this.startX =
                        (target.position().left - pageX) / window.scale;
                    this.startY =
                        (target.position().top - pageY) / window.scale;
                    this.touchTarget = target;
                    this.touchTarget.css({ "z-index": 999 });
                }

                e.preventDefault();
                e.stopPropagation();
            }

            function onTouchMove(e) {
                if (this.touchTarget) {
                    var pageX = e.pageX;
                    var pageY = e.pageY;
                    if (e.originalEvent.changedTouches) {
                        pageX = e.originalEvent.changedTouches[0].clientX;
                        pageY = e.originalEvent.changedTouches[0].clientY;
                    }

                    var moveX = pageX / window.scale;
                    var moveY = pageY / window.scale;

                    TweenLite.set(this.touchTarget, {
                        x: moveX + this.startX,
                        y: moveY + this.startY,
                    });

                    e.preventDefault();
                    e.stopPropagation();
                }
            }

            function onTouchEnd(e) {
                if (this.touchTarget) {
                    this.touchTarget.css({ "z-index": "" });
                    var hitSelector;
                    var hitTarget = null;
                    if (this.touchTarget.is(".drag-item")) {
                        this.endEle = this.touchTarget;
                        hitSelector = ".drag-area";
                    } else {
                        this.startEle = this.touchTarget;
                        hitSelector = ".drag-item";
                    }

                    // hit 체크
                    var sx, sy, sw, sh, ex, ey, ew, eh;
                    for (
                        var i = 0;
                        i < this.element.find(hitSelector).length;
                        i++
                    ) {
                        var area = this.element.find(hitSelector).eq(i);
                        if (this.touchTarget.is(".drag-item"))
                            this.startEle = area;
                        else this.endEle = area;

                        sx = this.startEle.position().left / window.scale;
                        sy = this.startEle.position().top / window.scale;
                        sw = this.startEle.width();
                        sh = this.startEle.height();
                        ex = this.endEle.position().left / window.scale;
                        ey = this.endEle.position().top / window.scale;
                        ew = this.endEle.width();
                        eh = this.endEle.height();

                        // 오른쪽 객체 기준으로 hit 체크
                        if (ey > sy - eh / 2 && ey < sy + sh - eh / 2) {
                            // 왼쪽 객체의 끝에서 100픽셀까지 정답으로 체크
                            if (ex > sx + sw - 100 && ex < sx + sw) {
                                if (!this.options.multi) {
                                    if (!this.endEle.data("drag"))
                                        hitTarget = this.startEle;
                                } else {
                                    hitTarget = this.startEle;
                                }
                                break;
                            }
                        }
                    }

                    if (hitTarget) {
                        if (check.call(this, this.endEle, this.startEle)) {
                            this.touchTarget.data("area", hitTarget);
                            hitTarget.data("area", this.touchTarget);
                            hitTarget.data("drag", this.touchTarget);
                            this.startEle.addClass("active");
                            this.endEle.addClass("active");

                            this.element.find(".bg-white").show();
                            this.endEle.find(".bg-shadow").show();
                            this.startEle.css({ "z-index": 10 });
                            this.endEle.css({ "z-index": 12 });
                            var pos1 = { x: (1280 - (sw + ew)) / 2, y: 330 };
                            var pos2 = {
                                x: (1280 - (sw + ew)) / 2 + sw - 19,
                                y: 330,
                            };
                            TweenLite.set(this.startEle, {
                                x: pos1.x,
                                y: pos1.y,
                            });
                            TweenLite.set(this.endEle, {
                                x: pos2.x,
                                y: pos2.y,
                            });

                            playRight_step1.call(this);

                            this.element.trigger("quizAnswer", [
                                hitTarget.index(),
                                this.touchTarget.index(),
                            ]);

                            this.isDrag = false;

                            if (this.clearAudio) {
                                this.naAudioList[1].stop();
                                this.clearAudio.stop();
                                this.clearAudio.play();
                                /*
							for(var i=0; i< this.clearAudio.length; i++)
							{
								this.clearAudio[i].stop();
							}
							if( this.clearAudio.length == 1) this.clearAudio[0].play();
							else                             this.clearAudio[hitTarget.index()].play();
							*/
                            }
                        } else {
                            TweenLite.to(this.touchTarget, 0.6, {
                                x: this.touchTarget.data("x"),
                                y: this.touchTarget.data("y"),
                                ease: Cubic.easeOut,
                            });

                            if (this.failedAudio) {
                                this.failedAudio.stop();
                                this.failedAudio.play();
                            }

                            this.element.trigger("quizWrong");
                        }
                    } else {
                        TweenLite.to(this.touchTarget, 0.6, {
                            x: this.touchTarget.data("x"),
                            y: this.touchTarget.data("y"),
                            ease: Cubic.easeOut,
                        });
                    }

                    this.startX = this.startY = 0;
                    this.touchTarget = null;
                }
            }

            function check(target, area) {
                if (typeof this.options.answer[area.index()] === "number") {
                    if (
                        target.data("idx") == this.options.answer[area.index()]
                    ) {
                        return true;
                    }
                } else {
                    if (
                        $.inArray(
                            target.data("idx"),
                            this.options.answer[area.index()]
                        ) > -1
                    ) {
                        return true;
                    }
                }
                return false;
            }

            function playRight_step1() {
                var owner = this;

                setTimeout(function () {
                    playRight_step2.call(owner, owner.startEle.index());
                }, 1500);
            }

            function playRight_step2(idx) {
                var owner = this;

                this.startEle.hide();
                this.endEle.hide();
                var ty = 60 + (110 * this.startEle.data("idx") - 1);
                TweenLite.set(this.startEle, { y: ty });
                TweenLite.set(this.endEle, { y: ty });

                this.element.find(".sentence").hide();
                this.element.find(".sentence").eq(idx).show();
                this.element.find(".popup").removeClass("hide");
                owner.element.find(".bg-white").hide();

                var audio = this.sentenceAudioList[idx];
                playAudio.call(this, audio);

                // 클릭 설정
                this.startEle.data("idx", idx);
                this.startEle.css({ "z-index": 0 });
                this.endEle.data("idx", idx);
                this.endEle.css({ "z-index": 0 });
                this.startEle.on("click", function () {
                    var s_idx = $(this).data("idx");
                    showPopup.call(owner, s_idx);
                });
                this.endEle.on("click", function () {
                    var s_idx = $(this).data("idx");
                    showPopup.call(owner, s_idx);
                });
            }

            function showPopup(idx) {
                var owner = this;

                owner.element.find(".sentence").hide();
                owner.element.find(".sentence").eq(idx).show();
                owner.element.find(".popup").removeClass("hide");
                owner.element.find(".bg-white").hide();

                var audio = this.sentenceAudioList[idx];
                playAudio.call(this, audio);
            }

            function onFinishCheck() {
                var owner = this;

                setTimeout(function () {
                    if (owner.checkAnswer() && !owner.clear) {
                        owner.clear = true;
                        owner.dispose();
                        owner.stop();
                        owner.element.trigger("End");
                    } else {
                        owner.element.find(".popup").addClass("hide");
                        owner.isDrag = true;
                    }
                }, 1000);
            }

            function setAudio() {
                var owner = this;
                var audio;

                // 나레이션
                for (var i = 0; i < PageData.na_mp3.length; i++) {
                    audio = new AudioControl(PageData.na_mp3[i], {});
                    this.naAudioList.push(audio);
                }

                // 문장 사운드
                for (var i = 0; i < PageData.sentence_mp3.length; i++) {
                    audio = new AudioControl(PageData.sentence_mp3[i], {
                        onFinish: $.proxy(onFinishCheck, owner),
                    });
                    this.sentenceAudioList.push(audio);
                }
                // 정오 효과음
                for (var i = 0; i < PageData.eff_mp3.length; i++) {
                    audio = new AudioControl(PageData.eff_mp3[i], {});
                    this.effAudioList.push(audio);
                }
            }

            function playAudio(audio) {
                if (this.nowAudio != null && this.nowAudio != audio) {
                    this.nowAudio.stop();
                }

                if (audio == null) return;

                this.nowAudio = audio;
                this.nowAudio.play();

                gtag &&
                    gtag("event", "audio-play", {
                        id: audio.source,
                        timer: audio.timer,
                    });
            }

            return IPattern.extend({
                init: function (element, options) {
                    this._super(element, options);
                    this.touchTarget = null;
                    this.startX = 0;
                    this.startY = 0;
                    this.clearAudio = null;
                    this.failedAudio = null;
                    this.finishAudio = null;
                    this.isDrag = true;
                    this.timer = null;
                    this.startEle = null;
                    this.endEle = null;
                    this.sentenceAudioList = [];
                    this.nowAudio = null;
                    this.effAudioList = [];
                    this.naAudioList = [];
                    this.clear = false;
                    this.type = this.element.is(".type2") ? "type2" : "type1";

                    var owner = this;
                    if (this.options.failedAudio)
                        this.failedAudio = new AudioControl(
                            this.options.failedAudio
                        );
                    if (this.options.clearAudio)
                        this.clearAudio = new AudioControl(
                            this.options.clearAudio
                        );
                    if (this.options.finishAudio)
                        this.finishAudio = new AudioControl(
                            this.options.finishAudio,
                            {
                                onFinish: function () {
                                    owner.element.find(".drag-item").show();
                                    owner.element.find(".drag-area").show();
                                    exportRoot.goodMc.gotoAndStop(0);
                                    owner.element.find(".bg-white").hide();
                                    owner.element
                                        .find(".popup")
                                        .addClass("hide");
                                },
                            }
                        );

                    setAudio.call(this);
                },

                start: function () {
                    var owner = this;

                    this._super();

                    this.element.find(".drag-item").each(function (i) {
                        var x =
                            owner.type == "type2"
                                ? parseInt($(this).attr("x2"))
                                : parseInt($(this).attr("x"));
                        var y = parseInt($(this).attr("y"));
                        TweenLite.set($(this), { x: x, y: y });
                        $(this)
                            .data("x", x)
                            .data("y", y)
                            .css({ top: 0, left: 0 });
                        $(this).data("idx", i);
                    });
                    this.element.find(".drag-area").each(function (i) {
                        var x = parseInt($(this).attr("x"));
                        var y = parseInt($(this).attr("y"));
                        TweenLite.set($(this), { x: x, y: y });
                        $(this)
                            .data("x", x)
                            .data("y", y)
                            .css({ top: 0, left: 0 });
                        $(this).data("idx", i);
                    });

                    setDraggable.call(this);

                    this.naAudioList[1].play();
                },

                stop: function () {
                    var owner = this;

                    exportRoot.goodMc.gotoAndStop(1);
                    owner.effAudioList[6].play();
                    owner.element.find(".bg-white").hide();
                    this.element.find(".popup").addClass("hide");

                    setTimeout(function () {
                        if (owner.finishAudio) owner.finishAudio.play();
                    }, 500);
                },

                checkAnswer: function () {
                    var len = 0;
                    if (this.options.multi) {
                        for (var i = 0; i < this.options.answer.length; i++) {
                            if (typeof this.options.answer[i] === "object") {
                                for (
                                    var j = 0;
                                    j < this.options.answer[i].length;
                                    j++
                                ) {
                                    len++;
                                }
                            } else {
                                len++;
                            }
                        }
                    } else {
                        len = this.options.answer.length;
                    }
                    if (this.options.customLength)
                        len = this.options.customLength;
                    if (this.element.find(".drag-item.active").length == len) {
                        return true;
                    }

                    return false;
                },

                dispose: function () {
                    this.element
                        .find(".drag-item .area")
                        .off("touchstart mousedown");
                    $(window).off("touchmove mousemove");
                    $(window).off("touchend mouseup");

                    this.element.find(".drag-item").removeClass("active");
                    this.element.find(".drag-area").removeClass("active");
                },
            });
        })();

    // 메인 기본 옵션
    ActivityDraggable.DEFAULT = {
        answer: [],
        failedAudio: "",
        clearAudio: "",
        multi: false,
        customLength: null,
    };

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.activityDraggable");
            var options = $.extend(
                {},
                ActivityDraggable.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data(
                    "ui.activityDraggable",
                    (data = new ActivityDraggable($this, options))
                );
            if (typeof option == "string") data[option](params);
            $this.data("quizType", "activityDraggable");
        });
    }

    window.ActivityDraggable = ActivityDraggable;

    $.fn.activityDraggable = Plugin;
    $.fn.activityDraggable.Constructor = ActivityDraggable;
})(jQuery);
