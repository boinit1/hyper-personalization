//documentReady
$(function () {
    //preview-word 생성
    $("*[data-ui='preview-word']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).previewWord(option);
    });
});

/*
 *	PreviewWord
 */
(function ($) {
    "use strict";

    var PreviewWord =
        PreviewWord ||
        (function () {
            function initCont() {
                var owner = this;

                setButton.call(this);
                setAudio.call(this);
                setWordBox.call(owner);

                this.element.on("wordClear", function () {
                    console.log("wordClear=================");
                });

                // 테스트
                // setTimeout(function(){owner.start.call(owner);},500);
            }

            function setButton() {
                var owner = this;

                this.element.find(".close.btn1").on("click", function () {
                    owner.stop();
                });
                this.element
                    .find(".wordShow-cont .close")
                    .on("click", function () {
                        closeWordShow.call(owner);
                    });
            }

            function setAudio() {
                var owner = this;

                var audio;
                for (var i = 0; i < PageData.previewWord_word.length; i++) {
                    audio = new AudioControl(PageData.previewWord_word_mp3[i], {
                        onFinish: function (audioDom) {
                            if ($(audioDom).is(".play1")) {
                                // console.log("단어 2번 재생 완료");
                                $(audioDom).removeClass("play1");
                                owner.timer = setTimeout(function () {
                                    showKo.call(owner);
                                }, 50);
                            } else {
                                // console.log("단어 1번 재생 완료");
                                $(audioDom).addClass("play1");
                                owner.timer = setTimeout(function () {
                                    showEn.call(owner);
                                }, 50);
                            }
                            /*
					// 1
					if($(audioDom).is(".play1"))
					{
						// console.log("단어 2번 재생 완료");
						$(audioDom).removeClass("play1");
						owner.timer = setTimeout(function(){
							showEn.call(owner);
						}, 1000);
					}
					else
					{
						// console.log("단어 1번 재생 완료");
						$(audioDom).addClass("play1");
						// playAudio.call(owner, owner.audioList[owner.showCount]);
					}
					*/
                        },
                    });
                    this.audioList.push(audio);
                }

                if (this.options.dingdongAudio) {
                    this.audioDingdong = new AudioControl(
                        this.options.dingdongAudio,
                        {}
                    );
                }
            }

            function playAudio(audio) {
                if (this.nowAudio != null && this.nowAudio != audio) {
                    this.nowAudio.stop();
                }

                if (audio == null) return;

                this.nowAudio = audio;
                if (this.isPlaying) this.nowAudio.play();

                gtag &&
                    gtag("event", "audio-play", {
                        id: audio.source,
                        timer: audio.timer,
                    });
            }

            function setWordBox() {
                var owner = this;

                var wordBox = this.element.find(".wordBox");
                var box = wordBox;
                var x = 0,
                    y = 0;
                for (var i = 0; i < this.wordList.length; i++) {
                    if (i != 0)
                        box = wordBox
                            .clone()
                            .appendTo(this.element.find(".wordBox-list"));
                    x = i % 4;
                    y = Math.floor(i / 4);
                    box.data("idx", i);
                    box.data("x", x);
                    box.data("y", y);
                    box.find(".text").text(this.wordList[i]);

                    // 단어 3줄
                    var origin = x * 33.3 + "% " + y * 50 + "%";
                    // 단어 2줄
                    if (this.wordBoxLen == 8) {
                        origin = x * 33.3 + "% " + (y * 50 + 25) + "%";
                    }

                    box.css({
                        "-moz-transform-origin": origin,
                        "transform-origin": origin,
                        "-ms-transform-origin": origin,
                        "-webkit-transform-origin": origin,
                    });
                    // TweenLite.set(box, {x:0,y:0});
                    this.wordBox.push(box);

                    var img = $("<img src='' />");
                    var img_num = $.pad(i + 1, 4);
                    img.attr(
                        "src",
                        "./images/preview/word/word_image_" + img_num + ".png"
                    );
                    owner.element.find(".wordShow-cont .image").append(img);
                }

                this.element.find(".wordBox").each(function () {
                    var box = $(this);
                    box.addClass("x" + box.data("x"));
                    box.addClass("y" + box.data("y"));
                    box.on("click", function () {
                        gtag("event", "word-box", {
                            action: "click",
                            wordBox: this.innerText,
                        });
                        owner.isPlaying = true;
                        showWordShow.call(owner, $(this).data("idx"));
                    });
                });
            }

            function startAutoPlay() {
                var owner = this;

                this.element
                    .find(".wordBox .effect")
                    .addClass("ani-wordEffect");
                this.element.find(".wordBox").addClass("disable");

                this.timer = setTimeout(function () {
                    if (owner.isPlaying) {
                        owner.autoPlay = true;
                        showWordShow.call(owner, 0);
                    }
                }, 2500);
            }

            function showWordShow(idx) {
                var owner = this;

                this.showCount = idx;

                var box = this.wordBox[idx];
                box.css("zIndex", 3);
                var bx = box.offset().left + box.width() / 2;
                var by = box.offset().top + box.height() / 2;

                box.addClass("ani-wordBoxScale");
                box.off("animationend").on("animationend", function (e) {
                    $(this).css("zIndex", 0);
                    box.removeClass("ani-wordBoxScale");
                });

                var cont = owner.element.find(".wordShow-cont");
                setWord.call(owner);
                // cont.css("zIndex", 4);
                var origin = bx + "px " + by + "px";
                cont.css({
                    "-moz-transform-origin": origin,
                    "transform-origin": origin,
                    "-ms-transform-origin": origin,
                    "-webkit-transform-origin": origin,
                });
                cont.addClass("ani-wordShow");
                cont.show();
                cont.off("animationend").one("animationend", function (e) {
                    showImage.call(owner);
                });
            }

            function closeWordShow() {
                var owner = this;

                var cont = owner.element.find(".wordShow-cont");
                cont.removeClass("ani-wordShow");
                cont.hide();

                this.element.find(".wordBox").removeClass("disable");

                playAudio.call(owner, null);
                this.reset();
            }

            function setWord() {
                var owner = this;
                owner.element.find(".wordShow-cont .image img").hide();
                owner.element
                    .find(".wordShow-cont .image img")
                    .eq(owner.showCount)
                    .show();
                owner.element
                    .find(".wordShow-cont .en")
                    .html(this.wordList[owner.showCount]);
                owner.element
                    .find(".wordShow-cont .en")
                    .removeClass()
                    .addClass("en")
                    .addClass("eq-" + owner.showCount);
                owner.element
                    .find(".wordShow-cont .ko")
                    .html(this.wordList_ko[owner.showCount]);
                owner.element
                    .find(".wordShow-cont .ko")
                    .removeClass()
                    .addClass("ko")
                    .addClass("eq-" + owner.showCount);
                owner.element
                    .find(".wordShow-cont .image")
                    .removeClass("ani-word-img ani-word-img2");
                owner.element
                    .find(".wordShow-cont .en")
                    .removeClass("ani-word-text ani-word-text2");
                owner.element
                    .find(".wordShow-cont .ko")
                    .removeClass("ani-word-text2-ko");
            }

            function showImage() {
                var owner = this;
                if (!this.isPlaying) return;
                // console.log("========== 단어 보이기 시작", this.showCount);
                // console.log("딩동");
                if (this.audioDingdong) this.audioDingdong.play();
                owner.element
                    .find(".wordShow-cont .image")
                    .addClass("ani-word-img");
                owner.element
                    .find(".wordShow-cont .image")
                    .off("animationend")
                    .on("animationend", function () {
                        // 이미지 보이고 사운드 2번 들려주기
                        var audio = owner.audioList[owner.showCount];
                        $(audio.audio).removeClass("play1");
                        playAudio.call(owner, audio);
                    });
            }

            function showEn() {
                var owner = this;
                if (!this.isPlaying) return;
                // console.log("영어 텍스트 보이기");
                // console.log("딩동");
                if (this.audioDingdong) this.audioDingdong.play();
                owner.element
                    .find(".wordShow-cont .image")
                    .addClass("ani-word-img2");
                owner.element.find(".wordShow-cont .image").off("animationend");
                owner.element
                    .find(".wordShow-cont .en")
                    .addClass("ani-word-text");

                this.timer = setTimeout(function () {
                    playAudio.call(owner, owner.audioList[owner.showCount]);
                }, 1000);
                /*
			this.timer = setTimeout(function(){
				showKo.call(owner);
			}, 1500);
			*/
            }

            function showKo() {
                var owner = this;
                if (!this.isPlaying) return;
                // console.log("한글 텍스트 보이기");

                owner.element
                    .find(".wordShow-cont .en")
                    .addClass("ani-word-text2");
                owner.element
                    .find(".wordShow-cont .ko")
                    .addClass("ani-word-text2-ko");
                owner.element
                    .find(".wordShow-cont .ko")
                    .off("animationend")
                    .on("animationend", function () {
                        console.log(
                            "========== 단어 보이기 끝",
                            owner.showCount
                        );
                        if (!owner.checkEnd() && owner.autoPlay) {
                            console.log("2초 뒤 다음 단어 계속 재생");
                            owner.timer = setTimeout(function () {
                                owner.showCount++;
                                setWord.call(owner);
                                showImage.call(owner);
                            }, 1000);
                        }
                    });
            }

            return IPattern.extend({
                init: function (element, options) {
                    var owner = this;

                    this._super(element, options);
                    this.wordList = PageData.previewWord_word;
                    this.wordList_ko = PageData.previewWord_word_ko;
                    this.wordBoxLen = parseInt(
                        this.element.find(".wordBox-cont").data("len")
                    );
                    this.wordBox = [];
                    this.showCount = 0;
                    this.wordClear = false;
                    this.audioList = [];
                    this.audioDingdong = null;
                    this.nowAudio = null;
                    this.autoPlay = false;
                    this.isPlaying = false;
                    this.timer = null;

                    $(window).one("load", function () {
                        initCont.call(owner);
                    });
                },

                start: function () {
                    this.reset();

                    this.element.removeClass("hide");
                    this.element.trigger("wordStart");

                    this.isPlaying = true;
                    startAutoPlay.call(this);
                },

                stop: function () {
                    this.element.addClass("hide");

                    this.isPlaying = false;
                    this.reset();
                    this.element.trigger("wordEnd");
                },

                reset: function () {
                    // this.wordClear = false;

                    clearTimeout(this.timer);
                    this.timer = null;
                    this.showCount = 0;
                    this.nowAudio = null;
                    this.isPlaying = false;
                    this.autoPlay = false;

                    this.element
                        .find(".wordBox .effect")
                        .removeClass("ani-wordEffect");
                    this.element
                        .find(".wordShow-cont")
                        .removeClass("ani-wordShow");
                },

                checkEnd: function () {
                    if (this.showCount >= this.wordList.length - 1) {
                        if (!this.wordClear) {
                            this.element.trigger("wordClear");
                            this.wordClear = true;
                        }
                        return true;
                    }
                    return false;
                },

                dispose: function () {},
            });
        })();

    // 기본 옵션
    PreviewWord.DEFAULT = {};

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.previewWord");
            var options = $.extend(
                {},
                PreviewWord.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data(
                    "ui.previewWord",
                    (data = new PreviewWord($this, options))
                );
            if (typeof option == "string") data[option](params);
            $this.data("patternType", "previewWord");
        });
    }

    window.PreviewWord = PreviewWord;

    $.fn.previewWord = Plugin;
    $.fn.previewWord.Constructor = PreviewWord;
})(jQuery);
