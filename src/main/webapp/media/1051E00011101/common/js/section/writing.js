//documentReady
$(function ()
{
	//writing 생성
	$("*[data-ui='writing']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).writing(option);
	});

});

/*
*	Writing
*/
(function ($){
	'use strict';

	var Writing = Writing || (function ()
	{
		function initCont()
		{
			var owner = this;

			this.start();
		}

		function startWriting()
		{
			var owner = this;
			console.log("start writing");
		}

		return IPattern.extend({

			init : function (element, options)
			{
				this._super(element, options);

				initCont.call(this);
			},

			start : function ()
			{
				startWriting.call(this);
			},

			reset : function()
			{
				
			},

			dispose : function ()
			{
				
			}	
		});

	})();

	// 기본 옵션
	Writing.DEFAULT = {};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.writing');
            var options =  $.extend({}, Writing.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.writing', (data = new Writing($this, options)));
			if (typeof option == 'string') data[option](params);
        });
    }

	window.Writing = Writing;

    $.fn.writing = Plugin;
    $.fn.writing.Constructor = Writing;

})(jQuery);

