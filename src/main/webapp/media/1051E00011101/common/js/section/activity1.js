//documentReady
$(function () {
    //activity 생성
    $("*[data-ui='activity']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).activity(option);
    });
});

/*
 *	Activity
 */
(function ($) {
    "use strict";

    var Activity =
        Activity ||
        (function () {
            function initCont() {
                var owner = this;

                setEvent.call(this);

                // setAudio.call(this);
            }
            function setAudio() {
                var owner = this;

                var audio;
                for (var i = 0; i < PageData.narration.length; i++) {
                    audio = new AudioControl(PageData.narration[i], {
                        onFinish: function (audioDom) {
                            /*
					TweenLite.to(owner.element.find(".na_container").eq(owner.currentTalk), 1, {autoAlpha:0, y:-20, ease:Sine.easeIn, onComplete:function(){
						startTalk.call(owner);
					}})
					*/
                        },
                    });
                    this.audioList.push(audio);
                }
            }

            function playAudio(audio) {
                if (this.nowAudio != null && this.nowAudio != audio) {
                    this.nowAudio.stop();
                }

                if (audio == null) return;

                this.nowAudio = audio;
                this.nowAudio.play();

                gtag &&
                    gtag("event", "audio-play", {
                        id: audio.source,
                        timer: audio.timer,
                    });
            }

            function setEvent() {
                var owner = this;

                this.element.find("#game1").on("End", function () {
                    console.log("낱말 카드 게임 종료 이벤트");
                    if (owner.clear) return;
                    owner.clear = true;
                    owner.currentGame++;
                    $(".main").trigger("studyFinish");

                    /*
				// 2초 후 다음 게임 시작
				setTimeout(function(){
					console.log("문장 게임 시작해");
					exportRoot.timeMc.visible = false;
					owner.element.find("#game1").addClass("hide");
					owner.element.find("#game1").cardFlip("dispose");
					owner.element.find("#game2").removeClass("hide");
					owner.element.find("#game2").activityDraggable("start");
				},2000);
				*/
                });
                /*
			this.element.find("#game2").on("End", function (){
				console.log("문장 드래그 게임 종료 이벤트");
				owner.currentGame ++;
				$(".main").trigger("studyFinish");
			});
			*/
            }

            function startActivity() {
                var owner = this;

                owner.element.find("#game1").removeClass("hide");
                owner.element.find("#game1").cardFlip("start");

                // owner.element.find("#game2").removeClass("hide");
                // owner.element.find("#game2").activityDraggable("start");
            }

            return ISection.extend({
                init: function (element, options) {
                    this._super(element, options);
                    this.currentGame = 0;
                    this.audioList = [];
                    this.nowAudio = null;

                    this.clear = false;
                    initCont.call(this);
                },

                start: function () {
                    startActivity.call(this);
                },

                reset: function () {
                    this.nowAudio = null;
                },

                dispose: function () {},
            });
        })();

    // 기본 옵션
    Activity.DEFAULT = {};

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.activity");
            var options = $.extend(
                {},
                Activity.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data(
                    "ui.activity",
                    (data = new Activity($this, options))
                );
            if (typeof option == "string") data[option](params);
        });
    }

    window.Activity = Activity;

    $.fn.activity = Plugin;
    $.fn.activity.Constructor = Activity;
})(jQuery);

var exportRoot;
