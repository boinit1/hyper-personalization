/**
 * 그리기 JS
 * 캔버스 렌더링
 */
window.addEventListener("load", () => {
  console.log('%c ::: drawing.js load complete 🚀 :::', 'background: #222; color: #bada55');

  // 이벤트 리스너
  // drawStart 전역변수 canvas.js 정의

  // 그리기 시작
  drawCanvas.addEventListener(drawStart, startPosition);
  addEventListener('resize', canvasResizeCalculation);
  canvasResizeCalculation();
});

/**
 * 그리기 전역 변수
 */
let canvasRatio, canvasContainScale;
let canvasArea, canvasDefault;
let pointerType;
let pointer;
let ppts = [];
let x, y;
let x1, y1;


/**
 * Resizing 비율 계산
 */
const canvasResizeCalculation = () => {
  canvasArea = drawCanvas.getBoundingClientRect();
  canvasDefault = {
    width: drawCanvas.width,
    height: drawCanvas.height
  };
  canvasRatio = Math.min(canvasArea.width / canvasDefault.width, canvasArea.height / canvasDefault.height);
  canvasContainScale = (canvasArea.width * canvasRatio) / canvasArea.width;
}

/**
 * 그리기 시작
 */
const startPosition = (e) => {
  if (pointerType) {
    drawCtx.closePath();
    removeEventListener(drawMove, move, { passive: true });
    removeEventListener(drawEnd, finishedPosition, { once: true });
    ppts = [];
    if (e.pointerType != 'pen') {
      pointerType = null;
      pointer = null;
      return;
    }
  }
  ({ pointerType } = e);
  pointer = e.pointerId;
  canvasResizeCalculation();
  x = e.clientX,
  y = e.y;
  x1 = (x - canvasArea.left) / canvasContainScale;
  y1 = (y - canvasArea.top) / canvasContainScale;
  drawCtx.lineWidth = thickness;
  drawCtx.lineJoin = 'round';
  drawCtx.lineCap = "round";
  drawCtx.strokeStyle = colors;
  if (selectDrawTool == "eraser") { // Eraser
    drawCanvas.style.opacity = 1;
    mainCtx.globalCompositeOperation = "destination-out";
  } else {
    mainCtx.globalCompositeOperation = "source-over";
  }
  removeListAllClassName(selectDrowToolOption, 'on');
  drawCtx.beginPath();
  drawCtx.moveTo(x1, y1);
  move(e);
  addEventListener(drawMove, move, { passive: true });
  addEventListener(drawEnd, finishedPosition, { once: true });
}

/**
 * 무브 이벤트
 * @param {clientX, clientY, pointerId} param0 
 * @returns 
 */
const move = ({ clientX, clientY, pointerId }) => {
  if (pointer != pointerId) return
  x = (clientX - canvasArea.left) / canvasContainScale;
  y = (clientY - canvasArea.top) / canvasContainScale;
  if (selectDrawTool == 'eraser') {
    mainCtx.drawImage(drawCanvas, 0, 0, drawCanvas.width, drawCanvas.height);
    drawCtx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
  } else ppts.push({ x: x, y: y });
  draw(x, y);
}

/**
 * 그리기
 * @param {number} x 
 * @param {number} y 
 */
const draw = (x, y) => {
  drawCtx.lineTo(x, y);
  drawCtx.stroke();
  if (selectDrawTool == 'eraser') {
    mainCtx.drawImage(drawCanvas, 0, 0, drawCanvas.width, drawCanvas.height);
    drawCtx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
  }
}

/**
 * 그리기 완료
 */
const finishedPosition = async (e) => {
  drawCtx.closePath();
  removeEventListener(drawMove, move, { passive: true });
  removeEventListener(drawEnd, finishedPosition, { once: true });
  pointerType = null;
  pointer = null;
  if (ppts.length == 1) {
    mainCtx.drawImage(drawCanvas, 0, 0, drawCanvas.width, drawCanvas.height);
    drawCtx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
  } else {
    if (selectDrawTool == "eraser") {
      addHistory(mainCanvas.toDataURL());
      return;
    }
    if (ppts.length) {
      await delay(10)
      offCtx.lineWidth = thickness;
      offCtx.lineJoin = 'round';
      offCtx.lineCap = "round";
      offCtx.strokeStyle = colors;
      offCtx.beginPath();
      offCtx.moveTo(ppts[0].x, ppts[0].y);
      while (ppts.length > 2) {
        const { x, y } = ppts.shift();
        offCtx.quadraticCurveTo(x, y, (x + ppts[0].x) / 2, (y + ppts[0].y) / 2);
      }
      offCtx.quadraticCurveTo(ppts[0].x, ppts[0].y, ppts[1].x, ppts[1].y);
      offCtx.stroke();
      offCtx.closePath();
      mainCtx.drawImage(offCanvas, 0, 0);
      offCtx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
      if (ppts.length) addHistory(mainCanvas.toDataURL());
      ppts = [];
      drawCtx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
      return;
    }
  }
  if (ppts.length) addHistory(mainCanvas.toDataURL());
  ppts = [];
}