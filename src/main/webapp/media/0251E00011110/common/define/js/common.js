/**
 * common.js
 */
window.addEventListener("load", function () {
  console.log('%c ::: common.js load complete 🚀 :::', 'background: #222; color: #bada55');

  onresize();
  addEventListener('resize', onresize);
  addEventListener('load', onresize);
});

const isMobile = ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i).test(navigator.userAgent));

const $ = function(sel){return document.querySelector(sel)};
const $$ = function(sel){return document.querySelectorAll(sel)};
const $frag = (function(){let range = document.createRange();return function(v){return range.createContextualFragment(v)}})();
const delay = (_ = 250, $) => new Promise(res => {
	if(typeof _ == 'number'){	
		setTimeout(res, _);
	}else{
		($ || this).addEventListener(_, res, {passive: true, once: true});
	}
});
/***************************
	Resizing
****************************/
let defaultSize, ratio, containScale, containerSize, container, offsetX, offsetY;
let background;
const onresize = function(){
  defaultSize = {width: 1280, height: 800};
  ratio = Math.min(innerWidth / defaultSize.width, innerHeight / defaultSize.height);
  containScale = (innerWidth * ratio) / innerWidth;

  container = document.querySelector("#wrap");
  container.style.transform = "scale("+containScale+")";
  containerSize = container.getBoundingClientRect();
    
  container.style.marginLeft = (offsetX = (innerWidth - containerSize.width) / 2) + "px";
  offsetY = 0;

  background = document.querySelector("#bg");
  background.style.width = innerWidth + "px";
  background.style.height = innerHeight + "px";
}

/***************************
	효과음
****************************/
let url = window.location.href;
url = url.substr(0, url.lastIndexOf("/"));
let arrayEff=function(arr){
	return arr.map(function(v){
		let audio=new Audio();
		audio.src=v;
		return audio
	})
};
let eff = arrayEff([
	"../common/define/media/effect.mp3",           //0
	"../common/define/media/intro_define.mp3",     //1
	"../common/define/media/intro_quiz.mp3",       //2
]);
const effectFunction = function(path){
	try{
		switch(path){
			case "click":
				eff[0].pause();
				eff[0].currentTime = 0;
				eff[0].play();
				break;
      case "intro_define":
				eff[1].pause();
				eff[1].currentTime = 0;
				eff[1].play();
				break;
      case "intro_quiz":
				eff[2].pause();
				eff[2].currentTime = 0;
				eff[2].play();
				break;
			default:
				eff[path].pause();
				eff[path].currentTime = 0;
				eff[path].play();
				break;
		}
	}catch(e){
    console.warn(e);
		console.log("sound null!");
	}
}

/**
 * 목록 전체 클래스명 지우기
 * @param {Html} elementList 
 * @param {string} className 
 */
const removeListAllClassName = (elementList, className) => {
  for (const $ of elementList)
    $.classList.remove(className);
}

/**
 * 로컬 이미지 파일 저장
 */
const saveLocalImageFile = () => {
  const date = new Date();
  let $a = document.createElement("a");
  $a.href = saveCanvas.toDataURL("Image/png");
  $a.download = `image${date.getTime()}`;
  $a.click();
}