/**
 * 히스토리 JS
 * 이전, 다시 실행 기능
 */
window.addEventListener("load", () => {
  console.log('%c ::: history.js load complete 🚀 :::', 'background: #222; color: #bada55');
  // 히스토리 이미지 온로드
  historyImg.onload = () => { onloadHistoryRender() }
  // 이벤트 리스너
  $('[data-prev]').addEventListener('click', prevHistory);
  $('[data-next]').addEventListener('click', nextHistory);
});

/**
 * 히스토리 전역 변수
 */
const historyImg = new Image; // 히스토리 이미지 객체
let history = [];             // 히스토리 배열
let historyIdx = 0;           // 히스토리 인덱스
let isAllRemove = true;       // 전체 지워진 상태값

/**
 * 히스토리 이미지 렌더링
 */
const onloadHistoryRender = () => {
  mainCtx.clearRect(0, 0, mainCanvas.width, mainCanvas.height);
  mainCtx.scale(1 / devicePixelRatio, 1 / devicePixelRatio);
  mainCtx.drawImage(historyImg, 0, 0);
  mainCtx.scale(devicePixelRatio, devicePixelRatio);
}
/**
 * History 추가
 * @param {toDataURL} url mainCanvas.toDataURL()
 */
const addHistory = (url, isRemove) => {
  // 전체지우기 중복 제거
  if (isAllRemove && isRemove) return;
  isAllRemove = isRemove ? true : false;
  history.splice(historyIdx + 1, history.length, url);
  historyIdx = history.length - 1;
  if (historyIdx > -1) $('[data-prev]').classList.remove('off');
  if (historyIdx + 1 == history.length) $('[data-next]').classList.add('off');
}
/**
 * History 이전
 */
const prevHistory = () => {
  if (history.length < 1) return;
  isAllRemove = false;

  $('[data-next]').classList.remove('off');
  if (--historyIdx <= -1) {
    historyIdx = -1;
    mainCtx.clearRect(0, 0, mainCanvas.width, mainCanvas.height);
    $('[data-prev]').classList.add('off');
  } else {
    historyImg.src = history[historyIdx];
  }
  mainCtx.globalCompositeOperation = "source-over";
}
/**
 * History 다시 실행
 */
const nextHistory = (e) => {
  if (history.length < 1) return;
  isAllRemove = false;

  const nextBtn = e.target;
  if (historyIdx + 1 != history.length) {
    historyImg.src = history[++historyIdx];
  }
  $('[data-prev]').classList.remove('off');
  if (historyIdx + 1 == history.length) nextBtn.classList.add('off');
  mainCtx.globalCompositeOperation = "source-over";
}