/**
 * callback.js
 * smartwacam API 호출 
 * 서버 처리 이후 callback 함수 모음
 */
window.addEventListener("load", () => {
  console.log('%c ::: callback.js load complete 🚀 :::', 'background: #222; color: #bada55');
});

/**
 * 콜백 임시저장
 * @param {string} status Ok == 200
 */
const dataTypeImageUploadCallback = (status) => {
  if (status == "Ok") {
    // 임시저장 팝업
    var popTemporary = document.querySelector('#popTemporary');
    popTemporary.classList.remove("hide");
  } else {
    // 에러 팝업
    alertSave();
  }
}

/**
 * 콜백 저장 완료
 * @param {string} status Ok == 200
 */
const imageFileUploadCallback = (status) => {
  if (status == "Ok") {
    getSaveList(); // 저장된 내용 불러오기
    savePopup.classList.remove("hide");

    // 완료 API 호출
    try {
      swcontobj.fnClkStudyFinish();
    } catch (e) {
      console.warn(e);
      let timeOut = setTimeout(() => {
        swcontobj.fnClkStudyFinish();
        clearTimeout(timeOut);
      }, 200);
    }
  } else {
    // 에러 시
    alertSave();
  }
}

/**
 * 콜백 임시저장 불러오기
 * @param {string} status Ok == 200
 * @param {*} data 
 */
const dataTypeImageCallCallback = (status, data) => {
  if (status == "Ok") {
    let $img = new Image();
    $img.onload = function () {
      mainCtx.scale(1 / devicePixelRatio, 1 / devicePixelRatio);
      mainCtx.drawImage($img, 0, 0);
      mainCtx.scale(devicePixelRatio, devicePixelRatio);
    };
    $img.src = data;
    $img.onload();
    getSaveList();
  } else {
    console.warn("::: dataTypeImageCallCallback: 임시저장 파일 불러오기 오류 :::");
  }
}

/**
 * 콜백 삭제
 * @param {string} status Ok == 200
 */
const deleteUploadImageCallback = async(status) => {
  if (status == "Ok") {
    await getSaveList();
    saveSelectPopup.removeAttribute('data-seq');
    saveSelectPopup.querySelector('.img').remove();
    saveSelectPopup.classList.add("hide");
    savePopup.classList.add("hide");
    deletePopup.classList.add("hide");
    if (saveLength < 3) {
      savePopup.classList.remove("full");
    }
  } else {
    alertDelete();
  }
}

/**
 * 콜백 저장 실패 alert
 */
const alertSave = () => {
  const alertClose = $(".alert_save .alertClose");
  $(".alert_save").classList.remove("hide");
  alertClose.onclick = () => {
    effectFunction("click");
    alertClose.parentElement.classList.add("hide");
  }
}
/**
 * 콜백 삭제 실패 alert
 */
const alertDelete = () => {
  const alertClose = $(".alert_delete .alertClose");
  $(".alert_delete").classList.remove("hide");
  alertClose.onclick = () => {
    effectFunction("click");
    alertClose.parentElement.classList.add("hide");
  }
}