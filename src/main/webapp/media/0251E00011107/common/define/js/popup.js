/**
 * popup.js
 */
window.addEventListener("load", () => {
  console.log('%c ::: popup.js load complete 🚀 :::', 'background: #222; color: #bada55');

  // 저장완료 보기 버튼 이벤트 리스너
  for (let i = 0; i < bogiBtn.length; i++) {
    bogiBtn[i].addEventListener(drawStart, bogiFn);
  }
  // 보기팝업 삭제 버튼 이벤트 리스너
  selectDeleteBtn?.addEventListener(drawStart, openDeletePop);

  // 친구 답안
  drawTipBtn.addEventListener(drawStart, () => {
    drawTipPop.classList.remove("hide");
  });
  // 친구 답안 닫기
  drawTipCloseBtn.addEventListener(drawStart, () => {
    drawTipPop.classList.add("hide");
  });
  // 보기 팝업 닫기
  selectPopCloseBtn.addEventListener(drawStart, () => {
    savePopup.classList.remove("hide");
    saveSelectPopup.classList.add("hide");
    effectFunction('click');
  });
  // 팝업 닫기
  for (let i = 0; i < drawClose.length; i++) {
    drawClose[i].addEventListener(drawStart, () => {
      effectFunction('click');
      var j = 0;
      while (j < commonPopup.length) {
        commonPopup[j].classList.add("hide");
        j++
      };
    });
  }
  // 삭제 팝업 아니오, 취소 버튼
  deletePopNo.addEventListener(drawStart, closeDeletePop);
  deletePopClose.addEventListener(drawStart, closeDeletePop);

  // 삭제 팝업 예
  deletePopYes.addEventListener(drawStart, deleteFn);
});

/**
 * 팝업 전역변수
 */
const bogiBtn = $$(".btn_bogi");
const selectDeleteBtn = $(".selectDeleteBtn");
const drawTipPop = $('.drawTipPop');
const drawTipBtn = $('.drawTipBtn');
const drawTipCloseBtn = $('.drawTipCloseBtn');
const drawClose = $$('.drawClose');
const commonPopup = $$('.commonPopup');
const saveSelectPopup = $("#save_select");
const selectPopCloseBtn = $('.selectPopClose');
const deletePopup = $('#save_delete');
const deletePopYes = $('.deletePopYes');
const deletePopNo = $('.deletePopNo');
const deletePopClose = $('.deletePopClose');

/**
 * 보기 버튼
 */
const bogiFn = (e) => {
  const target = e.target;
  const i = target.getAttribute('data-i');
  const bogiBox = $(`.bogiBox${(i*1)+1}`);
  const alive = bogiBox.getAttribute('class').includes('on');
  if (!alive) return;
  effectFunction("click");
  const $img = saveSelectPopup.querySelector('.img') || new Image();
  try {
    $img.src = saveList[i].url;
    // 삭제시 필요 고유키
    saveSelectPopup.setAttribute('data-seq', saveList[i].seq);

    $img.className = 'img';

    $img.onload = () => {
      saveSelectPopup.setAttribute('data-i', i);
      saveSelectPopup.appendChild($img);
      savePopup.classList.add("hide");
      saveSelectPopup.classList.remove("hide");
    }
  } catch (e) {
    console.warn(e);
    alert('보기버튼 오류발생!\n다시 시도해 주세요.')
  }
}

/**
 * 삭제 팝업 오픈
 */
const openDeletePop = () => {  
  effectFunction("click");
  deletePopup.classList.remove("hide");
}
  
/**
 * 삭제 버튼
 */
const deleteFn = () => {
  effectFunction("click");
  // 이미지 삭제 API 호출 => deleteUploadImageCallback() 콜백
  try {
    swcontobj.DeleteUploadImage(saveSelectPopup.getAttribute('data-seq'));
  } catch (e) {    
    console.warn(e);
    deleteUploadImageCallback();
  }
}

/**
 * 삭제 팝업 닫기
 */
const closeDeletePop = () => {
  effectFunction("click");
  deletePopup.classList.add("hide");
}