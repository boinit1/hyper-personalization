/**
 * 그리기 툴 JS
 * 굵기, 색상, 지우개 등
 */
window.addEventListener("load", function () {
  console.log('%c ::: drawingTools.js load complete 🚀 :::', 'background: #222; color: #bada55');

  // 이벤트 리스너
  // drawStart 전역변수 canvas.js 정의

  // 툴 선택 이벤트 리스너
  for (let i = 0; i < drawingTools.length; i++) {
    drawingTools[i].addEventListener(drawStart, drawingToolChoice);
  }

  // 색상 선택 이벤트 리스너
  for (let i = 0; i < colorPicker.length; i++) {
    colorPicker[i].addEventListener(drawStart, colorChoice);
  }

  // 두께 핸들러
  dragElement(document.getElementById("thicknessHandler"), 91, 17, setThickness);

  // 모두 지우기  
  allRemove.addEventListener(drawStart, allClear);
});

/**
 * 그리기 툴 전역변수
 */
const drawingTools = $$(".drawTool dt"); // 그리기 툴 목록
const colorPicker = $$("[data-color]");  // 색상 목록
const sketchImg = $('.canvasWrap img#sketch');
let thickness = 8.4; // 펜두께
let colors = '#030000'; // 펜컬러
let selectDrawTool = 'pen'; // 선택한 그리기 툴
const selectDrowToolOption = $$('.drawTool dd');
const allRemove = $('[data-all-remove]'); // 모두 지우기
const pen = $('[data-drawing-tool="pen"]');

/**
 * 그리기 툴 선택
 */
 const drawingToolChoice = (e) => {
  const selectTool = e.target;
  effectFunction("click"); // 효과음

  // 밑그림
  if (selectTool.classList.contains('background')) {
    if (selectTool.textContent == "배경 가리기") {
      selectTool.textContent = "배경 보이기"
      selectTool.classList.add('off');
      sketchImg.style.opacity = 0;
    } else {
      selectTool.textContent = "배경 가리기";
      selectTool.classList.remove('off');
      sketchImg.removeAttribute('style');
    }
  }

  // 굵기, 색상 등 별도의 선택옵션 창
  const $next = selectTool.nextElementSibling;
  // 옵션창 전체 가리기
  removeListAllClassName(selectDrowToolOption, 'on');
  
  // 선택한 툴 옵션창 토글
  if ($next && $next.localName == 'dd')
    $next.classList.toggle('on');

  // 선택 툴
  selectDrawTool = selectTool.getAttribute("data-drawing-tool") || selectDrawTool;
}

/**
 * 색 선택
 */
const colorChoice = (e) => {
  const selectColor = e.target;
  e.stopPropagation();
  effectFunction("click");
  colors = selectColor.getAttribute("data-color");
  for (let i = 0; i < colorPicker.length; i++) {
    colorPicker[i].classList.remove("select");
  }
  selectColor.classList.add("select");
  selectColor.closest('dd').classList.remove('on');

  let penColor = colors.replace('#', 'c');

  // 팬 촉 컬러
  pen.className = `color ${penColor}`;
}

/**
 * 굵기 드래그
 * @param {HTMLElement} elmnt 
 * @param {number} min 
 * @param {number} max 
 * @param {(number) => void} setThickness 
 */
const dragElement = (elmnt, min, max, setThickness) => {
  let pos1 = 0, pos2 = 0;
  let dragTimer;
  const dragMouseDown = (e, eventName) => {
    e = e || window.event;
    if(eventName != 'touchstart') e?.preventDefault();
    pos2 = e.clientY || e?.changedTouches?.[0]?.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
    elmnt.addEventListener('touchmove', (e) => {
      dragTimerEvent(e);
    });
    elmnt.addEventListener('touchend', (e) => {
      closeDragElement(e);
    });
  }

  const dragTimerEvent = (e) => {
    if (dragTimer) clearTimeout(dragTimer);
    dragTimer = setTimeout(() => {
      elementDrag(e, 'touchmove');
    }, 2);
  }

  const elementDrag = (e, eventName) => {
    e = e || window.event;
    if(eventName != 'touchmove') e?.preventDefault();
    pos1 = pos2 - (e.clientY || e?.changedTouches?.[0].clientY);
    pos2 = e.clientY || e?.changedTouches?.[0].clientY;
    let y = (elmnt.offsetTop - pos1);
    // 위쪽이 y값이 줄어듬
    if (y > min) y = min;
    if (y < max) y = max;
    elmnt.style.top = y + "px";

    let thickness = 1 + ((min - y) * 0.2);
    let pointSize = 14 + ((min - y) * 0.27);
    setThickness(thickness, pointSize);
  }

  const closeDragElement = () => {
    document.onmouseup = null;
    document.onmousemove = null;
    elmnt.onmouseup = null;
    elmnt.onmousemove = null;
  }

  elmnt.onmousedown = dragMouseDown;
  elmnt.addEventListener('touchstart', (e) => {
    dragMouseDown(e, 'touchstart');
  });
}

/**
 * 펜두께 조절
 * @param {number} changeThickness 
 */
function setThickness(changeThickness, pointSize) {
  thickness = changeThickness;

  let leftPosition = 28 + ((24 - pointSize) / 2);
  $('#thicknessHandler').style.width = `${pointSize}px`;
  $('#thicknessHandler').style.height = `${pointSize}px`;
  $('#thicknessHandler').style.borderRadius = `${pointSize}px`;
  $('#thicknessHandler').style.left = `${leftPosition}px`;
}

/**
 * 모두 지우기
 */
const allClear = () => {
  mainCtx.clearRect(0, 0, mainCanvas.width, mainCanvas.height);
  addHistory(mainCanvas.toDataURL(), true);
}