(function ($) {
  function VideoWrapper() {
    this.video = null;
    this.opts = null;
    this.playIndex = 0;
    this.isCanPlay = false;
    this.isFullscreen = false;
    this.timeInstance = null;
    this.timeSec = 4000;
    this.lastPlayTime = 0;
    this.rightProgressDisable = false;
    this.isStartLoading = false;
    this.isFirstPlay = false;
    this.playtimeInstance = null;
    this.errorTimeInstance = null;
    this.clickType = null;
    this.config = { muted: false, zIndex: 1, volume: 1 };

    this.init = function () {
      this.config = $.extend(this.config, this.opts.config);

      this.append(this.utils.createControlHtml());

      var testVideo = document.createElement('video');

      this.isCanPlay = testVideo.canPlayType ? true : false;
      if (!this.isCanPlay) {
        alert('video를 지원하지 않습니다.');
      }

      this.find('*').attr('data-video', 'video_player');

      if (this.opts != null) {
        if (this.opts.callback) {
          this.opts.callback(this);
        }

        if (this.opts.subtitles != null) {
          //자막 초기화
          this.subTitle.set(this.opts.subtitles);
        }
      }
    };

    this.load = function (playIndex, callback) {
      try {
        this.isStartLoading = true;
      } catch (e) {}

      if (typeof playIndex == 'function') {
        callback = playIndex;
        playIndex = 0;
      }
      this.api.finished = false;

      if (playIndex && playIndex <= this.opts.playList.length) {
        this.playIndex = playIndex;
      }

      if (!this.find('video').get(0) && this.isCanPlay) {
        var option = this.opts.playList[this.playIndex];
        var screen = this.find('.video_screen');
        var videoBox = this.find('.video_box');

        this.video = this.utils.createVideoTag(option);

        if (option.style) {
          screen.attr('style', option.style);
        }

        if (option.isLockToolbar) {
          screen.addClass('lock');
          videoBox.addClass('lock');
        } else {
          screen.removeClass('lock');
          videoBox.removeClass('lock');
        }

        screen.html(this.video);

        this.uiListener(this);

        this.api.paused = !this.video.autoplay;
        this.api.setVideoListener(this);
        this.rightProgressDisable = option.rightProgressDisable;

        if (!this.api.paused) {
          this.play();
        }
      }

      this.find('.player_loading').addClass('on');

      if (callback) {
        callback(this);
      }
    };

    //private
    this._playing = function (e) {
      this.api.playing = true;
    };

    this.api = {
      event: [
        'play',
        'playing',
        'pause',
        'loadeddata',
        'loadedmetadata',
        'loadstart',
        'timeupdate',
        'seeked',
        'seeking',
        'ended',
        'durationchange',
        'canplay',
        'canplaythrough',
        'volumechange',
        'progress',
        'error',
      ],
      test: this.config.volume,
      currentSpeed: 1,
      volumeLevel: this.config.muted
        ? 0
        : typeof this.config.volume === 'undefined'
        ? 1
        : this.config.volume,
      // states
      disabled: false,
      finished: null,
      loading: false,
      muted: false,
      paused: true,
      playing: false,
      ready: false,
      splash: false,
      debug: false,
      dragging: false,
      vDragging: false,
      fullscreen: (function () {
        return (
          (typeof document.webkitCancelFullScreen == 'function' &&
            !/Mac OS X 10_5.+Version\/5\.0\.\d Safari/.test(
              navigator.userAgent
            )) ||
          document.mozFullScreenEnabled ||
          typeof document.exitFullscreen == 'function' ||
          typeof document.msExitFullscreen == 'function'
        );
      })(),
      setVideoListener: function (player) {
        for (var i = 0; i < this.event.length; i++) {
          player.utils.listenEvent(
            player,
            player.video,
            this.event[i],
            function (e) {
              if (player.utils.debug) {
              }
              player.trigger(e.type);
            },
            false
          );
        }

        this.ready = true;
      },
      removeVideoListener: function (player) {
        for (var i = 0; i < this.event.length; i++) {
          player.utils.removeListener(player, this.event[i]);
        }
      },
      seek: function (time, player) {
        player.video.currentTime = time;
      },
    };

    this.utils = {
      handlers: {},
      eventArguments: {},
      listenEvent: function (elem, obj, type, hndlr, isOne) {
        var internalHandler = function (ev) {
          if (elem.utils.handlers[elem.id + type] && isOne) {
            elem.removeEventListener(type, internalHandler);
            elem.utils.handlers[elem.id + type].splice(
              elem.utils.handlers[elem.id + type].indexOf(internalHandler),
              1
            );
          }

          //var args = [ev].concat(elem.utils.eventArguments[ev.timeStamp + ev.type] || []);
          var args = [ev];
          if (hndlr) hndlr.apply(this, args);
        };

        obj.addEventListener(type, internalHandler);
        this.handlers[elem.id + type] = internalHandler;
      },
      getType: function (type) {
        return /mpegurl/i.test(type) ? 'application/x-mpegurl' : type;
      },
      speeds: [
        [1.4, '빠르게'],
        [1.0, '보 통'],
        [0.8, '느리게'],
      ],
      createControlHtml: function () {
        var playerStr = '';
        playerStr += '<div class="video_context_menu"></div>';
        playerStr += '<div class="video_screen"></div>';
        playerStr +=
          '<div class="video_box">' +
          '<div class="pause_box">' +
          '<p class="pause_shadow"></p>' +
          '<a href="#" class="replay player">다시재생</a>' +
          '<a href="#" class="pause_play">재생</a>' +
          '</div>' +
          '<span class="tool_bg"></span>' +
          '<div class="tool_box">' +
          '<a class="play"><span class="player">재생</span></a>' +
          '<div class="timeline" data-name="timeline">' +
          '<span class="shadow" data-name="timeline"></span>' +
          '<p class="buffer" data-name="timeline"></p>' +
          '<p class="progress" data-name="timeline">' +
          '<span class="progress_btn player" data-name="timeline"></span>' +
          '</p>' +
          '</div>' +
          '<div class="video_timer"><span class="video_play_time">00:00</span> / <span class="video_total_time">00:00</span></div>' +
          '<div class="volumeline" data-name="volumeline">' +
          '<span class="shadow" data-name="timeline"></span>' +
          '<p class="volume_progress" data-name="volumeline">' +
          '<span class="volume_progress_btn player" data-name="volumeline"></span>' +
          '</p>' +
          '</div>' +
          '<div class="volume_speed_box" data-nodeid="volume-box">' +
          '<a href="#" class="speed_item selected_volume" data-item="' +
          this.speeds[1][0] +
          '" data-nodeid="volume-box">' +
          this.speeds[1][1] +
          '<span data-nodeid="volume-box">' +
          this.speeds[1][0] +
          '×</span></a>' +
          '<ul class="speed_list" data-nodeid="volume-box">';
        for (var i = 0; i < this.speeds.length; i++) {
          playerStr +=
            '<li class="speed_item ' +
            (i == 1 ? 'on' : '') +
            '" data-item="' +
            this.speeds[i][0] +
            '" data-nodeid="volume-box">' +
            this.speeds[i][1] +
            '<span data-nodeid="volume-box">' +
            this.speeds[i][0] +
            '×</span></li>';
        }
        playerStr +=
          '</ul>' +
          '</div>' +
          '</div>' +
          '<div id="subtitles">' +
          '<div class="title_box"><span class="sub"></span><span class="btn_close_subtitle"></span></div>' +
          '<span class="btn_subtitles"></span>' +
          '</div>' +
          '</div>';
        playerStr += '<div class="header_bar"><p class="bg"></p></div>';
        playerStr += '<p class="player_loading"></p>';
        return playerStr;
      },
      videoTagCache: null,
      createVideoTag: function (video) {
        if (typeof video.autoplay === undefined) video.autoplay = false;
        if (typeof video.preload === undefined) video.preload = 'metadata';

        if (this.videoTagCache) {
          this.videoTagCache.type = getType(video.type);
          this.videoTagCache.src = video.src;
          this.videoTagCache.removeAttribute('crossorigin');
          return videoTagCache;
        }

        var el = document.createElement('video');
        el.src = video.src;
        el.type = this.getType(video.type);
        el.playsinline = true;
        el.className = 'video';
        el.autoplay = video.autoplay ? 'autoplay' : false;
        el.preload = video.preload;
        el.setAttribute('x-webkit-airplay', 'allow');
        el.setAttribute('style', 'width:100%;height:100%;');

        return el;
      },
      unloadVideo: function (elem) {
        if (this.videoTagCache) {
          this.videoTagCache = null;
        }
      },
      removeListener: function (elem, type) {
        for (var k in this.handlers) {
          elem.video.removeEventListener(type, this.handlers[elem.id + type]);
          delete this.handlers[elem.id + type];
        }
      },
      fullscreen: function (player) {
        if (player.api.fullscreen) {
          var nua = navigator.userAgent;
          var events = [
            [
              'requestFullScreen',
              'exitFullscreen',
              'fullscreenchange',
              'isFullScreen',
            ],
            [
              'webkitRequestFullScreen',
              'webkitCancelFullScreen',
              'webkitfullscreenchange',
              'webkitIsFullScreen',
            ],
            [
              'mozRequestFullScreen',
              'mozCancelFullScreen',
              'mozfullscreenchange',
              'mozIsFullScreen',
            ],
            [
              'msRequestFullscreen',
              'msExitFullscreen',
              ,
              'msfullscreenchange',
              'msIsFullScreen',
            ],
          ]; //on,off

          for (var i = 0; i < events.length; i++) {
            if (player.isFullscreen) {
              if (typeof document[events[i][1]] == 'function') {
                document[events[i][1]]();
                $(document).off('keyup');
                player._fullscreenExit();
                return;
              }
            } else {
              if (typeof player.get(0)[events[i][0]] == 'function') {
                player.get(0)[events[i][0]](Element.ALLOW_KEYBOARD_INPUT);
                //esc key event
                $(document).on('keyup', function (e) {
                  var key = e.which;
                  if (key == 27) {
                    player._fullscreenExit();
                    $(document).off('keyup');
                  }
                });
                //esc key event add
                $(document).on(events[i][2], function () {
                  if (!document[events[i][3]]) {
                    player._fullscreenExit();
                    $(document).off(events[i][2]);
                  }
                });

                if (
                  /(safari)[ \/]([\w.]+)/.test(nua) &&
                  !/(chrome)[ \/]([\w.]+)/.test(nua) &&
                  !document.webkitCurrentFullScreenElement &&
                  !document.mozFullScreenElement
                ) {
                  player.get(0)[events[i][0]]();
                }

                player._fullscreen();
                return;
              }
            }
          }
        }
      },
      slide: {
        offset: null,
        size: 0,
        width: 0,
        height: 0,
        max: 0,
        value: 0,
        fire: function (value, player, isSeek) {
          return player.seekTo(value * 100, isSeek);
        },
        throttle: function (fn, delay) {
          var locked;

          return function () {
            if (!locked) {
              fn.apply(this, arguments);
              locked = 1;
              setTimeout(function () {
                locked = 0;
              }, delay);
            }
          };
        },
        calc: function (obj) {
          this.offset = obj.offset();
          this.width = obj.width();
          this.height = obj.height();

          this.size = this.width;
          this.max = this.toDelta(this.maxValue);
        },
        mousemove: function (e) {
          var pageX = e.pageX || e.clientX;
          if (
            !pageX &&
            e.originalEvent &&
            e.originalEvent.touches &&
            e.originalEvent.touches.length
          ) {
            pageX = e.originalEvent.touches[0].pageX;
          }

          var delta = pageX - this.offset.left;
          delta = Math.max(0, Math.min(this.max || this.size, delta));

          var value = delta / this.size;
          this.value = this.move(value, 0);
          return this.value;
        },
        move: function (value, speed) {
          if (speed === undefined) {
            speed = 0;
          }
          if (value > 1) value = 1;

          var to = Math.round(value * 1000) / 10 + '%';

          return value;
        },
        toDelta: function (value) {
          return Math.max(0, Math.min(this.size, value * this.width));
        },
      },
    };

    this._setupPlayer = function () {
      if (this.find('.video_box').hasClass('is_over')) {
        return;
      }

      if (this.api.playing && this.api.ready) {
        if (this.opts['playingback']) {
          this.opts['playingback']();
        }

        this.find('.tool_bg').addClass('off');
        this.find('.tool_box').addClass('off');
        this.find('.tool_box .timeline').css('width', '');

        this.find('.header_bar').addClass('off');

        if (this.subTitle.isActiv) {
          this.subTitle.node.addClass('playing');
        }
      }

      clearInterval(this.timeInstance);
      this.timeInstance = null;
    };

    this._play = function (e) {
      if (e == 'canplay') {
        return;
      }

      this.api.paused = false;
      if (this.playtimeInstance == null) {
        this.removeClass('ready pause').addClass('play');
      }

      if (this.timeInstance != null) {
        clearInterval(this.timeInstance);
        this.timeInstance = null;
      }

      if (!this.opts.playList[this.playIndex].isLockToolbar) {
        this.timeInstance = setInterval(this._setupPlayer.bind(this), 5000);
      }

      const video = this.find('video')[0];
      gtag("event", "video-play", {
        videoSrc: video.src,
        pauseTime: video.currentTime,
      });
    };

    this._error = function (e) {
      this.api.ready = false;

      this.find('.tool_bg').hide();
      this.find('.video_box > .tool_box').hide();

      console.log('재생할수 없습니다=' + this.getUrl());

      if (this.opts['error']) {
        if (this.opts['error']() > 1) {
          this._finished();
        }
      } else {
        this._finished();
      }

      // this.find('.player_loading').removeClass('on');
    };

    this._pause = function () {
      this.api.playing = false;
      this.api.paused = true;

      // if (this.isFirstPlay) {
      //     this.removeClass('pause play').addClass('ready');
      // } else {
      this.removeClass('ready play').addClass('pause');
      // }

      if (this.timeInstance != null) {
        clearInterval(this.timeInstance);
        this.timeInstance = null;
      }

      this.find('.tool_bg').removeClass('off');
      this.find('.video_box > .tool_box').removeClass('off');

      this.find('.header_bar').removeClass('off');

      if (this.opts['pauseback']) {
        this.opts['pauseback']();
      }

      if (this.subTitle.isActiv) {
        this.subTitle.node.removeClass('playing');
      }

      const video = this.find('video')[0];
      gtag("event", "video-pause", {
        videoSrc: video.src,
        pauseTime: video.currentTime,
      });
    };

    this._resume = function () {
      if (this.api.ready) {
        var min = Math.floor(this.video.duration / 60).toString();
        var sec = Math.floor(this.video.duration % 60).toString();

        this.find('.video_total_time').html(
          min.padLeft(2, '0').concat(':').concat(sec.padLeft(2, '0'))
        );
        this.find('.video_timer').addClass('on');
        this.playTimeJq = this.find('.video_play_time');
      }
    };

    this._seekVideo = function () {
      if (
        this.isFirstPlay &&
        this.opts['firstPlay'] &&
        this.video.currentTime > 0
      ) {
        this.opts['firstPlay']();
        this.isFirstPlay = false;
      }

      if (this.api.ready && this.playTimeJq && !this.api.dragging) {
        var min = Math.floor(this.video.currentTime / 60).toString();
        var sec = Math.floor(this.video.currentTime % 60).toString();
        var buffer = this.video.buffered.end(0);

        this.playTimeJq.html(
          min.padLeft(2, 0).concat(':').concat(sec.padLeft(2, 0))
        );

        this.find('.progress').css({
          width:
            ((this.video.currentTime / this.video.duration) * 100).toFixed(2) +
            '%',
        });
        this.find('.buffer').css({
          width: ((buffer / this.video.duration) * 100).toFixed(2) + '%',
        });
      }

      if (this.opts['timeupdate']) {
        this.opts['timeupdate']();
      }

      if (this.opts.subtitles && this.subTitle.isActiv) {
        this.subTitle.get(this.video.currentTime);
      }
    };

    this._seek = function (time) {
      if (this.api.ready && this.playTimeJq) {
        var min = Math.floor(time / 60).toString();
        var sec = Math.floor(time % 60).toString();
        var buffer = this.video.buffered.end(0);

        this.playTimeJq.html(
          min.padLeft(2, 0).concat(':').concat(sec.padLeft(2, 0))
        );

        this.find('.progress').css({
          width: ((time / this.video.duration) * 100).toFixed(2) + '%',
        });
        this.find('.buffer').css({
          width: ((buffer / this.video.duration) * 100).toFixed(2) + '%',
        });
      }
    };

    this._fullscreenExit = function () {
      try {
        this.trigger('fullscreenExit');
      } catch (e) {}

      this.isFullscreen = false;
      // this.find('.fullscreen').removeClass('on');
      this.find('.video_box').removeClass('is_fullscreen');
      this.find('.tool_box .timeline').attr('data-width', '').css('width', '');
    };

    this._finished = function () {
      if (this.isFullscreen) {
        this.utils.fullscreen(this);
      }

      this.api.finished = true;
      this.removeClass('play');
      if (this.opts.subtitles) {
        this.subTitle.close();
      }
      try {
        this.trigger('finish');
        if (this.finished) {
          this.finished();
        }
      } catch (e) {
        alert(e);
      }
    };

    this._unload = function () {
      if (this.api.ready) {
        this.utils.unloadVideo();
        this.api.removeVideoListener(this);
        this.api.ready = false;

        clearInterval(this.timeInstance);
      }
    };

    this.subTitle = {
      isActiv: false,
      syncPosition: -1,
      node: null,
      titleBox: null,
      sub: null,
      sideButton: null,
      sync: {}, //0:{e:3600,title:'자막'} 0=starttime,e=endtime
      set: function (sync) {
        this.node = $('#subtitles');
        this.titleBox = this.node.find('.title_box');
        this.sub = this.titleBox.find('.sub');
        this.sideButton = this.node.find('.btn_subtitles');
        this.sync = sync;
      },
      get: function (time) {
        time = parseInt(time);

        if (this.sync[time]) {
          this.sub.html(this.sync[time].title);
        } else {
          var prevSync = null;
          var prevKey = -1;
          for (var k in this.sync) {
            if (parseInt(k) > time) {
              break;
            }

            prevKey = k;
          }

          prevSync = this.sync[prevKey];

          this.sub.html(
            prevSync && parseInt(prevSync.e) + 1 > time ? prevSync.title : ''
          );
        }
      },
      hidden: function () {
        this.isActiv = false;
        this.titleBox.hide();
        this.sideButton
          .show()
          .addClass('slideLeftShow')
          .css('animation-duration', '100ms');
      },
      show: function () {
        this.isActiv = true;
        this.sideButton.hide();
        this.titleBox.show();
      },
      open: function () {
        //this.show();
        this.node.show();
      },
      close: function () {
        this.node.hide();
      },
    };
  }

  VideoWrapper.prototype.uiListener = function (player) {
    this.on('play', function (e) {
      player._play(e);
      return false;
    });
    this.on('playing', function (e) {
      player._playing(e);
      return false;
    });
    this.on('pause', function (e) {
      player._pause(e);
      return false;
    });
    this.on('ended', function (e) {
      player._finished(e);
      return false;
    });
    this.on('durationchange', function (e) {});
    this.on('timeupdate', function (e) {
      player._seekVideo();
      return false;
    });
    // this.on('canplaythrough', function (e) {
    //     if (!player.isFirstPlay) {
    //         player.api.muted = player.video.muted = true;
    //         player.play();
    //         player.isFirstPlay = true;
    //     }
    // });

    this.on('loadeddata', function (e) {
      player._resume();
      player.find('.player_loading').removeClass('on');
      return false;
    });
    //   this.on('volumechange', function (e) {
    //     return false;
    //     });
    this.on('error', function (e) {
      player._error(player.error);
      return false;
    });
    this.on(this.clickType, function (e) {
      e = e.target ? e.target : e.srcElement;
      var dn = $(e).attr('data-name');

      if (player.speedBox.hasClass('on')) {
        player.speedBox.removeClass('on');
        return false;
      }

      if (
        player.clickType != 'click' &&
        player.api.playing &&
        player.find('.tool_box').hasClass('off')
      ) {
        return false;
      }

      if (!dn || dn.indexOf('timeline') < 0) {
        player.find('.play').trigger(player.clickType);
      }

      return false;
    });

    this.find('.video_box .tool_box').on(this.clickType, function (e) {
      e = e.target ? e.target : e.srcElement;
      var nodeId = $(e).attr('data-nodeid');

      if (player.speedBox.hasClass('on') && nodeId != 'volume-box') {
        player.speedBox.removeClass('on');
      }
      return false;
    });

    this.find('.play').on(this.clickType, function () {
      if (player.api.paused) {
        player.play();
      } else if (player.api.playing) {
        player.pause();
      }

      return false;
    });

    this.find('.replay').on(this.clickType, function () {
      player.seek(0);

      if (player.api.paused) {
        player.play();
      }

      return false;
    });

    // this.find('.fullscreen').on(this.clickType, function () {
    //     player.utils.fullscreen(player);
    //     return false;
    // });

    //   this.find('.speaker').on(this.clickType, function (e) {
    //       player.api.muted = player.video.muted = !player.api.muted;
    //       var that = $(this);
    //       if (player.api.muted) {
    //           that.addClass('on');
    //       } else {
    //           that.removeClass('on');
    //       }

    //       return false;
    //   });

    if (this.opts.subtitles) {
      this.find('.btn_close_subtitle').on(
        this.clickType,
        function () {
          this.subTitle.hidden();
          return false;
        }.bind(this)
      );
      this.find('.btn_subtitles').on(
        this.clickType,
        function () {
          this.subTitle.show();
          return false;
        }.bind(this)
      );
    }

    var eventDown = 'mousedown';
    var eventMove = 'mousemove';
    var eventUp = 'mouseup mouseleave';
    var eventOv = 'mouseover';
    var eventOut = 'mouseenter mouseleave';

    if (this.clickType != 'click') {
      eventDown = 'pointerdown';
      eventMove = 'pointermove';
      eventUp = 'pointerout pointerleave pointercancel';
      eventOv = eventDown;
      eventOut = 'pointerdown pointerout';
    }

    this.on(eventOut, function (e) {
      var option = player.opts.playList[player.playIndex];
      if (e.type == 'mouseleave' || e.type == 'pointerout') {
        if (player.timeInstance != null) {
          clearInterval(player.timeInstance);
          player.timeInstance = null;
        }

        if (!option.isLockToolbar) {
          player.timeInstance = setInterval(function () {
            var api = player.api;
            if (api.dragging) {
              return;
            }

            if (api.playing && api.ready) {
              if (player.opts['playingback']) {
                player.opts['playingback']();
              }

              player.find('.tool_bg').addClass('off');
              player.find('.tool_box').addClass('off');
              player.find('.tool_box .timeline').css('width', '');

              player.find('.header_bar').addClass('off');

              if (player.subTitle.isActiv) {
                player.subTitle.node.addClass('playing');
              }
            }
            clearInterval(player.timeInstance);
            player.timeInstance = null;
          }, player.timeSec);
        }
        player.find('.video_box').removeClass('is_over');
      } else {
        if (player.timeInstance != null) {
          clearInterval(player.timeInstance);
          player.timeInstance = null;
        }

        if (player.isFullscreen) {
          var timelineJq = player.find('.tool_box .timeline');
          timelineJq.css('width', timelineJq.attr('data-width') + 'px');
        }

        if (!option.isLockToolbar) {
          player.find('.video_box').addClass('is_over');
          player.find('.tool_bg').removeClass('off');
          player.find('.tool_box').removeClass('off');

          player.find('.header_bar').removeClass('off');
        }
        if (player.opts['pauseback']) {
          player.opts['pauseback']();
        }

        if (player.subTitle.isActiv) {
          player.subTitle.node.removeClass('playing');
        }
      }

      return false;
    });

    this.find('.timeline').on(eventDown, function (e) {
      if (!player.api.ready) {
        return false;
      }

      if (player.lastPlayTime < player.video.currentTime) {
        player.lastPlayTime = player.video.currentTime;
      }

      var slide = player.utils.slide;
      var delayedFire = slide.throttle(slide.fire, 30);
      var that = $(this);
      player.api.dragging = true;

      slide.calc(that);

      if (!slide.fire(slide.mousemove(e), player)) {
        player.api.dragging = false;
        that.find('.timeline').removeClass('dragging');
        return false;
      }

      that.addClass('dragging');

      that.parent().on(eventMove, function (e) {
        delayedFire(slide.mousemove(e), player, false);
        return false;
      });

      that.parent().on(eventUp, function () {
        slide.fire(slide.value, player, true);
        player.api.dragging = false;

        $(this).off(eventMove).off(eventUp);
        $(this).find('.timeline').removeClass('dragging');

        //   clearInterval(player.timeInstance);
        //   player.timeInstance = setInterval(player._setupPlayer.bind(player), player.timeSec);
        return false;
      });

      return false;
    });

    this.slideVolume = Object.assign({}, player.utils.slide);
    this.slideVolume.progress = this.find('.volume_progress');
    this.slideVolume.btn = this.find('.volume_progress_btn');

    this.find('.volumeline').on(eventDown, function (e) {
      if (!player.api.ready) {
        return false;
      }

      player.slideVolume.fire = function (val, player) {
        var w = (val * 100).toFixed(2);
        w = w < 4 ? 0 : w;
        player.slideVolume.progress.css({ width: w + '%' });
        player.video.volume = w / 100;

        if (w == 0) {
          player.slideVolume.btn.addClass('mute');
        } else {
          player.slideVolume.btn.removeClass('mute');
        }

        return true;
      };

      var delayedFire = player.slideVolume.throttle(
        player.slideVolume.fire,
        10
      );
      var that = $(this);
      player.api.vDragging = true;

      player.slideVolume.calc(that);
      var progressEv = e.target ? e.target : e.srcElement;

      if (
        progressEv.classList[0] != 'volume_progress_btn' &&
        !player.slideVolume.fire(player.slideVolume.mousemove(e), player)
      ) {
        player.api.vDragging = false;
        that.find('.volumeline').removeClass('dragging');
        return false;
      }

      that.addClass('dragging');

      that.parent().on(eventMove, function (e) {
        delayedFire(player.slideVolume.mousemove(e), player, false);
        return false;
      });

      that.parent().on(eventUp, function () {
        player.slideVolume.fire(player.slideVolume.value, player, true);
        player.api.vDragging = false;

        $(this).off(eventMove).off(eventUp);
        $(this).find('.volumeline').removeClass('dragging');

        // clearInterval(player.timeInstance);
        // player.timeInstance = setInterval(player._setupPlayer.bind(player), player.timeSec);
        return false;
      });

      return false;
    });

    if (this.opts.speed) {
      //1.0
      this.speed(this.opts.speed);
    }

    if (this.opts.volume) {
      //1.0
      this.slideVolume.progress.css({ width: this.opts.volume * 100 + '%' });
    }

    this.speedBox = this.find('.volume_speed_box');
    this.speedList = this.speedBox.find('.speed_list>li');
    this.find('.speed_item').on(this.clickType, function () {
      var that = $(this);
      if (that.hasClass('selected_volume')) {
        player.speedBox.addClass('on');
      } else {
        player.speed(that.attr('data-item'));
        player.speedBox.removeClass('on');
      }
      return false;
    });
  };

  VideoWrapper.prototype.play = function (callback) {
    try {
      if (!this.api.ready) {
        throw '로드할수 없습니다.\n잘못된 주소입니다.';
      }
      this.video.play();
      if (callback == 'pause') {
        this.playtimeInstance = window.setTimeout(
          function () {
            this.video.pause();
            window.clearTimeout(this.playtimeInstance);
            this.playtimeInstance = null;
          }.bind(this),
          500
        );
        return;
      }

      if (callback) {
        callback(this);
      }
    } catch (e) {
      console.log(e);
    }
  };

  VideoWrapper.prototype.fullscreen = function () {
    try {
      this.trigger('fullscreen');
    } catch (e) {
      console.log(e);
    }

    this.isFullscreen = true;
    // this.find('.fullscreen').addClass('on');
    this.find('.video_box').addClass('is_fullscreen');

    var w = $(document).width() - 536;
    this.find('.tool_box .timeline')
      .attr('data-width', w)
      .css('width', w + 'px');
  };

  VideoWrapper.prototype.pause = function (callback) {
    if (!this.api.paused) {
      this.video.pause();
    }

    if (callback) {
      callback();
    }
  };

  VideoWrapper.prototype.seek = function (time, callback) {
    if (Number(time) > -1) {
      this.api.seek(time, this);
      this._seekVideo();
    }

    if (callback) {
      callback();
    }
  };

  VideoWrapper.prototype.seekTo = function (position, isSeek) {
    if (position > -1) {
      var time = (position / 100) * this.video.duration;

      if (this.rightProgressDisable && this.lastPlayTime < time) {
        return false;
      }

      if (isSeek) {
        this.api.seek(time, this);
      }
      this._seek(time);
    }

    return true;
  };

  VideoWrapper.prototype.getDuration = function () {
    try {
      var dt = this.video.duration;
      if (isNaN(dt)) {
        return 0;
      } else {
        return dt;
      }
    } catch (e) {
      return 0;
    }
  };

  VideoWrapper.prototype.getCurrentTime = function () {
    try {
      return this.video.currentTime;
    } catch (e) {
      return 0;
    }
  };

  VideoWrapper.prototype.getUrl = function () {
    try {
      return this.video.src;
    } catch (e) {
      return 'none';
    }
  };

  VideoWrapper.prototype.mute = function (callback) {};

  VideoWrapper.prototype.volume = function (callback) {};

  VideoWrapper.prototype.speed = function (speed) {
    this.video.playbackRate = speed;
    var speeds = this.utils.speeds;
    var position = 0;
    for (var i = 0; i < speeds.length; i++) {
      if (speed >= speeds[i][0]) {
        position = i;
        break;
      }
    }

    this.speedList.removeClass('on').eq(position).addClass('on');
    this.find('.selected_volume').html(
      speeds[position][1].concat('<span>', speed, '×</span>')
    );
  };

  VideoWrapper.prototype.stop = function () {};

  VideoWrapper.prototype.unload = function (callback) {
    this._unload();
    if (this.video) {
      this.video.src = '';
    }

    this.html('');
    this.removeClass('play pause ready');

    if (callback) {
      callback();
    }

    return this;
  };

  VideoWrapper.prototype.disable = function (flag) {};

  $.fn.videoWrapper = function (opts = {}, callback) {
    if (typeof opts == 'string') {
      opts = {};
    }

    if ($.isFunction(opts)) {
      callback = opts;
      opts = {};
    }

    opts.callback = callback;
    opts = $.extend(
      {
        config: null,
        playList: null,
      },
      opts
    );

    //중복방지 필요없음
    //var vw = this.data('videoWrapper');
    // if (!vw) {
    // }

    $.fn.extend(this, new VideoWrapper());

    this.opts = opts;
    this.id = this.attr('id');
    this.clickType = $utils.clickType || 'click';
    this.init();

    return this;
  };
})(jQuery);

//자리수 채우기
String.prototype.padLeft = function (len, str) {
  return window.$utils.pad(this, len, str || '0');
};
Number.prototype.padLeft = function (len, str) {
  return window.$utils.pad(this.toString(), len, str || '0');
};

window.$utils = {
  clickType: /iPhone|iPad|iPod|Android/i.test(window.navigator.userAgent)
    ? 'pointerdown'
    : 'click',
  pad: function (strNum, len, jChar) {
    if (!strNum) {
      return '';
    }
    return strNum.length >= len
      ? strNum
      : new Array(len - strNum.length + 1).join(jChar) + strNum;
  },
};

/**
 * 영상 플레이어
 */
window.videoPlayer = {
  playerId: 'video_wrapper',
  classBoxName: 'video_wrapper',
  httpMovieURL: '',
  src: '',
  jqPlayer: null,
  isPlayStart: false, //첫재생 체크
  isFirstPlay: false,
  isCreatePlayer: false, //플레이어 초기화유무
  isFinish: false,
  isFullScreen: false,
  playTime: 0,
  api: null,
  rightProgressDisable: false,
  errorTimer: null,
  errorCnt: 0,
  create: function (playerId, classBoxName, src, opts) {
    if (playerId) {
      this.playerId = playerId;
    }

    if (classBoxName) {
      this.classBoxName = classBoxName;
    }

    this.src = function (src) {
      //영상주소
      var uri = Array.isArray(src) ? src[0] : src;
      if (src.indexOf('http') < 0) {
        uri = this.httpMovieURL.concat(uri);
      }
      return uri;
    }.bind(this)(src);

    var options = {};
    if (opts) {
      for (var k in opts) {
        //옵션 키이름 소문자 처리
        options[k.toLowerCase()] = opts[k];
      }
    }

    console.log(this.isCreatePlayer);

    if (!this.isCreatePlayer) {
      //메뉴이동시 플레이어제거후 다시 생성
      var wrapper =
        '<div id="' + this.playerId + '" class="video_player"></div>';
      var box = $('#' + this.playerId);

      if (box.get(0)) {
        //플레이어 제거
        box.remove();
      }

      $('.wrapper').append(wrapper);

      //{playList,config},init callback
      this.jqPlayer = $('#' + this.playerId).videoWrapper(
        {
          playList: [
            {
              src: this.src,
              type: 'video/mp4',
              title: '',
              autoplay: options.autoplay,
              preload: 'auto',
              style: '',
              isLockToolbar: false,
              rightProgressDisable: this.rightProgressDisable, //next disable
            },
          ],
          volume: 0.7,
          playingback: options.playingback, //재생
          pauseback: options.pauseback, //일시정지
          error: function () {
            if (
              this.errorCnt < 1 &&
              this.jqPlayer.getUrl().indexOf('https') > -1
            ) {
              this.remove();

              if (src.indexOf('http') > -1) {
                src = src.replace('https', 'http');
              } else {
                this.httpMovieURL = this.httpMovieURL.replace('https', 'http');
              }
              opts.autoplay = true;
              this.create(playerId, classBoxName, src, opts);
            } else {
              this.errorTimer = window.setTimeout(
                this.finished.bind(this, options),
                3000
              );

              return;
            }

            this.errorCnt++;
            this.jqPlayer.find('.replay').hide();
          }.bind(this),
          firstPlay: function () {}.bind(this),
        },
        function (player) {
          //init callback
          if (options.init) {
            options.init(player);
          }
        }
      );

      this.isPlayStart = false;

      this.jqPlayer.on(
        'play',
        function () {
          this.jqPlayer.find('.replay').hide();

          if (!this.isPlayStart) {
            //첫실행 유무
            // if (!this.isFirstPlay) {
            //     return;
            // }

            if (this.jqPlayer.playtimeInstance) {
              this.jqPlayer.removeClass('play').addClass('pause');
              return;
            }

            this.jqPlayer.isFirstPlay = true;
            this.isPlayStart = true; //첫실행 완료
          }
        }.bind(this)
      );

      this.jqPlayer.on('finish', this.finished.bind(this, options));

      //video load
      this.jqPlayer.load();
      this.isCreatePlayer = true;
    } else {
      this.show();
      //drag
      this.jqPlayer.rightProgressDisable = this.rightProgressDisable;
      this.jqPlayer.play();
    }
  },
  finished: function (options) {
    this.isFinish = true;
    this.rightProgressDisable = false;
    this.playTime = 0;
    //this.unload(false);

    this.isPlayStart = false;

    // this.jqPlayer.find('.replay').show();
    this.jqPlayer.find('.pause_box').remove();

    if (options.finish) {
      options.finish(this.jqPlayer);
    }
  },
  show: function () {
    $('.btn_replay').hide();
    this.jqPlayer.show();
  },
  unload: function (isPlayerUnload) {
    if (this.jqPlayer) {
      this.jqPlayer.pause();
      if (isPlayerUnload) {
        this.jqPlayer.unload();
      }
      this.jqPlayer.hide();
    }
  },
  replay: function () {
    this.jqPlayer.play();
  },
  remove: function () {
    if (this.jqPlayer && this.jqPlayer.get(0)) {
      if (
        this.isPlayStart &&
        this.jqPlayer != null &&
        this.jqPlayer.css('display') != 'none'
      ) {
        this.playTime = this.jqPlayer.video.currentTime;
      }

      this.unload(true);
      this.isCreatePlayer = false;
      this.isFullScreen = false;
      this.jqPlayer.playtimeInstance = null;
      this.jqPlayer.api.playing = false;
      this.jqPlayer.remove();
      this.jqPlayer = null;
    }
  },
  fullScreenExit: function () {
    //네이티브 백키 사용시 적용위해 추가
    try {
      document.webkitExitFullscreen();
      this.jqPlayer._fullscreenExit();
    } catch (e) {
      console.log(e);
    }
  },
};

/*
  window.videoPlayer.create('', '', 'https://ncdn.smart-wisecamp.com/vod/contents/52/3/5231E00015701/5231E00015701.mp4', {finish: function(){
          alert();
      }.bind(this), autoplay: true});
*/
