"use strict";

var PageData = {
	// 오디오
	narration :[
		"./audio/k_01.mp3", 
    ],
    
    subtitleData :
    [
        // 캐릭터 번호 object로 입력 가능. {ch:[1,6],ch_subtitle:6}
        // 0:Jayden, 1:Yuri, 2:Jiho, 3:Nora
        // 장면 1
        [
            3.302, 5.427, 0,
            "Good afternoon, Yuri.",
            "안녕, 유리.",
        ],
        [
            5.754, 7.768, 1,
            "Good afternoon, Jayden.",
            "안녕, 제이든.",
        ],
        [
            8.442, 11.130, 0,
            "What do you do on weekends?",
            "너는 주말마다 무엇을 하니?",
        ],
        [
            11.666, 14.142, 1,
            "I usually watch a movie.",
            "나는 보통 영화를 봐.",
        ],
        [
            14.438, 17.994, 0,
            "Oh, let’s watch a movie this Sunday.",
            "오, 이번주 일요일에 영화 보자.",
        ],
        // 장면 2
        [
            18.235, 19.925, 1,
            "Sounds good!",
            "좋은 생각이야!",
        ],
        [
            26.669, 28.230, 2,
            "Hi, Nora.",
            "안녕, 노라.",
        ],
        [
            28.470, 30.078, 3,
            "Hi, Jiho.",
            "안녕, 지호.",
        ],
        [
            30.309, 33.117, 2,
            "What do you do on Saturdays?",
            "너는 토요일마다 무엇을 하니?",
        ],
        [
            33.708, 35.639, 3,
            "I go to the park.",
            "나는 공원에 가.",
        ],
        [
            35.815, 37.829, 3,
            "I ride a bike there.",
            "나는 거기서 자전거를 타.",
        ],
        [
            38.826, 40.434, 2,
            "That’s great.",
            "정말 멋지다.",
        ],
        [
            40.674, 43.362, 3,
            "What do you do on Saturdays?",
            "너는 토요일마다 무엇을 하니?",
        ],
        [
            44.174, 46.151, 2,
            "I go to the farm.",
            "나는 농장에 가.",
        ],
        [
            46.474, 48.534, 2,
            "I grow vegetables.",
            "나는 채소를 길러.",
        ],
        
    ]
}


