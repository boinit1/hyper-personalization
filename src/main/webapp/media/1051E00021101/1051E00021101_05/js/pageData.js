"use strict";

var PageData = {
	// 나레이션 퀴즈1,2 타이틀
	na_mp3 : [
		"./audio/k_01_01.mp3",
		"./audio/k_02_01.mp3"
	],
	// 정오 효과음, 카드 성공 실패 효과음, 엔딩 효과음
	eff_mp3 : [
		"./audio/quiz_o.mp3",
		"./audio/quiz_x.mp3",
		"./audio/card_o.mp3",
		"./audio/card_x.mp3",
		"./audio/goodending.mp3",
		"./audio/badending.mp3",
		"./audio/good.mp3"
	],
	// 타이머 효과음
	timer_mp3 : "./audio/timer.mp3",
	// game-card
	card_word : ["Saturday","weekend","grow","farm","usually","movie"],
	card_word_ko : ["토요일","주말","기르다","농장","보통, 대개","영화"],
}


