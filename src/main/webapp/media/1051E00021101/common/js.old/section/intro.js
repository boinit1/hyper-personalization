//documentReady
$(function ()
{
	//intro-video 생성
	$("*[data-ui='intro-video']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).introVideo(option);
	});

});

/*
*	IntroVideo
*/
(function ($){
	'use strict';

	var IntroVideo = IntroVideo || (function ()
	{
		function initVideo()
		{
			this.videoControl = new VideoControl( this.element, this.options.source, {
				onUpdate:$.proxy(onUpdate, this),
				onFinish:$.proxy(onFinish, this)
			});

			this.start();
		}
		function onUpdate(video, per)
		{
			// console.log("per", per);
		}

		function onFinish(video)
		{
			// console.log("finish");
			video.currentTime = video.duration;
		}

		return IPattern.extend({

			init : function (element, options)
			{
				this._super(element, options);

				this.videoControl = null;
				initVideo.call(this);
			},

			start : function ()
			{
				this.videoControl.play();
			},

			dispose : function ()
			{
				
			}	
		});

	})();

	// 기본 옵션
	IntroVideo.DEFAULT = {source:""};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.introVideo');
            var options =  $.extend({}, IntroVideo.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.introVideo', (data = new IntroVideo($this, options)));
			if (typeof option == 'string') data[option](params);
			$this.data("quizType", 'introVideo');
        });
    }

	window.IntroVideo = IntroVideo;

    $.fn.introVideo = Plugin;
    $.fn.introVideo.Constructor = IntroVideo;

})(jQuery);

