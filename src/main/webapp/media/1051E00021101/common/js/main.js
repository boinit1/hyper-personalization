//documentReady
$(function ()
{
	//메인 생성
	$("*[data-ui='main']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).main(option);
	});
});


/*
*	Main
*/
(function ($){
	'use strict';

	var Main = Main || (function ()
	{

		function initMain()
		{
			var owner  = this;

			if($.getUrlVar("skip") != "Y" && this.options.introMovie)
			{
				this.introVideo = new VideoControl(this.element.find(".intro-movie"), this.options.introMovie, {onFinish:function ()
				{
					owner.introVideo.dispose();

					startSection.call(owner);
				}});
				this.introVideo.play();
			}
			else
			{
				owner.element.find(".intro-movie").remove();
				setTimeout(function ()
				{
					startSection.call(owner);
				}, 0);
			}

			this.element.on("stop_bgm",function(){
				owner.bgmAudio.stop();
			});
			this.element.on("start_bgm",function(){
				owner.bgmAudio.play();
			});
		}

		function setBgm()
		{
			var owner  = this;

			if(this.options.bgm)
			{
				this.bgmAudio = new AudioControl( this.options.bgm, {onFinish:function ()
				{
					owner.bgmAudio.play();
				}});
				this.bgmAudio.play();
			}
		}

		function setEvent()
		{
			var owner = this;

			this.clickAudio = new AudioControl( "../common/audio/click.mp3", {});
			this.element.on("click_sound",function(){
				owner.clickAudio.stop();
				owner.clickAudio.play();
			});

			this.element.on("studyFinish",function(){
				try
				{
					swcontobj.fnClkStudyFinish()
				}
				catch(e)
				{
					console.error(e);
				}
			});

			this.element.on("ccReady",function(){
				console.log("ccReady");
				initMain.call(owner);
			});
		}

		function startSection()
		{
			var owner = this;
			setBgm.call(owner);

			var ui = this.element.find(".section").data("ui");
			this.element.find(".section").eq(this.currentSection)[ui]("start");
		}

		/*
		function finish()
		{
			var owner = this;
			if(this.options.bgm)	owner.bgmAudio.stop();

			if(this.options.end)
			{
				this.endAudio = new AudioControl(this.options.end);
            	owner.endAudio.play();
			}
		}
		*/

		return Class.extend({

			init : function (element, options)
			{
				this.element = element;
				this.options = options;
				this.currentSection = 0;
				this.bgmAudio;
				this.endAudio;

				setEvent.call(this);
				if($("body").find("#animation_container").length == 0)
				{
					initMain.call(this);
				}
			}
		});

	})();

	// 메인 기본 옵션
	Main.DEFAULT = {bgm : "", end:""};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.main');
            var options =  $.extend({}, Main.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.main', (data = new Main($this, options)));
			if (typeof option == 'string') data[option](params);
        });
    }

	window.Main = Main;

    $.fn.main = Plugin;
    $.fn.main.Constructor = Main;

})(jQuery);

