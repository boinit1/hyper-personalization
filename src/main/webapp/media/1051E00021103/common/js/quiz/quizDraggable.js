//documentReady
$(function () {
    //quiz-counter 생성
    $("*[data-ui='quiz-draggable']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).quizDraggable(option);
    });
});

/*
 *	FrameAnimation
 */
(function ($) {
    "use strict";

    var QuizDraggable =
        QuizDraggable ||
        (function () {
            function setDraggable() {
                var owner = this;
                this.element.find(".drag-item").each(function (i) {
                    $(this).data("idx", i + 1);
                    $(this)
                        .find(".area")
                        .on(touchstart, $.proxy(onTouchStart, owner));
                });

                $(window).on(touchmove, $.proxy(onTouchMove, this));
                $(window).on(touchend, $.proxy(onTouchEnd, this));
            }

            function onTouchStart(e) {
                if (!this.isDrag) return;

                var pageX = e.pageX;
                var pageY = e.pageY;

                if (e.originalEvent.changedTouches) {
                    pageX = e.originalEvent.changedTouches[0].clientX;
                    pageY = e.originalEvent.changedTouches[0].clientY;
                }

                var target = $(e.currentTarget).parent();
                if (!target.data("area")) {
                    this.startX =
                        (target.position().left - pageX) / window.scale;
                    this.startY =
                        (target.position().top - pageY) / window.scale;
                    this.touchTarget = target;
                    this.touchTarget.css({ "z-index": 999 });
                }

                e.preventDefault();
                e.stopPropagation();
            }

            function onTouchMove(e) {
                if (this.touchTarget) {
                    var pageX = e.pageX;
                    var pageY = e.pageY;
                    if (e.originalEvent.changedTouches) {
                        pageX = e.originalEvent.changedTouches[0].clientX;
                        pageY = e.originalEvent.changedTouches[0].clientY;
                    }

                    var moveX = pageX / window.scale;
                    var moveY = pageY / window.scale;

                    TweenLite.set(this.touchTarget, {
                        x: moveX + this.startX,
                        y: moveY + this.startY,
                    });

                    e.preventDefault();
                    e.stopPropagation();
                }
            }

            function onTouchEnd(e) {
                if (this.touchTarget) {
                    this.touchTarget.css({ "z-index": "" });
                    var hitSelector = ".drag-area";
                    var hitTarget = null;

                    // hit 체크
                    var sx, sy, sw, sh, ex, ey, ew, eh;
                    for (
                        var i = 0;
                        i < this.element.find(hitSelector).length;
                        i++
                    ) {
                        var area = this.element.find(hitSelector).eq(i);

                        sx = area.position().left / window.scale;
                        sy = area.position().top / window.scale;
                        sw = area.width();
                        sh = area.height();
                        ex = this.touchTarget.position().left / window.scale;
                        ey = this.touchTarget.position().top / window.scale;
                        ew = this.touchTarget.width();
                        eh = this.touchTarget.height();

                        if (ex > sx - sw && ex < sx + sw) {
                            if (ey > sy - sh && ey < sy + sh) {
                                if (!this.options.multi) {
                                    if (!this.touchTarget.data("drag"))
                                        hitTarget = area;
                                } else {
                                    hitTarget = area;
                                }
                                break;
                            }
                        }
                    }

                    if (hitTarget) {
                        if (check.call(this, this.touchTarget, hitTarget)) {
                            gtag("event", "puzzle-correct", {
                                puzzle1: $(this.touchTarget).text().trim(),
                            });

                            this.touchTarget.data("area", hitTarget);
                            hitTarget.data("drag", this.touchTarget);
                            var moveX = sx;
                            var moveY = sy;
                            TweenLite.set(this.touchTarget, {
                                x: moveX,
                                y: moveY,
                            });

                            this.touchTarget.addClass("active");
                            this.element.trigger("quizAnswer", [
                                hitTarget.index(),
                                this.touchTarget.index(),
                            ]);

                            if (this.clearAudio) {
                                for (
                                    var i = 0;
                                    i < this.clearAudio.length;
                                    i++
                                ) {
                                    this.clearAudio[i].stop();
                                }
                                if (this.clearAudio.length == 1)
                                    this.clearAudio[0].play();
                                else this.clearAudio[hitTarget.index()].play();

                                this.isDrag = false;
                            }
                            if (this.checkAnswer()) {
                                this.dispose();
                            }
                        } else {
                            gtag("event", "puzzle-wrong", {
                                puzzle1: $(this.touchTarget).text().trim(),
                            });

                            if (this.options.clone) {
                                this.touchTarget.remove();
                            } else {
                                TweenLite.to(this.touchTarget, 0.6, {
                                    x: this.touchTarget.data("x"),
                                    y: this.touchTarget.data("y"),
                                    ease: Cubic.easeOut,
                                });
                            }

                            if (this.failedAudio) {
                                this.failedAudio.stop();
                                this.failedAudio.play();
                            }
                            this.element.trigger("quizWrong");
                        }
                    } else {
                        if (this.options.clone) {
                            this.touchTarget.remove();
                        } else {
                            TweenLite.to(this.touchTarget, 0.6, {
                                x: this.touchTarget.data("x"),
                                y: this.touchTarget.data("y"),
                                ease: Cubic.easeOut,
                            });
                        }
                    }

                    this.startX = this.startY = 0;
                    this.touchTarget = null;
                }
            }

            function cloneDrag(target) {
                var newTarget = target.clone();
                newTarget.addClass("clone");
                newTarget.data("idx", target.data("idx"));
                newTarget.on(this.touchstart, $.proxy(this.touchStart, this));
                target.parent().append(newTarget);
                return newTarget;
            }

            function check(target, area) {
                if (typeof this.options.answer[area.index()] === "number") {
                    if (
                        target.data("idx") == this.options.answer[area.index()]
                    ) {
                        return true;
                    }
                } else {
                    if (
                        $.inArray(
                            target.data("idx"),
                            this.options.answer[area.index()]
                        ) > -1
                    ) {
                        return true;
                    }
                }
                return false;
            }

            return IPattern.extend({
                init: function (element, options) {
                    this._super(element, options);
                    this.touchTarget = null;
                    this.startX = 0;
                    this.startY = 0;
                    this.clearAudio = null;
                    this.failedAudio = null;
                    this.isDrag = true;

                    var owner = this;
                    if (this.options.failedAudio)
                        this.failedAudio = new AudioControl(
                            this.options.failedAudio
                        );
                    if (this.options.clearAudio) {
                        this.clearAudio = new Array();
                        if (typeof this.options.clearAudio === "string") {
                            var audioControl = new AudioControl(
                                this.options.clearAudio,
                                {
                                    onFinish: function () {
                                        if (owner.checkAnswer()) {
                                            owner.element
                                                .trigger("quizClear")
                                                .removeClass("start")
                                                .addClass("finish");
                                        }
                                        owner.isDrag = true;
                                    },
                                }
                            );

                            this.clearAudio.push(audioControl);
                        } else {
                            for (
                                var i = 0;
                                i < this.options.clearAudio.length;
                                i++
                            ) {
                                var audioControl = new AudioControl(
                                    this.options.clearAudio[i],
                                    {
                                        onFinish: function () {
                                            if (owner.checkAnswer()) {
                                                owner.element
                                                    .trigger("quizClear")
                                                    .removeClass("start")
                                                    .addClass("finish");
                                            }
                                            owner.isDrag = true;
                                        },
                                    }
                                );

                                this.clearAudio.push(audioControl);
                            }
                        }
                    }
                },

                start: function () {
                    this._super();

                    this.element.find(".drag-item").each(function (i) {
                        var x = parseInt($(this).attr("x"));
                        var y = parseInt($(this).attr("y"));
                        TweenLite.set($(this), { x: x, y: y, force3D: true });
                        $(this)
                            .data("x", x)
                            .data("y", y)
                            .css({ top: 0, left: 0 });
                        $(this).data("idx", i);
                    });

                    setDraggable.call(this);
                },

                checkAnswer: function () {
                    var len = 0;
                    if (this.options.multi) {
                        for (var i = 0; i < this.options.answer.length; i++) {
                            if (typeof this.options.answer[i] === "object") {
                                for (
                                    var j = 0;
                                    j < this.options.answer[i].length;
                                    j++
                                ) {
                                    len++;
                                }
                            } else {
                                len++;
                            }
                        }
                    } else {
                        len = this.options.answer.length;
                    }
                    if (this.options.customLength)
                        len = this.options.customLength;
                    if (this.element.find(".drag-item.active").length == len) {
                        return true;
                    }

                    return false;
                },

                dispose: function () {
                    this.element
                        .find(".drag-item .area")
                        .off("touchstart mousedown");
                    $(window).off("touchmove mousemove");
                    $(window).off("touchend mouseup");
                },
            });
        })();

    // 메인 기본 옵션
    QuizDraggable.DEFAULT = {
        answer: [],
        clone: false,
        failedAudio: "",
        clearAudio: "",
        multi: false,
        customLength: null,
    };

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.quizDraggable");
            var options = $.extend(
                {},
                QuizDraggable.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data(
                    "ui.quizDraggable",
                    (data = new QuizDraggable($this, options))
                );
            if (typeof option == "string") data[option](params);
            $this.data("quizType", "quizDraggable");
        });
    }

    window.QuizDraggable = QuizDraggable;

    $.fn.quizDraggable = Plugin;
    $.fn.quizDraggable.Constructor = QuizDraggable;
})(jQuery);
