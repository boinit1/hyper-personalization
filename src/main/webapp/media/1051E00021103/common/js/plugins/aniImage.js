//documentReady
$(function ()
{
	//ani-image 생성
	$("*[data-ui='ani-image']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).aniImage(option);
	});

});

/**
 * 	json image animation
 */
(function ($){
	'use strict';

	var AniImage = AniImage || (function ()
	{
		function initAni ()
		{
		}

		function onUpdate()
		{
			this.frame = this.frame % this.frametotals + 1;

			this.element.css("background-image", 'url('+PageData[this.options.data].png[this.frame-1]+')');
		}

		return Class.extend({
			init : function (element, options)
			{
				this.element = element;
				this.options = options;
				this.callBack;
				this.framerate = null;
				this.bg = null;

				if(this.options.bgID)
				{
					this.bg = $(this.options.bgID);
				}

				if(this.options.data)
				{

					this.framerate = PageData[this.options.data].framerate;
					this.frame = 0;
					this.frametotals = PageData[this.options.data].png.length;
					this.paused = true;
					this.timer;

					initAni.call(this);
					this.element.hide();
				}
			},

			play : function()
			{
				if(this.framerate == null) return;

				this.timer = setInterval($.proxy(onUpdate, this), 1000/this.framerate);
				onUpdate.call(this);
				this.element.show();
				if(this.bg != null) this.bg.show();
			},

			stop : function()
			{
				if(this.framerate == null) return;
				
				clearInterval(this.timer);
				this.element.hide();
				if(this.bg != null) this.bg.hide();
			}
		});
	})();

	// 기본 옵션
	AniImage.DEFAULT = {framerate:30};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.aniImage');
            var options =  $.extend({}, AniImage.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.aniImage', (data = new AniImage($this, options)));
			if (typeof option == 'string') data[option](params);
			$this.data("quizType", 'aniImage');
        });
    }

    $.fn.aniImage = Plugin;
    $.fn.aniImage.Constructor = AniImage;

	window.AniImage = AniImage;
})(jQuery);