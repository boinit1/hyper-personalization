//documentReady
$(function () {
    //reading 생성
    $("*[data-ui='reading']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).reading(option);
    });
});

/*
 *	Reading
 */
(function ($) {
    "use strict";

    var Reading =
        Reading ||
        (function () {
            function initCont() {
                var owner = this;

                setEvent.call(this);
                setAudio.call(this);
            }

            function setAudio() {
                var owner = this;

                if (this.options.introAudio)
                    this.introAudio = new AudioControl(
                        this.options.introAudio,
                        {}
                    );

                $(owner.introAudio.audio).on("loadeddata", function () {
                    owner.introAudioLoaded = true;
                });
                owner.introAudio.audio.load();

                var audio;
                /*
			for(var i = 0; i < PageData.narration.length; i++)
			{
				audio = new AudioControl(PageData.narration[i], {onFinish:function(audioDom){
				}});
				this.audioList.push(audio);
			}
			*/
            }

            function playAudio(audio) {
                if (this.nowAudio != null && this.nowAudio != audio) {
                    this.nowAudio.stop();
                }

                if (audio == null) return;

                this.nowAudio = audio;
                this.nowAudio.play();

                gtag("event", "audio-play", {
                    id: audio.source,
                    timer: audio.timer,
                });
            }

            function setEvent() {
                var owner = this;

                // 완강 체크;
                this.element.on("checkClear", function () {
                    if (
                        owner.element.find("#reading_cont").data("clear") &&
                        owner.element.find("#quiz_cont").data("clear")
                    ) {
                        console.log("완강 처리 ");
                        // $(".main").trigger("studyFinish");
                    }
                });

                this.element.find("#reading_cont").on("goQuiz", function () {
                    // console.log("퀴즈로 이동하기");
                    owner.currentGame++;
                    owner.element.find("#reading_cont").addClass("hide");
                    owner.element.find("#quiz_cont").removeClass("hide");
                    var quizType = owner.element
                        .find("#quiz_cont")
                        .data("quizType");
                    owner.element.find("#reading_cont")[quizType]("start");
                    exportRoot.goodMc.visible = true;
                });

                this.element.find("#quiz_cont").on("goReading", function () {
                    // console.log("리딩으로 이동");
                    owner.currentGame--;
                    owner.element.find("#quiz_cont").addClass("hide");
                    owner.element.find("#reading_cont").removeClass("hide");
                    // owner.element.find("#reading_cont").readingContent("start");
                    owner.element
                        .find("#reading_cont")
                        .readingContent("restart");
                    exportRoot.icon1.visible = false;
                    exportRoot.icon2.visible = false;
                    exportRoot.btn_next.visible = false;
                    exportRoot.goodMc.visible = false;
                });
            }

            function startReading() {
                var owner = this;

                // 1. 인트로
                if (this.options.skipIntro != "Y") {
                    // console.log("audio ready: ", owner.introAudio.audio.readyState);
                    if (owner.introAudio.audio.readyState == 4) {
                        exportRoot.intro.gotoAndPlay(1);
                        exportRoot.intro.visible = true;
                        owner.introAudio.play();
                    } else {
                        // console.log(navigator.userAgent);
                        // ie에서 오디로 load 명령이 안 먹힌다. play 한번 시켜주자.
                        owner.introAudio.play();

                        owner.interval = setInterval(function () {
                            // console.log("LLLL", owner.introAudioLoaded);
                            if (owner.introAudioLoaded) {
                                exportRoot.intro.gotoAndPlay(1);
                                exportRoot.intro.visible = true;
                                clearInterval(owner.interval);
                            }
                        }, 1000 / 30);
                    }
                    exportRoot.intro.onFinish = function () {
                        console.log("인트로 끝");
                        exportRoot.intro.visible = false;
                        owner.element.find("#reading_cont").removeClass("hide");
                        owner.element
                            .find("#reading_cont")
                            .readingContent("start");
                    };
                } else {
                    // 2. read
                    owner.element.find("#reading_cont").removeClass("hide");
                    owner.element.find("#reading_cont").readingContent("start");

                    // 3. quiz
                    // owner.element.find("#quiz_cont").removeClass("hide");
                    // var quizType = owner.element.find("#quiz_cont").data("quizType");
                    // owner.element.find("#reading_cont")[quizType]("start");
                }
            }

            return ISection.extend({
                init: function (element, options) {
                    this._super(element, options);
                    this.currentGame = 0;
                    this.audioList = [];
                    this.nowAudio = null;
                    this.introAudio = null;
                    this.introAudioLoaded = false;

                    initCont.call(this);
                },

                start: function () {
                    startReading.call(this);
                },

                reset: function () {
                    this.nowAudio = null;
                },

                dispose: function () {},
            });
        })();

    // 기본 옵션
    Reading.DEFAULT = {};

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.reading");
            var options = $.extend(
                {},
                Reading.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data("ui.reading", (data = new Reading($this, options)));
            if (typeof option == "string") data[option](params);
        });
    }

    window.Reading = Reading;

    $.fn.reading = Plugin;
    $.fn.reading.Constructor = Reading;
})(jQuery);

var exportRoot;
