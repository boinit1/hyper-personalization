//documentReady
$(function () {
    //writing 생성
    $("*[data-ui='writing']").each(function (i) {
        var option = $(this).attr("data-option")
            ? $.parseJSON($(this).attr("data-option"))
            : {};
        $(this).writing(option);
    });
});

/*
 *	Writing
 */
(function ($) {
    "use strict";

    var Writing =
        Writing ||
        (function () {
            function initCont() {
                var owner = this;

                setEvent.call(this);
                setAudio.call(this);
            }

            function setAudio() {
                var owner = this;

                if (this.options.introAudio)
                    this.introAudio = new AudioControl(
                        this.options.introAudio,
                        {}
                    );

                var audio;
                /*
			for(var i = 0; i < PageData.narration.length; i++)
			{
				audio = new AudioControl(PageData.narration[i], {onFinish:function(audioDom){
				}});
				this.audioList.push(audio);
			}
			*/
            }

            function playAudio(audio) {
                if (this.nowAudio != null && this.nowAudio != audio) {
                    this.nowAudio.stop();
                }

                if (audio == null) return;

                this.nowAudio = audio;
                this.nowAudio.play();

                gtag("event", "audio-play", {
                    id: audio.source,
                    timer: audio.timer,
                });
            }

            function setEvent() {
                var owner = this;

                // 완강 체크;
                this.element.on("checkClear", function () {
                    if (owner.element.find("#write_cont").data("clear")) {
                        console.log("완강 처리 ");
                        gtag('event', 'writing', {
                            text: owner.element.find("#write_cont .text-box")[0].innerText
                        })
                        $(".main").trigger("studyFinish");
                    }
                });

                this.element.find("#reading_cont").on("goQuiz", function () {
                    // console.log("퀴즈로 이동하기");
                    owner.currentGame++;
                    // owner.element.find("#reading_cont").addClass("hide");
                    owner.element.find("#write_cont").removeClass("hide");
                    owner.element.find("#write_cont").writingQuiz("start");
                });

                this.element.find("#write_cont").on("goReading", function () {
                    // console.log("리딩으로 이동");
                    owner.currentGame--;
                    owner.element.find("#write_cont").addClass("hide");
                    // owner.element.find("#reading_cont").removeClass("hide");
                    // owner.element.find("#reading_cont").readingContent("restart_write");
                    // exportRoot.icon1.visible = false;
                    // exportRoot.icon2.visible = false;
                });
            }

            function startWriting() {
                var owner = this;

                owner.element.find("#write_cont").removeClass("hide");
                owner.element.find("#write_cont").writingQuiz("start");
            }

            return ISection.extend({
                init: function (element, options) {
                    this._super(element, options);
                    this.currentGame = 0;
                    this.audioList = [];
                    this.nowAudio = null;
                    this.introAudio = null;

                    initCont.call(this);
                },

                start: function () {
                    startWriting.call(this);
                },

                reset: function () {
                    this.nowAudio = null;
                },

                dispose: function () {},
            });
        })();

    // 기본 옵션
    Writing.DEFAULT = {};

    function Plugin(option, params) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data("ui.writing");
            var options = $.extend(
                {},
                Writing.DEFAULT,
                typeof option == "object" && option
            );
            if (!data)
                $this.data("ui.writing", (data = new Writing($this, options)));
            if (typeof option == "string") data[option](params);
        });
    }

    window.Writing = Writing;

    $.fn.writing = Plugin;
    $.fn.writing.Constructor = Writing;
})(jQuery);
