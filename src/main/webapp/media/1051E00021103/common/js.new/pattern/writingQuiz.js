//documentReady
$(function ()
{
	//writing-quiz 생성
	$("*[data-ui='writing-quiz']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).writingQuiz(option);
	});

});

/*
*	WritingQuiz
*/
(function ($){
	'use strict';

	var WritingQuiz = WritingQuiz || (function ()
	{
		function initcont()
		{
			var owner = this;

			this.element.find(".guide-effect").css("z-index",4);
			this.element.find(".btn-reading").on("click", function(){
				// owner.reset();
				// owner.element.trigger("goReading");
				owner.element.find(".popup-guide").removeClass("hide");
				exportRoot.popMc.gotoAndStop(1);
			});

			this.element.find(".popup-guide .btn-close").on("click", function(){
				owner.element.find(".popup-guide").addClass("hide");
				exportRoot.popMc.gotoAndStop(0);
			});

			this.element.find(".btn-finish").on("click", function(){
				if(owner.clear) return;
				$(this).addClass("disable");
				goFinish.call(owner);
				owner.checkAnswer();
			});

			this.element.find(".textarea.input").on("keyup",function(e){
				var str = $(this).val();
				var str_arr = str.split("\n");  // 줄바꿈 기준으로 나눔 
				var rows = str_arr.length;  // row = 줄 수 
				var maxRows = 8; 
				/*
				for(var i = 0; i < str_arr.length; i++)
				{
					// 한 줄에 55자 이상 작성하면 2줄로 인식해라.
					if(str_arr[i].length > 55)
					{
						rows++;
					} 
				}
				*/
				if(rows > maxRows)
				{
					// var lastChar = str.slice(0,-1); //열 
					// $(this).val(lastChar);
					var modifiedText = $(this).val().split("\n").slice(0, maxRows);
 					$(this).val(modifiedText.join("\n"));
				}
			});
		}

		function goFinish()
		{
			var owner = this;

			this.finishAudio.play();

			this.element.find(".textarea.input").blur();
			var str = this.element.find(".textarea.input").val();
			var str_arr = str.split("\n");
			var outputText = str.replace(/\n/gi, "<br/>");
			outputText = outputText.replace(/\s/gi, "&nbsp;");
			// console.log(outputText);
			
			this.element.find(".textarea.output").html(outputText);
			this.element.find(".textarea.input").attr("disabled","disabled");
			this.element.find(".textarea.input").hide();

			this.element.find(".text-box .text-bg").show();
			TweenLite.set(this.element.find(".text-box .text-bg"), {opacity:0, ease:Sine.easeOut});
			TweenLite.to(this.element.find(".text-box .text-bg"), 0.5, {opacity:1, ease:Sine.easeOut, onComplete:function(){
				if(owner.options.saveImage == "Y") saveImage.call(owner);
				else goFinish2.call(owner);
			}});
		}

		function goFinish2()
		{
			var textBox = this.element.find(".text-box");
			if(this.element.find(".text-box").length > 1)
			{
				textBox = this.element.find(".text-box").eq(0);
			}

			textBox.css("z-index",2);
			TweenLite.to(textBox, 1, {delay:0, scale:0.75, rotation:4, ease:Back.easeInOut});
			TweenLite.set(this.element.find(".btn-finish"), {delay:0, opacity:0, ease:Sine.easeOut});

			TweenLite.set(this.element.find(".stamp"), {scale:3});
			TweenLite.to(this.element.find(".stamp"), 0.5, {delay:1, autoAlpha:1, ease:Bounce.easeOut});			
			TweenLite.to(this.element.find(".stamp"), 0.5, {delay:1, scale:1, ease:Bounce.easeOut});
		}

		function saveImage()
		{
			var owner = this;
			var target = (this.element.find(".text-box.clone").length > 0) ? this.element.find(".text-box.clone") : this.element.find(".text-box");
            html2canvas(target, {
                onrendered: function(canvas) {
					var src = canvas.toDataURL("image/jpeg");
					var filename = "save.jpg";
					// var src = canvas.toDataURL("image/png");
					// var filename = "save.png";
					src = src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
					$('<a href="' + src + '" download="'+ filename +'">Download</a>')[0].click()
					// console.log(src);

					goFinish2.call(owner);
				}
			});
		}

		function quizStart()
		{
			var owner = this;
			console.log("quizStart");
			if(!owner.finish) this.element.find(".textarea.input").eq(0).focus();

			if(!this.naOnce)
			{
				this.nowAudio = this.naAudio;
				this.nowAudio.play();
				this.naOnce = true;
			}
			
		}

		return IPattern.extend({

			init : function (element, options)
			{
				this._super(element, options);
				this.finishAudio = null;
				this.nowAudio = null;
				this.timer = null;
				this.clear = false;
				this.naAudio = null;
				this.naOnce = false;
				this.effStartTime = PageData.eff_start_time;


				var owner = this;
				if(this.options.finishAudio) this.finishAudio = new AudioControl(this.options.finishAudio);
				this.naAudio = new AudioControl(PageData.quiz_mp3[0], {
					onUpdate:function(audio){
						if(audio.currentTime > owner.effStartTime)
						{
							if(!owner.element.find(".guide-effect").is(".on"))
							{
								owner.element.find(".guide-effect").addClass("on");
							}
						}
					}, 
					onFinish:function(){
						owner.element.find(".guide-effect").removeClass("on");
					}
				});

				initcont.call(this);
			},

			start : function ()
			{
				exportRoot.icon2.visible = true;
				quizStart.call(this);
			},
			restart : function ()
			{
				exportRoot.icon2.visible = true;
			},

			checkAnswer : function ()
			{
				if(!this.clear)
				{
					this.clear = true;
					this.element.data("clear", true);
					$(".section").trigger("checkClear");
				}
			},

			reset : function()
			{
			},

			dispose : function ()
			{
			}
		});

	})();

	// 메인 기본 옵션
	WritingQuiz.DEFAULT = { finishAudio:""};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.writingQuiz');
            var options =  $.extend({}, WritingQuiz.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.writingQuiz', (data = new WritingQuiz($this, options)));
			if (typeof option == 'string') data[option](params);
			$this.data("quizType", 'writingQuiz');
        });
    }

	window.WritingQuiz = WritingQuiz;

    $.fn.writingQuiz = Plugin;
    $.fn.writingQuiz.Constructor = WritingQuiz;

})(jQuery);

