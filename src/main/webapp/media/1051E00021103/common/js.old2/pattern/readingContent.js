//documentReady
$(function ()
{
	//reading-content 생성
	$("*[data-ui='reading-content']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).readingContent(option);
	});

});

/*
*	ReadingContent
*/
(function ($){
	'use strict';

	var ReadingContent = ReadingContent || (function ()
	{
		function initCont()
		{
			var owner = this;

			setAudio.call(this);
			setBtn.call(this);
		}

		function setAudio()
		{
			var owner = this;
			var audio;
			
			// 사운드
			for(var i = 0; i < PageData.scene.length; i++)
			{
				var data = PageData.scene[i];
				audio = new AudioControl(data.na_mp3, {onFinish:$.proxy(onFinishNa, owner)});
				this.naAudioList.push(audio);
				this.readAudioList[i] = [];
				for(var j = 0; j < data.read_mp3.length; j++)
				{
					audio = new AudioControl(data.read_mp3[j], {onFinish:$.proxy(onFinishCheckRead, owner)});
					this.readAudioList[i].push(audio);
				}
			}
		}

		function setBtn()
		{
			var owner = this;

			if(!this.element.data("setBtn"))
			{
				this.element.data("setBtn", true);

				this.element.find(".btn-sound").on("click", $.proxy(onClickSound, owner));
				this.element.find(".btn-kor").on("click", $.proxy(onClickKor, owner));
				this.element.find(".btn-close").on("click", $.proxy(onClickNext, owner));
				this.element.find(".btn-next").on("click", $.proxy(onClickNext, owner));
				this.element.find(".btn-tab").on("click", function(){
					if($(this).is(".disable")) return;
					onClickTab.call(owner, $(this).index());
				});

				this.element.find(".scene").each(function(i){
					$(this).find(".text .text-item").each(function(j){
						$(this).data("idx", j);
					});
					$(this).find(".text .text-item").on("click",function(){
						if(owner.isNarration) 
						{
							naStop.call(owner);
							sceneStart.call(owner);
						}
						
						if(owner.isReading == true)
						{
							stopRead.call(owner);
						}
						startReadOnce.call(owner, i, $(this).data("idx"));
					});
				});
			}
		}

		function firstStart()
		{
			var owner = this;

			naStart.call(this);
		}

		function naStart()
		{
			var owner = this;

			clearInterval(this.timerBgImage);

			this.currentSceneEle = this.element.find(".scene").eq(this.currentScene);
			this.currentSceneEle.removeClass("hide");
			this.currentSceneEle.find(".bg_image .image").addClass("hide");
			this.currentSceneEle.find(".bg_image .image").eq(0).removeClass("hide");

			var len = this.element.find(".scene").length;
			if(this.currentScene == len-1 && this.element.find(".btn-close").css("display") == "none")
			{
				exportRoot.btn_next.visible = true;
				this.element.find(".btn-next").removeClass("disable");
			}
			else
			{
				exportRoot.btn_next.visible = false;
				this.element.find(".btn-next").addClass("disable");
			}

			// console.log("첫 진입 나레이션 시작");
			if(this.nowAudio) this.nowAudio.stop();
			this.nowAudio = this.naAudioList[this.currentScene];
			if(this.nowAudio.source == "undefined" || this.nowAudio.source == undefined || this.nowAudio.source == null)
			{
				this.nowAudio = null;
				sceneStart.call(this);
			}
			else
			{
				this.nowAudio.play();
				this.currentSceneEle.find(".ani-image").aniImage("play");
				// $(".main").addClass("disable");
				this.isNarration = this;
			}
		}

		function naStop()
		{
			if(this.nowAudio) this.nowAudio.stop();
			this.currentSceneEle.find(".ani-image").aniImage("stop");
			this.isNarration = false;
		}

		function onFinishNa()
		{
			var owner = this;

			// console.log("나레이션 완료");
			this.currentSceneEle.find(".ani-image").aniImage("stop");
			this.nowAudio = null;
			this.isNarration = false;
			
			this.element.find(".eff-sound").addClass("ani-eff-sound");
			sceneStart.call(owner);
			// $(".main").removeClass("disable");
		}

		function sceneStart()
		{
			var owner = this;
			
			this.currentSceneEle = this.element.find(".scene").eq(this.currentScene);
			this.currentSceneEle.removeClass("hide");
			this.currentSceneEle.find(".bg_image .image").addClass("hide");
			this.currentSceneEle.find(".bg_image .image").eq(0).removeClass("hide");

			var len = this.element.find(".scene").length;
			if(this.currentScene == len-1 && this.element.find(".btn-close").css("display") == "none")
			{
				exportRoot.btn_next.visible = true;
				this.element.find(".btn-next").removeClass("disable");
			}
			else
			{
				exportRoot.btn_next.visible = false;
				this.element.find(".btn-next").addClass("disable");
			}

			if(this.currentSceneEle.find(".bg_image .image").length > 1)
			{
				this.timerBgImage = setInterval($.proxy(bgStart, owner, owner.currentSceneEle.find(".bg_image")), 1000/0.8);
			}
		}

		function sceneStop()
		{
			var owner = this;

			stopRead.call(this);
			clearInterval(this.timerBgImage);
		}

		function sceneReset()
		{
			var owner = this;

			var btn = this.element.find(".btn-kor");
			btn.addClass("on");
			onClickKor.call(this);

			sceneStop.call(this);
			this.currentSceneEle.addClass("hide");
		}

		function bgStart(bgImage)
		{
			var len = bgImage.find(".image").length;
			if(len <= 1) return;
			
			var now = bgImage.find(".image").not(".hide").index();
			var next = (now+1)%len;
			bgImage.find(".image").eq(now).addClass("hide");
			bgImage.find(".image").eq(next).removeClass("hide");
		}

		// 버튼 이벤트
		function onClickKor()
		{
			var owner = this;

			var btn = this.element.find(".btn-kor");
			if(btn.is(".on"))
			{
				btn.removeClass("on");
				this.currentSceneEle.find(".text .ko").hide();
			}
			else
			{
				btn.addClass("on");
				this.currentSceneEle.find(".text .ko").show();
			}
		}

		function onClickSound()
		{
			var owner = this;

			if(this.isNarration) 
			{
				naStop.call(this);
				sceneStart.call(this);
			}
			
			this.element.find(".eff-sound").removeClass("ani-eff-sound");

			var btn = this.element.find(".btn-sound");
			if(btn.is(".on"))
			{
				btn.removeClass("on");
				stopRead.call(owner);
			}
			else
			{
				btn.addClass("on");
				startRead.call(owner);
			}
		}

		function onClickTab(idx)
		{
			var owner = this;

			if(this.isNarration) naStop.call(this);

			var tab = this.element.find(".tab");
			if(tab.is(".on")) tab.removeClass("on");
			else tab.addClass("on");
			this.element.find(".btn-tab").removeClass("disable");
			this.element.find(".btn-tab").eq(idx).addClass("disable");
			
			sceneReset.call(this);
			this.currentScene = idx;
			
			if(this.element.find(".btn-close").css("display") == "none") naStart.call(this);
			else sceneStart.call(this);
			
			this.element.find(".eff-sound").removeClass("ani-eff-sound");
		}

		function onClickNext()
		{
			var owner = this;

			if(this.isNarration) naStop.call(this);

			this.stop();
			this.element.trigger("goQuiz");

			this.element.find(".eff-sound").removeClass("ani-eff-sound");
		}

		function startRead()
		{
			var owner = this;

			if(this.nowAudio) this.nowAudio.stop();

			this.isReading = true;
			this.nowAudio = this.readAudioList[this.currentScene][this.currentLine];
			this.nowAudio.play();
			this.currentSceneEle.find(".text .text-item").removeClass("active");
			this.currentSceneEle.find(".text .text-item").eq(this.currentLine).addClass("active");
		}

		function startReadOnce(sceneIndex, lineIndex)
		{
			var owner = this;

			if(this.nowAudio) this.nowAudio.stop();

			this.nowAudio = this.readAudioList[sceneIndex][lineIndex];
			this.nowAudio.play();
			this.currentSceneEle.find(".text .text-item").removeClass("active");
			this.currentSceneEle.find(".text .text-item").eq(lineIndex).addClass("active");
		}

		function stopRead()
		{
			var owner = this;

			this.isReading = false;
			this.currentSceneEle.find(".text .text-item").removeClass("active");
			if(this.nowAudio) this.nowAudio.stop();

			this.element.find(".btn-sound").removeClass("on");
			this.currentLine = 0;
		}

		function onFinishCheckRead()
		{
			var owner = this;

			this.nowAudio = null;
			this.currentLine++;

			if(this.currentLine >= this.readAudioList[this.currentScene].length)
			{
				stopRead.call(this);

				this.clearScene[this.currentScene] = true;
				this.checkEnd();
			}
			else
			{
				this.currentSceneEle.find(".text .text-item").removeClass("active");
				this.timer = setTimeout(function(){
					if(owner.isReading)
					{
						startRead.call(owner);
					}
				}, 1000);
			}
		}

		return IPattern.extend({

			init : function (element, options)
			{
				this._super(element, options);
				this.nowAudio = null;
				this.naAudioList = [];
				this.readAudioList = [];
				this.timer = null;
				this.timerBgImage = null;
				
				this.currentScene = 0;
				this.currentSceneEle = null;
				this.currentLine = 0;
				this.isReading = false;
				this.clearScene = [];
				this.clear = false;
				this.isNarration = false;

				initCont.call(this);
			},

			start : function ()
			{
				var owner = this;

				setTimeout(function(){
					exportRoot.btn_next.gotoAndStop(0);
				},1000/30);
				
				this.currentSceneEle = this.element.find(".scene").eq(this.currentScene);
				this.currentSceneEle.removeClass("hide");

				this.timer = setTimeout(function(){
					firstStart.call(owner);
				}, 500);
			},

			restart : function ()
			{
				console.log("restart");
				this.currentScene = 0;
				this.currentSceneEle = this.element.find(".scene").eq(this.currentScene);
				this.currentSceneEle.removeClass("hide");
				
				exportRoot.btn_next.gotoAndStop(0);
				exportRoot.btn_next.visible = false;
				this.element.find(".btn-next").hide();
				this.element.find(".btn-close").show();


				var tab = this.element.find(".tab");
				tab.removeClass("on");
				this.element.find(".btn-tab").addClass("disable");
				this.element.find(".btn-tab").eq(1).removeClass("disable");
	
				sceneStart.call(this);
			},

			restart_write : function ()
			{
				setBtn.call(this);
				
				this.element.find(".btn-close").show();
				sceneStart.call(this);
			},

			stop : function()
			{
				sceneReset.call(this);
				exportRoot.btn_next.visible = false;
			},

			checkEnd : function ()
			{
				var count = 0;
				var len = this.element.find(".scene").length;
				if(this.currentScene == len-1)
				{
					exportRoot.btn_next.gotoAndPlay(1);
				}
				for(var i = 0; i < len; i++)
				{
					if(this.clearScene[i]) count++;
				}
				if(count == len) 
				{
					if(!this.clear)
					{
						this.clear = true;
						this.element.data("clear", true);
						$(".section").trigger("checkClear");
					}
				}
			},

			dispose : function ()
			{
			}
		});

	})();

	// 기본 옵션
	ReadingContent.DEFAULT = {};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.readingContent');
            var options =  $.extend({}, ReadingContent.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.readingContent', (data = new ReadingContent($this, options)));
			if (typeof option == 'string') data[option](params);
			$this.data("quizType", 'readingContent');
        });
    }

	window.ReadingContent = ReadingContent;

    $.fn.readingContent = Plugin;
    $.fn.readingContent.Constructor = ReadingContent;

})(jQuery);

