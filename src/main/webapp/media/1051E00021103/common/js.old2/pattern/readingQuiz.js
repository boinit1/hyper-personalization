//documentReady
$(function ()
{
	//reading-quiz 생성
	$("*[data-ui='reading-quiz']").each(function ( i )
	{
		var option = $(this).attr("data-option") ? $.parseJSON($(this).attr("data-option")) : {};
		$(this).readingQuiz(option);
	});

});

/*
*	ReadingQuiz
*/
(function ($){
	'use strict';

	var ReadingQuiz = ReadingQuiz || (function ()
	{
		function initcont()
		{
			var owner = this;

			this.element.find(".btn-reading").on("click", function(){
				owner.reset();
				owner.element.trigger("goReading");
			});

			// 퀴즈1 TF 체크 유형
			this.element.find(".checkbox").on("click", function(){
				if($(this).parent().is(".active")) return;

				var quizLine = $(this).parents().find(".quiz-line").has(this);
				var right = check.call(owner, quizLine.index(), $(this).index());
				if(right)
				{
					owner.clearAudio.stop();
					owner.clearAudio.play();
					$(this).parent().addClass("active");
					$(this).parent().parent().addClass("clear");
					$(this).addClass("on");
					showOX.call(owner, true);
				}
				else
				{
					owner.failedAudio.stop();
					owner.failedAudio.play();
					showOX.call(owner, false);
				}
			});

			// 퀴즈 2 드래그 유형
			if(this.element.find("#quiz-drag").length > 0)
			{
				this.quizType = "drag";
				console.log(this.quizType, "유형");
				this.element.find("#quiz-drag").on("quizClear", function(){
					// showOX.call(owner, true);
				});
				this.element.find("#quiz-drag").on("quizAnswer", function(){
					showOX.call(owner, true);
				});
				this.element.find("#quiz-drag").on("quizWrong", function(){
					showOX.call(owner, false);
				});
				this.element.find("#quiz-drag").quizDraggable("start");
			}
		}

		function quizStart()
		{
			// console.log("quizStart");
			if(!this.naOnce)
			{
				this.nowAudio = this.naAudio;
				this.nowAudio.play();
				this.naOnce = true;
			}
		}

		function showOX(value)
		{
			var owner = this;

			this.element.find(".ox").show();
			this.element.find(".ox .image").hide();
			var ele = (value) ? this.element.find(".ox .o") : this.element.find(".ox .x");
			ele.show();
			TweenLite.set(ele, {opacity:0, ease:Sine.easeOut});
			TweenLite.to(ele, 0.5, {opacity:1, ease:Sine.easeOut});

			var delay = (value) ? 1000 : 1500;
			this.timer = setTimeout(function(){
				owner.element.find(".ox").hide();
			}, delay);

			if(value)
			{
				owner.checkAnswer();
			}
		}


		function check(ans_idx, tf_idx)
		{
			var owner = this;
			if(tf_idx+1 == this.options.answer[ans_idx]) return true;

			return false;
		}
		return IPattern.extend({

			init : function (element, options)
			{
				this._super(element, options);
				this.clearAudio = null;
				this.failedAudio = null;
				this.finishAudio = null;
				this.goodAudio = null;
				this.nowAudio = null;
				this.timer = null;
				this.clear = false;
				this.naAudio = null;
				this.naOnce = false;
				this.quizType = "TF"; // "TF", "drag"

				var owner = this;
				if(this.options.failedAudio) this.failedAudio = new AudioControl(this.options.failedAudio);
				if(this.options.clearAudio) this.clearAudio = new AudioControl(this.options.clearAudio);
				if(this.options.finishAudio) this.finishAudio = new AudioControl(this.options.finishAudio);
				if(this.options.goodAudio) this.goodAudio = new AudioControl(this.options.goodAudio);
				this.naAudio = new AudioControl(PageData.quiz_mp3[0]);

				initcont.call(this);
			},

			start : function ()
			{
				exportRoot.icon1.visible = true;
				quizStart.call(this);
			},

			checkAnswer : function ()
			{
				var owner = this;

				var count = this.element.find(".check-cont.active").length;
				var len = this.element.find(".check-cont").length;
				if(this.quizType == "drag") {
					count = this.element.find(".drag-item.active").length;
					len = this.element.find(".drag-item").length;
				}

				if(count == len)
				{
					console.log("완강 처리 ");
					$(".main").trigger("studyFinish");

					if(!this.clear)
					{
						this.clear = true;
						this.element.data("clear", true);
						$(".section").trigger("checkClear");
					}

					this.timer = setTimeout(function(){
						owner.goodAudio.play();
						exportRoot.goodMc.gotoAndStop(1);
						owner.element.find(".bg-white.good").show();
						setTimeout(function(){
							owner.finishAudio.play();
						},500);
					}, 1000)
				}
			},

			reset : function()
			{
				clearTimeout(this.timer);
				this.naAudio.stop();

				// this.element.find(".check-cont").removeClass("active");
				// this.element.find(".checkbox").removeClass("on");
			},

			dispose : function ()
			{
			}
		});

	})();

	// 메인 기본 옵션
	ReadingQuiz.DEFAULT = { answer : [], failedAudio:"", clearAudio:"", finishAudio:"", customLength:null};

    function Plugin(option, params)
    {
        return this.each(function ()
        {
            var $this = $(this);
            var data = $this.data('ui.readingQuiz');
            var options =  $.extend({}, ReadingQuiz.DEFAULT, typeof option == "object" && option);
            if (!data) $this.data('ui.readingQuiz', (data = new ReadingQuiz($this, options)));
			if (typeof option == 'string') data[option](params);
			$this.data("quizType", 'readingQuiz');
        });
    }

	window.ReadingQuiz = ReadingQuiz;

    $.fn.readingQuiz = Plugin;
    $.fn.readingQuiz.Constructor = ReadingQuiz;

})(jQuery);

