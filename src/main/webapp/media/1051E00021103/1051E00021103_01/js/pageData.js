"use strict";

var PageData = {
	// quiz
	quiz_mp3 : [
		"./audio/read_s3_na_01.mp3"
	],

	// reading-content
	scene : [
		{
			na_mp3 : "./audio/read_s1_na_01.mp3",
			read_mp3 : 
			[
				"./audio/read_s1_01.mp3",
				"./audio/read_s1_02.mp3",
				"./audio/read_s1_03.mp3",
				"./audio/read_s1_04.mp3"
			]
		},
		{
			// na_mp3 : "./audio/read_s2_na_01.mp3",
			read_mp3 : 
			[
				"./audio/read_s2_01.mp3",
				"./audio/read_s2_02.mp3",
				"./audio/read_s2_03.mp3",
				"./audio/read_s2_04.mp3"
			]
		}
	],

	aniData1 : {
		framerate:6,
		png:[
		"./images/na_cut_1.png",
		"./images/na_cut_2.png",
		"./images/na_cut_1.png",
		"./images/na_cut_3.png",
		"./images/na_cut_2.png",
		"./images/na_cut_2.png",
		]
	},
	aniData2 : {
		framerate:6,
		png:[
		"./images/na_cut2_1.png",
		"./images/na_cut2_2.png",
		"./images/na_cut2_1.png",
		"./images/na_cut2_3.png",
		"./images/na_cut2_2.png",
		"./images/na_cut2_2.png"
		]
	},
	aniData3 : {
		framerate:6,
		png:[
		"./images/na_cut1_1.png",
		"./images/na_cut1_2.png",
		"./images/na_cut1_1.png",
		"./images/na_cut1_3.png",
		"./images/na_cut1_2.png",
		"./images/na_cut1_2.png"
		]
	}
}