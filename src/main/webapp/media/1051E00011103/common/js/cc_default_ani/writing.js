(function (cjs, an) {

	var p; // shortcut to reference prototypes
	var lib={};var ss={};var img={};
	lib.ssMetadata = [
			{name:"writing_atlas_", frames: [[1955,363,93,107],[1607,428,154,129],[0,1746,696,256],[698,1746,174,226],[1607,248,160,178],[1871,0,157,228],[1576,559,75,220],[1282,248,174,226],[1769,248,160,178],[1282,476,157,228],[1653,559,75,220],[1458,248,147,199],[1441,629,133,151],[1441,476,133,151],[874,1746,130,201],[1949,868,48,193],[1901,1599,147,82],[1350,1901,140,76],[973,1951,113,63],[1006,1689,207,116],[1556,1689,200,109],[1739,1829,147,82],[1906,1473,140,76],[1282,0,587,246],[1903,524,117,100],[1901,1683,112,95],[1767,1975,134,54],[874,1949,97,77],[1939,1888,67,98],[1958,1780,63,106],[1982,729,66,105],[1950,1063,68,104],[1888,1780,68,103],[1965,626,83,101],[1730,644,102,148],[1931,230,117,131],[1834,644,129,117],[1797,868,139,108],[1834,763,146,103],[1797,978,150,100],[1797,1080,151,99],[1947,1181,99,78],[1763,524,138,118],[1797,1181,148,100],[1282,706,156,85],[1384,1826,162,73],[698,1974,167,66],[1492,1913,169,62],[1767,1913,170,60],[1784,1283,201,73],[1763,428,190,94],[1556,1800,181,111],[1174,1831,174,125],[1384,1689,170,135],[1215,1689,167,140],[1006,1807,166,142],[1758,1716,111,111],[1906,1358,113,113],[1784,1599,115,115],[1784,1480,117,117],[1784,1358,120,120],[0,0,1280,800],[0,1274,900,470],[0,802,900,470],[902,1267,880,420],[902,802,893,463],[1663,1913,102,102]]},
			{name:"writing_atlas_2", frames: [[502,141,8,8],[328,340,27,23],[456,490,27,23],[216,482,41,29],[0,483,41,29],[492,73,9,4],[510,28,2,2],[429,468,66,20],[377,214,23,4],[92,305,44,17],[144,509,21,19],[259,504,25,23],[493,432,19,17],[66,509,22,20],[497,396,13,19],[492,28,16,23],[393,117,15,22],[492,0,18,26],[200,509,13,15],[216,186,16,18],[503,73,6,4],[0,246,16,11],[502,132,10,7],[247,0,108,58],[0,111,48,46],[452,122,48,46],[467,210,12,10],[452,210,13,10],[312,169,3,3],[510,39,2,1],[510,36,2,1],[50,111,8,14],[510,32,2,2],[63,457,100,25],[165,459,96,21],[43,484,49,23],[311,474,52,24],[214,291,67,27],[399,433,92,33],[62,54,50,116],[114,54,42,130],[317,60,38,138],[413,79,37,141],[94,484,23,31],[119,484,23,30],[473,396,22,34],[357,117,34,65],[357,0,54,115],[188,0,57,112],[0,0,60,109],[452,79,58,41],[0,434,61,47],[328,365,69,68],[377,468,50,34],[452,170,55,38],[233,404,72,53],[485,490,24,25],[473,366,35,28],[307,435,68,37],[62,0,124,52],[307,404,19,17],[63,434,16,15],[234,126,9,9],[286,504,22,22],[92,259,44,44],[413,0,77,77],[250,340,76,62],[0,373,79,59],[0,314,83,57],[85,326,86,55],[0,259,90,53],[158,114,74,70],[247,60,68,78],[234,140,69,75],[0,172,69,72],[305,200,70,69],[285,271,71,67],[71,186,69,71],[216,217,67,72],[377,222,66,73],[445,222,64,75],[142,256,70,68],[81,383,76,61],[173,326,75,63],[432,299,73,65],[142,186,72,68],[358,297,72,66],[399,366,72,65],[159,391,72,64],[263,474,46,28],[158,104,20,8],[165,482,49,25],[71,172,24,12],[0,159,28,11],[363,504,21,21],[55,166,3,3],[311,500,24,24],[386,504,21,21],[250,320,26,14],[30,159,23,11],[305,151,7,7],[55,159,5,5],[393,141,17,19],[409,504,14,17],[234,114,10,10],[50,149,8,8],[377,184,29,28],[429,490,25,25],[481,210,12,10],[502,122,10,8],[393,162,17,18],[167,509,15,15],[305,160,7,7],[497,417,12,12],[50,138,9,9],[377,435,19,17],[144,484,16,15],[305,140,9,9],[180,104,6,6],[285,236,17,17],[184,509,14,14],[358,271,16,16],[285,255,14,14],[502,160,7,7],[305,176,5,5],[492,53,20,18],[493,451,17,15],[285,217,18,17],[497,468,15,15],[357,184,16,14],[18,246,13,11],[502,151,7,7],[305,169,5,5],[337,500,24,24],[43,509,21,21],[97,172,12,12],[50,127,9,9],[158,54,26,48]]}
	];
	
	
	// symbols:
	
	
	
	(lib.CachedTexturedBitmap_445 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(0);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_446 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(1);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_447 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(0);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_448 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(1);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_449 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(2);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_450 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(3);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_451 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(4);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_452 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(5);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_461 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(6);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_462 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(7);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_463 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(8);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_464 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(9);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_465 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(10);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_466 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(11);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_467 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(12);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_468 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(13);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_469 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(14);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_470 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(15);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_471 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(16);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_472 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(17);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_473 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(18);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_474 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(19);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_475 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(20);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_476 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(2);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_477 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(3);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_479 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(4);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_480 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(5);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_481 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(6);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_482 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(7);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_484 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(8);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_485 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(9);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_486 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(10);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_487 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(11);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_488 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(12);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_489 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(13);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_490 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(14);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_491 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(15);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_498 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(21);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_502 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(22);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_503 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(16);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_504 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(17);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_505 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(18);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_506 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(23);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_507 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(19);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_508 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(20);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_509 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(21);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_510 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(22);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_511 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(23);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_512 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(24);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_513 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(25);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_514 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(26);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_515 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(27);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_516 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(28);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_517 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(29);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_518 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(30);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_519 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(31);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_520 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(32);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_521 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(24);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_522 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(25);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_523 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(33);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_524 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(34);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_525 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(35);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_526 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(36);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_527 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(37);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_528 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(38);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_529 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(26);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_530 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(27);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_531 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(28);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_532 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(39);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_533 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(40);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_534 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(41);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_535 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(42);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_536 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(43);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_537 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(44);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_538 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(45);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_539 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(46);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_540 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(47);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_541 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(48);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_542 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(49);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_543 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(29);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_544 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(30);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_545 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(31);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_546 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(32);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_547 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(50);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_548 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(51);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_549 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(52);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_550 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(33);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_551 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(34);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_552 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(35);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_553 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(36);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_554 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(37);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_555 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(38);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_556 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(39);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_557 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(40);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_558 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(53);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_559 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(54);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_560 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(55);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_561 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(41);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_562 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(42);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_563 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(43);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_564 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(44);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_565 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(45);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_566 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(46);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_567 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(47);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_568 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(48);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_569 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(56);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_570 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(57);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_571 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(58);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_572 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(59);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_573 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(49);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_574 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(50);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_575 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(51);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_576 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(52);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_577 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(53);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_578 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(54);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_579 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(55);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_580 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(60);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_581 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(61);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_582 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(62);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_583 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(63);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_584 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(64);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_585 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(65);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_586 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(56);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_587 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(57);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_588 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(58);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_589 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(59);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_590 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(60);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_591 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(61);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_620 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(66);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_621 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(67);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_622 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(68);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_623 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(69);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_624 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(70);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_625 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(71);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_626 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(72);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_627 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(73);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_628 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(74);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_629 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(75);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_630 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(76);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_632 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(77);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_633 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(78);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_634 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(79);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_635 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(80);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_636 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(81);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_637 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(82);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_638 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(83);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_639 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(84);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_640 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(85);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_641 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(86);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_642 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(87);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_644 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(88);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_645 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(62);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_646 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(63);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_647 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(64);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_648 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(65);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_649 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(89);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_650 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(90);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_651 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(91);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_652 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(92);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_655 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(93);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_656 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(94);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_657 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(95);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_658 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(96);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_659 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(97);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_662 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(98);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_663 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(99);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_664 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(100);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_665 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(101);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_666 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(102);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_667 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(103);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_668 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(104);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_669 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(105);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_670 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(106);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_671 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(107);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_672 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(108);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_673 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(109);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_674 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(110);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_675 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(111);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_676 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(112);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_678 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(113);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_679 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(114);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_680 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(115);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_681 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(116);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_682 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(117);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_683 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(118);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_684 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(119);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_685 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(120);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_686 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(121);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_687 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(122);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_688 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(123);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_689 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(124);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_690 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(125);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_691 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(126);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_692 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(127);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_693 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(128);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_694 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(129);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_695 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(130);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_696 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(131);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_697 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(132);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_698 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(133);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_699 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(134);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_700 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(135);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_701 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(136);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_702 = function() {
		this.initialize(ss["writing_atlas_2"]);
		this.gotoAndStop(137);
	}).prototype = p = new cjs.Sprite();
	
	
	
	(lib.CachedTexturedBitmap_703 = function() {
		this.initialize(ss["writing_atlas_"]);
		this.gotoAndStop(66);
	}).prototype = p = new cjs.Sprite();
	// helper functions:
	
	function mc_symbol_clone() {
		var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
		clone.gotoAndStop(this.currentFrame);
		clone.paused = this.paused;
		clone.framerate = this.framerate;
		return clone;
	}
	
	function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
		var prototype = cjs.extend(symbol, cjs.MovieClip);
		prototype.clone = mc_symbol_clone;
		prototype.nominalBounds = nominalBounds;
		prototype.frameBounds = frameBounds;
		return prototype;
		}
	
	
	(lib.Symbol128 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_703();
		this.instance.parent = this;
		this.instance.setTransform(-47.25,-47.25,0.9316,0.9316);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-47.2,-47.2,95,95);
	
	
	(lib.Symbol127 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_702();
		this.instance.parent = this;
		this.instance.setTransform(-15.9,-28.8,1.2171,1.2171);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-15.9,-28.8,31.700000000000003,58.400000000000006);
	
	
	(lib.Symbol126 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_701();
		this.instance.parent = this;
		this.instance.setTransform(-6.55,-6.55,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_700();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-8.5,-8.5,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-8.5,-8.5,16.9,16.9);
	
	
	(lib.Symbol125 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_699();
		this.instance.parent = this;
		this.instance.setTransform(-15,-14.4,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_698();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-17.15,-16.6,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-17.1,-16.6,33.8,33.900000000000006);
	
	
	(lib.Symbol124 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_697();
		this.instance.parent = this;
		this.instance.setTransform(-3.2,-3.2,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_696();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-5,-4.9,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-5,-4.9,9.9,9.9);
	
	
	(lib.Symbol123 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_695();
		this.instance.parent = this;
		this.instance.setTransform(-9.05,-7.8,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_694();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-11.05,-9.75,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-11,-9.7,22.5,19.7);
	
	
	(lib.Symbol122 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_693();
		this.instance.parent = this;
		this.instance.setTransform(-10.7,-10.2,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_692();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-12.75,-12.15,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-12.7,-12.1,25.299999999999997,23.9);
	
	
	(lib.Symbol121 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_691();
		this.instance.parent = this;
		this.instance.setTransform(-12.15,-10.35,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_690();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-14.15,-12.3,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-14.1,-12.3,28.2,25.4);
	
	
	(lib.Symbol120 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_689();
		this.instance.parent = this;
		this.instance.setTransform(-3.2,-3.2,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_688();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-4.9,-5,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-4.9,-5,9.9,9.9);
	
	
	(lib.Symbol119 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_687();
		this.instance.parent = this;
		this.instance.setTransform(-9.7,-9.5,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_686();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-11.4,-11.3,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-11.4,-11.3,22.6,22.6);
	
	
	(lib.Symbol118 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_685();
		this.instance.parent = this;
		this.instance.setTransform(-9.85,-10.2,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_684();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-11.9,-12.25,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-11.9,-12.2,24,23.9);
	
	
	(lib.Symbol117 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_683();
		this.instance.parent = this;
		this.instance.setTransform(-4.5,-4.4,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_682();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-6.1,-6.1,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-6.1,-6.1,12.7,12.7);
	
	
	(lib.Symbol116 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_681();
		this.instance.parent = this;
		this.instance.setTransform(-11.25,-10.2,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_680();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-13.3,-12.15,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-13.3,-12.1,26.8,23.9);
	
	
	(lib.Symbol115 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_679();
		this.instance.parent = this;
		this.instance.setTransform(-6.55,-6.55,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_678();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-8.5,-8.5,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-8.5,-8.5,16.9,16.9);
	
	
	(lib.Symbol114 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_697();
		this.instance.parent = this;
		this.instance.setTransform(-3.2,-3.2,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_676();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-4.9,-5.05,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-4.9,-5,9.9,9.8);
	
	
	(lib.Symbol113 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_675();
		this.instance.parent = this;
		this.instance.setTransform(-10.2,-10.7,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_674();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-12.05,-12.5,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-12,-12.5,23.9,25.4);
	
	
	(lib.Symbol112 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_673();
		this.instance.parent = this;
		this.instance.setTransform(-6.9,-5.25,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_672();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-8.55,-6.9,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-8.5,-6.9,16.9,14.100000000000001);
	
	
	(lib.Symbol111 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_671();
		this.instance.parent = this;
		this.instance.setTransform(-17.5,-17.25,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_670();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-20.05,-19.85,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-20,-19.8,40.9,39.400000000000006);
	
	
	(lib.Symbol110 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_669();
		this.instance.parent = this;
		this.instance.setTransform(-5.25,-5.25,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_668();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-7.1,-7.1,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-7.1,-7.1,14.1,14.1);
	
	
	(lib.Symbol109 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_667();
		this.instance.parent = this;
		this.instance.setTransform(-9.55,-11.65,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_666();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-11.55,-13.6,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-11.5,-13.6,23.9,26.799999999999997);
	
	
	(lib.Symbol108 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_665();
		this.instance.parent = this;
		this.instance.setTransform(-3.2,-3.2,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_664();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-5,-5,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-5,-5,9.9,9.9);
	
	
	(lib.Symbol107 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_663();
		this.instance.parent = this;
		this.instance.setTransform(-16.05,-7.8,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_662();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-18,-9.75,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-18,-9.7,36.7,19.7);
	
	
	(lib.Symbol106 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_701();
		this.instance.parent = this;
		this.instance.setTransform(-6.55,-6.55,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_700();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-8.5,-8.5,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-8.5,-8.5,16.9,16.9);
	
	
	(lib.Symbol105 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_659();
		this.instance.parent = this;
		this.instance.setTransform(-14.5,-14.7,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_658();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-16.7,-16.8,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-16.7,-16.8,33.9,33.900000000000006);
	
	
	(lib.Symbol22copy3 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_657();
		this.instance.parent = this;
		this.instance.setTransform(4.7,-5.85,0.9893,0.9893);
	
		this.instance_1 = new lib.CachedTexturedBitmap_656();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-8.65,-8.8,0.9893,0.9893);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-8.6,-8.8,20.799999999999997,20.8);
	
	
	(lib.Symbol22copy2 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_655();
		this.instance.parent = this;
		this.instance.setTransform(-8.3,-24.9,0.9893,0.9893);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-8.3,-24.9,27.7,10.899999999999999);
	
	
	(lib.Symbol21copy3 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_657();
		this.instance.parent = this;
		this.instance.setTransform(2.4,-5.75,0.9893,0.9893);
	
		this.instance_1 = new lib.CachedTexturedBitmap_656();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-11.55,-8.7,0.9893,0.9893);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-11.5,-8.7,20.8,20.799999999999997);
	
	
	(lib.Symbol21copy2 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_652();
		this.instance.parent = this;
		this.instance.setTransform(-15.2,-24.2,0.9893,0.9893);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-15.2,-24.2,23.799999999999997,11.899999999999999);
	
	
	(lib.Symbol19copy4 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_651();
		this.instance.parent = this;
		this.instance.setTransform(-13,-7.85,0.5225,0.5225);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-13,-7.8,25.6,13);
	
	
	(lib.Symbol19copy3 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_650();
		this.instance.parent = this;
		this.instance.setTransform(-1.15,-1.45,0.5225,0.5225);
	
		this.instance_1 = new lib.CachedTexturedBitmap_649();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-12.4,-7.1,0.5225,0.5225);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-12.4,-7.1,24.1,14.7);
	
	
	(lib.Symbol8 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer_1
		this.instance = new lib.CachedTexturedBitmap_648();
		this.instance.parent = this;
		this.instance.setTransform(3.5,3.5);
	
		this.instance_1 = new lib.CachedTexturedBitmap_647();
		this.instance_1.parent = this;
		this.instance_1.setTransform(10,41);
	
		this.instance_2 = new lib.CachedTexturedBitmap_646();
		this.instance_2.parent = this;
	
		this.instance_3 = new lib.CachedTexturedBitmap_645();
		this.instance_3.parent = this;
		this.instance_3.setTransform(4,5);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,904,475), null);
	
	
	(lib.Symbol5 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer_1
		this.instance = new lib.CachedTexturedBitmap_591();
		this.instance.parent = this;
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,1280,800), null);
	
	
	(lib.ㅎㄹㅇㄹㄴㅇㄹㄴㅇㅁㄹㅇㄴㅁ = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_583();
		this.instance.parent = this;
		this.instance.setTransform(-15.75,-15.7,1.448,1.448);
	
		this.instance_1 = new lib.CachedTexturedBitmap_584();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-31.85,-31.85,1.448,1.448);
	
		this.instance_2 = new lib.CachedTexturedBitmap_585();
		this.instance_2.parent = this;
		this.instance_2.setTransform(-55.85,-55.85,1.448,1.448);
	
		this.instance_3 = new lib.CachedTexturedBitmap_586();
		this.instance_3.parent = this;
		this.instance_3.setTransform(-79.95,-79.95,1.448,1.448);
	
		this.instance_4 = new lib.CachedTexturedBitmap_587();
		this.instance_4.parent = this;
		this.instance_4.setTransform(-81.55,-81.55,1.448,1.448);
	
		this.instance_5 = new lib.CachedTexturedBitmap_588();
		this.instance_5.parent = this;
		this.instance_5.setTransform(-83.25,-83.25,1.448,1.448);
	
		this.instance_6 = new lib.CachedTexturedBitmap_589();
		this.instance_6.parent = this;
		this.instance_6.setTransform(-84.85,-84.85,1.448,1.448);
	
		this.instance_7 = new lib.CachedTexturedBitmap_590();
		this.instance_7.parent = this;
		this.instance_7.setTransform(-86.5,-86.5,1.448,1.448);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[]},1).wait(2));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-86.5,-86.5,173.8,173.8);
	
	
	(lib.ㅎㄹㅇㄴㄻㄴㄻㄴㅇ = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_582();
		this.instance.parent = this;
		this.instance.setTransform(-15.25,-15.25,3.3934,3.3934);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-15.2,-15.2,30.5,30.5);
	
	
	(lib.Symbol104 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_581();
		this.instance.parent = this;
		this.instance.setTransform(-11.25,-10.1,1.4095,1.4095);
	
		this.instance_1 = new lib.CachedTexturedBitmap_580();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-13.2,-12.15,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-13.2,-12.1,26.799999999999997,23.9);
	
	
	(lib.Symbol102 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_524();
		this.instance.parent = this;
		this.instance.setTransform(0,0.05,1.2287,1.2287);
	
		this.instance_1 = new lib.CachedTexturedBitmap_523();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-2.5,-2.45,1.2287,1.2287);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = getMCSymbolPrototype(lib.Symbol102, new cjs.Rectangle(-2.5,-2.4,122.9,30.7), null);
	
	
	(lib.Symbol101 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_522();
		this.instance.parent = this;
		this.instance.setTransform(0,0,1.033,1.033);
	
		this.instance_1 = new lib.CachedTexturedBitmap_521();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-2.5,-2.4,1.033,1.033);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = getMCSymbolPrototype(lib.Symbol101, new cjs.Rectangle(-2.5,-2.4,120.9,103.30000000000001), null);
	
	
	(lib.Symbol100copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_520();
		this.instance.parent = this;
		this.instance.setTransform(16.7,10.85,1.1221,1.1221);
	
		this.instance_1 = new lib.CachedTexturedBitmap_519();
		this.instance_1.parent = this;
		this.instance_1.setTransform(12.25,-1.4,1.1221,1.1221);
	
		this.instance_2 = new lib.CachedTexturedBitmap_518();
		this.instance_2.parent = this;
		this.instance_2.setTransform(-24.4,-9.6,1.1221,1.1221);
	
		this.instance_3 = new lib.CachedTexturedBitmap_517();
		this.instance_3.parent = this;
		this.instance_3.setTransform(17.55,-11.05,1.1221,1.1221);
	
		this.instance_4 = new lib.CachedTexturedBitmap_516();
		this.instance_4.parent = this;
		this.instance_4.setTransform(-5.1,-23.3,1.1221,1.1221);
	
		this.instance_5 = new lib.CachedTexturedBitmap_515();
		this.instance_5.parent = this;
		this.instance_5.setTransform(5.7,-20.85,1.1221,1.1221);
	
		this.instance_6 = new lib.CachedTexturedBitmap_514();
		this.instance_6.parent = this;
		this.instance_6.setTransform(-18.1,-18.4,1.1221,1.1221);
	
		this.instance_7 = new lib.CachedTexturedBitmap_513();
		this.instance_7.parent = this;
		this.instance_7.setTransform(-26.9,-26.45,1.1221,1.1221);
	
		this.instance_8 = new lib.CachedTexturedBitmap_512();
		this.instance_8.parent = this;
		this.instance_8.setTransform(-26.9,-24.75,1.1221,1.1221);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-26.9,-26.4,53.9,53.3);
	
	
	(lib.Symbol97copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_511();
		this.instance.parent = this;
		this.instance.setTransform(-293.5,-123.2);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-293.5,-123.2,587,246);
	
	
	(lib.Symbol96copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_510();
		this.instance.parent = this;
		this.instance.setTransform(-70.15,-38);
	
		this.instance_1 = new lib.CachedTexturedBitmap_509();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-73.3,-41.15);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-73.3,-41.1,147,82);
	
	
	(lib.Symbol95 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_508();
		this.instance.parent = this;
		this.instance.setTransform(-99.95,-54.6);
	
		this.instance_1 = new lib.CachedTexturedBitmap_507();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-103.3,-58);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-103.3,-58,207,116);
	
	
	(lib.Symbol94copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_506();
		this.instance.parent = this;
		this.instance.setTransform(-53.85,-29.15);
	
		this.instance_1 = new lib.CachedTexturedBitmap_505();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-56.3,-31.6);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-56.3,-31.6,113,63);
	
	
	(lib.Symbol93 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_504();
		this.instance.parent = this;
		this.instance.setTransform(-70.15,-38);
	
		this.instance_1 = new lib.CachedTexturedBitmap_503();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-73.3,-41.15);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-73.3,-41.1,147,82);
	
	
	(lib.Symbol92 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_502();
		this.instance.parent = this;
		this.instance.setTransform(-108.85,41.8,0.5556,0.5556);
	
		this.instance_1 = new lib.CachedTexturedBitmap_502();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-39.35,41.8,0.5556,0.5556);
	
		this.instance_2 = new lib.CachedTexturedBitmap_502();
		this.instance_2.parent = this;
		this.instance_2.setTransform(40.8,41.8,0.5556,0.5556);
	
		this.instance_3 = new lib.CachedTexturedBitmap_502();
		this.instance_3.parent = this;
		this.instance_3.setTransform(158.05,23.1,0.5556,0.5556);
	
		this.instance_4 = new lib.CachedTexturedBitmap_498();
		this.instance_4.parent = this;
		this.instance_4.setTransform(157.7,-53.95,0.5556,0.5556);
	
		this.instance_5 = new lib.CachedTexturedBitmap_498();
		this.instance_5.parent = this;
		this.instance_5.setTransform(118.2,-57.95,0.5556,0.5556);
	
		this.instance_6 = new lib.CachedTexturedBitmap_498();
		this.instance_6.parent = this;
		this.instance_6.setTransform(80.2,-24.45,0.5556,0.5556);
	
		this.instance_7 = new lib.CachedTexturedBitmap_498();
		this.instance_7.parent = this;
		this.instance_7.setTransform(3.75,-24.45,0.5556,0.5556);
	
		this.instance_8 = new lib.CachedTexturedBitmap_498();
		this.instance_8.parent = this;
		this.instance_8.setTransform(-77.75,-24.45,0.5556,0.5556);
	
		this.instance_9 = new lib.CachedTexturedBitmap_498();
		this.instance_9.parent = this;
		this.instance_9.setTransform(-132.75,-7.45,0.5556,0.5556);
	
		this.instance_10 = new lib.CachedTexturedBitmap_498();
		this.instance_10.parent = this;
		this.instance_10.setTransform(-163.15,-53.95,0.5556,0.5556);
	
		this.instance_11 = new lib.CachedTexturedBitmap_491();
		this.instance_11.parent = this;
		this.instance_11.setTransform(155.15,-56.65,0.5556,0.5556);
	
		this.instance_12 = new lib.CachedTexturedBitmap_490();
		this.instance_12.parent = this;
		this.instance_12.setTransform(68.3,-59.65,0.5556,0.5556);
	
		this.instance_13 = new lib.CachedTexturedBitmap_489();
		this.instance_13.parent = this;
		this.instance_13.setTransform(-11.75,-31.45,0.5556,0.5556);
	
		this.instance_14 = new lib.CachedTexturedBitmap_488();
		this.instance_14.parent = this;
		this.instance_14.setTransform(-91.55,-31.45,0.5556,0.5556);
	
		this.instance_15 = new lib.CachedTexturedBitmap_487();
		this.instance_15.parent = this;
		this.instance_15.setTransform(-181.85,-58.4,0.5556,0.5556);
	
		this.instance_16 = new lib.CachedTexturedBitmap_486();
		this.instance_16.parent = this;
		this.instance_16.setTransform(147.65,-64.15,0.5556,0.5556);
	
		this.instance_17 = new lib.CachedTexturedBitmap_485();
		this.instance_17.parent = this;
		this.instance_17.setTransform(60.8,-67.15,0.5556,0.5556);
	
		this.instance_18 = new lib.CachedTexturedBitmap_484();
		this.instance_18.parent = this;
		this.instance_18.setTransform(-19.25,-38.95,0.5556,0.5556);
	
		this.instance_19 = new lib.CachedTexturedBitmap_484();
		this.instance_19.parent = this;
		this.instance_19.setTransform(-99.05,-38.95,0.5556,0.5556);
	
		this.instance_20 = new lib.CachedTexturedBitmap_482();
		this.instance_20.parent = this;
		this.instance_20.setTransform(-189.35,-65.9,0.5556,0.5556);
	
		this.instance_21 = new lib.CachedTexturedBitmap_481();
		this.instance_21.parent = this;
		this.instance_21.setTransform(147.65,-57.15,0.5556,0.5556);
	
		this.instance_22 = new lib.CachedTexturedBitmap_480();
		this.instance_22.parent = this;
		this.instance_22.setTransform(60.8,-60.15,0.5556,0.5556);
	
		this.instance_23 = new lib.CachedTexturedBitmap_479();
		this.instance_23.parent = this;
		this.instance_23.setTransform(-19.25,-31.95,0.5556,0.5556);
	
		this.instance_24 = new lib.CachedTexturedBitmap_479();
		this.instance_24.parent = this;
		this.instance_24.setTransform(-99.05,-31.95,0.5556,0.5556);
	
		this.instance_25 = new lib.CachedTexturedBitmap_477();
		this.instance_25.parent = this;
		this.instance_25.setTransform(-189.35,-58.9,0.5556,0.5556);
	
		this.instance_26 = new lib.CachedTexturedBitmap_476();
		this.instance_26.parent = this;
		this.instance_26.setTransform(-193.35,-71.15,0.5556,0.5556);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_26},{t:this.instance_25},{t:this.instance_24},{t:this.instance_23},{t:this.instance_22},{t:this.instance_21},{t:this.instance_20},{t:this.instance_19},{t:this.instance_18},{t:this.instance_17},{t:this.instance_16},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-193.3,-71.1,386.6,142.2);
	
	
	(lib.Symbol81copy2 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_474();
		this.instance.parent = this;
		this.instance.setTransform(-4.35,-4.2,0.8333,0.8333);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-4.3,-4.2,13.3,15);
	
	
	(lib.Symbol81copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_473();
		this.instance.parent = this;
		this.instance.setTransform(-4.4,-4.25);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-4.4,-4.2,13,15);
	
	
	(lib.Symbol80copy2 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_472();
		this.instance.parent = this;
		this.instance.setTransform(-7.45,-8.05,0.8333,0.8333);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-7.4,-8,15,21.6);
	
	
	(lib.Symbol80copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_471();
		this.instance.parent = this;
		this.instance.setTransform(-7.45,-8.05);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-7.4,-8,15,22);
	
	
	(lib.Symbol79copy2 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_470();
		this.instance.parent = this;
		this.instance.setTransform(-7.05,-9.4,0.8333,0.8333);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-7,-9.4,13.3,19.200000000000003);
	
	
	(lib.Symbol79copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_469();
		this.instance.parent = this;
		this.instance.setTransform(-7.05,-9.4);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-7,-9.4,13,19);
	
	
	(lib.Symbol78copy2 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_468();
		this.instance.parent = this;
		this.instance.setTransform(-7.25,-8.45,0.8333,0.8333);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-7.2,-8.4,18.3,16.6);
	
	
	(lib.Symbol78copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_467();
		this.instance.parent = this;
		this.instance.setTransform(-7.25,-8.5);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-7.2,-8.5,19,17);
	
	
	(lib.Symbol77copy2 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_466();
		this.instance.parent = this;
		this.instance.setTransform(-10.25,-9.85,0.8333,0.8333);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-10.2,-9.8,20.799999999999997,19.1);
	
	
	(lib.Symbol77copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_465();
		this.instance.parent = this;
		this.instance.setTransform(-10.25,-9.9);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-10.2,-9.9,21,19);
	
	
	(lib.Symbol26 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_464();
		this.instance.parent = this;
		this.instance.setTransform(-16.4,-10.9,1.2977,1.2977);
	
		this.instance_1 = new lib.CachedTexturedBitmap_463();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-38.9,1.45,1.2977,1.2977);
	
		this.instance_2 = new lib.CachedTexturedBitmap_462();
		this.instance_2.parent = this;
		this.instance_2.setTransform(-43,-12.9,1.2977,1.2977);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-43,-12.9,85.7,26);
	
	
	(lib.Symbol20copy4 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_461();
		this.instance.parent = this;
		this.instance.setTransform(-29.75,9,1.1148,1.1148);
	
		this.instance_1 = new lib.CachedTexturedBitmap_461();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-27.9,5.2,1.1148,1.1148);
	
		this.instance_2 = new lib.CachedTexturedBitmap_461();
		this.instance_2.parent = this;
		this.instance_2.setTransform(-24.95,7.9,1.1148,1.1148);
	
		this.instance_3 = new lib.CachedTexturedBitmap_461();
		this.instance_3.parent = this;
		this.instance_3.setTransform(26.5,8.55,1.1148,1.1148);
	
		this.instance_4 = new lib.CachedTexturedBitmap_461();
		this.instance_4.parent = this;
		this.instance_4.setTransform(21.4,7.65,1.1148,1.1148);
	
		this.instance_5 = new lib.CachedTexturedBitmap_461();
		this.instance_5.parent = this;
		this.instance_5.setTransform(25.15,4.65,1.1148,1.1148);
	
		this.instance_6 = new lib.CachedTexturedBitmap_461();
		this.instance_6.parent = this;
		this.instance_6.setTransform(1.1,7.3,1.1148,1.1148);
	
		this.instance_7 = new lib.CachedTexturedBitmap_461();
		this.instance_7.parent = this;
		this.instance_7.setTransform(-3.6,6.4,1.1148,1.1148);
	
		this.instance_8 = new lib.CachedTexturedBitmap_461();
		this.instance_8.parent = this;
		this.instance_8.setTransform(-1.3,2.45,1.1148,1.1148);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-29.7,2.5,58.5,8.8);
	
	
	(lib.Symbol20copy3 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_452();
		this.instance.parent = this;
		this.instance.setTransform(-5.15,-7.6,1.1148,1.1148);
	
		this.instance_1 = new lib.CachedTexturedBitmap_451();
		this.instance_1.parent = this;
		this.instance_1.setTransform(1.4,-12.55,1.1148,1.1148);
	
		this.instance_2 = new lib.CachedTexturedBitmap_450();
		this.instance_2.parent = this;
		this.instance_2.setTransform(-46.5,-12.55,1.1148,1.1148);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-46.5,-12.5,93.6,32.3);
	
	
	(lib.Symbol18 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_449();
		this.instance.parent = this;
		this.instance.setTransform(-16.35,-13.8,1.1952,1.1952);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-16.3,-13.8,32.2,27.5);
	
	
	(lib.Symbol17 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_448();
		this.instance.parent = this;
		this.instance.setTransform(-16.35,-13.8,1.1952,1.1952);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-16.3,-13.8,32.2,27.5);
	
	
	(lib.suc_Symbol328m = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_447();
		this.instance.parent = this;
		this.instance.setTransform(-2.25,-2.25,0.5459,0.5459);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
	
	}).prototype = getMCSymbolPrototype(lib.suc_Symbol328m, new cjs.Rectangle(-2.2,-2.2,4.300000000000001,4.300000000000001), null);
	
	
	(lib.착 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.CachedTexturedBitmap_445();
		this.instance.parent = this;
		this.instance.setTransform(19.8,14.75,0.7516,0.7516);
	
		this.instance_1 = new lib.CachedTexturedBitmap_446();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-4.2,9.55,0.7516,0.7516);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[]},2).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-4.2,0,115.8,106.5);
	
	
	(lib.Symbol7copy4 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 3
		this.instance = new lib.Symbol26("synched",0);
		this.instance.parent = this;
		this.instance.setTransform(41.45,-0.45);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({startPosition:0},0).to({scaleX:1.0737,rotation:4.4878,x:45.55,y:23.95},5).to({scaleX:0.9999,scaleY:0.9999,rotation:-2.5567,x:39.05,y:-6.2},1).to({scaleX:1,scaleY:1,rotation:-9.9404,x:34.95,y:-17.95},1).to({rotation:-12.3921,x:32.05,y:-35.4},5).to({rotation:0,x:38.75,y:-11},4).to({x:41.45,y:4.85},2).to({rotation:-1.7042,x:41.1,y:-3.5},3).to({rotation:0,x:41.45,y:-0.45},3).to({startPosition:0},1).wait(48));
	
		// Symbol 22
		this.instance_1 = new lib.Symbol22copy3("synched",0);
		this.instance_1.parent = this;
		this.instance_1.setTransform(61.95,26.5);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(16).to({startPosition:0},0).to({scaleX:1.4084,scaleY:0.6945,x:66.5,y:65.8},5).to({scaleX:0.8171,scaleY:1.2464,x:59.95,y:21.35},1).to({scaleX:0.8676,scaleY:1.1758,x:60.4,y:7.5},1).to({scaleX:1,scaleY:1,x:61.95,y:-12.05},5).to({scaleX:0.8455,x:62.7,y:41.9},4).to({scaleX:1.2428,scaleY:0.6255,x:64.7,y:45},2).to({scaleX:0.9007,scaleY:1.0769,x:60.85,y:20.4},3).to({scaleX:1,scaleY:1,x:61.95,y:26.5},3).to({startPosition:0},1).wait(48));
	
		// Symbol 21
		this.instance_2 = new lib.Symbol21copy3("synched",0);
		this.instance_2.parent = this;
		this.instance_2.setTransform(39.35,26.5);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(16).to({startPosition:0},0).to({scaleX:1.4084,scaleY:0.6945,x:34.8,y:65.8},5).to({scaleX:0.8171,scaleY:1.2464,x:41.45,y:21.35},1).to({scaleX:0.8676,scaleY:1.1758,x:40.85,y:7.5},1).to({scaleX:1,scaleY:1,x:39.35,y:-12.05},5).to({scaleX:0.8455,x:38.35,y:41.9},4).to({scaleX:1.2428,scaleY:0.6255,x:36.65,y:45},2).to({scaleX:0.9007,scaleY:1.0769,x:40.5,y:20.4},3).to({scaleX:1,scaleY:1,x:39.35,y:26.5},3).to({startPosition:0},1).wait(48));
	
		// Symbol 20
		this.instance_3 = new lib.Symbol20copy4("synched",0);
		this.instance_3.parent = this;
		this.instance_3.setTransform(50.7,28.25);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(16).to({startPosition:0},0).to({scaleX:1.1813,scaleY:0.8383,y:61.5},5).to({scaleX:0.8217,scaleY:1.1036,x:50.6,y:24.75},1).to({scaleX:0.8834,scaleY:1.2498,x:50.7,y:10.85},1).to({scaleX:1,scaleY:1,y:-7.6},5).to({scaleX:0.793,scaleY:1.194,y:32.85},4).to({scaleX:1.1617,scaleY:0.9254,y:44.25},2).to({scaleX:0.9418,scaleY:1.0684,y:24.15},3).to({scaleX:1,scaleY:1,y:28.25},3).to({startPosition:0},1).wait(48));
	
		// Symbol 19
		this.instance_4 = new lib.Symbol19copy4("synched",0);
		this.instance_4.parent = this;
		this.instance_4.setTransform(50.8,48.3);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(16).to({startPosition:0},0).to({scaleX:1.5629,y:81.55},5).to({scaleX:0.6358,scaleY:2.6667,y:48.95},1).to({scaleX:0.8013,scaleY:1.7576,y:37.55},1).to({scaleX:1,scaleY:1,y:12.55},5).to({scaleX:0.6689,scaleY:2.25,y:56.85},4).to({scaleX:1,scaleY:1,y:64.3},2).to({y:45.55},3).to({y:48.3},3).to({startPosition:0},1).wait(48));
	
		// Symbol 18
		this.instance_5 = new lib.Symbol18("synched",0);
		this.instance_5.parent = this;
		this.instance_5.setTransform(81.6,47.1);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(16).to({startPosition:0},0).to({scaleY:0.6781,rotation:-9.9564,x:95,y:77.75},5).to({scaleX:0.9996,scaleY:0.9825,rotation:-3.5402,x:83.65,y:44.6},1).to({scaleX:1,scaleY:1.1657,rotation:0,x:78.35,y:32.8},1).to({scaleY:1,y:18.8},5).to({scaleX:1.0479,scaleY:0.7867,skewX:-39.8181,skewY:-56.9359,x:80.15,y:50.95},4).to({scaleX:1,scaleY:1,skewX:0,skewY:0,x:81.6,y:58.1},2).to({y:44.35},3).to({y:47.1},3).to({startPosition:0},1).wait(48));
	
		// Symbol 17
		this.instance_6 = new lib.Symbol17("synched",0);
		this.instance_6.parent = this;
		this.instance_6.setTransform(20.05,47.1);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(16).to({startPosition:0},0).to({scaleY:0.6781,rotation:14.9993,x:8.2,y:77.1},5).to({scaleX:0.9994,scaleY:0.9823,rotation:5.5105,x:18.65,y:44.2},1).to({scaleX:1,scaleY:1.1657,rotation:0,x:22.55,y:32.8},1).to({scaleY:1,y:18.8},5).to({scaleX:0.8155,scaleY:0.7723,skewX:39.816,skewY:53.4217,x:24.3,y:55.1},4).to({scaleX:1,scaleY:1,skewX:0,skewY:0,x:20.05,y:58.1},2).to({y:44.35},3).to({y:47.1},3).to({startPosition:0},1).wait(48));
	
		// Layer 11
		this.instance_7 = new lib.CachedTexturedBitmap_644();
		this.instance_7.parent = this;
		this.instance_7.setTransform(0.65,0.6,1.3933,1.3933);
	
		this.instance_8 = new lib.CachedTexturedBitmap_620();
		this.instance_8.parent = this;
		this.instance_8.setTransform(-1.5,4.7,1.3933,1.3933);
	
		this.instance_9 = new lib.CachedTexturedBitmap_621();
		this.instance_9.parent = this;
		this.instance_9.setTransform(-3.8,8.8,1.3933,1.3933);
	
		this.instance_10 = new lib.CachedTexturedBitmap_622();
		this.instance_10.parent = this;
		this.instance_10.setTransform(-6.05,12.95,1.3933,1.3933);
	
		this.instance_11 = new lib.CachedTexturedBitmap_623();
		this.instance_11.parent = this;
		this.instance_11.setTransform(-8.25,17.1,1.3933,1.3933);
	
		this.instance_12 = new lib.CachedTexturedBitmap_624();
		this.instance_12.parent = this;
		this.instance_12.setTransform(-10.5,21.2,1.3933,1.3933);
	
		this.instance_13 = new lib.CachedTexturedBitmap_625();
		this.instance_13.parent = this;
		this.instance_13.setTransform(-0.3,-5.85,1.3933,1.3933);
	
		this.instance_14 = new lib.CachedTexturedBitmap_626();
		this.instance_14.parent = this;
		this.instance_14.setTransform(3.75,-24.45,1.3933,1.3933);
	
		this.instance_15 = new lib.CachedTexturedBitmap_627();
		this.instance_15.parent = this;
		this.instance_15.setTransform(3.1,-26.6,1.3933,1.3933);
	
		this.instance_16 = new lib.CachedTexturedBitmap_628();
		this.instance_16.parent = this;
		this.instance_16.setTransform(2.5,-28.7,1.3933,1.3933);
	
		this.instance_17 = new lib.CachedTexturedBitmap_629();
		this.instance_17.parent = this;
		this.instance_17.setTransform(1.85,-30.7,1.3933,1.3933);
	
		this.instance_18 = new lib.CachedTexturedBitmap_630();
		this.instance_18.parent = this;
		this.instance_18.setTransform(1.3,-32.8,1.3933,1.3933);
	
		this.instance_19 = new lib.CachedTexturedBitmap_632();
		this.instance_19.parent = this;
		this.instance_19.setTransform(2.9,-26.95,1.3933,1.3933);
	
		this.instance_20 = new lib.CachedTexturedBitmap_633();
		this.instance_20.parent = this;
		this.instance_20.setTransform(4,-22.95,1.3933,1.3933);
	
		this.instance_21 = new lib.CachedTexturedBitmap_634();
		this.instance_21.parent = this;
		this.instance_21.setTransform(5.15,-19,1.3933,1.3933);
	
		this.instance_22 = new lib.CachedTexturedBitmap_635();
		this.instance_22.parent = this;
		this.instance_22.setTransform(6.25,-15.1,1.3933,1.3933);
	
		this.instance_23 = new lib.CachedTexturedBitmap_636();
		this.instance_23.parent = this;
		this.instance_23.setTransform(2,-5.2,1.3933,1.3933);
	
		this.instance_24 = new lib.CachedTexturedBitmap_637();
		this.instance_24.parent = this;
		this.instance_24.setTransform(-2.2,4.55,1.3933,1.3933);
	
		this.instance_25 = new lib.CachedTexturedBitmap_638();
		this.instance_25.parent = this;
		this.instance_25.setTransform(-1.3,1.35,1.3933,1.3933);
	
		this.instance_26 = new lib.CachedTexturedBitmap_639();
		this.instance_26.parent = this;
		this.instance_26.setTransform(-0.25,-1.65,1.3933,1.3933);
	
		this.instance_27 = new lib.CachedTexturedBitmap_640();
		this.instance_27.parent = this;
		this.instance_27.setTransform(0.65,-4.85,1.3933,1.3933);
	
		this.instance_28 = new lib.CachedTexturedBitmap_641();
		this.instance_28.parent = this;
		this.instance_28.setTransform(0.65,-3.05,1.3933,1.3933);
	
		this.instance_29 = new lib.CachedTexturedBitmap_642();
		this.instance_29.parent = this;
		this.instance_29.setTransform(0.65,-1.15,1.3933,1.3933);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7}]}).to({state:[{t:this.instance_7}]},16).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).to({state:[{t:this.instance_27}]},1).to({state:[{t:this.instance_28}]},1).to({state:[{t:this.instance_29}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},1).wait(48));
		this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(16).to({_off:true},1).wait(11).to({_off:false,y:-34.8},0).to({_off:true},1).wait(11).to({_off:false,y:0.6},0).wait(49));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-12.7,-57.1,127.60000000000001,152.2);
	
	
	(lib.Symbol7copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Symbol 22
		this.instance = new lib.Symbol22copy2("synched",0);
		this.instance.parent = this;
		this.instance.setTransform(61.95,26.5);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(35).to({startPosition:0},0).to({scaleX:1.4084,scaleY:0.6945,rotation:-17.423,x:65.5,y:60.95},5).to({scaleX:0.8171,scaleY:1.2464,rotation:33.2481,x:54.1,y:24.6},1).to({scaleX:0.8676,scaleY:1.1758,rotation:22.7053,x:56.4,y:9.5},1).to({scaleX:1,scaleY:1,rotation:29.9992,x:58.9,y:-12.05},5).to({scaleX:0.8455,rotation:0,x:58.7,y:33.7},4).to({scaleX:1.2428,scaleY:0.6255,rotation:-29.9985,x:66.25,y:39},2).to({scaleX:0.9007,scaleY:1.0769,rotation:14.9989,x:60.85,y:20.35},3).to({scaleX:1,scaleY:1,rotation:0,x:61.95,y:26.5},3).wait(1));
	
		// Symbol 21
		this.instance_1 = new lib.Symbol21copy2("synched",0);
		this.instance_1.parent = this;
		this.instance_1.setTransform(39.35,26.5);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(35).to({startPosition:0},0).to({scaleX:1.4084,scaleY:0.6945,rotation:23.7434,x:35.75,y:58.15},5).to({scaleX:0.8171,scaleY:1.2464,rotation:-21.7291,x:43.65,y:25.35},1).to({scaleX:0.8676,scaleY:1.1758,rotation:-14.9993,x:44.85,y:7.5},1).to({scaleX:1,scaleY:1,rotation:-35.4391,x:45.75,y:-14.6},5).to({scaleX:0.8455,rotation:0,x:42.35,y:31.7},4).to({scaleX:1.2428,scaleY:0.6255,rotation:29.9985,x:35.8,y:37.6},2).to({scaleX:0.9007,scaleY:1.0769,rotation:-14.9989,x:40.5,y:20.35},3).to({scaleX:1,scaleY:1,rotation:0,x:39.35,y:26.5},3).wait(1));
	
		// Symbol 20
		this.instance_2 = new lib.Symbol20copy3("synched",0);
		this.instance_2.parent = this;
		this.instance_2.setTransform(50.7,28.25);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(35).to({startPosition:0},0).to({scaleX:1.1813,scaleY:0.8383,y:61.5},5).to({scaleX:0.8217,scaleY:1.1036,x:50.6,y:24.75},1).to({scaleX:0.8834,scaleY:1.2498,x:50.7,y:10.85},1).to({scaleX:1,scaleY:1,y:-7.6},5).to({scaleX:0.793,scaleY:1.194,y:32.85},4).to({scaleX:1.1617,scaleY:0.9254,y:44.25},2).to({scaleX:0.9418,scaleY:1.0684,y:24.15},3).to({scaleX:1,scaleY:1,y:28.25},3).wait(1));
	
		// Symbol 19
		this.instance_3 = new lib.Symbol19copy3("synched",0);
		this.instance_3.parent = this;
		this.instance_3.setTransform(50.8,48.3);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(35).to({startPosition:0},0).to({scaleX:1.5629,y:81.55},5).to({scaleX:0.6358,scaleY:2.6667,y:48.95},1).to({scaleX:0.8013,scaleY:1.7576,y:37.55},1).to({scaleX:1,scaleY:1,y:12.55},5).to({scaleX:0.6689,scaleY:2.25,y:56.85},4).to({scaleX:1,scaleY:1,y:64.3},2).to({y:45.55},3).to({y:48.3},3).wait(1));
	
		// Symbol 18
		this.instance_4 = new lib.Symbol18("synched",0);
		this.instance_4.parent = this;
		this.instance_4.setTransform(81.65,47.1);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(35).to({startPosition:0},0).to({scaleY:0.6781,rotation:-9.9564,x:95,y:77.75},5).to({scaleX:0.9996,scaleY:0.9825,rotation:-3.5402,x:83.65,y:44.6},1).to({scaleX:1,scaleY:1.1657,rotation:0,x:78.35,y:32.8},1).to({scaleY:1,y:18.8},5).to({scaleX:1.0479,scaleY:0.7867,skewX:-39.8181,skewY:-56.9359,x:80.15,y:50.95},4).to({scaleX:1,scaleY:1,skewX:0,skewY:0,x:81.65,y:58.1},2).to({y:44.35},3).to({y:47.1},3).wait(1));
	
		// Symbol 17
		this.instance_5 = new lib.Symbol17("synched",0);
		this.instance_5.parent = this;
		this.instance_5.setTransform(20.05,47.1);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(35).to({startPosition:0},0).to({scaleY:0.6781,rotation:14.9993,x:8.2,y:77.1},5).to({scaleX:0.9994,scaleY:0.9823,rotation:5.5105,x:18.65,y:44.2},1).to({scaleX:1,scaleY:1.1657,rotation:0,x:22.55,y:32.8},1).to({scaleY:1,y:18.8},5).to({scaleX:0.8155,scaleY:0.7723,skewX:39.816,skewY:53.4217,x:24.3,y:55.1},4).to({scaleX:1,scaleY:1,skewX:0,skewY:0,x:20.05,y:58.1},2).to({y:44.35},3).to({y:47.1},3).wait(1));
	
		// Layer 11
		this.instance_6 = new lib.CachedTexturedBitmap_644();
		this.instance_6.parent = this;
		this.instance_6.setTransform(0.65,0.6,1.3933,1.3933);
	
		this.instance_7 = new lib.CachedTexturedBitmap_620();
		this.instance_7.parent = this;
		this.instance_7.setTransform(-1.5,4.7,1.3933,1.3933);
	
		this.instance_8 = new lib.CachedTexturedBitmap_621();
		this.instance_8.parent = this;
		this.instance_8.setTransform(-3.8,8.8,1.3933,1.3933);
	
		this.instance_9 = new lib.CachedTexturedBitmap_622();
		this.instance_9.parent = this;
		this.instance_9.setTransform(-6.05,12.95,1.3933,1.3933);
	
		this.instance_10 = new lib.CachedTexturedBitmap_623();
		this.instance_10.parent = this;
		this.instance_10.setTransform(-8.25,17.1,1.3933,1.3933);
	
		this.instance_11 = new lib.CachedTexturedBitmap_624();
		this.instance_11.parent = this;
		this.instance_11.setTransform(-10.5,21.15,1.3933,1.3933);
	
		this.instance_12 = new lib.CachedTexturedBitmap_625();
		this.instance_12.parent = this;
		this.instance_12.setTransform(-0.3,-5.85,1.3933,1.3933);
	
		this.instance_13 = new lib.CachedTexturedBitmap_626();
		this.instance_13.parent = this;
		this.instance_13.setTransform(3.75,-24.45,1.3933,1.3933);
	
		this.instance_14 = new lib.CachedTexturedBitmap_627();
		this.instance_14.parent = this;
		this.instance_14.setTransform(3.1,-26.6,1.3933,1.3933);
	
		this.instance_15 = new lib.CachedTexturedBitmap_628();
		this.instance_15.parent = this;
		this.instance_15.setTransform(2.5,-28.7,1.3933,1.3933);
	
		this.instance_16 = new lib.CachedTexturedBitmap_629();
		this.instance_16.parent = this;
		this.instance_16.setTransform(1.85,-30.7,1.3933,1.3933);
	
		this.instance_17 = new lib.CachedTexturedBitmap_630();
		this.instance_17.parent = this;
		this.instance_17.setTransform(1.3,-32.8,1.3933,1.3933);
	
		this.instance_18 = new lib.CachedTexturedBitmap_632();
		this.instance_18.parent = this;
		this.instance_18.setTransform(2.9,-26.95,1.3933,1.3933);
	
		this.instance_19 = new lib.CachedTexturedBitmap_633();
		this.instance_19.parent = this;
		this.instance_19.setTransform(4,-22.95,1.3933,1.3933);
	
		this.instance_20 = new lib.CachedTexturedBitmap_634();
		this.instance_20.parent = this;
		this.instance_20.setTransform(5.15,-19,1.3933,1.3933);
	
		this.instance_21 = new lib.CachedTexturedBitmap_635();
		this.instance_21.parent = this;
		this.instance_21.setTransform(6.25,-15.1,1.3933,1.3933);
	
		this.instance_22 = new lib.CachedTexturedBitmap_636();
		this.instance_22.parent = this;
		this.instance_22.setTransform(2,-5.2,1.3933,1.3933);
	
		this.instance_23 = new lib.CachedTexturedBitmap_637();
		this.instance_23.parent = this;
		this.instance_23.setTransform(-2.2,4.55,1.3933,1.3933);
	
		this.instance_24 = new lib.CachedTexturedBitmap_638();
		this.instance_24.parent = this;
		this.instance_24.setTransform(-1.3,1.35,1.3933,1.3933);
	
		this.instance_25 = new lib.CachedTexturedBitmap_639();
		this.instance_25.parent = this;
		this.instance_25.setTransform(-0.25,-1.65,1.3933,1.3933);
	
		this.instance_26 = new lib.CachedTexturedBitmap_640();
		this.instance_26.parent = this;
		this.instance_26.setTransform(0.65,-4.85,1.3933,1.3933);
	
		this.instance_27 = new lib.CachedTexturedBitmap_641();
		this.instance_27.parent = this;
		this.instance_27.setTransform(0.65,-3.05,1.3933,1.3933);
	
		this.instance_28 = new lib.CachedTexturedBitmap_642();
		this.instance_28.parent = this;
		this.instance_28.setTransform(0.65,-1.15,1.3933,1.3933);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6,p:{y:0.6}}]}).to({state:[{t:this.instance_6,p:{y:0.6}}]},35).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_6,p:{y:-34.8}}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).to({state:[{t:this.instance_27}]},1).to({state:[{t:this.instance_28}]},1).to({state:[{t:this.instance_6,p:{y:0.6}}]},1).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-10.5,-39.2,125.4,134.2);
	
	
	(lib.Symbol4 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// timeline functions:
		this.frame_0 = function() {
			this.stop();
		}
	
		// actions tween:
		this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));
	
		// Layer_1
		this.instance = new lib.Symbol7copy4();
		this.instance.parent = this;
		this.instance.setTransform(1038.8,577.65,0.7177,0.7177,0,0,0,52.3,42.5);
	
		this.instance_1 = new lib.Symbol7copy();
		this.instance_1.parent = this;
		this.instance_1.setTransform(955.7,595.15,0.7177,0.7177,-7.7465,0,0,53.1,47);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_1},{t:this.instance}]},1).wait(1));
	
		// Layer_4
		this.instance_2 = new lib.Symbol8();
		this.instance_2.parent = this;
		this.instance_2.setTransform(650,410,1,1,0,0,0,452,237.5);
		this.instance_2._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off:false},0).wait(1));
	
		// Layer_3
		this.instance_3 = new lib.Symbol5();
		this.instance_3.parent = this;
		this.instance_3.setTransform(640,400,1,1,0,0,0,640,400);
		this.instance_3.alpha = 0.6484;
		this.instance_3._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({_off:false},0).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(0,0,1280,800);
	
	
	(lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 2
		this.instance = new lib.ㅎㄹㅇㄴㄻㄴㄻㄴㅇ("single",0);
		this.instance.parent = this;
		this.instance.setTransform(0,116.3);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).to({y:-132.4},10,cjs.Ease.get(1)).to({_off:true},1).wait(3));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-15.2,-147.6,30.5,279.2);
	
	
	(lib.Symbol85 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.Symbol100copy("synched",0);
		this.instance.parent = this;
		this.instance.setTransform(27,26.55);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:360},9).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-10.8,-11,75.7,75.5);
	
	
	(lib.Symbol76copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Symbol 81
		this.instance = new lib.Symbol81copy("synched",0);
		this.instance.parent = this;
		this.instance.setTransform(12.5,-15.05);
	
		this.instance_1 = new lib.Symbol81copy2("synched",0);
		this.instance_1.parent = this;
		this.instance_1.setTransform(12.5,-18.35,1.2,1.2);
		this.instance_1._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(26).to({startPosition:0},0).to({_off:true,scaleX:1.2,scaleY:1.2,y:-18.35},5,cjs.Ease.get(1)).to({_off:false,scaleX:1,scaleY:1,y:-15.05},8,cjs.Ease.get(-1)).wait(50).to({startPosition:0},0).to({_off:true},1).wait(44));
		this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(26).to({_off:false},5,cjs.Ease.get(1)).to({_off:true,scaleX:1,scaleY:1,y:-15.05},8,cjs.Ease.get(-1)).wait(95));
	
		// Symbol 80
		this.instance_2 = new lib.Symbol80copy("synched",0);
		this.instance_2.parent = this;
		this.instance_2.setTransform(0.65,-13.2);
	
		this.instance_3 = new lib.Symbol80copy2("synched",0);
		this.instance_3.parent = this;
		this.instance_3.setTransform(0.65,-15.95,1.2,1.2);
		this.instance_3._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(24).to({startPosition:0},0).to({_off:true,scaleX:1.2,scaleY:1.2,y:-15.95},5,cjs.Ease.get(1)).to({_off:false,scaleX:1,scaleY:1,y:-13.2},8,cjs.Ease.get(-1)).wait(52).to({startPosition:0},0).to({_off:true},1).wait(44));
		this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(24).to({_off:false},5,cjs.Ease.get(1)).to({_off:true,scaleX:1,scaleY:1,y:-13.2},8,cjs.Ease.get(-1)).wait(97));
	
		// Symbol 79
		this.instance_4 = new lib.Symbol79copy("synched",0);
		this.instance_4.parent = this;
		this.instance_4.setTransform(-11.85,-4.85);
	
		this.instance_5 = new lib.Symbol79copy2("synched",0);
		this.instance_5.parent = this;
		this.instance_5.setTransform(-13.55,-7.1,1.2,1.2,0,0,0,-0.1,0);
		this.instance_5._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(22).to({startPosition:0},0).to({_off:true,regX:-0.1,scaleX:1.2,scaleY:1.2,x:-13.55,y:-7.1},5,cjs.Ease.get(1)).to({_off:false,regX:0,scaleX:1,scaleY:1,x:-11.85,y:-4.85},8,cjs.Ease.get(-1)).wait(54).to({startPosition:0},0).to({_off:true},1).wait(44));
		this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(22).to({_off:false},5,cjs.Ease.get(1)).to({_off:true,regX:0,scaleX:1,scaleY:1,x:-11.85,y:-4.85},8,cjs.Ease.get(-1)).wait(99));
	
		// Symbol 78
		this.instance_6 = new lib.Symbol78copy("synched",0);
		this.instance_6.parent = this;
		this.instance_6.setTransform(-23.65,3.3);
	
		this.instance_7 = new lib.Symbol78copy2("synched",0);
		this.instance_7.parent = this;
		this.instance_7.setTransform(-25.3,0.8,1.2,1.2);
		this.instance_7._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(20).to({startPosition:0},0).to({_off:true,scaleX:1.2,scaleY:1.2,x:-25.3,y:0.8},5,cjs.Ease.get(1)).to({_off:false,scaleX:1,scaleY:1,x:-23.65,y:3.3},8,cjs.Ease.get(-1)).wait(56).to({startPosition:0},0).to({_off:true},1).wait(44));
		this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(20).to({_off:false},5,cjs.Ease.get(1)).to({_off:true,scaleX:1,scaleY:1,x:-23.65,y:3.3},8,cjs.Ease.get(-1)).wait(101));
	
		// Symbol 77
		this.instance_8 = new lib.Symbol77copy("synched",0);
		this.instance_8.parent = this;
		this.instance_8.setTransform(-33.5,14.75);
	
		this.instance_9 = new lib.Symbol77copy2("synched",0);
		this.instance_9.parent = this;
		this.instance_9.setTransform(-34.4,13.85,1.2,1.2);
		this.instance_9._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(18).to({startPosition:0},0).to({_off:true,scaleX:1.2,scaleY:1.2,x:-34.4,y:13.85},5,cjs.Ease.get(1)).to({_off:false,scaleX:1,scaleY:1,x:-33.5,y:14.75},8,cjs.Ease.get(-1)).wait(58).to({startPosition:0},0).to({_off:true},1).wait(44));
		this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(18).to({_off:false},5,cjs.Ease.get(1)).to({_off:true,scaleX:1,scaleY:1,x:-33.5,y:14.75},8,cjs.Ease.get(-1)).wait(103));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-46.6,-25.6,69.9,50.6);
	
	
	(lib.Symbol70copy2 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 2
		this.instance = new lib.Symbol76copy("synched",0,false);
		this.instance.parent = this;
		this.instance.setTransform(-15.95,-32.8);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(90));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-62.6,-58.4,70,50.699999999999996);
	
	
	(lib.suc_Symbol329m = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.suc_Symbol328m();
		this.instance.parent = this;
		this.instance.setTransform(1.6,73.55,1.1013,1.1013,-1.2759);
	
		this.instance_1 = new lib.suc_Symbol328m();
		this.instance_1.parent = this;
		this.instance_1.setTransform(-1.55,-73.7,1.1013,1.1013,-1.2759);
	
		this.instance_2 = new lib.suc_Symbol328m();
		this.instance_2.parent = this;
		this.instance_2.setTransform(-19.35,71.05,1.1022,1.1022,15.2747);
	
		this.instance_3 = new lib.suc_Symbol328m();
		this.instance_3.parent = this;
		this.instance_3.setTransform(19.4,-71.15,1.1022,1.1022,15.2747);
	
		this.instance_4 = new lib.suc_Symbol328m();
		this.instance_4.parent = this;
		this.instance_4.setTransform(73.85,0.45,1.0973,1.0973,105.5321);
	
		this.instance_5 = new lib.suc_Symbol328m();
		this.instance_5.parent = this;
		this.instance_5.setTransform(-70.7,-18,1.0973,1.0973,105.5321);
	
		this.instance_6 = new lib.suc_Symbol328m();
		this.instance_6.parent = this;
		this.instance_6.setTransform(-62.4,-37.95,1.0922,1.0922,121.2619);
	
		this.instance_7 = new lib.suc_Symbol328m();
		this.instance_7.parent = this;
		this.instance_7.setTransform(62.5,37.9,1.0922,1.0922,121.2619);
	
		this.instance_8 = new lib.suc_Symbol328m();
		this.instance_8.parent = this;
		this.instance_8.setTransform(-36.15,-63.6,1.0937,1.0937,150.3316);
	
		this.instance_9 = new lib.suc_Symbol328m();
		this.instance_9.parent = this;
		this.instance_9.setTransform(36.25,63.55,1.0937,1.0937,150.3316);
	
		this.instance_10 = new lib.suc_Symbol328m();
		this.instance_10.parent = this;
		this.instance_10.setTransform(-20,-70.4,1.0945,1.0945,164.1145);
	
		this.instance_11 = new lib.suc_Symbol328m();
		this.instance_11.parent = this;
		this.instance_11.setTransform(20.05,70.45,1.0945,1.0945,164.1145);
	
		this.instance_12 = new lib.suc_Symbol328m();
		this.instance_12.parent = this;
		this.instance_12.setTransform(-72.8,0.7,1.0973,1.0973,105.5321);
	
		this.instance_13 = new lib.suc_Symbol328m();
		this.instance_13.parent = this;
		this.instance_13.setTransform(70.75,19.7,1.0973,1.0973,105.5321);
	
		this.instance_14 = new lib.suc_Symbol328m();
		this.instance_14.parent = this;
		this.instance_14.setTransform(-70.35,21.3,1.099,1.099,73.1634);
	
		this.instance_15 = new lib.suc_Symbol328m();
		this.instance_15.parent = this;
		this.instance_15.setTransform(70.4,-21.3,1.099,1.099,73.1634);
	
		this.instance_16 = new lib.suc_Symbol328m();
		this.instance_16.parent = this;
		this.instance_16.setTransform(-63,38.05,1.0999,1.0999,58.8735);
	
		this.instance_17 = new lib.suc_Symbol328m();
		this.instance_17.parent = this;
		this.instance_17.setTransform(63,-38.05,1.0999,1.0999,58.8735);
	
		this.instance_18 = new lib.suc_Symbol328m();
		this.instance_18.parent = this;
		this.instance_18.setTransform(-36.9,63.75,1.1014,1.1014,30.0728);
	
		this.instance_19 = new lib.suc_Symbol328m();
		this.instance_19.parent = this;
		this.instance_19.setTransform(36.95,-63.8,1.1014,1.1014,30.0728);
	
		this.instance_20 = new lib.suc_Symbol328m();
		this.instance_20.parent = this;
		this.instance_20.setTransform(-52.25,-52.25,1.1032,1.1032,135);
	
		this.instance_21 = new lib.suc_Symbol328m();
		this.instance_21.parent = this;
		this.instance_21.setTransform(52.1,52.15,1.1032,1.1032,135);
	
		this.instance_22 = new lib.suc_Symbol328m();
		this.instance_22.parent = this;
		this.instance_22.setTransform(-52.15,52.15,1.1032,1.1032,45);
	
		this.instance_23 = new lib.suc_Symbol328m();
		this.instance_23.parent = this;
		this.instance_23.setTransform(52.25,-52.25,1.1032,1.1032,45);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_23},{t:this.instance_22},{t:this.instance_21},{t:this.instance_20},{t:this.instance_19},{t:this.instance_18},{t:this.instance_17},{t:this.instance_16},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));
	
	}).prototype = getMCSymbolPrototype(lib.suc_Symbol329m, new cjs.Rectangle(-75.6,-76.2,152.5,152.10000000000002), null);
	
	
	(lib.ㅎㄹㅇ흉ㄶㄴㅇㅎㄹㄴ = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance.parent = this;
		this.instance.setTransform(-0.6,-0.15,0.9999,0.9999,-30.0057,0,0,-0.1,144);
	
		this.instance_1 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_1.parent = this;
		this.instance_1.setTransform(-0.25,-0.4,0.9999,0.9999,-60.0057,0,0,-0.1,144);
	
		this.instance_2 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_2.parent = this;
		this.instance_2.setTransform(-0.5,-0.45,0.9999,0.9999,-90.0052,0,0,-0.1,144);
	
		this.instance_3 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_3.parent = this;
		this.instance_3.setTransform(-0.15,0.05,0.9999,0.9999,-120.0041,0,0,-0.1,144);
	
		this.instance_4 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_4.parent = this;
		this.instance_4.setTransform(-0.35,0.15,0.9999,0.9999,-150.0042,0,0,-0.1,144.1);
	
		this.instance_5 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_5.parent = this;
		this.instance_5.setTransform(0.55,0.4,1,1,179.9956,0,0,0,144.1);
	
		this.instance_6 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_6.parent = this;
		this.instance_6.setTransform(0.45,0.1,1,1,149.9968,0,0,0,144.1);
	
		this.instance_7 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_7.parent = this;
		this.instance_7.setTransform(0.1,0.45,1,1,119.9969,0,0,0,144.1);
	
		this.instance_8 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_8.parent = this;
		this.instance_8.setTransform(0.5,0.1,1,1,89.9974,0,0,0.1,144);
	
		this.instance_9 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_9.parent = this;
		this.instance_9.setTransform(0.2,-0.5,1,1,59.9984,0,0,0,144);
	
		this.instance_10 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_10.parent = this;
		this.instance_10.setTransform(0.45,-0.1,1,1,29.9992,0,0,0,144.1);
	
		this.instance_11 = new lib.ㅎㄹㅇㄴㄹㅊㅍㄴㅇㄻㄴㄹㅇ("synched",0,false);
		this.instance_11.parent = this;
		this.instance_11.setTransform(0.55,-0.4,1,1,0,0,0,0,144.1);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(15));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-292,-292.1,584.1,584.2);
	
	
	(lib.Symbol99 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Symbol 85
		this.instance = new lib.Symbol85("synched",0);
		this.instance.parent = this;
		this.instance.setTransform(-63.55,42.65,0.4486,0.4486,-39.5583,0,0,26.9,25.7);
		this.instance.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:25.6,scaleX:0.8912,scaleY:0.8912,rotation:1.8191,guide:{path:[-62.8,43,-23.7,-22,46.4,-21.9]},alpha:1,mode:"single"},9).wait(16).to({rotation:1.8191},0).to({_off:true},1).wait(134));
	
		// Symbol 85
		this.instance_1 = new lib.Symbol85("synched",0);
		this.instance_1.parent = this;
		this.instance_1.setTransform(-76.3,68.4,0.3781,0.3781,-61.9033,0,0,26.9,25.5);
		this.instance_1.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:27.1,regY:25.6,scaleX:0.7511,scaleY:0.7511,rotation:-20.5273,guide:{path:[-75.9,68.5,-51.8,13.4,-11.7,-8.1]},alpha:1,mode:"single"},9).wait(16).to({rotation:-20.5273},0).to({_off:true},1).wait(134));
	
		// Symbol 85
		this.instance_2 = new lib.Symbol85("synched",0);
		this.instance_2.parent = this;
		this.instance_2.setTransform(-84.35,89.8,0.3028,0.3028,-80.795,0,0,26.9,25.8);
		this.instance_2.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:27,regY:25.6,scaleX:0.6014,scaleY:0.6014,rotation:-39.4201,guide:{path:[-84.2,89.8,-70,48.6,-48.6,22.7]},alpha:1,mode:"single"},9).wait(16).to({startPosition:0},0).to({_off:true},1).wait(134));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-93.3,-45.2,164.39999999999998,144.3);
	
	
	(lib.Symbol98copy = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Symbol 85
		this.instance = new lib.Symbol85("synched",0);
		this.instance.parent = this;
		this.instance.setTransform(35.3,-56.05,0.4456,0.4456,-2.8972,0,0,26.9,25.7);
		this.instance.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:25.6,scaleX:0.8912,scaleY:0.8912,guide:{path:[35.5,-56,26.7,19.5,-28.9,29.2]},alpha:1,mode:"single",startPosition:9},9).wait(16).to({startPosition:9},0).to({_off:true},1).wait(134));
	
		// Symbol 85
		this.instance_1 = new lib.Symbol85("synched",0);
		this.instance_1.parent = this;
		this.instance_1.setTransform(37.1,-80.65,0.3755,0.3755,-35.4744,0,0,26.9,25.6);
		this.instance_1.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:0.7511,scaleY:0.7511,rotation:-35.4734,guide:{path:[37.1,-80.6,36,-23.6,14.1,4.2]},alpha:1,mode:"single"},9).wait(16).to({startPosition:0},0).to({_off:true},1).wait(134));
	
		// Symbol 85
		this.instance_2 = new lib.Symbol85("synched",0);
		this.instance_2.parent = this;
		this.instance_2.setTransform(36.4,-100.9,0.3007,0.3007,-53.9209,0,0,27.1,25.6);
		this.instance_2.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:26.9,regY:25.4,scaleX:0.6014,scaleY:0.6014,guide:{path:[36.9,-100.9,38.4,-60.6,31.2,-32.7]},alpha:1,mode:"single"},9).wait(16).to({startPosition:0},0).to({_off:true},1).wait(134));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-53.9,-111.9,108.1,167.10000000000002);
	
	
	(lib.suc_Symbol330m = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.suc_Symbol329m();
		this.instance.parent = this;
		this.instance.setTransform(-0.5,0,0.3229,0.3229);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,alpha:0},14).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-76.1,-76.2,152.5,152.10000000000002);
	
	
	(lib.Symbol129 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.ㅎㄹㅇ흉ㄶㄴㅇㅎㄹㄴ("synched",0,false);
		this.instance.parent = this;
		this.instance.setTransform(0,0,0.2947,0.2947);
		this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 0, 0)];
		this.instance.cache(-294,-294,588,588);
	
		this.instance_1 = new lib.ㅎㄹㅇㄹㄴㅇㄹㄴㅇㅁㄹㅇㄴㅁ("synched",0,false);
		this.instance_1.parent = this;
		this.instance_1.setTransform(0,0,0.6906,0.6906);
		this.instance_1.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 0, 0)];
		this.instance_1.cache(-88,-88,178,178);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1,p:{startPosition:0}},{t:this.instance,p:{startPosition:0}}]}).to({state:[{t:this.instance_1,p:{startPosition:9}},{t:this.instance,p:{startPosition:14}}]},19).to({state:[]},1).wait(102));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-86,-86,172,172);
	
	
	(lib.suc_Symbol331m = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.suc_Symbol330m("synched",2);
		this.instance.parent = this;
		this.instance.setTransform(0.25,-2.15,0.3549,0.3549,7.5186);
	
		this.instance_1 = new lib.suc_Symbol330m("synched",6);
		this.instance_1.parent = this;
		this.instance_1.setTransform(0.3,-1.05,0.5573,0.5573,7.5177);
	
		this.instance_2 = new lib.suc_Symbol330m("synched",11);
		this.instance_2.parent = this;
		this.instance_2.setTransform(0.55,-1.8,0.7627,0.7627,7.518);
	
		this.instance_3 = new lib.suc_Symbol330m("synched",14);
		this.instance_3.parent = this;
		this.instance_3.setTransform(0.3,0.05);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(31));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-75.8,-76.1,152.5,152.1);
	
	
	(lib.Symbol103 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// Layer 1
		this.instance = new lib.착("synched",0,false);
		this.instance.parent = this;
		this.instance.setTransform(183.8,259.95,1.8753,1.6369,0,-154.8003,22.0402,49.6,57.1);
		this.instance.alpha = 0.5703;
		this.instance._off = true;
		this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 255, 255, 0)];
		this.instance.cache(-6,-2,120,111);
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(20).to({skewX:-154.8004,startPosition:4},0).to({_off:true},1).wait(27));
	
		// Symbol 101
		this.instance_1 = new lib.Symbol101();
		this.instance_1.parent = this;
		this.instance_1.setTransform(97.65,324.65,1.0479,1.0479,56.2181,0,0,58,72.6);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:1.1476,scaleY:0.8667,rotation:55.5098,x:89.5,y:333.8},1,cjs.Ease.get(1)).to({regY:72.7,scaleX:0.9556,scaleY:1.1967,rotation:55.9357,x:94.85,y:327.8},1).to({regY:72.6,scaleX:0.8617,scaleY:1.3645,rotation:56.2179,x:97.65,y:324.65},2).to({scaleX:1.0479,scaleY:1.0479,rotation:56.2181},3).wait(15).to({_off:true},1).wait(27));
	
		// Layer 8
		this.instance_2 = new lib.Symbol128("synched",0);
		this.instance_2.parent = this;
		this.instance_2.setTransform(163.2,280.6,0.3308,1.3785,0,-33.3729,-31.9524);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleY:1.513,skewX:-34.7706,skewY:-32.6593,x:145,y:296.1},1,cjs.Ease.get(1)).to({scaleX:0.3303,scaleY:1.4212,skewX:-33.831,skewY:-32.0997,x:185.3,y:267.85},1).to({scaleX:0.3302,scaleY:1.3842,skewX:-27.1218,skewY:-25.6225,x:364.95,y:166.95},2).to({scaleX:0.3308,scaleY:1.3785,skewX:-13.4332,skewY:-12.0113,x:436.45,y:190.4,alpha:0},3).to({_off:true},1).wait(42));
	
		// Layer 2
		this.instance_3 = new lib.CachedTexturedBitmap_525();
		this.instance_3.parent = this;
		this.instance_3.setTransform(137.05,289.65,1.4095,1.4095);
	
		this.instance_4 = new lib.CachedTexturedBitmap_526();
		this.instance_4.parent = this;
		this.instance_4.setTransform(140.2,290.7,1.4095,1.4095);
	
		this.instance_5 = new lib.CachedTexturedBitmap_527();
		this.instance_5.parent = this;
		this.instance_5.setTransform(149.5,293.5,1.4095,1.4095);
	
		this.instance_6 = new lib.CachedTexturedBitmap_528();
		this.instance_6.parent = this;
		this.instance_6.setTransform(165.05,297.9,1.4095,1.4095);
	
		this.instance_7 = new lib.CachedTexturedBitmap_529();
		this.instance_7.parent = this;
		this.instance_7.setTransform(186.75,292.25,1.4095,1.4095);
	
		this.instance_8 = new lib.CachedTexturedBitmap_530();
		this.instance_8.parent = this;
		this.instance_8.setTransform(229.3,313.25,1.4095,1.4095);
	
		this.instance_9 = new lib.CachedTexturedBitmap_531();
		this.instance_9.parent = this;
		this.instance_9.setTransform(264.1,325.35,1.4095,1.4095);
	
		this.instance_10 = new lib.CachedTexturedBitmap_532();
		this.instance_10.parent = this;
		this.instance_10.setTransform(291.25,334.15,1.4095,1.4095);
	
		this.instance_11 = new lib.CachedTexturedBitmap_533();
		this.instance_11.parent = this;
		this.instance_11.setTransform(310.55,338.55,1.4095,1.4095);
	
		this.instance_12 = new lib.CachedTexturedBitmap_534();
		this.instance_12.parent = this;
		this.instance_12.setTransform(322.2,341.15,1.4095,1.4095);
	
		this.instance_13 = new lib.CachedTexturedBitmap_535();
		this.instance_13.parent = this;
		this.instance_13.setTransform(326,342,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[]},1).wait(37));
	
		// Layer 3
		this.instance_14 = new lib.CachedTexturedBitmap_536();
		this.instance_14.parent = this;
		this.instance_14.setTransform(140.2,223.45,1.4095,1.4095);
	
		this.instance_15 = new lib.CachedTexturedBitmap_537();
		this.instance_15.parent = this;
		this.instance_15.setTransform(138.9,220.5,1.4095,1.4095);
	
		this.instance_16 = new lib.CachedTexturedBitmap_538();
		this.instance_16.parent = this;
		this.instance_16.setTransform(134.85,206.55,1.4095,1.4095);
	
		this.instance_17 = new lib.CachedTexturedBitmap_539();
		this.instance_17.parent = this;
		this.instance_17.setTransform(128.15,160.15,1.4095,1.4095);
	
		this.instance_18 = new lib.CachedTexturedBitmap_540();
		this.instance_18.parent = this;
		this.instance_18.setTransform(118.8,94.5,1.4095,1.4095);
	
		this.instance_19 = new lib.CachedTexturedBitmap_541();
		this.instance_19.parent = this;
		this.instance_19.setTransform(84.4,51.4,1.4095,1.4095);
	
		this.instance_20 = new lib.CachedTexturedBitmap_542();
		this.instance_20.parent = this;
		this.instance_20.setTransform(56.2,16.2,1.4095,1.4095);
	
		this.instance_21 = new lib.CachedTexturedBitmap_543();
		this.instance_21.parent = this;
		this.instance_21.setTransform(34.35,-11.2,1.4095,1.4095);
	
		this.instance_22 = new lib.CachedTexturedBitmap_544();
		this.instance_22.parent = this;
		this.instance_22.setTransform(18.7,-30.75,1.4095,1.4095);
	
		this.instance_23 = new lib.CachedTexturedBitmap_545();
		this.instance_23.parent = this;
		this.instance_23.setTransform(9.35,-42.45,1.4095,1.4095);
	
		this.instance_24 = new lib.CachedTexturedBitmap_546();
		this.instance_24.parent = this;
		this.instance_24.setTransform(6.2,-46.35,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[]},1).wait(37));
	
		// Layer 4
		this.instance_25 = new lib.CachedTexturedBitmap_547();
		this.instance_25.parent = this;
		this.instance_25.setTransform(108.6,264.6,1.4095,1.4095);
	
		this.instance_26 = new lib.CachedTexturedBitmap_548();
		this.instance_26.parent = this;
		this.instance_26.setTransform(113.45,251,1.4095,1.4095);
	
		this.instance_27 = new lib.CachedTexturedBitmap_549();
		this.instance_27.parent = this;
		this.instance_27.setTransform(128.05,210.25,1.4095,1.4095);
	
		this.instance_28 = new lib.CachedTexturedBitmap_550();
		this.instance_28.parent = this;
		this.instance_28.setTransform(152.35,142.5,1.4095,1.4095);
	
		this.instance_29 = new lib.CachedTexturedBitmap_551();
		this.instance_29.parent = this;
		this.instance_29.setTransform(186.3,47.4,1.4095,1.4095);
	
		this.instance_30 = new lib.CachedTexturedBitmap_552();
		this.instance_30.parent = this;
		this.instance_30.setTransform(261.3,-1.05,1.4095,1.4095);
	
		this.instance_31 = new lib.CachedTexturedBitmap_553();
		this.instance_31.parent = this;
		this.instance_31.setTransform(322.6,-40.85,1.4095,1.4095);
	
		this.instance_32 = new lib.CachedTexturedBitmap_554();
		this.instance_32.parent = this;
		this.instance_32.setTransform(370.35,-73.9,1.4095,1.4095);
	
		this.instance_33 = new lib.CachedTexturedBitmap_555();
		this.instance_33.parent = this;
		this.instance_33.setTransform(404.35,-99,1.4095,1.4095);
	
		this.instance_34 = new lib.CachedTexturedBitmap_556();
		this.instance_34.parent = this;
		this.instance_34.setTransform(424.85,-114.5,1.4095,1.4095);
	
		this.instance_35 = new lib.CachedTexturedBitmap_557();
		this.instance_35.parent = this;
		this.instance_35.setTransform(431.55,-119.65,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_25}]},2).to({state:[{t:this.instance_26}]},1).to({state:[{t:this.instance_27}]},1).to({state:[{t:this.instance_28}]},1).to({state:[{t:this.instance_29}]},1).to({state:[{t:this.instance_30}]},1).to({state:[{t:this.instance_31}]},1).to({state:[{t:this.instance_32}]},1).to({state:[{t:this.instance_33}]},1).to({state:[{t:this.instance_34}]},1).to({state:[{t:this.instance_35}]},1).to({state:[]},1).wait(37));
	
		// Layer 5
		this.instance_36 = new lib.CachedTexturedBitmap_558();
		this.instance_36.parent = this;
		this.instance_36.setTransform(125.75,280.1,1.4095,1.4095);
	
		this.instance_37 = new lib.CachedTexturedBitmap_559();
		this.instance_37.parent = this;
		this.instance_37.setTransform(134,266.65,1.4095,1.4095);
	
		this.instance_38 = new lib.CachedTexturedBitmap_560();
		this.instance_38.parent = this;
		this.instance_38.setTransform(158.7,225.55,1.4095,1.4095);
	
		this.instance_39 = new lib.CachedTexturedBitmap_561();
		this.instance_39.parent = this;
		this.instance_39.setTransform(199.85,155,1.4095,1.4095);
	
		this.instance_40 = new lib.CachedTexturedBitmap_562();
		this.instance_40.parent = this;
		this.instance_40.setTransform(257.4,55.6,1.4095,1.4095);
	
		this.instance_41 = new lib.CachedTexturedBitmap_563();
		this.instance_41.parent = this;
		this.instance_41.setTransform(310.15,44.75,1.4095,1.4095);
	
		this.instance_42 = new lib.CachedTexturedBitmap_564();
		this.instance_42.parent = this;
		this.instance_42.setTransform(353.25,35.9,1.4095,1.4095);
	
		this.instance_43 = new lib.CachedTexturedBitmap_565();
		this.instance_43.parent = this;
		this.instance_43.setTransform(386.8,29,1.4095,1.4095);
	
		this.instance_44 = new lib.CachedTexturedBitmap_566();
		this.instance_44.parent = this;
		this.instance_44.setTransform(410.8,23.35,1.4095,1.4095);
	
		this.instance_45 = new lib.CachedTexturedBitmap_567();
		this.instance_45.parent = this;
		this.instance_45.setTransform(425.15,19.45,1.4095,1.4095);
	
		this.instance_46 = new lib.CachedTexturedBitmap_568();
		this.instance_46.parent = this;
		this.instance_46.setTransform(429.9,18,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_36}]},2).to({state:[{t:this.instance_37}]},1).to({state:[{t:this.instance_38}]},1).to({state:[{t:this.instance_39}]},1).to({state:[{t:this.instance_40}]},1).to({state:[{t:this.instance_41}]},1).to({state:[{t:this.instance_42}]},1).to({state:[{t:this.instance_43}]},1).to({state:[{t:this.instance_44}]},1).to({state:[{t:this.instance_45}]},1).to({state:[{t:this.instance_46}]},1).to({state:[]},1).wait(37));
	
		// Layer 6
		this.instance_47 = new lib.CachedTexturedBitmap_569();
		this.instance_47.parent = this;
		this.instance_47.setTransform(147.4,251.1,1.4095,1.4095);
	
		this.instance_48 = new lib.CachedTexturedBitmap_570();
		this.instance_48.parent = this;
		this.instance_48.setTransform(152.75,245.5,1.4095,1.4095);
	
		this.instance_49 = new lib.CachedTexturedBitmap_571();
		this.instance_49.parent = this;
		this.instance_49.setTransform(168.9,228.55,1.4095,1.4095);
	
		this.instance_50 = new lib.CachedTexturedBitmap_572();
		this.instance_50.parent = this;
		this.instance_50.setTransform(195.75,200.25,1.4095,1.4095);
	
		this.instance_51 = new lib.CachedTexturedBitmap_573();
		this.instance_51.parent = this;
		this.instance_51.setTransform(233.3,160.65,1.4095,1.4095);
	
		this.instance_52 = new lib.CachedTexturedBitmap_574();
		this.instance_52.parent = this;
		this.instance_52.setTransform(321.05,123.4,1.4095,1.4095);
	
		this.instance_53 = new lib.CachedTexturedBitmap_575();
		this.instance_53.parent = this;
		this.instance_53.setTransform(392.9,92.7,1.4095,1.4095);
	
		this.instance_54 = new lib.CachedTexturedBitmap_576();
		this.instance_54.parent = this;
		this.instance_54.setTransform(448.75,68.95,1.4095,1.4095);
	
		this.instance_55 = new lib.CachedTexturedBitmap_577();
		this.instance_55.parent = this;
		this.instance_55.setTransform(488.65,51.9,1.4095,1.4095);
	
		this.instance_56 = new lib.CachedTexturedBitmap_578();
		this.instance_56.parent = this;
		this.instance_56.setTransform(512.55,41.75,1.4095,1.4095);
	
		this.instance_57 = new lib.CachedTexturedBitmap_579();
		this.instance_57.parent = this;
		this.instance_57.setTransform(520.45,38.25,1.4095,1.4095);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_47}]},2).to({state:[{t:this.instance_48}]},1).to({state:[{t:this.instance_49}]},1).to({state:[{t:this.instance_50}]},1).to({state:[{t:this.instance_51}]},1).to({state:[{t:this.instance_52}]},1).to({state:[{t:this.instance_53}]},1).to({state:[{t:this.instance_54}]},1).to({state:[{t:this.instance_55}]},1).to({state:[{t:this.instance_56}]},1).to({state:[{t:this.instance_57}]},1).to({state:[]},1).wait(37));
	
		// Symbol 126
		this.instance_58 = new lib.Symbol126("synched",0);
		this.instance_58.parent = this;
		this.instance_58.setTransform(158.2,241.75);
		this.instance_58._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_58).wait(2).to({_off:false},0).wait(1).to({x:157.0344,y:229.9406},0).wait(1).to({x:153.5375,y:194.5125},0).wait(1).to({x:147.7094,y:135.4656},0).wait(1).to({x:139.55,y:52.8},0).wait(1).to({x:132.8125,y:42.3347,alpha:0.6944},0).wait(1).to({x:127.3,y:33.7722,alpha:0.4444},0).wait(1).to({x:123.0125,y:27.1125,alpha:0.25},0).wait(1).to({x:119.95,y:22.3556,alpha:0.1111},0).wait(1).to({x:118.1125,y:19.5014,alpha:0.0278},0).wait(1).to({x:117.5,y:18.55,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 125
		this.instance_59 = new lib.Symbol125("synched",0);
		this.instance_59.parent = this;
		this.instance_59.setTransform(155.85,278.5);
		this.instance_59._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_59).wait(2).to({_off:false},0).wait(1).to({regX:-0.2,regY:0.3,x:155.05,y:271.6},0).wait(1).to({x:153.25,y:250.05},0).wait(1).to({x:150.25,y:214.1},0).wait(1).to({regX:0,regY:0,x:146.3,y:163.5},0).wait(1).to({regX:-0.2,regY:0.3,x:136,y:147.15,alpha:0.6944},0).wait(1).to({x:127.8,y:133.6,alpha:0.4444},0).wait(1).to({x:121.4,y:123,alpha:0.25},0).wait(1).to({x:116.85,y:115.45,alpha:0.1111},0).wait(1).to({x:114.1,y:110.95,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:113.4,y:109.15,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 124
		this.instance_60 = new lib.Symbol124("synched",0);
		this.instance_60.parent = this;
		this.instance_60.setTransform(151.95,297.3);
		this.instance_60._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_60).wait(2).to({_off:false},0).wait(1).to({x:152.4094,y:290.75},0).wait(1).to({x:153.7875,y:271.1},0).wait(1).to({x:156.0844,y:238.35},0).wait(1).to({x:159.3,y:192.5},0).wait(1).to({x:149.7056,y:186.3736,alpha:0.6944},0).wait(1).to({x:141.8556,y:181.3611,alpha:0.4444},0).wait(1).to({x:135.75,y:177.4625,alpha:0.25},0).wait(1).to({x:131.3889,y:174.6778,alpha:0.1111},0).wait(1).to({x:128.7722,y:173.0069,alpha:0.0278},0).wait(1).to({x:127.9,y:172.45,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 123
		this.instance_61 = new lib.Symbol123("synched",0);
		this.instance_61.parent = this;
		this.instance_61.setTransform(170.45,290.65);
		this.instance_61._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_61).wait(2).to({_off:false},0).wait(1).to({regX:0.2,regY:0.1,x:173.95,y:282.55},0).wait(1).to({x:184,y:258.05},0).wait(1).to({x:200.75,y:217.2},0).wait(1).to({regX:0,regY:0,x:224,y:159.9},0).wait(1).to({regX:0.2,regY:0.1,x:233.6,y:146.85,alpha:0.6944},0).wait(1).to({x:241.3,y:136.15,alpha:0.4444},0).wait(1).to({x:247.3,y:127.8,alpha:0.25},0).wait(1).to({x:251.55,y:121.85,alpha:0.1111},0).wait(1).to({x:254.1,y:118.25,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:254.8,y:117,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 122
		this.instance_62 = new lib.Symbol122("synched",0);
		this.instance_62.parent = this;
		this.instance_62.setTransform(158.2,266.3);
		this.instance_62._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_62).wait(2).to({_off:false},0).wait(1).to({regX:-0.1,regY:-0.2,x:160.65,y:256.15},0).wait(1).to({x:168.35,y:226.35},0).wait(1).to({x:181.2,y:176.65},0).wait(1).to({regX:0,regY:0,x:199.35,y:107.35},0).wait(1).to({regX:-0.1,regY:-0.2,x:199.9,y:89.65,alpha:0.6944},0).wait(1).to({x:200.45,y:75.35,alpha:0.4444},0).wait(1).to({x:200.9,y:64.25,alpha:0.25},0).wait(1).to({x:201.2,y:56.3,alpha:0.1111},0).wait(1).to({x:201.35,y:51.5,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:201.55,y:50.15,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 121
		this.instance_63 = new lib.Symbol121("synched",0);
		this.instance_63.parent = this;
		this.instance_63.setTransform(159.7,285.85);
		this.instance_63._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_63).wait(2).to({_off:false},0).wait(1).to({regX:-0.1,regY:0.4,x:165.6,y:272},0).wait(1).to({x:183.7,y:229.4},0).wait(1).to({x:213.85,y:158.4},0).wait(1).to({regX:0,regY:0,x:256.15,y:58.6},0).wait(1).to({regX:-0.1,regY:0.4,x:256.25,y:41.95,alpha:0.6944},0).wait(1).to({x:256.4,y:28.05,alpha:0.4444},0).wait(1).to({x:256.55,y:17.25,alpha:0.25},0).wait(1).to({x:256.65,y:9.5,alpha:0.1111},0).wait(1).to({x:256.7,y:4.85,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:256.85,y:2.95,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 120
		this.instance_64 = new lib.Symbol120("synched",0);
		this.instance_64.parent = this;
		this.instance_64.setTransform(159.3,256.35);
		this.instance_64._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_64).wait(2).to({_off:false},0).wait(1).to({x:168.3594,y:241.3063},0).wait(1).to({x:195.5375,y:196.175},0).wait(1).to({x:240.8344,y:120.9563},0).wait(1).to({x:304.25,y:15.65},0).wait(1).to({x:304.6778,y:0.5556,alpha:0.6944},0).wait(1).to({x:305.0278,y:-11.7944,alpha:0.4444},0).wait(1).to({x:305.3,y:-21.4,alpha:0.25},0).wait(1).to({x:305.4944,y:-28.2611,alpha:0.1111},0).wait(1).to({x:305.6111,y:-32.3778,alpha:0.0278},0).wait(1).to({x:305.65,y:-33.75,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 119
		this.instance_65 = new lib.Symbol119("synched",0);
		this.instance_65.parent = this;
		this.instance_65.setTransform(178.8,274.55);
		this.instance_65._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_65).wait(2).to({_off:false},0).wait(1).to({regX:-0.1,x:188.15,y:258.05},0).wait(1).to({x:216.6,y:208.7},0).wait(1).to({x:264,y:126.45},0).wait(1).to({regX:0,x:330.45,y:11.3},0).wait(1).to({regX:-0.1,x:344.95,y:-1.35,alpha:0.6944},0).wait(1).to({x:356.95,y:-11.7,alpha:0.4444},0).wait(1).to({x:366.25,y:-19.75,alpha:0.25},0).wait(1).to({x:372.9,y:-25.5,alpha:0.1111},0).wait(1).to({x:376.9,y:-28.95,alpha:0.0278},0).wait(1).to({regX:0,x:378.35,y:-30.15,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 118
		this.instance_66 = new lib.Symbol118("synched",0);
		this.instance_66.parent = this;
		this.instance_66.setTransform(172.65,260.45);
		this.instance_66._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_66).wait(2).to({_off:false},0).wait(1).to({regX:0.1,regY:-0.3,x:183,y:248.25},0).wait(1).to({x:213.9,y:212.6},0).wait(1).to({x:265.35,y:153.2},0).wait(1).to({regX:0,regY:0,x:337.35,y:70.4},0).wait(1).to({regX:0.1,regY:-0.3,x:337.45,y:70.1,alpha:0.6944},0).wait(1).to({alpha:0.4444},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.1111},0).wait(1).to({alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:337.35,y:70.4,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 117
		this.instance_67 = new lib.Symbol117("synched",0);
		this.instance_67.parent = this;
		this.instance_67.setTransform(169.65,270.45);
		this.instance_67._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_67).wait(2).to({_off:false},0).wait(1).to({regX:0.2,regY:0.2,x:185.95,y:255.35},0).wait(1).to({x:234.25,y:209.6},0).wait(1).to({x:314.8,y:133.3},0).wait(1).to({regX:0,regY:0,x:427.35,y:26.3},0).wait(1).to({regX:0.2,regY:0.2,x:443.2,y:23.85,alpha:0.6944},0).wait(1).to({x:456.05,y:21.65,alpha:0.4444},0).wait(1).to({x:466.05,y:20,alpha:0.25},0).wait(1).to({x:473.15,y:18.8,alpha:0.1111},0).wait(1).to({x:477.45,y:18.05,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:478.7,y:17.65,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 116
		this.instance_68 = new lib.Symbol116("synched",0);
		this.instance_68.parent = this;
		this.instance_68.setTransform(187.7,279.5);
		this.instance_68._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_68).wait(2).to({_off:false},0).wait(1).to({regX:0.1,regY:-0.2,x:205.75,y:266.9},0).wait(1).to({x:259.7,y:229.8},0).wait(1).to({x:349.55,y:168},0).wait(1).to({regX:0,regY:0,x:475.3,y:81.65},0).wait(1).to({regX:0.1,regY:-0.2,x:490.65,y:79.9,alpha:0.6944},0).wait(1).to({x:503.15,y:78.65,alpha:0.4444},0).wait(1).to({x:512.9,y:77.7,alpha:0.25},0).wait(1).to({x:519.8,y:77,alpha:0.1111},0).wait(1).to({x:524,y:76.55,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:525.3,y:76.65,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 115
		this.instance_69 = new lib.Symbol115("synched",0);
		this.instance_69.parent = this;
		this.instance_69.setTransform(183.55,288.75);
		this.instance_69._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_69).wait(2).to({_off:false},0).wait(1).to({x:202.3375,y:278.6},0).wait(1).to({x:258.7,y:248.15},0).wait(1).to({x:352.6375,y:197.4},0).wait(1).to({x:484.15,y:126.35},0).wait(1).to({x:503.7819,y:129.1917,alpha:0.6944},0).wait(1).to({x:519.8444,y:131.5167,alpha:0.4444},0).wait(1).to({x:532.3375,y:133.325,alpha:0.25},0).wait(1).to({x:541.2611,y:134.6167,alpha:0.1111},0).wait(1).to({x:546.6153,y:135.3917,alpha:0.0278},0).wait(1).to({x:548.4,y:135.65,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 114
		this.instance_70 = new lib.Symbol114("synched",0);
		this.instance_70.parent = this;
		this.instance_70.setTransform(190.8,290.05);
		this.instance_70._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_70).wait(2).to({_off:false},0).wait(1).to({regY:-0.1,x:206.2,y:277.75},0).wait(1).to({x:252.5,y:241.3},0).wait(1).to({x:329.7,y:180.5},0).wait(1).to({regY:0,x:437.75,y:95.55},0).wait(1).to({regY:-0.1,x:452.35,y:87.55,alpha:0.6944},0).wait(1).to({x:464.35,y:81.1,alpha:0.4444},0).wait(1).to({x:473.65,y:76.1,alpha:0.25},0).wait(1).to({x:480.3,y:72.55,alpha:0.1111},0).wait(1).to({x:484.3,y:70.4,alpha:0.0278},0).wait(1).to({regY:0,x:485.65,y:69.8,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 113
		this.instance_71 = new lib.Symbol113("synched",0);
		this.instance_71.parent = this;
		this.instance_71.setTransform(160.5,282.5);
		this.instance_71._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_71).wait(2).to({_off:false},0).wait(1).to({regX:-0.1,regY:0.2,x:175.95,y:271.45},0).wait(1).to({x:222.7,y:237.85},0).wait(1).to({x:300.6,y:181.8},0).wait(1).to({regX:0,regY:0,x:409.75,y:103.2},0).wait(1).to({regX:-0.1,regY:0.2,x:421,y:99.2,alpha:0.6944},0).wait(1).to({x:430.25,y:95.8,alpha:0.4444},0).wait(1).to({x:437.5,y:93.2,alpha:0.25},0).wait(1).to({x:442.65,y:91.3,alpha:0.1111},0).wait(1).to({x:445.75,y:90.15,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:446.9,y:89.6,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 112
		this.instance_72 = new lib.Symbol112("synched",0);
		this.instance_72.parent = this;
		this.instance_72.setTransform(175.5,282.9);
		this.instance_72._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_72).wait(2).to({_off:false},0).wait(1).to({regX:-0.1,regY:0.1,x:186.45,y:274.85},0).wait(1).to({x:219.65,y:250.55},0).wait(1).to({x:274.95,y:210},0).wait(1).to({regX:0,regY:0,x:352.55,y:153.15},0).wait(1).to({regX:-0.1,regY:0.1,x:362.45,y:146.45,alpha:0.6944},0).wait(1).to({x:370.7,y:140.9,alpha:0.4444},0).wait(1).to({x:377.05,y:136.6,alpha:0.25},0).wait(1).to({x:381.65,y:133.55,alpha:0.1111},0).wait(1).to({x:384.35,y:131.7,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:385.4,y:131,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 111
		this.instance_73 = new lib.Symbol111("synched",0);
		this.instance_73.parent = this;
		this.instance_73.setTransform(159.2,280.8);
		this.instance_73._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_73).wait(2).to({_off:false},0).wait(1).to({regX:0.4,regY:-0.1,x:174.75,y:274.3},0).wait(1).to({x:220.2,y:255.2},0).wait(1).to({x:296,y:223.3},0).wait(1).to({regX:0,regY:0,x:401.7,y:178.8},0).wait(1).to({regX:0.4,regY:-0.1,x:426.75,y:179.1,alpha:0.6944},0).wait(1).to({x:446.95,y:179.45,alpha:0.4444},0).wait(1).to({x:462.65,y:179.7,alpha:0.25},0).wait(1).to({x:473.85,y:179.9,alpha:0.1111},0).wait(1).to({x:480.6,y:180,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:482.45,y:180.15,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 110
		this.instance_74 = new lib.Symbol110("synched",0);
		this.instance_74.parent = this;
		this.instance_74.setTransform(149.3,283.8);
		this.instance_74._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_74).wait(2).to({_off:false},0).wait(1).to({x:158.6438,y:279.8563},0).wait(1).to({x:186.675,y:268.025},0).wait(1).to({x:233.3938,y:248.3063},0).wait(1).to({x:298.8,y:220.7},0).wait(1).to({x:318.6458,y:221.1278,alpha:0.6944},0).wait(1).to({x:334.8833,y:221.4778,alpha:0.4444},0).wait(1).to({x:347.5125,y:221.75,alpha:0.25},0).wait(1).to({x:356.5333,y:221.9444,alpha:0.1111},0).wait(1).to({x:361.9458,y:222.0611,alpha:0.0278},0).wait(1).to({x:363.75,y:222.1,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 109
		this.instance_75 = new lib.Symbol109("synched",0);
		this.instance_75.parent = this;
		this.instance_75.setTransform(159.2,289.05);
		this.instance_75._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_75).wait(2).to({_off:false},0).wait(1).to({regX:0.4,regY:-0.2,x:163.8,y:287.4},0).wait(1).to({x:176.5,y:283.2},0).wait(1).to({x:197.65,y:276.15},0).wait(1).to({regX:0,regY:0,x:226.85,y:266.5},0).wait(1).to({regX:0.4,regY:-0.2,x:253.2,y:268.65,alpha:0.6944},0).wait(1).to({x:274.5,y:270.65,alpha:0.4444},0).wait(1).to({x:291,y:272.15,alpha:0.25},0).wait(1).to({x:302.85,y:273.25,alpha:0.1111},0).wait(1).to({x:309.9,y:273.9,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:311.9,y:274.35,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 108
		this.instance_76 = new lib.Symbol108("synched",0);
		this.instance_76.parent = this;
		this.instance_76.setTransform(180.1,293);
		this.instance_76._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_76).wait(2).to({_off:false},0).wait(1).to({x:200.6813,y:290.8844},0).wait(1).to({x:262.425,y:284.5375},0).wait(1).to({x:365.3313,y:273.9594},0).wait(1).to({x:509.4,y:259.15},0).wait(1).to({x:522.7222,y:261.7931,alpha:0.6944},0).wait(1).to({x:533.6222,y:263.9556,alpha:0.4444},0).wait(1).to({x:542.1,y:265.6375,alpha:0.25},0).wait(1).to({x:548.1556,y:266.8389,alpha:0.1111},0).wait(1).to({x:551.7889,y:267.5597,alpha:0.0278},0).wait(1).to({x:553,y:267.8,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 107
		this.instance_77 = new lib.Symbol107("synched",0);
		this.instance_77.parent = this;
		this.instance_77.setTransform(166.55,291.95);
		this.instance_77._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_77).wait(2).to({_off:false},0).wait(1).to({regX:0.3,regY:0.1,x:178.2,y:291.3},0).wait(1).to({x:212.4,y:289.05},0).wait(1).to({x:269.3,y:285.35},0).wait(1).to({regX:0,regY:0,x:348.75,y:280.1},0).wait(1).to({regX:0.3,regY:0.1,x:365.4,y:281.7,alpha:0.6944},0).wait(1).to({x:378.8,y:282.95,alpha:0.4444},0).wait(1).to({x:389.2,y:283.95,alpha:0.25},0).wait(1).to({x:396.65,y:284.6,alpha:0.1111},0).wait(1).to({x:401.1,y:285.05,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:402.3,y:285.1,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 106
		this.instance_78 = new lib.Symbol106("synched",0);
		this.instance_78.parent = this;
		this.instance_78.setTransform(180.75,302.45);
		this.instance_78._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_78).wait(2).to({_off:false},0).wait(1).to({x:193.0813,y:304.1094},0).wait(1).to({x:230.075,y:309.0875},0).wait(1).to({x:291.7313,y:317.3844},0).wait(1).to({x:378.05,y:329},0).wait(1).to({x:399.8819,y:338.3806,alpha:0.6944},0).wait(1).to({x:417.7444,y:346.0556,alpha:0.4444},0).wait(1).to({x:431.6375,y:352.025,alpha:0.25},0).wait(1).to({x:441.5611,y:356.2889,alpha:0.1111},0).wait(1).to({x:447.5153,y:358.8472,alpha:0.0278},0).wait(1).to({x:449.5,y:359.7,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 105
		this.instance_79 = new lib.Symbol105("synched",0);
		this.instance_79.parent = this;
		this.instance_79.setTransform(179.5,301.35);
		this.instance_79._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_79).wait(2).to({_off:false},0).wait(1).to({regX:0.2,regY:0.1,x:195.55,y:301.2},0).wait(1).to({x:243.15,y:300.6},0).wait(1).to({x:322.45,y:299.55},0).wait(1).to({regX:0,regY:0,x:433.3,y:298},0).wait(1).to({regX:0.2,regY:0.1,x:444.4,y:301.75,alpha:0.6944},0).wait(1).to({x:453.35,y:304.8,alpha:0.4444},0).wait(1).to({x:460.3,y:307.15,alpha:0.25},0).wait(1).to({x:465.25,y:308.85,alpha:0.1111},0).wait(1).to({x:468.25,y:309.85,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:469.05,y:310.1,alpha:0},0).to({_off:true},1).wait(37));
	
		// Symbol 104
		this.instance_80 = new lib.Symbol104("synched",0);
		this.instance_80.parent = this;
		this.instance_80.setTransform(178.5,300.1);
		this.instance_80._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_80).wait(2).to({_off:false},0).wait(1).to({regX:0.2,regY:-0.2,x:183.55,y:303.7},0).wait(1).to({x:198.15,y:315.25},0).wait(1).to({x:222.45,y:334.5},0).wait(1).to({regX:0,regY:0,x:256.35,y:361.65},0).wait(1).to({regX:0.2,regY:-0.2,x:276.6,y:371.7,alpha:0.6944},0).wait(1).to({x:293.05,y:380.1,alpha:0.4444},0).wait(1).to({x:305.8,y:386.65,alpha:0.25},0).wait(1).to({x:314.95,y:391.3,alpha:0.1111},0).wait(1).to({x:320.4,y:394.1,alpha:0.0278},0).wait(1).to({regX:0,regY:0,x:322.05,y:395.25,alpha:0},0).to({_off:true},1).wait(37));
	
		// Layer 11
		this.instance_81 = new lib.suc_Symbol331m("synched",2,false);
		this.instance_81.parent = this;
		this.instance_81.setTransform(264.25,-16.5,2.3403,2.3403);
		this.instance_81._off = true;
		this.instance_81.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 204, 0, 0)];
		this.instance_81.cache(-78,-78,157,156);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_81).wait(8).to({_off:false},0).wait(5).to({startPosition:12},0).to({alpha:0,startPosition:19},7).wait(2).to({startPosition:21},0).to({_off:true},1).wait(27));
	
		// Layer 10
		this.instance_82 = new lib.suc_Symbol331m("synched",7,false);
		this.instance_82.parent = this;
		this.instance_82.setTransform(402.35,319.85,1.5863,1.5863);
		this.instance_82._off = true;
		this.instance_82.filters = [new cjs.ColorFilter(0, 0, 0, 1, 51, 153, 0, 0)];
		this.instance_82.cache(-78,-78,157,156);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_82).wait(6).to({_off:false},0).wait(7).to({startPosition:21},0).to({alpha:0,startPosition:28},7).wait(2).to({startPosition:30},0).to({_off:true},1).wait(27));
	
		// Layer 9
		this.instance_83 = new lib.suc_Symbol331m("synched",0,false);
		this.instance_83.parent = this;
		this.instance_83.setTransform(226.75,196.15,1.1877,1.1877);
		this.instance_83._off = true;
		this.instance_83.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 0, 0, 0)];
		this.instance_83.cache(-78,-78,157,156);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_83).wait(4).to({_off:false},0).wait(9).to({startPosition:18},0).to({alpha:0,startPosition:25},7).wait(2).to({startPosition:27},0).to({_off:true},1).wait(27));
	
		// Symbol 102
		this.instance_84 = new lib.Symbol102();
		this.instance_84.parent = this;
		this.instance_84.setTransform(162.8,281.05,1.0479,1.0479,56.2181,0,0,59,12.8);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_84).to({scaleX:1.1472,rotation:55.5109,x:145.5,y:296.15},1,cjs.Ease.get(1)).to({regX:58.9,scaleX:0.9478,scaleY:1.0778,rotation:55.9376,x:170.1,y:277.3},1).to({regX:59,scaleX:0.8503,scaleY:1.0953,rotation:56.2178,x:182.5,y:267.8},2).to({scaleX:1.0479,scaleY:1.0479,rotation:56.2181,x:162.8,y:281.05},3).wait(15).to({_off:true},1).wait(27));
	
		// Layer 7
		this.instance_85 = new lib.Symbol127("synched",0);
		this.instance_85.parent = this;
		this.instance_85.setTransform(67.25,365.55);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_85).to({scaleX:1.1581,scaleY:0.7649,skewX:-10.6758,x:63.2,y:366.85},1,cjs.Ease.get(1)).to({scaleX:1.0527,scaleY:1.0074,skewX:14.8077,x:52.4,y:368.3},1).to({scaleX:1,scaleY:1.1299,skewX:27.7417,x:47,y:369},2).to({scaleY:1,skewX:0,x:67.25,y:365.55},3).wait(15).to({startPosition:0},0).to({_off:true},1).wait(27));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(0,-194.7,754.5,735.5);
	
	
	(lib.Symbol84 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// timeline functions:
		this.frame_25 = function() {
			this.stop();
		}
	
		// actions tween:
		this.timeline.addTween(cjs.Tween.get(this).wait(25).call(this.frame_25).wait(1));
	
		// Layer 6
		this.instance = new lib.Symbol92("synched",0);
		this.instance.parent = this;
		this.instance.setTransform(175.15,151.2,1.8,1.8,0,0,0,-13,27.1);
		this.instance._off = true;
	
		this.instance_1 = new lib.Symbol129("synched",0);
		this.instance_1.parent = this;
		this.instance_1.setTransform(281.6,184.55);
		this.instance_1.alpha = 0.6094;
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},2).to({state:[]},1).to({state:[{t:this.instance_1}]},4).wait(16));
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off:false},0).to({scaleX:1.4311,scaleY:1.4311},2,cjs.Ease.get(-1)).to({_off:true},1).wait(20));
	
		// Layer 2
		this.instance_2 = new lib.Symbol129("synched",0);
		this.instance_2.parent = this;
		this.instance_2.setTransform(179.6,20.6);
		this.instance_2.alpha = 0.6094;
		this.instance_2._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).wait(20));
	
		// Symbol 103
		this.instance_3 = new lib.Symbol103("single",0);
		this.instance_3.parent = this;
		this.instance_3.setTransform(-15.05,151.85,0.7095,0.7095,0,0,0,258.1,210);
		this.instance_3.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:4.95,y:131.85,alpha:1,mode:"synched",loop:false},3).wait(11).to({startPosition:11},0).to({y:151.85,alpha:0,startPosition:15},4).wait(8));
	
		// Symbol 103
		this.instance_4 = new lib.Symbol103("single",0);
		this.instance_4.parent = this;
		this.instance_4.setTransform(416.05,170.85,0.7095,0.7095,0,0,180,258.1,210);
		this.instance_4.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_4).to({x:377.05,y:131.85,alpha:1,mode:"synched",loop:false},3).wait(11).to({startPosition:11},0).to({y:151.85,alpha:0,startPosition:15},4).wait(8));
	
		// Layer 4
		this.instance_5 = new lib.Symbol92("synched",0);
		this.instance_5.parent = this;
		this.instance_5.setTransform(175.15,151.15,0.3,0.3,0,0,0,-13,27);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_5).to({regY:27.1,scaleX:1.8,scaleY:1.8,y:151.2},3,cjs.Ease.get(1)).to({regY:27,scaleX:0.97,scaleY:0.97,y:151.15},3,cjs.Ease.get(-1)).to({regY:27.1,scaleX:1,scaleY:1,y:151.2},3).wait(17));
	
		// Layer 1
		this.instance_6 = new lib.Symbol99("synched",0);
		this.instance_6.parent = this;
		this.instance_6.setTransform(28.35,43.65);
	
		this.instance_7 = new lib.CachedTexturedBitmap_475();
		this.instance_7.parent = this;
		this.instance_7.setTransform(320.8,167.95);
	
		this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7},{t:this.instance_6}]}).wait(26));
	
		// Layer 5
		this.instance_8 = new lib.Symbol98copy("synched",0);
		this.instance_8.parent = this;
		this.instance_8.setTransform(397.65,191.05);
	
		this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(26));
	
		// Symbol 96
		this.instance_9 = new lib.Symbol96copy("synched",0);
		this.instance_9.parent = this;
		this.instance_9.setTransform(479.65,144.45);
		this.instance_9.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_9).to({x:405.75,alpha:1},6,cjs.Ease.get(1)).wait(20));
	
		// Symbol 95
		this.instance_10 = new lib.Symbol95("synched",0);
		this.instance_10.parent = this;
		this.instance_10.setTransform(302.25,66.05);
		this.instance_10.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_10).to({x:264.75,alpha:1},6,cjs.Ease.get(1)).wait(20));
	
		// Symbol 94
		this.instance_11 = new lib.Symbol94copy("synched",0);
		this.instance_11.parent = this;
		this.instance_11.setTransform(4.85,67.45);
		this.instance_11.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_11).to({x:24.6,alpha:1},6,cjs.Ease.get(1)).wait(20));
	
		// Symbol 93
		this.instance_12 = new lib.Symbol93("synched",0);
		this.instance_12.parent = this;
		this.instance_12.setTransform(-73.75,150.95);
		this.instance_12.alpha = 0;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_12).to({x:-34.7,alpha:1},6,cjs.Ease.get(1)).wait(20));
	
		// Layer 3
		this.instance_13 = new lib.Symbol97copy("synched",0);
		this.instance_13.parent = this;
		this.instance_13.setTransform(185.5,123);
		this.instance_13.alpha = 0;
		this.instance_13._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(6).to({_off:false},0).to({y:127.2,alpha:1},6,cjs.Ease.get(1)).wait(14));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(-173.7,-135.3,746.0999999999999,506.90000000000003);
	
	
	(lib.Symbol7 = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// timeline functions:
		this.frame_0 = function() {
			this.stop();
		}
	
		// actions tween:
		this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));
	
		// Layer_1
		this.instance = new lib.Symbol84();
		this.instance.parent = this;
		this.instance.setTransform(641.75,395.65,1,1,0,0,0,189.9,116);
		this.instance._off = true;
	
		this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(0,0,1024.2,581.8);
	
	
	// stage content:
	(lib.writing = function(mode,startPosition,loop) {
		this.initialize(mode,startPosition,loop,{});
	
		// popMc
		this.popMc = new lib.Symbol4();
		this.popMc.name = "popMc";
		this.popMc.parent = this;
		this.popMc.setTransform(163.6,114.4,1,1,0,0,0,163.6,114.4);
	
		this.timeline.addTween(cjs.Tween.get(this.popMc).wait(1));
	
		// goodMc
		this.goodMc = new lib.Symbol7();
		this.goodMc.name = "goodMc";
		this.goodMc.parent = this;
		this.goodMc.setTransform(640,400,1,1,0,0,0,640,400);
	
		this.timeline.addTween(cjs.Tween.get(this.goodMc).wait(1));
	
		// icon2
		this.icon2 = new lib.Symbol70copy2();
		this.icon2.name = "icon2";
		this.icon2.parent = this;
		this.icon2.setTransform(1204.95,111.4);
	
		this.timeline.addTween(cjs.Tween.get(this.icon2).wait(1));
	
	}).prototype = p = new cjs.MovieClip();
	p.nominalBounds = new cjs.Rectangle(1785.3,457.4,-575.2,-354.9);
	// library properties:
	lib.properties = {
		id: '5CBD32883315A04494BA8B63ACB2358B',
		width: 1280,
		height: 800,
		fps: 30,
		color: "#FFFFFF",
		opacity: 1.00,
		manifest: [
			{src:"../common/images/cc/writing_atlas_.png", id:"writing_atlas_"},
			{src:"../common/images/cc/writing_atlas_2.png", id:"writing_atlas_2"}
		],
		preloads: []
	};
	
	
	
	// bootstrap callback support:
	
	(lib.Stage = function(canvas) {
		createjs.Stage.call(this, canvas);
	}).prototype = p = new createjs.Stage();
	
	p.setAutoPlay = function(autoPlay) {
		this.tickEnabled = autoPlay;
	}
	p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
	p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
	p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
	p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }
	
	p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }
	
	an.bootcompsLoaded = an.bootcompsLoaded || [];
	if(!an.bootstrapListeners) {
		an.bootstrapListeners=[];
	}
	
	an.bootstrapCallback=function(fnCallback) {
		an.bootstrapListeners.push(fnCallback);
		if(an.bootcompsLoaded.length > 0) {
			for(var i=0; i<an.bootcompsLoaded.length; ++i) {
				fnCallback(an.bootcompsLoaded[i]);
			}
		}
	};
	
	an.compositions = an.compositions || {};
	an.compositions['5CBD32883315A04494BA8B63ACB2358B'] = {
		getStage: function() { return exportRoot.getStage(); },
		getLibrary: function() { return lib; },
		getSpriteSheet: function() { return ss; },
		getImages: function() { return img; }
	};
	
	an.compositionLoaded = function(id) {
		an.bootcompsLoaded.push(id);
		for(var j=0; j<an.bootstrapListeners.length; j++) {
			an.bootstrapListeners[j](id);
		}
	}
	
	an.getComposition = function(id) {
		return an.compositions[id];
	}
	
	
	
	})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
	var createjs, AdobeAn;