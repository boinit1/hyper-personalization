<%@page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page import="java.io.*"%>

<%@page import="java.util.UUID"%>

<%@page import="java.text.SimpleDateFormat"%>

<%
	
	//파일정보
	String sFileInfo = "";
	//파일명을 받는다 - 일반 원본파일명
	String filename = request.getHeader("file-name");
	//파일 확장자
	String filename_ext = filename.substring(filename.lastIndexOf(".")+1);
	//확장자를소문자로 변경
	filename_ext = filename_ext.toLowerCase();
		
	//이미지 검증 배열변수
	String[] allow_file = {"jpg","png","bmp","gif"};
	
	//돌리면서 확장자가 이미지인지 
	int cnt = 0;
	for(int i=0; i<allow_file.length; i++) {
		if(filename_ext.equals(allow_file[i])){
			cnt++;
		}
	}
	
	//이미지가 아님
	if(cnt == 0) {
		out.println("NOTALLOW_"+filename);
	} else {
	//이미지이므로 신규 파일로 디렉토리 설정 및 업로드	
	
	String dftFilePath = EgovProperties.getProperty("file.root.path");						//파일 기본경로
	String detailFilePath = EgovProperties.getProperty("editor.root.dir") + File.separator;	//파일 상세 경로
	
	//파일 기본경로 _ 상세경로
	String filePath = dftFilePath + detailFilePath;
	File file = new File(filePath);
	if(!file.exists()) {
		file.mkdirs();
	}
	String realFileNm = "";
	SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
	String today= formatter.format(new java.util.Date());
	realFileNm = today+UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf(".")).trim();
	String rlFileNm = filePath + realFileNm;
	///////////////// 서버에 파일쓰기 ///////////////// 
	InputStream is = request.getInputStream();
	OutputStream os=new FileOutputStream(rlFileNm);
	int numRead;
	byte b[] = new byte[Integer.parseInt(request.getHeader("file-size"))];
	while((numRead = is.read(b,0,b.length)) != -1){
		os.write(b,0,numRead);
	}
	if(is != null) {
		is.close();
	}
	os.flush();
	os.close();
	///////////////// 서버에 파일쓰기 /////////////////
	
	// 정보 출력
	sFileInfo += "&bNewLine=true";
	//sFileInfo += "&sFileName="+ realFileNm;;
	// img 태그의 title 속성을 원본파일명으로 적용시켜주기 위함
	sFileInfo += "&sFileURL=/resources/" + EgovProperties.getProperty("editor.root.dir") +"/"+ realFileNm;
	sFileInfo += "&sFileName="+ filename;
	sFileInfo += "&a=a";
	out.println(sFileInfo);
	}
%>
