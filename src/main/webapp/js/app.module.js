var Module = Module || {};

Module.common = (function() {
	function _isNull(obj) {
		if (typeof obj === 'undefined' || obj === null || obj === 'null') {
			return true;
		}
		return false;
	};
	function _isEmpty(obj) {
		if (_isNull(obj)) {
			return true;
		}
		if (Array.isArray(obj)) {
			if (!obj.length) {
				return true;
			}
			return false;
		}
		if (typeof obj === 'object') {
			if (!Object.keys(obj).length) {
				return true;
			}
			return false;
		}
		var str = String(obj).trim();
		if (str.length <= 0) {
			return true;
		}
		if (str === 'NaN') {
			return true;
		}
		return false;
	};
	function _promiseForAjax(url, param) {
		if (_isEmpty(url)) {
			return;
		}
		return new Promise(function(resolve, reject) {
			var promise = $.ajax({
				url: url,
				type: 'post',
				data: JSON.stringify(param),
				contentType: 'application/json',
				timeout: 10000,
				beforeSend: function(jqXHR) {
					jqXHR.setRequestHeader('Accept', 'application/json');
					jqXHR.setRequestHeader('AJAX', true);
					$("body").LoadingOverlay("show", Module.ui.loadingSetup());
				}
			}).done(function (data, textStatus, xhr) {
				$("body").LoadingOverlay("hide", true);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$("body").LoadingOverlay("hide", true);
				vex.dialog.confirm({
					message: '오류가 발생되었습니다.',
					buttons: [
						$.extend({}, vex.dialog.buttons.NO, {text: '확인'})
					],
					callback: function (isOk) {}
				});
			});
			promise.then(resolve, reject);
		});
	};
	function _fileUpload(url, formData, callback) {
		if (_isEmpty(url) && _isEmpty(formData)) {
			return;
		}
		$.ajax({
			type: "POST",
			enctype: 'multipart/form-data',
			url: url,
			data: formData,
			processData: false,
			contentType: false,
			cache: false,
			timeout: 6000000,
			beforeSend: function(jqXHR) {
				$("body").LoadingOverlay("show", Module.ui.loadingSetup());
			},
			complete: function() {
				$("body").LoadingOverlay("hide", true);
			},
			xhr: function(){
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.onprogress = function(e){
					var per = e.loaded * 100 / e.total;
					console.log(per);
				};
				return xhr;
			},
			success: function (result) {
				if ('0000' == result.resultCode) {
					if (typeof callback === 'function') {
						callback(result);
					}
				} else {
					if (Module.common.isEmpty(result.errorMessage)) {
						result = {};
						result.errorMessage = '오류가 발생되었습니다.';
					}
					vex.dialog.confirm({
						message: result.errorMessage,
						buttons: [
							$.extend({}, vex.dialog.buttons.NO, {text: '확인'})
						],
						callback: function (isOk) {}
					});
				}
			},
			error: function (e) {
				$("body").LoadingOverlay("hide", true);
				
				vex.dialog.confirm({
					message: '오류가 발생되었습니다.',
					buttons: [
						$.extend({}, vex.dialog.buttons.NO, {text: '확인'})
					],
					callback: function (isOk) {}
				});
				
				vex.dialog.confirm({
					message: '오류가 발생되었습니다.',
					buttons: [
						$.extend({}, vex.dialog.buttons.NO, {text: '확인'})
					],
					callback: function (isOk) {}
				});
			}
		});
	};
	function _excelDownload(url, param) {
		$.ajax({
			url: url,
			cache: false,
			type: 'post',
			data: JSON.stringify(param),
			contentType: 'application/json',
			beforeSend: function(jqXHR) {
				jqXHR.setRequestHeader('Accept', 'application/json');
				jqXHR.setRequestHeader('AJAX', true);
				$("body").LoadingOverlay("show", Module.ui.loadingSetup());
			},
			complete: function() {
				$("body").LoadingOverlay("hide", true);
			},
			success: function (data) {
				if ('success' == data.resultCode) {
					var link = document.createElement('a');
					link.href='/admin/api/files/excel/download/' + data.resultData.fileId;
					link.click();
					link.remove();
				}
			},
			error: function (e) {
				$("body").LoadingOverlay("hide", true);
				vex.dialog.confirm({
					message: '오류가 발생되었습니다.',
					buttons: [
						$.extend({}, vex.dialog.buttons.NO, {text: '확인'})
					],
					callback: function (isOk) {}
				});
			}
		});
	};
	function _byteCalculation(bytes) {
		var bytes = parseInt(bytes);
		var s = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
		var e = Math.floor(Math.log(bytes)/Math.log(1024));
		if (e == "-Infinity") {
			return "0 "+s[0];
		} else {
			return (bytes/Math.pow(1024, Math.floor(e))).toFixed(2)+" "+s[e];
		}
	};
	return {
		isEmpty: _isEmpty,
		promiseForAjax: _promiseForAjax,
		fileUpload: _fileUpload,
		excelDownload: _excelDownload,
		byteCalculation: _byteCalculation
	};
}());

Module.string = (function() {

}());

Module.ui = (function() {
	function _gridTheme() {
		const options = {
			selection: {
				background: '#4daaf9',
				border: '#004082'
			},
			scrollbar: {
				background: '#f5f5f5',
				thumb: '#d9d9d9',
				active: '#c1c1c1'
			},
			row: {
				even: {
					background: '#f6f6f7'
				}
//				,
//				hover: {
//					background: '#00cbb4'
//				}
			},
			cell: {
				normal: {
					background: '#fbfbfb',
					border: '#e0e0e0',
					showVerticalBorder: true
				},
				header: {
					background: '#565656',
					border: '#c3c3c3',
					text: '#fff',
					showVerticalBorder: true
				},
				rowHeader: {
					background: '#565656',
					border: '#c3c3c3',
					text: '#fff',
					showVerticalBorder: true
				},
				selectedHeader: {
					background: '#d8d8d8'
				},
				focused: {
					border: '#00cbb4'
				}
			}
		};
		tui.Grid.applyTheme('striped', options);
	};
	function _gridFitHeight(target, isPaging) {
		if (typeof target != 'undefined' && target != null
				&& target != '' && typeof target == 'object') {
			var target = $('#grid');
			var obj = $('.comnWrap');
			if (target.offset()) {
				var targetTop = target.offset().top;
				var headerHeight = 40;
				var paginationHeight = 28 + 20 + 12 - 5;
				var targetHeight = obj.height() - targetTop - headerHeight;
				return isPaging? (targetHeight - paginationHeight) : targetHeight + 5;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	};
	function _loadingSetup() {
		return {
			image: "/img/loading.svg",
			imageAnimation: false,
			imageAutoResize: true,
			imageResizeFactor: 1,
			textColor: "#006c45",
		}
	};
	function _dialog(message, buttons, callback) {
		if (Module.common.isEmpty(buttons)) {
			buttons = [
				$.extend({}, vex.dialog.buttons.NO, {text: '닫기'})
			]
		}
		if (Module.common.isEmpty(callback)) {
			vex.dialog.confirm({
				unsafeMessage: message,
				buttons: buttons,
				callback: function(){}
			});
		} else {
			vex.dialog.confirm({
				unsafeMessage: message,
				buttons: buttons,
				callback: callback
			});
		}
	};
	function _toast(type, title, text, position, onClose) {
		if (Module.common.isEmpty(type)) {
			return;
		}

		// tm, tl, ml, mm, ml, br, bm, bl
		if (Module.common.isEmpty(position)) {
			position = 'tr';
		}
		tata[type](title, text, {
			duration: 3 * 1000,
			position: position,
			progress: true,
			holding: false,
			animate: 'fade', // slide or fade
			closeBtn: true,
			onClick: null,
			onClose: onClose
		});
	};
	function _drawSelectbox(id, source, param, setting, callback) {
		if (Module.common.isEmpty(id) && Module.common.isEmpty(param)) {
			return;
		}
		/*
		setting: {
			option: {label: '전체', value: 'all'},
			selected: {target: 'all'}
		}
		*/
		$("#" + id).empty();
		if (!Module.common.isEmpty(setting)) {
			var selected = '';
			if (!Module.common.isEmpty(setting.selected)) {
				if (setting.selected.target == setting.option.value) {
					selected = 'selected';
				}
			}
			if (!Module.common.isEmpty(setting.option)) {
				$("#" + id).append('<option ' + selected + ' value="' + setting.option.value + '">' + setting.option.label + '</option');
			}
		}
		switch(source) {
			case 'code':
				Module.common.promiseForAjax('/admin/api/commmon/code/' + param.codeGroup)
				.then(function(result) {
					if ('success' == result.resultCode) {
						$.each( result.resultData, function(i, item) {
							var selected = '';
							if (!Module.common.isEmpty(setting.selected)) {
								if (setting.selected.target == item.code) {
									selected = 'selected';
								}
							}
							$("#" + id).append('<option ' + selected + ' value="' + item.code + '">' + item.codeName + '</option');
						});

						if (typeof callback === 'function') {
							callback(result.resultData);
						}
					}
				});
				break;
			case 'curriculum':
				Module.common.promiseForAjax('/admin/api/textbook/curriculum/list', param)
				.then(function(result) {
					if ('success' == result.resultCode) {
						$.each( result.resultData, function(i, item) {
							var selected = '';
							if (!Module.common.isEmpty(setting.selected)) {
								if (setting.selected.target == item.textbookSeq) {
									selected = 'selected';
								}
							}
							var value;
							var label;
							if ('LEVEL' == param.type) {
								value = item.textbookCurriculumLevelCd;
								label = item.textbookCurriculumLevelName;
							} else if ('REV' == param.type) {
								value = item.textbookCurriculumRevCd;
								label = item.textbookCurriculumRevName;
							} else if ('COURSE' == param.type) {
								value = item.textbookCurriculumCourseCd;
								label = item.textbookCurriculumCourseName;
							} else if ('GRADE' == param.type) {
								value = item.textbookCurriculumGradeCd;
								label = item.textbookCurriculumGradeName;
							} else if ('TERM' == param.type) {
								value = item.textbookCurriculumTermCd;
								label = item.textbookCurriculumTermName;
							}
							$("#" + id).append('<option ' + selected + ' value="' + value + '">' + label + '</option');
						});

						if (typeof callback === 'function') {
							callback(result.resultData);
						}
					}
				});
				break;
			case 'textbook':
				Module.common.promiseForAjax('/admin/api/textbook/list', param)
				.then(function(result) {
					if ('success' == result.resultCode) {
						$.each( result.resultData, function(i, item) {
							var selected = '';
							if (!Module.common.isEmpty(setting.selected)) {
								if (setting.selected.target == item.textbookSeq) {
									selected = 'selected';
								}
							}
							$("#" + id).append('<option ' + selected + ' value="' + item.textbookSeq + '">' + item.textbookTitle + '</option');
						});

						if (typeof callback === 'function') {
							callback(result.resultData);
						}
					}
				});
				break;
			case 'lesson':
				Module.common.promiseForAjax('/admin/api/textbook/lesson/list', param)
				.then(function(result) {
					if ('success' == result.resultCode) {
						$.each( result.resultData, function(i, item) {
							var selected = '';
							if (!Module.common.isEmpty(setting.selected)) {
								if (setting.selected.target == item.textbookSeq) {
									selected = 'selected';
								}
							}

							var label;
							var value = item.textbookLessonSeq;
							if ('UNIT_HIGH' == item.textbookLessonTypeCd) {
								label = item.textbookLessonUnitHighNum + '.' + item.textbookLessonUnitHighName;
							} else if ('UNIT_MID' == item.textbookLessonTypeCd) {
								if (item.textbookLessonUnitMidNum) {
									label = item.textbookLessonUnitMidNum + '.' + item.textbookLessonUnitMidName;
								} else {
									label = item.textbookLessonUnitMidName;
								}
							} else if ('UNIT_LOW' == item.textbookLessonTypeCd) {
								label = item.textbookLessonUnitLowNum + '.' + item.textbookLessonUnitLowName;
							} else if ('PERIOD' == item.textbookLessonTypeCd) {
								label = '[' + item.textbookLessonPeriodNum + '] ' + item.textbookLessonPeriodName;
							}

							$("#" + id).append('<option ' + selected + ' value="' + value + '">' + label + '</option');
						});

						if (typeof callback === 'function') {
							callback(result.resultData);
						}
					}
				});
				break;
		}

	};
	return {
		gridTheme: _gridTheme,
		gridFitHeight: _gridFitHeight,
		loadingSetup: _loadingSetup,
		dialog: _dialog,
		toast: _toast,
		drawSelectbox: _drawSelectbox
	}
}());

class RowNumberRenderer {
	constructor(props) {
		var el = document.createElement('span');
		this.el = el;
		// el.innerHTML = `${props.formattedValue}`;
		this.el.innerHTML = this.getRowNum(props);
	}

	getElement() {
		return this.el;
	}

	getRowNum(props) {
		var pageInfo = props.grid.getPagination();
		var currentPage = pageInfo._currentPage;
		return Number(props.formattedValue) + (currentPage - 1) * pageInfo._options.itemsPerPage;
	}

	render(props) {
		// this.el.innerHTML = `${props.formattedValue}`;
		this.el.innerHTML = this.getRowNum(props);
	}
}

class CheckboxRenderer {
	constructor(props) {
		const { grid, rowKey } = props;
		const label = document.createElement('label');
		label.className = 'checkbox';
		label.setAttribute('for', String(rowKey));

		const hiddenInput = document.createElement('input');
		hiddenInput.className = 'hidden-input';
		hiddenInput.id = String(rowKey);

		const customInput = document.createElement('span');
		customInput.className = 'custom-input';

		label.appendChild(hiddenInput);
		label.appendChild(customInput);

		hiddenInput.type = 'checkbox';
		hiddenInput.addEventListener('change', () => {
			if (hiddenInput.checked) {
				grid.check(rowKey);
			} else {
				grid.uncheck(rowKey);
			}
		});

		this.el = label;

		this.render(props);
	}

	getElement() {
		return this.el;
	}

	render(props) {
		const hiddenInput = this.el.querySelector('.hidden-input');
		const checked = Boolean(props.value);
		hiddenInput.checked = checked;
	}
}