/**
 * Convert a single file-input element into a 'multiple' input list
 * Usage:
 *
 *   1. Create a file input element (no name)
 *      eg. <input type="file" id="first_file_element">
 *
 *   2. Create a DIV for the output to be written to
 *      eg. <div id="files_list"></div>
 *
 *   3. Instantiate a MultiSelector object, passing in the DIV and an (optional) maximum number of files
 *      eg. var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 3 );
 *
 *   4. Add the first element
 *      eg. multi_selector.addElement( document.getElementById( 'first_file_element' ) );
 */

function MultiSelector( list_target, max ){

	// Where to write the list
	this.list_target = list_target;
	// How many elements?
	this.maxSize = 0;
	this.count = 0;
	// How many elements?
	this.id = 0;
	// Is there a maximum?
	if( max ){
		this.max = max * 1024 * 1024;//최대 100MB
	} else {
		this.max = -1;
	};
	
	/**
	 * Add a new file input element
	 */
	this.addElement = function( element ){

		// Make sure it's a file input element
		if( element.tagName == 'INPUT' && element.type == 'file' ){

			// Element name -- what number am I?
			element.name = 'f_' + (this.id++);

			// Add reference to this object
			element.multi_selector = this;

			// What to do when a file is selected
			element.onchange = function(){

				debugger;
			    if(!this.multi_selector.chkFileSize(this.files[0].size)){
			        return;
			    }
			    if(!this.multi_selector.chkFileType(this.files[0].name.split('.').pop())){
			        return;
			    }
				// New file input
				var new_element = document.createElement( 'input' );
				new_element.type = 'file';
				new_element.name = this.files[0].name;

				// Add new element
				this.parentNode.insertBefore( new_element, this );

				// Apply 'update' to element
				this.multi_selector.addElement( new_element );

				// Update list
				this.multi_selector.addListRow( this );
				
				// Hide this: we can't use display:none because Safari doesn't like it
				this.style.position = 'absolute';
				this.style.left = '-1000px';
				this.style.top = '-1000px';
				this.style.display = 'none';
				this.style.visibility = 'hidden';
				this.style.width = '0';
				this.style.height = '0';
				this.style.overflow = 'hidden';

				new_element.onkeypress = function(){
					return false;
				};

				// File element counter
				this.multi_selector.maxSize += this.files[0].size;
				
			};
			// If we've reached maximum number, disable input element
			if( this.max != -1 && this.maxSize >= this.max ){
				element.disabled = true;
			};

			// File element counter
			this.count++;
			// Most recent element
			this.current_element = element;
			
		} else {
			// This can only be applied to file input elements!
			alert( 'Error: not a file input element' );
		};

	};

	/**
	 * Add a new row to the list of files
	 */
	this.addListRow = function( element ){

		// Row div
		var new_row = document.createElement( 'li' );

		// checkbox for delete
		var new_row_checklabel = document.createElement( 'label' );
		new_row_checklabel.className = 'chkN';
		var new_row_checkbox = document.createElement( 'input' );
		new_row_checkbox.type = 'checkbox';
		new_row_checkbox.id = "f" + element.multi_selector.id;
		new_row_checklabel.appendChild( new_row_checkbox );
		// label for fileName
		var new_row_label = document.createElement( 'label' );
		new_row_label.for = new_row_checkbox.id;
		// Set row value
		var Name = element.files[0].name;
	    var Size = element.files[0].size/1024/1024;
	    var ModificationDate = element.files[0].lastModifiedDate;
		new_row_label.innerHTML = '<p>' + Name + '</p>' + '<p>' + Size.toFixed(3) + ' MB</p>';

		// References
		new_row.element = element;

		// Add childs
		new_row.appendChild( new_row_checklabel );
		new_row.appendChild( new_row_label );

		// Add it to the list
		this.list_target.appendChild( new_row );
	};
	
	this.chkFileType = function (ext) {
        var isAllow = false;

        if (ext) {
            switch (ext.toUpperCase()) {
                case 'XLSX':
                case 'XLS':
                case 'PPT':
                case 'PPTX':
                case 'HWP':
                case 'DOC':
                case 'DOCX':
                case 'ZIP':
                case 'PDF':
                    isAllow = true;
                    break;
            }
        }
        if (!isAllow) {
            alert('xlsx, xls, ppt, pptx, hwp, doc, docx, zip, pdf 파일만 업로드 가능합니다.');
        }

        return isAllow;
    }
	
	this.chkFileSize = function (size) {
        var isAllow = false;

        if (size > (10485760 * 2)) {
            alert('업로드 파일 용량은 20MB 이하만 가능합니다.');
            return false;
        } else {
            return true;
        }
    }
};

function MultiSelector2( list_target, max ){

	// Where to write the list
	this.list_target = list_target;
	// How many elements?
	this.count = 0;
	// How many elements?
	this.id = 0;
	// Is there a maximum?
	
	if( max ){
		this.max = max;
	} else {
		this.max = -1;
	};
	
	/**
	 * Add a new file input element
	 */
	this.addElement = function( element){

		// Make sure it's a file input element
		if( element.tagName == 'INPUT' && element.type == 'file' ){

			// Element name -- what number am I?
			//element.name = this.id;

			// Add reference to this object
			element.multi_selector = this;
			
			element.multiple = true;

			// What to do when a file is selected
			element.onchange = function(){

				// New file input
				var new_element = document.createElement( 'input' );
				new_element.type = 'file';
				new_element.name = this.name;

				// Add new element
				this.parentNode.appendChild( new_element, this );

				// Apply 'update' to element
				this.multi_selector.addElement( new_element);

				// Update list
				this.multi_selector.addListRow( this );
				
				// Hide this: we can't use display:none because Safari doesn't like it
				this.style.position = 'absolute';
				this.style.left = '-1000px';
				this.style.top = '-1000px';
				this.style.display = 'none';
				this.style.visibility = 'hidden';
				this.style.width = '0';
				this.style.height = '0';
				this.style.overflow = 'hidden';

				new_element.onkeypress = function(){
					return false;
				};

			};
			// If we've reached maximum number, disable input element
			if( this.max != -1 && this.count >= this.max ){
				element.disabled = true;
			};

			// File element counter
			this.count++;
			// Most recent element
			this.current_element = element;
			
		} else {
			// This can only be applied to file input elements!
			alert( 'Error: not a file input element' );
		};

	};

	/**
	 * Add a new row to the list of files
	 */
	this.addListRow = function( element ){

		// Row div
		var new_row = document.createElement( 'div' );

		// Delete button
		var new_row_button = document.createElement( 'input' );
		new_row_button.type = 'button';
		new_row_button.value = 'Delete';

		// References
		new_row.element = element;

		// Delete function
		new_row_button.onclick= function(){

			// Remove element from form
			this.parentNode.element.parentNode.removeChild( this.parentNode.element );

			// Remove this row from the list
			this.parentNode.parentNode.removeChild( this.parentNode );

			// Decrement counter
			this.parentNode.element.multi_selector.count--;

			// Re-enable input element (if it's disabled)
			this.parentNode.element.multi_selector.current_element.disabled = false;

			//    which nixes your already queued uploads
			return false;
		};

		// Set row value
		new_row.innerHTML = element.value;

		// Add button
		new_row.appendChild( new_row_button );

		// Add it to the list
		this.list_target.appendChild( new_row );
	};

};


function SingleSelector( list_target, max ){
	this.max = 1;

	// Where to write the list
	this.list_target = list_target;
	// How many elements?
	this.count = 0;
	// How many elements?
	this.id = 0;
	// Is there a maximum?
	
	if( max ){
		this.max = max;
	} else {
		this.max = -1;
	};
	
	/**
	 * Add a new file input element
	 */
	this.addElement = function( element){
		//debugger;
		// Make sure it's a file input element
		if( element.tagName == 'INPUT' && element.type == 'file' ){

			// Element name -- what number am I?
			//element.name = this.id;

			// Add reference to this object
			element.multi_selector = this;			
			element.multiple = false;
			// What to do when a file is selected
			element.onchange = function(){
				// New file input
				var new_element = document.createElement( 'input' );
				new_element.type = 'file';
				new_element.name = this.name;
				// Add new element
				this.parentNode.appendChild( new_element, this );
				// Apply 'update' to element
				this.multi_selector.addElement( new_element);
				// Update list
				this.multi_selector.addListRow( this );				
				// Hide this: we can't use display:none because Safari doesn't like it
				this.style.position = 'absolute';
				this.style.left = '-1000px';
				this.style.top = '-1000px';
				this.style.display = 'none';
				this.style.visibility = 'hidden';
				this.style.width = '0';
				this.style.height = '0';
				this.style.overflow = 'hidden';

				new_element.onkeypress = function(){
					return false;
				};
			};
			// If we've reached maximum number, disable input element
			if( this.max != -1 && this.count >= this.max ){
				element.disabled = true;
			};

			// File element counter
			this.count++;
			// Most recent element
			this.current_element = element;
		} else {
			// This can only be applied to file input elements!
			alert( 'Error: not a file input element' );
		};

	};

	/**
	 * Add a new row to the list of files
	 */
	this.addListRow = function( element ){
		if(!element.files[0])
			return false;

		debugger;
		// Row div
		var new_row = document.createElement( 'li' );

		// checkbox for delete
		var new_row_checklabel = document.createElement( 'label' );
		new_row_checklabel.className = 'chkN';
		var new_row_checkbox = document.createElement( 'input' );
		new_row_checkbox.type = 'checkbox';
		new_row_checkbox.id = "f" + element.multi_selector.id;
		new_row_checklabel.appendChild( new_row_checkbox );
		// label for fileName
		var new_row_label = document.createElement( 'label' );
		new_row_label.for = new_row_checkbox.id;
		// Set row value
		var Name = element.files[0].name;
	    var Size = element.files[0].size/1024/1024;
	    var ModificationDate = element.files[0].lastModifiedDate;

		new_row_label.innerHTML = '<span>(' + Size.toFixed(3) + ' MB)</span>' + '<span>' + Name + '</span>';

		// Delete button
		var new_row_button = document.createElement( 'label' );
		new_row_button.classList.add("fileDelete");

		// References
		new_row.element = element;
		// Delete function
		new_row_button.onclick= function(){
			// Remove element from form
			this.parentNode.element.parentNode.removeChild( this.parentNode.element );
			// Remove this row from the list
			this.parentNode.parentNode.removeChild( this.parentNode );
			// Decrement counter
			this.parentNode.element.multi_selector.count--;
			// Re-enable input element (if it's disabled)
			//this.parentNode.element.multi_selector.current_element.disabled = false;
			//    which nixes your already queued uploads
			return false;
		};

		// Add childs
		new_row.appendChild( new_row_checklabel );
		new_row.appendChild( new_row_label );
		new_row.appendChild( new_row_button );
		
		//기존 리스트 삭제
		//this.list_target.innerHTML = '';
		
		// Add it to the list
		this.list_target.appendChild( new_row );
	};

};

function readImage(input) {
	var preview = input.getAttribute("data-preview");
	if(!preview) { preview = "preview-image";}
		
    // 인풋 태그에 파일이 있는 경우
    if(input.files && input.files[0]) {
        // 이미지 파일인지 검사 (생략)
        // FileReader 인스턴스 생성
        const reader = new FileReader()
        // 이미지가 로드가 된 경우
        reader.onload = e => {
            const previewImage = document.getElementById(preview)
            previewImage.src = e.target.result
        }
        // reader가 이미지 읽도록 하기
        reader.readAsDataURL(input.files[0])
    }
}

