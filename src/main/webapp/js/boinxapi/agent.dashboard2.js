/**
 * author: 김동현
 */

let observer = new MutationObserver((mutations) => {
	for(var i=0 ; i<mutations.length ; i++){
		if(mutations[i].type == "attributes"){
			if(mutations[i].target.tagName == "INPUT"){			
				if(mutations[i].attributeName == "data-value" || mutations[i].attributeName == "value"){
					if(mutations[i].oldValue == mutations[i].target.value){
						// 넘어감
					}else if(isEmpty(mutations[i].oldValue) && isEmpty(mutations[i].target.value)){
						// 넘어감
					}else if(isEmpty(mutations[i].oldValue) && !isEmpty(mutations[i].target.value)){
						// 값입력
						console.log("observer1(입력) : " + mutations[i].oldValue + " > " + mutations[i].target.value);
					}else if(!isEmpty(mutations[i].oldValue) && isEmpty(mutations[i].target.value)){
						// 값삭제
						console.log("observer1(삭제) : " + mutations[i].oldValue + " > " + mutations[i].target.value);
					}else if(mutations[i].oldValue != mutations[i].target.value){
						// 값변경
						console.log("observer1(변경) : " + mutations[i].oldValue + " > " + mutations[i].target.value);
					}					
				}	
			}
		}
	}
})

$(document).ready(function(){

	let target = document.querySelector('section');	
	let option = {
	    attributes: true,
	    childList: true,
	    attributeOldValue: true,
	    subtree: true
	};	
	observer.observe(target, option);
	
	setEvent();	
});

function setEvent(){
	setInputTagEvent();
}

function setInputTagEvent(){
	console.log("setTagEvent : " + "setInputTagEvent");    
    $('.conceptSelectBox input').change(function() {
	    alert("click!"); 
	});
}
 
 