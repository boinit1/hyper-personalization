/**
 * author: 김동현
 */

$(document).ready(function(){
	setEvent();	
});

function setEvent(){
	setInputTagEvent();
}

function setInputTagEvent(){
	try{	
		var inputs = $("input");	
		
		for(var i=0 ; i<inputs.length ; i++){
			inputs[i].onchange = function() {			
				// 값 입력
				if(this.oldValue == ""){
					sendXapi("keypressed", "값입력", "Activity", window.location.href + "&inputName=" + this.name + "&oldInputValue="+ encodeURIComponent(this.oldValue) + "&inputValue="+ encodeURIComponent(this.value));
				}
				
				// 값 삭제
				if(this.value == "" && this.oldValue != ""){
					sendXapi("cleared_input", "입력값삭제", "Activity", window.location.href + "&inputName=" + this.name + "&oldInputValue="+ encodeURIComponent(this.oldValue));
				}
				
				// 값 변경
				if(this.value != "" && this.oldValue != "" && (this.oldValue != this.value)){
					sendXapi("keypressed", "값입력", "Activity", window.location.href + "&inputName=" + this.name + "&oldInputValue="+ encodeURIComponent(this.oldValue) + "&inputValue="+ encodeURIComponent(this.value));
				}
			};
			
			inputs[i].onfocus = function() {
				this.oldValue = this.value;
			};
		}
		
		
	} catch (error) {
	}
}