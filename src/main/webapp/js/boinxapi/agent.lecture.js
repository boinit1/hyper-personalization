/**
 * author: 김동현
 */

let observer = new MutationObserver((mutations) => {
	setEvent();
	for(var i=0 ; i<mutations.length ; i++){
		if(mutations[i].type == "attributes"){
			if(mutations[i].target.tagName == "INPUT"){			
				if(mutations[i].attributeName == "data-value" || mutations[i].attributeName == "value"){
					if(mutations[i].oldValue == mutations[i].target.value){
						// 넘어감
					}else if(isEmpty(mutations[i].oldValue) && isEmpty(mutations[i].target.value)){
						// 넘어감
					}else if(isEmpty(mutations[i].oldValue) && !isEmpty(mutations[i].target.value)){
						// 값입력
						sendXapi("keypressed", "값입력", "Activity", window.location.href + "&inputName=" + mutations[i].target.name + "&oldInputValue="+ encodeURIComponent(mutations[i].oldValue) + "&inputValue="+ encodeURIComponent(mutations[i].target.value));
						console.log("observer1(입력) : " + mutations[i].oldValue + " > " + mutations[i].target.value);
					}else if(!isEmpty(mutations[i].oldValue) && isEmpty(mutations[i].target.value)){
						// 값삭제
						sendXapi("cleared_input", "입력값삭제", "Activity", window.location.href + "&inputName=" + mutations[i].target.name + "&oldInputValue="+ encodeURIComponent(mutations[i].oldValue));
						console.log("observer1(삭제) : " + mutations[i].oldValue + " > " + mutations[i].target.value);
					}else if(mutations[i].oldValue != mutations[i].target.value){
						// 값변경
						sendXapi("keypressed", "값입력", "Activity", window.location.href + "&inputName=" + mutations[i].target.name + "&oldInputValue="+ encodeURIComponent(mutations[i].oldValue) + "&inputValue="+ encodeURIComponent(mutations[i].target.value));
						console.log("observer1(변경) : " + mutations[i].oldValue + " > " + mutations[i].target.value);
					}					
				}	
			}
		}
	}
})

$(document).ready(function(){
	let target = document.querySelector('#viewer');	
	if(target == null){
		target = document.querySelector('#wrap')
	}	
	if(target == null){
		target = document.querySelector('body');
	}
	
	let option = {
	    attributes: true,
	    childList: true,
	    attributeOldValue: true,
	    subtree: true
	};
	
	observer.observe(target, option);
	
	setEvent();	
});

function setEvent(){
	setInputTagEvent();
	setVideoTagEvent();
}

function setInputTagEvent(){
	
}


var startTime = 0;
var endTime = 0;

function setVideoTagEvent(){
	
	try{	
		var sample = $("video").get(0);	
		var events = $._data(sample, 'playing');
		
		if(events == null) {
					
			sample.onplay = function() {
				if(startTime != endTime){
					sendXapiTime("replay", "영상 재시작", "Activity", window.location.href, null, null, startTime);
				}else{
					startTime = $("video").get(0).currentTime;
					sendXapiTime("played", "영상 재생", "Activity", window.location.href, null, null, startTime);
				}	
			};
			
			sample.onpause = function() {
				endTime = $("video").get(0).currentTime;
				sendXapiTime("paused", "영상 정지", "Activity", window.location.href, endTime, null, null);
			};
			
			sample.onended = function() {
				endTime = $("video").get(0).currentTime;
				sendXapiTime("complete", "영상 완료", "Activity", window.location.href, null, startTime, endTime);
			};
			
			sample.onseeked = function() {
				endTime = $("video").get(0).currentTime;
				sendXapiTime("skipped", "영상 스킵", "Activity", window.location.href, null, startTime, endTime);
			};
			
		}
	} catch (error) {
	}
	
}
 
 