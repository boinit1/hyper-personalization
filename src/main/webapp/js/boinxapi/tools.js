/**
 * author: 김동현
 */

var StatementRef = "";

function isEmpty(val) {
	if (typeof val == 'undefined')
		return true;
	else if (val == null)
		return true;
	else if (val.length < 1)
		return true;

	return false;
}

function sendXapi(verb, definition, objectType, objectId, description) {
	var jsonData = {"verb" : verb,
		   "definition" : definition,
		   "objectType" : objectType,
		   "objectId" : objectId,
		   "description" : description
		};
		
	$.ajax({
		url: "/comm/sendXapi.do",
		data: JSON.stringify(jsonData),
		type: "POST", //전송 타입
		async: true, //비동기 여부	
		contentType: "application/json; charset=utf-8",
        success: function (data) {
        	StatementRef = data;
        }
	});
}

function sendXapiScore(verb, definition, objectType, objectId, score, description) {
	var jsonData = {"verb" : verb,
		   "definition" : definition,
		   "objectType" : objectType,
		   "objectId" : objectId,
		   "score" : score,
		   "description" : description
		};
		
	$.ajax({
		url: "/comm/sendXapi.do",
		data: JSON.stringify(jsonData),
		type: "POST", //전송 타입
		async: true, //비동기 여부	
		contentType: "application/json; charset=utf-8",
        success: function (data) {
        	StatementRef = data;
        }
	});
}

function sendXapiAnswer(verb, definition, objectType, objectId, response, description) {
	var jsonData = {"verb" : verb,
		   "definition" : definition,
		   "objectType" : objectType,
		   "objectId" : objectId,
		   "response" : response,
		   "description" : description
		};
		
	$.ajax({
		url: "/comm/sendXapi.do",
		data: JSON.stringify(jsonData),
		type: "POST", //전송 타입
		async: true, //비동기 여부	
		contentType: "application/json; charset=utf-8",
        success: function (data) {
        	StatementRef = data;
        }
	});
}

function sendXapiTime(verb, definition, objectType, objectId, time, timeFrom, timeTo, description) {
	var jsonData = {"verb" : verb,
		   "definition" : definition,
		   "objectType" : objectType,
		   "objectId" : objectId,
		   "time" : time,
		   "timeFrom" : timeFrom,
		   "timeTo" : timeTo,
		   "description" : description
		};
		
	$.ajax({
		url: "/comm/sendXapi.do",
		data: JSON.stringify(jsonData),
		type: "POST", //전송 타입
		async: true, //비동기 여부	
		contentType: "application/json; charset=utf-8",
        success: function (data) {
        	StatementRef = data;
        }
	});
}