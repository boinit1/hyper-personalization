
/* lnb */
$(function() {
   /* $(".lnbSec li .lnb").click(function() {
        $(this).find(".ar").toggleClass("on");
        $(this).parent().find("dl").toggleClass("on");
    });*/

	$('.img-popup').click(function(e){
		e.preventDefault();
		var img = $(this).data("img");
		window.open('../common/pop_img.php?img='+img,'popimg','width=1000, height=800, scrolling=yes');
	});
});

//날짜처리
$(document).on('click', '.setBtn', function() {
    
    var v = $(this).data("intval");
    $('#s_sdate').datepicker('setDate', v);
    $('#s_edate').datepicker('setDate', 'today');
    $('.setBtn').removeClass("on");
    $(this).addClass("on");
	$('#intval').val(v);
});


//자릿수 만큼 0채우기 By BizAN 추가(2021.08.26)/////////////////
function fillZero(width, str){
    return str.length >= width ? str:new Array(width-str.length+1).join('0')+str;//남는 길이만큼 0으로 채움
}

//날짜처리(SELECT) By BizAN 추가(2021.07.31)/////////////////
//날짜선택 1
$(document).on('change', '#dateRangeSelect', function() {
    var v = $(this).val();
    //alert("GGGG =>"+v);
    if(v == "이번달"){
		var date = new Date();
		var firstday = new Date(date.getFullYear(), date.getMonth(), 1);
		$('#s_sdate').datepicker('setDate', firstday);
		$('#s_edate').datepicker('setDate', 'today');
	}else if (v == "기간설정" || v.length == 0) {
		$('#s_sdate').val("");
		$('#s_edate').val("");
	}else{
		$('#s_sdate').datepicker('setDate', v);
	    $('#s_edate').datepicker('setDate', 'today');
		$('#intval').val(v);
	}
});

//날짜선택 2
$(document).on('change', '#dateRangeSelect2', function() {
    var v = $(this).val();
    if(v == "이번달"){
		var date = new Date();
		var firstday = new Date(date.getFullYear(), date.getMonth(), 1);
		$('#s_sdate2').datepicker('setDate', firstday);
		$('#s_edate2').datepicker('setDate', 'today');		
	}else if (v == "기간설정" || v.length == 0) {
		$('#s_sdate2').val("");
		$('#s_edate2').val("");
    }else{
		$('#s_sdate2').datepicker('setDate', v);
	    $('#s_edate2').datepicker('setDate', 'today');
		$('#intval').val(v);
	}
});
///////////////////////////////////////////////////////////


/* 상품옵션 수정 공통 */
function popOptEdit(idx) {
    var url = "/admcontents/common/pop_opt_edit.php?idx="+idx;
    var name = "popup";
    var option = "width=900, height=600, top=100, left=200, location=no, scrollbars=yes"
    window.open(url, name, option);
}

/* 상품검색 팝업 공통 */
function popPrd(n) {
    var url = "/admcontents/common/pop_prd_search.php?type=" + n;
    var name = "popup";
    var option = "width=1200, height=700, top=100, left=200, location=no, scrollbars=yes"
    window.open(url, name, option);
}

/* 주문자정보 수정 팝업 */
function popEditOrd(idx){
	var url = "/admcontents/order/pop_ord_edit.php?idx="+idx;
	var name = "ord";
	var option = "width=700, height=500, top=100, left=200, location=no, scrollbars=yes"
	window.open(url, name, option);
}

/* 수령지정보 수정 팝업 */
function popEditArrival(idx){
	var url = "/admcontents/order/pop_arrival_edit.php?idx="+idx;
	var name = "arrival";
	var option = "width=700, height=640, top=100, left=200, location=no, scrollbars=yes"
	window.open(url, name, option);
}

/* 사유 팝업 공통 */
function popReason(idx){
	var url = "/admcontents/common/pop_reason.php?idx="+idx;
	var name = "reason";
	var option = "width=400, height=400, top=100, left=200, location=no, scrollbars=yes"
	window.open(url, name, option);
}

/* 사진 팝업 공통 */
function popPhoto(idx){
	var url = "/admcontents/order/pop_photo.php?idx=" + idx;
	var name = "photo";
	var option = "width=700, height=700, top=100, left=200, location=no, scrollbars=yes"
	window.open(url, name, option);
}

/* 회원검색 팝업 공통 */
function popMember(){
	var url = "/admin/_inc/pop_member.php";
	var name = "reason";
	var option = "width=400, height=400, top=100, left=200, location=no, scrollbars=yes"
	window.open(url, name, option);
}

/* 리뷰 사진 팝업 공통 */
function reviewPhoto(){
	var url = "/admin/_inc/pop_review_photo.php";
	var name = "reviewPhoto";
	var option = "width=700, height=700, top=100, left=200, location=no, scrollbars=yes"
	window.open(url, name, option);
}
function delProc(url) {
    if (confirm("삭제한 데이터는 복구하실 수 없습니다. 삭제하시겠습니까?")) {
        location.href = url;
    }
}

function callBackFunc(s,e) {
}
$(function() {
    //모든 datepicker에 대한 공통 옵션 설정
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd' //Input Display Format 변경
            ,
        showOtherMonths: true //빈 공간에 현재월의 앞뒤월의 날짜를 표시
            ,
        showMonthAfterYear: true //년도 먼저 나오고, 뒤에 월 표시
            ,
        changeYear: true //콤보박스에서 년 선택 가능
            ,
        changeMonth: true //콤보박스에서 월 선택 가능                
            ,
        showOn: "both" //button:버튼을 표시하고,버튼을 눌러야만 달력 표시 ^ both:버튼을 표시하고,버튼을 누르거나 input을 클릭하면 달력 표시  
            //		,buttonImage: "/admcontents/css/images/ui-icons_444444_256x240.png" //버튼 이미지 경로
            ,
        buttonImageOnly: false //기본 버튼의 회색 부분을 없애고, 이미지만 보이게 함
            //		,buttonText: "선택" //버튼에 마우스 갖다 댔을 때 표시되는 텍스트                
            ,
        yearSuffix: "년" //달력의 년도 부분 뒤에 붙는 텍스트
            ,
        monthNamesShort: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'] //달력의 월 부분 텍스트
            ,
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'] //달력의 월 부분 Tooltip 텍스트
            ,
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'] //달력의 요일 부분 텍스트
            ,
        dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'] //달력의 요일 부분 Tooltip 텍스트
            //,minDate: "-1M" //최소 선택일자(-1D:하루전, -1M:한달전, -1Y:일년전)
            //,maxDate: "+1M" //최대 선택일자(+1D:하루후, -1M:한달후, -1Y:일년후)                    
	    ,onSelect: function(selected,evnt) {
			callBackFunc(selected,evnt); 	
	    }
    });

    //input을 datepicker로 선언
    $(".calendar").datepicker();

    //From의 초기값을 오늘 날짜로 설정
    //$('.calendar').datepicker('setDate', 'today'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
});

$(document).on('click', '#btn-submit', function() {

    var err = 0;
    $('.required').each(function() {
        if ($(this).val() == "") {
            alert($(this).attr('title') + ": 필수항목입니다.");
            $(this).focus();
            err = 1;
            return false;
        }
    });

    if (err == 0) {
        $("#frm").submit();
    }
});

$(document).on('blur', '.onlyNum', function() {
    var v = $(this).val();
    if (!v) return false;
    var vv = v.replace(/,/g,"");
    if (isNaN(vv)) {
        alert("숫자로만 입력해주세요.");
        $(this).val('');
        $(this).focus();
    }else{
		$(this).val(number_format(vv));
	}
});

function getAddDayFromToday(addDays, delimiter) {
    var oToday = new Date();

    oToday.setDate(oToday.getDate() + addDays);
    //oToday.toLocaleString();

    var sYear = oToday.getFullYear();
    var sMonth = oToday.getMonth() + 1;
    var sDay = oToday.getDate();

    sMonth = "" + sMonth;
    sMonth = (sMonth.length == 1) ? "0" + sMonth : sMonth
    sDay = "" + sDay;
    sDay = (sDay.length == 1) ? "0" + sDay : sDay

    return sYear + delimiter + sMonth + delimiter + sDay;
}



/*
* 팝업창 
*/
function getCookie(names){
	var nameOfCookie = names + "=";
	var x = 0;
	while ( x <= document.cookie.length ){
		var y = (x+nameOfCookie.length);
		if(document.cookie.substring( x, y ) == nameOfCookie){
			if((endOfCookie=document.cookie.indexOf( ";", y )) == -1)
				endOfCookie = document.cookie.length;
			return unescape( document.cookie.substring( y, endOfCookie ) );
		}
		x = document.cookie.indexOf( " ", x ) + 1;
		if ( x == 0 )
			break;
	}
	return "";
}

function setCookie(name, value, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = name + "=" + value + ";" + expires + ";path=/";
}
function PopUpCookieClose(id,name)
{
	setCookie(name, "Y", 1); 
	$('#'+id).fadeOut();
}

function PopUpClose(id)
{
	$('#'+id).fadeOut();
}