var Common = Common || {};
Common.cancelModify = '<div style="text-align: center;">저장하지 않은 정보는 사라집니다.<br>취소하시겠습니까\?</div>';

// 빈 배열 체크
function isEmptyArr(arr)  {
	  if(Array.isArray(arr) && arr.length === 0)  {
	    return true;
	  }
	  
	  return false;
}
/*
 * 파일 미리보기
 */
function viewerOpen(viewerUrl, id) {
	window.open(viewerUrl + '/viewer/preview.html?source=AMIND-PREVIEW&file=' + id, 'title', 'height=' + screen.height + ', width=' + screen.width + 'fullscreen=yes');
}

/* 테이블 화면상 재정렬 
 * SortTable($('#divExtraCategoryTabDepth1Items'),'asc', 'extraCategoryTabSortNo');
 * */
function SortTable(tbody, order, nr) {
	var asc = order === 'asc';
	var temp = tbody.clone();
	tbody.empty();
	temp.find('tr').sort(function (a, b) {
	    if ($('td > input[name="'+nr+'"]', a).val() == "Name") return $('td > input[name="'+nr+'"]', a).val();
	    else if (asc) {
	        return $('td > input[name="'+nr+'"]', a).val().localeCompare($('td > input[name="'+nr+'"]', b).val());
	    } else {
	        return $('td > input[name="'+nr+'"]', a).val().localeCompare($('td > input[name="'+nr+'"]', b).val());
	    }
	}).appendTo(tbody);

}

/**
 * 체크박스제어
 */
function fnCheckAll() {
	$('.chk_all_srch').on('click', function(e) {
		var id		= $(this).attr('id');
		var name	= $(this).attr('name');
		
		fnCheckBox(id, name, false);
	});

	$('.chk_unit_srch').on('click', function(e) {
		var name	= $(this).attr('id');
		var tSize	= $('input[name^="'+name+'"]').length;
		var sSize	= $('input[name^="'+name+'"]:checked').length;
		var allChecked	= $('input[name="'+name+'_all"]:checked').length;

		if (sSize < tSize - 1 + allChecked) {
			$('input[name="'+name+'_all"]').prop('checked', false);
		}else{
			$('input[name="'+name+'_all"]').prop('checked', true);
		} 
	});

	$('.chk_all').on('click', function(e) {
		var id		= $(this).attr('id');
		var name	= $(this).attr('name');
		
		fnCheckBox(id, name, true);
	});

	$('.chk_all_reply').on('click', function(e) {
		var id		= $(this).attr('id');
		var name	= $(this).attr('name');
		
		fnCheckBoxReply(id, name, true);
	});

	$('.chk_unit').on('click', function(e) {
		var name	= $(this).attr('name');
		var tSize	= $('input[name="'+name+'"]').length;
		var sSize	= $('input[name="'+name+'"]:checked').length;
		
		if (sSize < tSize) {
			$('input[name="'+name+'_all"]').prop('checked', false);
		}else{
			$('input[name="'+name+'_all"]').prop('checked', true);
		} 
		if(ListControl){
			if($(this).is(':checked'))
				ListControl.addCheckedKey($(this).val());
			else
				ListControl.removeCheckedKey($(this).val());
		}
	});

	$('.chk_unit_reply').on('click', function(e) {
		var name	= $(this).attr('name');
		var tSize	= $('input[name="'+name+'"]').length;
		var sSize	= $('input[name="'+name+'"]:checked').length;
		
		if (sSize < tSize) {
			$('input[name="'+name+'_all"]').prop('checked', false);
		}else{
			$('input[name="'+name+'_all"]').prop('checked', true);
		} 
		if(ListControlReply){
			if($(this).is(':checked'))
				ListControlReply.addCheckedKeyReply($(this).val());
			else
				ListControlReply.removeCheckedKeyReply($(this).val());
		}
	});
	
}

function fnCheckBox(id, name, bAddKey) {
	var obj	= $('input[name="'+name+'"]');
	
	if (obj.is(":checked")) {
		$('input[name^="'+id+'"]').prop('checked', true);
		if(bAddKey)
			$('input[name^="'+id+'"]').each(function(index, item){
				ListControl.addCheckedKey($(item).val());
			});
	}
	else {
		$('input[name^="'+id+'"]').prop('checked', false);
		if(bAddKey)
			$('input[name^="'+id+'"]').each(function(index, item){
				ListControl.removeCheckedKey($(item).val());
			});
	}
}

function fnCheckBoxReply(id, name, bAddKey) {
	var obj	= $('input[name="'+name+'"]');
	
	if (obj.is(":checked")) {
		$('input[name^="'+id+'"]').prop('checked', true);
		if(bAddKey)
			$('input[name^="'+id+'"]').each(function(index, item){
				ListControlReply.addCheckedKeyReply($(item).val());
			});
	}
	else {
		$('input[name^="'+id+'"]').prop('checked', false);
		if(bAddKey)
			$('input[name^="'+id+'"]').each(function(index, item){
				ListControlReply.removeCheckedKeyReply($(item).val());
			});
	}
}


<!-- 불투명 배경 생성 함수 -->

function bgLayerOpen() {
    if(!$('.bgLayer').length) {
        $('<div class="bgLayer"></div>').appendTo($('body'));
    }
    var object = $(".bgLayer");
    var w = $(document).width()+12;
    var h = $(document).height();


    object.css({'width':w,'height':h}); 
    object.fadeIn(500);   //생성되는 시간 설정
}

<!-- //불투명 배경 생성 함수 -->

 

<!-- 불투명 배경 제거 함수 -->

function bgLayerClear(){
    var object = $('.bgLayer');

 

if(object.length) {
    object.fadeOut(500, function() {
    object.remove();

});

    }

}

<!-- //불투명 배경 제거 함수 -->
<!-- 화면 크기 변경 시 레이어 크기 변화 -->

$(function(){

$(window).resize(function() { //화면 크기 변할 시
    $('.bgLayer').css('width' ,  $(window).width() - 0 );
    $('.bgLayer').css('height' ,  $(window).height() - 0 );
});

});

<!-- //화면 크기 변경 시 레이어 크기 변화 -->
 

<!--스크롤 숨기는 부분 추가 -->

function bgLayerOpen() {
    if(!$('.bgLayer').length) {
        $('<div class="bgLayer"></div>').appendTo($('body'));
    }
    var object = $(".bgLayer");
    var w = $(document).width()+12;
    var h = $(document).height();


    object.css({'width':w,'height':h}); 
    object.fadeIn(500);   //생성되는 시간 설정
     $('html').css("overflow", "hidden");
}

<!--//스크롤 숨기는 부분 추가 -->

 

<!--스크롤 보이게하는 부분 추가 -->

function bgLayerClear(){
    var object = $('.bgLayer');

 

if(object.length) {
    object.fadeOut(500, function() {
    object.remove();

});

    }

    $('html').css("overflow", "scroll");

}

<!--//스크롤 보이게하는  부분 추가 -->

 


function number_format( number )
{
  var nArr = String(number).split('').join(',').split('');
  for( var i=nArr.length-1, j=1; i>=0; i--, j++)  if( j%6 != 0 && j%2 == 0) nArr[i] = '';
  return nArr.join('');
}

//daum zip
function execDaumPostcode(v) 
{
	new daum.Postcode({
		oncomplete: function(data) {
			// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

			// 도로명 주소의 노출 규칙에 따라 주소를 표시한다.
			// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
			var roadAddr = data.roadAddress; // 도로명 주소 변수
			var extraRoadAddr = ''; // 참고 항목 변수

			// 법정동명이 있을 경우 추가한다. (법정리는 제외)
			// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
			if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
				extraRoadAddr += data.bname;
			}
			// 건물명이 있고, 공동주택일 경우 추가한다.
			if (data.buildingName !== '' && data.apartment === 'Y') {
				extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
			}
			// 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
			if (extraRoadAddr !== '') {
				extraRoadAddr = ' (' + extraRoadAddr + ')';
			}

			// 우편번호와 주소 정보를 해당 필드에 넣는다.
			document.getElementById(v + 'zip').value = data.zonecode;
			document.getElementById(v + "addr1").value = roadAddr;
			//document.getElementById(v+"jibunAddress").value = data.jibunAddress;
		}
	}).open();
}

function getParameter(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getFormData(object) {
	const formData = new FormData();
	Object.keys(object).forEach(key => formData.append(key, object[key]));
	return formData;
}

function getFormDataToJson(formId) {
	var formData = $("#" + formId).serializeObject();
	return formData;
}

function getFormDataToJson_oneCheckNoArray(formId) {
	var formData = $("#" + formId).serializeObject2();
	return formData;
}

$.fn.serializeObject2=function(){"use strict";var a={},b=function(b,c){var d=a[c.name];"undefined"!=typeof d&&d!==null?$.isArray(d)?d.push(c.value):a[c.name]=[d,c.value]:a[c.name]=c.value};return $.each(this.serializeArray(),b),a};
$.fn.serializeObject= function() {
	var obj = null;
	try {
		if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
			var arr = this.serializeArray();

			var form = this[0];

			obj = {};
			for(var i=0 ; i<form.length ; i++){
				var id = form[i].id;
				var value = form[i].value;
				var checked = form[i].checked;
				var type = form[i].type;



				if(type == "textarea"){
//					// editor 에 따라 커스터마이징 해줘야 함.
//					if(tinymce.get(id) == null){
//					}else{
//						value = tinymce.get(id).getContent();
//					}
				}

				if(id && id!='undefined' && value && value!='undefined'){


					if(type == "checkbox"){
						if(checked){
							if(obj[id]){
								obj[id].push(value);
							}else{
								obj[id] = [value];
							}
						}
					}else if(type == "date"){
						obj[id] = value;
					}else if(type == "radio"){
						if(checked){
							obj[id] = value;
						}
					}else{
						obj[id] = value;
					}
				}
			}
		}
	} catch (e) {
		alert(e.message);
	} finally {
	}
	return obj;
};

function sendAjaxFormId(url, formId, callbackId, callback) {
	showLoadingImage();
	requestData = "{}";
	if(formId) {
		var formData = $("#" + formId).serializeObject();
		requestData = JSON.stringify(formData);
	}
	var returnData = null;
	$.ajax({
		url: url,
		processData: false,
		contentType: 'application/json',
		data: requestData,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		complete : function() {
			hideLoadingImage();
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if ("success" == responseJSON.resultCode) {
				if (responseJSON.errorMessage) {
					alert(responseJSON.errorMessage);
				}
				var resultDataFromServer = responseJSON.resultData;
				var pageInfoFromServer = responseJSON.pageInfo;
				var items = {};
				if (resultDataFromServer) {
					sendAjaxCallback(callbackId, resultDataFromServer, pageInfoFromServer);
					items.data = resultDataFromServer;
				}
				if (pageInfoFromServer) {
					items.recordsTotal = pageInfoFromServer.totalCount;
					items.recordsFiltered = pageInfoFromServer.totalCount;
				}
				try {
					dataTableCallback(items);
				} catch (error) {
				}
			} else {
				alert("[오류] " + responseJSON.errorMessage);
				hideLoadingImage();
			}
		},
		error: function (request, status, error) {
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
}

function sendAjaxJsonData(url, jsonData, callbackId, dataType, fLoadingImage, userCallback) {
	if(fLoadingImage) 
		showLoadingImage();
	requestData = "{}";
	//debugger;
	if(jsonData) {
		requestData = JSON.stringify(jsonData);
	}
	var returnData = null;
	$.ajax({
		url: url,
		processData: false,
		contentType: 'application/json',
		data: requestData,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		complete : function() {
			if(fLoadingImage) 
				hideLoadingImage();
		},
		success: function (res, textStatus, jqXHR) {
			if( dataType == 'html')
				if(userCallback)
					userCallback(callbackId, res);
				else
					sendAjaxCallback(callbackId, res);
			else {
				var responseJSON = jqXHR.responseJSON;
				if ("0000" == responseJSON.resultCode) {
					if (responseJSON.errorMessage) {
						// alert(responseJSON.errorMessage);
					}
					var resultDataFromServer = responseJSON.resultData;
					var pageInfoFromServer = responseJSON.pageInfo;
					var items = {};
					if (resultDataFromServer) {
						if(userCallback)
							userCallback(callbackId, resultDataFromServer, pageInfoFromServer);
						else
							sendAjaxCallback(callbackId, resultDataFromServer, pageInfoFromServer);
						items.data = resultDataFromServer;
					}
					if (pageInfoFromServer) {
						items.recordsTotal = pageInfoFromServer.totalCount;
						items.recordsFiltered = pageInfoFromServer.totalCount;
					}
					/*try {
						dataTableCallback(items);
					} catch (error) {
					}*/
				} else {
					alert("[오류] " + responseJSON.errorMessage);
					if(fLoadingImage) 
						hideLoadingImage();
				}
			}
		},
		error: function (request, status, error) {
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
}

function sendAjaxFileDown(url, jsonData, callbackId, dataType, fLoadingImage) {
	requestData = "{}";
	if(jsonData) {
		requestData = JSON.stringify(jsonData);
	}
	var returnData = null;
	$.ajax({
		url: url,
		processData: false,
		contentType: 'application/json',
		data: requestData,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
        xhrFields: {
            responseType: 'arraybuffer'
        }
	}).done(function(data, textStatus, jqXhr) {
        if (!data) {
            return;
        }
        try {
            var blob = new Blob([data], { type: jqXhr.getResponseHeader('content-type') });
            var fileName = getFileName(jqXhr.getResponseHeader('content-disposition'));
            fileName = decodeURI(fileName);
 
            if (window.navigator.msSaveOrOpenBlob) { // IE 10+
                window.navigator.msSaveOrOpenBlob(blob, fileName);
            } else { // not IE
                var link = document.createElement('a');
                var url = window.URL.createObjectURL(blob);
                link.href = url;
                link.target = '_self';
                if (fileName) link.download = fileName;
                document.body.append(link);
                link.click();
                link.remove();
                window.URL.revokeObjectURL(url);
            }
        } catch (e) {
            console.error(e);
            $("body").LoadingOverlay("hide", true);
            vex.dialog.confirm({
                message: '오류가 발생되었습니다.',
                buttons: [
                 $.extend({}, vex.dialog.buttons.NO, {text: '닫기'})
                ],
                callback: function (isOk) {}
               });

        }
    });
}

function getFileName (contentDisposition) {
    var fileName = contentDisposition
        .split(';')
        .filter(function(ele) {
            return ele.indexOf('filename') > -1
        })
        .map(function(ele) {
            return ele
                .replace(/"/g, '')
                .split('=')[1]
        });
    return fileName[0] ? fileName[0] : null
}
function sendTmsApi(url, jsonData, callbackId, asyncYn) {
	isAsync = true;
	if(asyncYn == 'N'){
		isAsync = false;
	}

	var requestData = {
			url: url,
			requestBody: JSON.stringify(jsonData)
	};
	var requestDataString = '{}';
	if(jsonData) {
		requestDataString = JSON.stringify(requestData);
	}
	$.ajax({
		url: "/admin/send/tms/api",
		processData: false,
		contentType: 'application/json',
		dataType:"json",
		async: isAsync,
		data: requestDataString,
		type: 'post',
		complete : function() {
			hideLoadingImage();
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if ("success" == responseJSON.resultCode) {
				if (responseJSON.errorMessage) {
					alert(responseJSON.errorMessage);
				}
				var resultDataFromServer = responseJSON.resultData;
				var pageInfoFromServer = responseJSON.pageInfo;
				if (resultDataFromServer) {
					sendAjaxCallback(callbackId, resultDataFromServer, pageInfoFromServer);
				}
			} else {
				alert("[오류] " + responseJSON.errorMessage);
				hideLoadingImage();
			}
		},
		error: function (request, status, error) {
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
}

function dataTableAjax(data, callback, settings){
	showLoadingImage();
	requestData = "{}";
	if(_dataTable){
		requestData.currentPage = _dataTable.info().page + 1;
		requestData.pagePerCount = data.length;
	}

	$.ajax({
		url: "/api/data/context/list",
		processData: false,
		contentType: 'application/json',
		data: requestData,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		complete : function() {
			hideLoadingImage();
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if ("success" == responseJSON.resultCode) {
				if (responseJSON.errorMessage) {
					alert(responseJSON.errorMessage);
				}
				var resultDataFromServer = responseJSON.resultData;
				var pageInfoFromServer = responseJSON.pageInfo;
				var items = {};
				if(pageInfoFromServer){
					items.recordsTotal = pageInfoFromServer.totalCount;
					items.recordsFiltered = pageInfoFromServer.totalCount;
				}
				if(resultDataFromServer){
					items.data = resultDataFromServer;
				}
				callback(items);
			} else {
				alert("[오류] " + responseJSON.errorMessage);
				hideLoadingImage();
			}
		},
		error: function (request, status, error) {
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
}


function formDataBind(formId, data) {
	$.each(data, function(key, value) {
		var tag = $('#' + formId).find('[name="' + key + '"]');
		if (tag.length > 0 && !isEmpty(value)) {
			if (tag.attr('data-type') == 'datetime') {
				var val = value;
				if (value.length == 14) {
					val = value.substring(0, 4) + '-' + value.substring(4, 6) + '-' + value.substring(6, 8) + ' '
						+ value.substring(8, 10) + ':' + value.substring(10, 12) + ':' + value.substring(12, 14);
				} else if (value.length == 12) {
					val = value.substring(0, 4) + '-' + value.substring(4, 6) + '-' + value.substring(6, 8) + ' '
						+ value.substring(8, 10) + ':' + value.substring(10, 12) + ':' + '00';
				} else if (value.length == 8) {
					val = value.substring(0, 4) + '-' + value.substring(4, 6) + '-' + value.substring(6, 8) + ' 00:00:00';
				}
				tag.val(val);
			} else if (tag.attr('data-type') == 'date') {
				var val = value;
				if (value.length == 8) {
					val = value.substring(0, 4) + '-' + value.substring(4, 6) + '-' + value.substring(6, 8);
				}
				tag.val(val);
			} else if (tag.attr('data-type') == 'time') {
				var val = value;
				if (value.length == 6) {
					val = value.substring(0, 2) + ':' + value.substring(2, 4) + ':' + value.substring(4, 6);
				}
				tag.val(val);
			} else if (tag.attr('data-type') == 'yyyymm') {
				if (value.length == 6) {
					var val = value.substring(0, 4) + '-' + value.substring(4, 6);
					tag.val(val);
				}
			} else if (tag.attr('type') == 'radio') {
				$('[name="' + key + '"][value="' + value + '"]').prop('checked', true)
			} else if (tag.attr('type') == 'checkbox') {
				if (value == 1 || value == 'Y') {
					tag.prop('checked', true);
				}
			} else if (tag.hasClass('cmc_txtarea')) {
				tag.val(value.replace(/\\r\\n/g, '\n'));
			} else {
				tag.val(value);
			}
		}
	});
}

function formDataInit(formId) {
	var ctr = $('#' + formId).find('input,select,textarea');
	ctr.each(function(k, v) {
		$('[name="' + v.name + '"]').val('');
	});
}

function formDataButtonInit(formId) {
	var ctr = $('#' + formId).find('button');
	ctr.remove();
}

function isEmpty(val) {
	if (typeof val == 'undefined')
		return true;
	else if (val == null)
		return true;
	else if (val.length < 1)
		return true;

	return false;
}


function showLoadingImage() {
//	var width = 0;
//	var height = 0;
//	var left = 0;
//	var top = 0;
//
//	width = 50;
//	height = 50;
//
//	top = ( $(window).height() - height ) / 2 + $(window).scrollTop();
//	left = ( $(window).width() - width ) / 2 + $(window).scrollLeft();
//
//	if($("#div_ajax_load_image").length != 0) {
//		$("#div_ajax_load_image").css({
//			"top": top+"px",
//			"left": left+"px"
//		});
//		$("#div_ajax_load_image").show();
//	}
//	else {
//		$('body').append('<div id="div_ajax_load_image" style="position:absolute; top:' + top + 'px; left:' + left + 'px; width:' + width + 'px; height:' + height + 'px; z-index:9999;  filter:alpha(opacity=50); opacity:alpha*0.5; margin:auto; padding:0; "><img src="../img/loading2.gif" style="width:151px; height:151px;"></div>');
//	}

	$("body").LoadingOverlay("show", Module.ui.loadingSetup());
}

function hideLoadingImage() {
//	try {
//		$("#div_ajax_load_image").hide();
//	} catch (error) {
//	}
	$("body").LoadingOverlay("hide", true);
}

function createDatatable(tableId, columns, isSearch, isPage, isServerSide, ajax, columnDefs){
	return $("#" + tableId).DataTable({
		autoWidth:false,
		lengthChange: (isPage) ? true : false,
		lengthMenu: [20,50,100],
		searching: (isSearch) ? true : false,
		ordering: false,
		info: false,
		paging: (isPage) ? true : false,
		serverSide: (isServerSide) ? true : false,
		data: [],
		columns: columns,
		displayLength: 20,
		ajax: ajax,
		columnDefs: columnDefs
	});
}

function createCheckBoxDatatable(tableId, columns, isSearch, isPage, isServerSide, ajax){
	return $("#" + tableId).DataTable({
		autoWidth:false,
		lengthChange: (isPage) ? true : false,
		lengthMenu: [20,50,100],
		searching: (isSearch) ? true : false,
		ordering: false,
		info: false,
		paging: (isPage) ? true : false,
		serverSide: (isServerSide) ? true : false,
		data: [],
		columns: columns,
		ajax: ajax,
		columnDefs: [
			{
				'targets': 0,
				'checkboxes': {
					'selectRow': true
				}
			}
		],
		select: {
			style: 'multi'
		}
	});
}

function setDatatableData(dataTable, data){
	dataTable.clear();
	$.each(data, function (index, value) {
		dataTable.row.add(value);
	});
	dataTable.columns.adjust().draw();
}

function getDatatableCurrentPage(dataTable){
	try {
		var info = dataTable.page.info();
		return info.page + 1;
	} catch (error) {
		return null;
	}
}

function getDatatablePagePerCount(dataTable){
	try {
		var info = dataTable.page.info();
		return info.length;
	} catch (error) {
		return null;
	}
}


function setSelectCodeData(isIncludeAllItem ,id, codeGroup, extraCategoryTabDepth, extraCategoryTabGroup, type){
	if(type){
		if(type == 'extra' || type == 'extra2'){
			return setComponentCategoryValue(isIncludeAllItem ,id, codeGroup, extraCategoryTabDepth, extraCategoryTabGroup, getSelectaApendTag, type)
		}else if(type == 'contentsExtra'){
			return setComponentContentsExtraCategoryValue(isIncludeAllItem, id, getSelectaApendTag);
		}
	}else{
		return setComponentCodeValue(isIncludeAllItem ,id, codeGroup, getSelectaApendTag);
	}
}

function getSelectaApendTag(value, name, id, isSelected){
	var tag = "<option value=\'" + value + "\' " + (isSelected ? "selected=\"selected\"" : "") + " >" + name + "</option>"
	return tag;
}

function setCheckCodeData(isIncludeAllItem ,id, codeGroup, extraCategoryTabDepth, extraCategoryTabGroup, type){
	if(type){
		if(type == 'extra' || type == 'extra2'){
			return setComponentCategoryValue(isIncludeAllItem ,id, codeGroup, extraCategoryTabDepth, extraCategoryTabGroup, getCheckaApendTag, type)
		}else if(type == 'contentsExtra'){
			return setComponentContentsExtraCategoryValue(isIncludeAllItem, id, getCheckaApendTag);
		}
	}else{
		return setComponentCodeValue(isIncludeAllItem ,id, codeGroup, getCheckaApendTag);
	}
}

function getCheckaApendTag(value, name, id){
	var tag = "<div class=\"form-check form-check-inline align-self-center\">"
		+ "<input class=\"form-check-input\" type=\"checkbox\" id=\"" + id + "\" value=\"" + value + "\">"
		+ "<label class=\"form-check-label\" for=\"inlineCheckbox1\">" + name + "</label>"
		+ "</div>";
	return tag;
}

function setComponentCodeValue(isIncludeAllItem ,id, codeGroup, getApendTagFunctuon){
	var returnData = new Map();
	$.ajax({
		url: '/admin/api/commmon/code/' + codeGroup,
		processData: false,
		contentType: 'application/json',
		data: "{}",
		async: false,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		complete : function() {
			hideLoadingImage();
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if ("success" == responseJSON.resultCode) {
				if (responseJSON.errorMessage) {
					alert(responseJSON.errorMessage);
				}
				var resultDataFromServer = responseJSON.resultData;

				if(isIncludeAllItem){
					$('#' + id).append(getApendTagFunctuon('','전체', id));
				}

				$.each(resultDataFromServer, function (index, item) {
					$('#' + id).append(getApendTagFunctuon(item.code,item.codeName, id));
					returnData.set(item.code, item.codeName);
				});

				$('#' + id + 'option:eq(0)').prop("selected", true);
			} else {
				alert("[오류] " + responseJSON.errorMessage);
				hideLoadingImage();
			}
		},
		error: function (request, status, error) {
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
	return returnData;
}

function setComponentCategoryValue(isIncludeAllItem ,id, extraCategoryCd, extraCategoryTabDepth, extraCategoryTabGroup, getApendTagFunctuon, type){
	var returnData = new Map();
	var jsonData = {
		url : '/admin/extra/category/tab/list',
		requestBody : JSON.stringify({
			'extraCategoryCd' : extraCategoryCd,
			'extraCategoryTabGroup' : extraCategoryTabGroup,
			'extraCategoryTabDepth' : extraCategoryTabDepth
		})
	}
	$.ajax({
		url: "/admin/send/tms/api",
		processData: false,
		contentType: 'application/json',
		data: JSON.stringify(jsonData),
		async: false,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		complete : function() {
			hideLoadingImage();
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if ("success" == responseJSON.resultCode) {
				if (responseJSON.errorMessage) {
					alert(responseJSON.errorMessage);
				}
				var resultDataFromServer = responseJSON.resultData;

				if(isIncludeAllItem){
					$('#' + id).append(getApendTagFunctuon('','전체', id));
				}

				$.each(resultDataFromServer, function (index, item) {
					if(type == 'extra2'){
						$('#' + id).append(getApendTagFunctuon(item.extraCategoryTabGroup,item.extraCategoryTabDepthTitle, id));
						returnData.set(item.extraCategoryTabGroup, item.extraCategoryTabDepthTitle);
					}else{
						$('#' + id).append(getApendTagFunctuon(item.extraCategoryTabDepthCd,item.extraCategoryTabDepthTitle, id));
						returnData.set(item.extraCategoryTabDepthCd, item.extraCategoryTabDepthTitle);
					}
				});

				$('#' + id + 'option:eq(0)').prop("selected", true);
			} else {
				alert("[오류] " + responseJSON.errorMessage);
				hideLoadingImage();
			}
		},
		error: function (request, status, error) {
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});

	return returnData;
}

function setComponentContentsExtraCategoryValue(isIncludeAllItem, id, getApendTagFunctuon){
	var returnData = new Map();
	var jsonData = {
		url : '/admin/contents/extra/category/list',
		requestBody : JSON.stringify({})
	}
	$.ajax({
		url: "/admin/send/tms/api",
		processData: false,
		contentType: 'application/json',
		data: JSON.stringify(jsonData),
		async: false,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		complete : function() {
			hideLoadingImage();
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if ("success" == responseJSON.resultCode) {
				var resultDataFromServer = responseJSON.resultData;
				if(isIncludeAllItem){
					$('#' + id).append(getApendTagFunctuon('','전체', id));
				}
				$.each(resultDataFromServer, function (index, item) {
					$('#' + id).append(getApendTagFunctuon(item.contentsExtraCategoryCd,item.contentsExtraCategoryTitle, id));
					returnData.set(item.contentsExtraCategoryCd, item.contentsExtraCategoryTitle);
				});

				$('#' + id + 'option:eq(0)').prop("selected", true);
			}
		},
		error: function (request, status, error) {
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});

	return returnData;
}

function downloadFilePop(fileUrl){
	var link = document.createElement('a');
    link.target='_blank';
    link.href=fileUrl;
    link.click();
    link.remove();
}

function downloadFile(fileId){
	if(isEmpty(fileId)){
		alert("다운로드할 파일이 없습니다.");
		return;
	}
	var form = $("<form style='display:none'></form>");
	form.attr('method', 'GET');
	form.attr('action', '/file/FileDown.do?fileId='+fileId);
	var input = $("<input type='submit'></input>");
	form.append(input);
	$('body').append(form);
	form.submit();
	form.remove();
}


function syncUploadFile(id, module,createUser){
	var fileId = "";

	if(!$('#'+id).prop('files')){
		return "";
	}

	var file_data = $('#'+id).prop('files')[0];
	var form_data = new FormData();
	form_data.append('files', file_data);
	form_data.append('createUser', createUser);

	$.ajax({
		url: '/admin/api/files/upload/'+ module,
		dataType: 'json',
		cache: false,
		contentType: false,
		async: false,
		processData: false,
		data: form_data,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		complete : function() {
			hideLoadingImage();
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if ("success" == responseJSON.resultCode) {
				if (responseJSON.errorMessage) {
					alert(responseJSON.errorMessage);
				}
				var resultDataFromServer = responseJSON.resultData;

				if(resultDataFromServer){
					fileId = resultDataFromServer[0].fileId;
				}else{

				}

			} else {
				alert("[오류] " + responseJSON.errorMessage);
				hideLoadingImage();
			}
		},
		error: function (request, status, error) {
			alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});

	return fileId;
}

function getLoginId() {
	var userId;
	$.ajax({
		url: '/admin/api/admin/user/id',
		processData: false,
		contentType: 'application/json',
		data: '{}',
		async: false,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if (responseJSON.errorMessage) {
				alert(responseJSON.errorMessage);
			}
			if ("success" == responseJSON.resultCode) {
				var resultDataFromServer = responseJSON.resultData;
				if (resultDataFromServer) {
					userId = resultDataFromServer.userId;
				}else{
					location.href = "/admin/login";
				}
			} else {
				location.href = "/admin/login";
			}
		},
		error: function (request, status, error) {
			location.href = "/admin/login";
		}
	});

	return userId;
}

function formatDate(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();
	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [year, month, day].join('-');
}

function checkLength(id, minLength, maxLength, isValid){
	var text = $('#'+id).val();

	if(isValid){
		if(text.length == 0){
			return false;
		}
	}else{
		if(text.length == 0){
			return true;
		}
	}

	if(minLength){
		if(text.length < minLength){
			return false;
		}
	}

	if(maxLength){
		if(text.length > maxLength){
			return false;
		}
	}

	return true;
}

function sleep(ms) {
	const wakeUpTime = Date.now() + ms;
	while (Date.now() < wakeUpTime) {}
}

function saveActionLog(action, actionItem) {debugger;
	var jsonData = {
		adminUserAction: action,
		adminUserActionItem: actionItem
	}

	requestData = "{}";
	if(jsonData) {
		requestData = JSON.stringify(jsonData);
	}

	$.ajax({
		url: '/admin/api/admin/user/log/item/create',
		processData: false,
		contentType: 'application/json',
		data: requestData,
		type: 'post',
		beforeSend: function (jqXHR) {
			jqXHR.setRequestHeader('Accept', 'application/json');
			jqXHR.setRequestHeader('AJAX', true);
		},
		success: function (res, textStatus, jqXHR) {
			var responseJSON = jqXHR.responseJSON;
			if (responseJSON.errorMessage) {
				alert(responseJSON.errorMessage);
			}
			if ("success" == responseJSON.resultCode) {
				var resultDataFromServer = responseJSON.resultData;
				if (resultDataFromServer) {
				}else{
					location.href = "/admin/login";
				}
			} else {
				location.href = "/admin/login";
			}
		},
		error: function (request, status, error) {
			location.href = "/admin/login";
		}
	});
}

(function($) {
	var Defaults = $.fn.select2.amd.require('select2/defaults');  
  $.extend(Defaults.defaults, {
  	dropdownPosition: 'auto'
  });  
 	var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');  
  var _positionDropdown = AttachBody.prototype._positionDropdown;  
  AttachBody.prototype._positionDropdown = function() {  
    var $window = $(window);  
		var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
		var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');   
		var newDirection = null;  
		var offset = this.$container.offset();  
		offset.bottom = offset.top + this.$container.outerHeight(false);		
		var container = {
    		height: this.$container.outerHeight(false)
		};
    
    container.top = offset.top;
    container.bottom = offset.top + container.height;

    var dropdown = {
      height: this.$dropdown.outerHeight(false)
    };

    var viewport = {
      top: $window.scrollTop(),
      bottom: $window.scrollTop() + $window.height()
    };

    var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
    var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);
    
    var css = {
      left: offset.left,
      top: container.bottom
    };

    var $offsetParent = this.$dropdownParent;

    if ($offsetParent.css('position') === 'static') {
      $offsetParent = $offsetParent.offsetParent();
    }

    var parentOffset = $offsetParent.offset();

    css.top -= parentOffset.top
    css.left -= parentOffset.left;
    
    var dropdownPositionOption = this.options.get('dropdownPosition');    
		if (dropdownPositionOption === 'above' || dropdownPositionOption === 'below') {    
    		newDirection = dropdownPositionOption;   
    } else {    		
        if (!isCurrentlyAbove && !isCurrentlyBelow) {
      			newDirection = 'below';
    		}
    		if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
      		newDirection = 'above';
    		} else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
      		newDirection = 'below';
    		}    
    }
    if (newDirection == 'above' ||
        (isCurrentlyAbove && newDirection !== 'below')) {
      css.top = container.top - parentOffset.top - dropdown.height;
    }
    if (newDirection != null) {
      this.$dropdown
        .removeClass('select2-dropdown--below select2-dropdown--above')
        .addClass('select2-dropdown--' + newDirection);
      this.$container
        .removeClass('select2-container--below select2-container--above')
        .addClass('select2-container--' + newDirection);
    }
    this.$dropdownContainer.css(css);
  };
  
})(window.jQuery);
