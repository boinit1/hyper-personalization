<%--
  Class Name : EgovIncFooter.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.04.15 BizAn             최초작성
 
  author   : (주)윈솔텍 
  since    : 2022.04.01
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
			<!-- pagination -->
			<div class="listTable noSearch">
				<div class="pagingWrap">
					<div class='prevGroup'>
						<!-- 현재 페이지 블럭이 1 이상이면 '이전'을 출력 -->
						<c:if test="${pager.curBlock > 1}">
							<span class="btnBackword"><a href="javascript:list('1')"></a></span>
						</c:if>
						<!-- 현재 페이지 블럭이 1 이상이면 처음페이지로 이동 -->
						<c:if test="${pager.curBlock > 1}">
							<span class="btnPrev"><a href="javascript:list(${pager.curPage } - 10)"></a></span>
						</c:if>
					</div>
					<div class='paging'>
						<!-- 페이지블럭 시작부터 끝까지 1씩 증가하는 페이지 출력 -->
						<c:forEach var="page_no" begin="${pager.blockBegin}" end="${pager.blockEnd}">
							<c:choose>
								<c:when test="${page_no == pager.curPage }">
										<span class='num on'><a href="#" id="curPage">${page_no }</a></span>
								</c:when>
								<c:otherwise>
										<span class='num'><a href="javascript:list('${page_no }')">${page_no }</a></span>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</div>
					<div class="nextGroup">
						<!-- 마지막페이지 블록으로 이동 -->
						<c:if test="${pager.curBlock < pager.totPage}">
							<span class="btnForword"><a href="javascript:list('${pager.totPage }')"></a></span>
						</c:if>
						<!-- 다음페이지로 이동 -->
						<c:if test="${pager.curBlock < pager.blockEnd}">
							<span class="btnNext"><a href="javascript:list(${pager.curPage} + 10)"></a></span>
						</c:if>
					</div>
				</div>
			</div>
			<div class="paging">
				<ul>
				</ul>
			</div>
			<!-- //pagination -->
			