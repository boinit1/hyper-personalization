<%--
  Class Name : IncFooter.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.04.15 BizAn             최초작성
 
  author   : (주)윈솔텍 
  since    : 2022.04.01
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		 </div>
		 
		 <div class="footer" style="text-align:center; font-size:14px; color:#fff; background:#555;"> 
		 ${address}
		 </div>
 
	</body>
</html>