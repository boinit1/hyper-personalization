<%--
  Class Name : IncHeader.jsp
  Description : 화면상단 Header(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.04.01 BizAn             최초작성
 
  author   : (주)윈솔텍 
  since    : 2022.04.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<div class="gnbSec"> 
		<div class="gnb">
		<span class="gnbMenu on"><a href="/movePage.do?mc=A00">기본설정</a></span>
		</div>
	</div>
