<%--
  Class Name : IncMeatFile.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성
 
  author   : (주)윈솔텍 
  since    : 2022.06.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

    
	<meta http-equiv="Content-Language" content="ko" >
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=0,maximum-scale=10,user-scalable=yes">
	<meta name="HandheldFriendly" content="true">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="imagetoolbar" content="no">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	
	<meta name="description" content="">
	<meta name="keywords" content="">
	<META name="robots" content="index,follow" />
	
	<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico" />
	
	<title>SPS DASHBOARD</title>
		
	<!-- Bootstrap -->
	<link rel="stylesheet" href="/css/bootstrap.min.css" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/tempusdominus-bootstrap-4.min.css" />
	<link rel="stylesheet" href="/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" href="/css/font-awesome.css" />

	<link href="/css/vex.css" rel="stylesheet">
	<link href="/css/vex-theme-default.css" rel="stylesheet">
	
	
	
	
	<!-- 공통 Css //-->
	<link rel="stylesheet" href="/css/common.css?20221223" />
	<link rel="stylesheet" href="/css/style.css" />
	<link rel="stylesheet" href="/css/jquery-ui.css" />
	<link rel="stylesheet" href="/css/wcms.admin.css" />
	
	
	
	<!-- 공통 Css //-->
	<script src="/js/jquery-3.5.1.min.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script src="/js/default.js?ver=1626223438"></script>
	
	
	<script src="/scriptlib/clipboard.min.js"></script>
    
	
	<!-- Bootstrap -->
	<script	src="/js/jquery.dataTables.min.js"></script>
	<script	src="/js/dataTables.bootstrap4.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>


	<script type="text/javascript" src="/js/moment.min.js"></script>
	<script type="text/javascript" src="/js/tempusdominus-bootstrap-4.min.js"></script>

	<script src="/js/tata.js"></script>
	<script src="/js/vex.min.js"></script>
	<script src="/js/vex.combined.min.js"></script>
	<script>vex.defaultOptions.className = 'vex-theme-default'</script >
	<script src="/js/loadingoverlay.min.js"></script>

	<!-- Custom -->
	<script type="text/javascript" src="/js/loadingbar.js"></script>
	<script src="/js/common.js"></script>
	<script src="/js/app.module.js"></script>
		
	<link rel="stylesheet" href="/css/reset.css" />
	
	<!-- CHART //-->
	<script src="/js/chart.js"></script>
	
	<!--  SPS Publishing -->
	<meta name="format-detection" content="telephone=no" />
	<link rel="stylesheet" href="/scriptlib/select2/select2.min.css" />
	<script src="/scriptlib/select2/select2.min.js"></script>	
    
	<!-- ZTree -->
	<link rel="stylesheet" href="/css/zTreeStyle.css">
	<script src="/js/ztree/jquery.ztree.core.js"></script>
	<script src="/js/ztree/jquery.ztree.core.min.js"></script>
	<script type="text/javascript" src="/js/ztree/jquery.ztree.excheck.js"></script>  
	<script type="text/javascript" src="/js/ztree/jquery.ztree.exedit.js"></script> 
	
	<!-- amchart -->
	<script src="/js/amcharts/lib/5/index.js"></script>
	<script src="/js/amcharts/lib/5/xy.js"></script>
	<script src="/js/amcharts/lib/5/themes/Animated.js"></script>
	<script src="/js/amcharts/lib/5/locales/de_DE.js"></script>
	<script src="/js/amcharts/lib/5/geodata/germanyLow.js"></script>
	<script src="/js/amcharts/lib/5/fonts/notosans-sc.js"></script>
	<script src="/js/amcharts/lib/3/amcharts.js"></script>
	<script src="/js/amcharts/lib/3/radar.js"></script>
	<script src="/js/amcharts/lib/3/themes/light.js"></script>
	
	<!-- 보인 xapi 모듈 -->
	<script type="text/javascript" src="/js/boinxapi/tools.js"></script>

<script>
	$(document).ready(function() {	
	   var referrer = document.referrer;
	   if(referrer){
		   if(referrer.includes('LearnView.do')){
			   sendXapi("exited", "강의 완료", "Activity", referrer);
		   }else if(referrer.includes('ProblemSolve.do')){
			   sendXapi("exited", "문제풀이 완료", "Activity", referrer);
		   }else if(referrer.includes('ProblemAnswer.do')){
			   sendXapi("exited", "문제해설 완료", "Activity", referrer);
		   }else{
			   //sendXapi("exited", "페이지 이탈", "Activity", referrer);	   
		   }
	   }	
	   
	   if(window.location.href.includes('LearnView.do')){
	   }else if(window.location.href.includes('ProblemSolve.do')){
	   }else if(window.location.href.includes('ProblemAnswer.do')){
	   }else{
		   //sendXapi("attempted", "페이지 방문", "Activity", window.location.href);   
	   }	   
	});
</script>