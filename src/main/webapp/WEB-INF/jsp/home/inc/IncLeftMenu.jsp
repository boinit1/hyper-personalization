<%--
  Class Name : IncLeftMenu.jsp
  Description :  좌메뉴 화면(include)
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.04.20 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2024.04.01
--%>
<%@page import="org.springframework.util.ObjectUtils"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import = "java.util.HashMap" %>
<%@ page import = "java.util.List" %>
<%@ page import = "egovframework.rte.psl.dataaccess.util.EgovMap" %>
<%@ page import ="sps.login.LoginVO" %>
<%
	// 세션 정보를 가져온다. LoginVO
     sps.login.LoginVO loginVO = sps.utils.UserDetailsHelper.getAuthenticatedUser();
	 pageContext.setAttribute("loginVO", loginVO) ;
     String contextPath = request.getContextPath();
	if(loginVO == null){
	    response.sendRedirect("/login/LoginUsr.do");
		return;
	}
	
	
%>

<!-- 스타일 설정 -->

<style type="text/css">

.bgLayer {display:none;position:absolute; top:0; left:0; width:100%; height:100%; background:#000; opacity:.5; filter:alpha(opacity=50); z-index:10;}  /* z-index가 10이다. 이보다 위에 보이기 위해선 팝을 이보다 크게 설정해야한다. */

</style>

<!-- //스타일 설정 -->





	<!-- leftSec //-->
	<div class="leftSec">
		<iframe src="" name="hiddenframe" id="hiddenframe" width="0" height="0"></iframe>
		
		<div class="mngUtilSec">
			<p class="mngName"><a href="/wuser/WUserMyInfo.do"><em>${loginVO.getUserNm()}</em></a><spring:message code='msg.info.greeting'/> &nbsp;&nbsp;&nbsp;<font color="yellow"><lable id="timer"></lable></font></p>
			
			<div class="btnArea arRight">
				<form action="/wlogin/actionLogout.do" method="GET" id="profileLogout">
<!-- 					<input type="hidden" th:name="${_csrf.parameterName}" th:value="${_csrf.token}"/> -->
					<span class="btnS black"><a href="#" onclick="document.getElementById('profileLogout').submit();"><spring:message code='btn.logout'/></a></span>
				</form>
			</div>
		</div>
		<div class="lnbSec">
			<ul>
			<c:forEach items="${menuList}" var="item" varStatus="status">
					
			    <c:if test="${item.menuDepth == '1'}">
			    <li class="on">
					<p class="lnb"><a href="${item.menuUrl}?menuId=${item.menuIdx}">${item.menuNm}</a></p>
				</li>
				</c:if>
				
				<c:if test="${item.menuDepth == '2'}">
				<li style="padding-left:20px;">
					<p class="lnb"><a href="${item.menuUrl}?menuId=${item.menuIdx}">${item.menuNm}</a></p>
				</li>
				</c:if>
					
			</c:forEach>
			</ul>
		</div>
	<div class="bottom"></div>
	</div>
	
	<!-- leftSec //-->
	
<script>
/* 전역 변수 */
var initMinute;  // 최초 설정할 시간(min)
var remainSecond;  // 남은시간(sec)
 
$(document).ready(function(){
   clearTime(30); // 세션 만료 적용 시간 
   setTimer();    // 문서 로드시 타이머 시작
});
 
function clearTime(min){ // 타이머 초기화 함수
   initMinute = min; 
   remainSecond = min*60; 
}
 
function setTimer(){ // 1초 간격으로 호출할 타이머 함수 
 
   // hh : mm 으로 남은시간 표기하기 위한 변수
   remainMinute_ = parseInt(remainSecond/60);
   remainSecond_ = remainSecond%60;
 
   if(remainSecond > 0){
      $("#timer").empty();
      $("#timer").append(Lpad(remainMinute_,2) + ":" + Lpad(remainSecond_,2));    // hh:mm 표기
      remainSecond--;
      setTimeout("setTimer()",1000); //1초간격으로 재귀호출!
   }else{
      alert("<spring:message code='msg.info.sessionexpire'/>");
      document.location.href = "/wlogin/WLoginUsr.do";
   }
}
 
function Lpad(str,len){  // hh mm형식으로 표기하기 위한 함수
   str = str+"";
   while(str.length<len){
      str = "0"+str;
   }
   return str;
}
</script>
	
