<%--
  Class Name : EnglishLearnView.jsp
  Description : 영어 학습영상 화면
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.10.18 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2022.10.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp?v=1" />

	<!-- GA 설정 START -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-5GJ4D8K26Q"></script>
</head>

<body>
<form id="frm" name="frm" autocomplete="off">

    <input type="hidden" id="bookCode" name="bookCode" value="${bookCode}">
	<input type="hidden" id="chapterCode" name="chapterCode" value="${chapterCode}">
	<input type="hidden" id="learnCode" name="learnCode" value="${learnCode}">
	<input type="hidden" id="mp4" name="mp4" value="${mp4}">
	<input type="hidden" id="videoYn" name="videoYn" value="${videoYn}">
	<input type="hidden" id="learnName" name="learnName" value="${learnName}">
	<input type="hidden" id="index" name="index" value="0">
	
	<div class="viewer" style="height:700px;">
			<div class="c-aligner" style="width:1250px;">
				<div class="viewer-content">
					
					<div id="mediaDiv" class="viewer-video">
						<c:if test="${videoYn == 'Y'}">  
						<video id="media" controls autoplay>지원되지 않는 영상입니다
						      <source src="" type="video/mp4">
                        </video>
                        </c:if>
                        <c:if test="${videoYn == 'N'}">
                        <iframe id="noMedia"
						    src="${mp4}"
						    width="1250px"
						    height="900px">
						</iframe>
						</c:if>
                    </div>
                    <!--  <button id="button">skip</button> -->
					
					<div class="viewer-function"><!-- active클래스 추가 시 활성화 -->
						<a href="#" class="viewer-trigger"></a>
						<div class="viewer-cont">
							<!-- 
							<div class="viewer-box">
								<div class="viewer-heading" id="viewerHeadingTitle">수학 5-①. <br/>자연수의 혼합계산(1)</div>
								<div class="viewer-listbox">
									<ul class="viewer-list" id="viewerList">
										<li id="indexView0" class="viewer-item" onclick="JavaScript:indexView('0','8','0');"><a href="#" class="viewer-link">시작</a></li>
										<li id="indexView1" class="viewer-item active" onclick="JavaScript:indexView('1','8','1');"><a href="#" class="viewer-link">개념 열기</a></li>
										<li id="indexView2" class="viewer-item" onclick="JavaScript:indexView('2','8','2');"><a href="#" class="viewer-link">개념 쏙쏙</a></li>
										<li id="indexView3" class="viewer-item" onclick="JavaScript:indexView('3','8','3');"><a href="#" class="viewer-link">퀴즈퀴즈</a></li>
										<li id="indexView4" class="viewer-item" onclick="JavaScript:indexView('4','8','4');"><a href="#" class="viewer-link">수학익힘</a></li>
										<li id="indexView5" class="viewer-item" onclick="JavaScript:indexView('5','8','5');"><a href="#" class="viewer-link">개념 쏙쏙</a></li>
										<li id="indexView6" class="viewer-item" onclick="JavaScript:indexView('6','8','6');"><a href="#" class="viewer-link">퀴즈퀴즈</a></li>
										<li id="indexView7" class="viewer-item" onclick="JavaScript:indexView('7','8','7');"><a href="#" class="viewer-link">수학익힘</a></li>
									</ul>
								</div>
							</div>
							 -->
							<div class="viewer-box viewer-box--blue">
<%-- 							<c:if test="${videoYn == 'Y'}"> --%>
								<div class="viewer-heading">다른 추천강의 보기</div>
								<div class="viewer-listbox">
									<ul class="viewer-list">
								    <c:forEach items="${learnList}" var="item" varStatus="status">
										<li id="viewrItem${status.index}" class="viewer-item <c:if test='${item.learnCode == learnCode}'>active</c:if>">
											<a href="/english/EnglishLearnView.do?bookCode=${bookCode}&chapterCode=${chapterCode}&learnCode=${learnCode}&topicCode=${item.topicCode}" class="viewer-link">
												<em>${item.shortName}</em>
												<span>${item.longName}</span>
											</a>
										</li>
									</c:forEach>
									   
									</ul>
								</div>
<%-- 							  </c:if> --%>
							</div>
						</div>
						<div class="viewer-buttons">
							<a href="JavaScript:goOut();" class="viewer-button">나가기</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$('.viewer-trigger').on('click', function(){
				$('.viewer-function').toggleClass('active');
				return false;
			});
		</script>
 </form>
	
	
	
<script type="text/javascript">

    var media = null;
    
	window.onload = function(){
		//media = document.getElementById('media');
		//play = document.getElementById("play");
        //content = document.getElementById("content");
        //content2 = document.getElementById("content2");
        
		
     }

	//상단 목차 뷰
	function indexView(indexCode, totIndex, curIndex) {
		
		$('#index').val(curIndex);
		
		//현재 강의  재생시간 저장
		mediaPlayTimeSave();
		//insertXApi("complete", "");
		
		//클릭한 추천강의 액티브로 변경
		for (i=0;i<totIndex;i++) {
			var tagId = 'indexView' + i;
			if (i == curIndex)
				document.getElementById(tagId).className = "viewer-item active";
			else
				document.getElementById(tagId).className = "viewer-item";
		}
		
		//강의 영상 읽어 오기
        
		var bookCode = $('#bookCode').val();
		var chapterCode = $('#chapterCode').val();
		//location.href="/english/englishLearnView.do?bookCode="+bookCode+"&chapterCode="+chapterCode+"&learnCode="+learnCode;
	}

    //추천강의 뷰
	function learnView(learnCode, mp4, shortName, longName, totIndex, curIndex) {
		
    	$('#learnCode').val(learnCode);
    	$('#index').val(curIndex);
    	
        //alert($('#learnCode').val());
		//현재 강의  재생시간 저장
		mediaPlayTimeSave();
		
		//클릭한 추천강의 액티브로 변경
		for (i=0;i<totIndex;i++) {
			var tagId = 'viewrItem' + i;
			if (i == curIndex)
				document.getElementById(tagId).className = "viewer-item active";
			else
				document.getElementById(tagId).className = "viewer-item";
		}
		
		//클릭한 추천강의를 위 제목 강의명으로 변경
		//document.getElementById('viewerHeadingTitle').innerHTML=shortName + "<br/>" + longName;
		
		//강의 목차 정보 읽어 오기
		requestBody = getFormDataToJson('frm');
		sendAjaxJsonData("/comm/getChapterList.do", requestBody, "callbackGetChapterList", 'json', false);
		
		//mp4 = '/media/' + '1051D00011103' + '/' + '1051D00011103_02' + '.mp4';  //테스트용
		//$('#mediaDiv video source').attr('src', videoFile);
		//$("#mediaDiv video")[0].load();
		
		media = document.querySelector('#media');
	    media.removeAttribute('src'); // src 프로퍼티를 제거
	    media.src = mp4; // 다른 미디어로 소스 변경
	    media.load(); // 새로운 정보를 다시 로드

	    media.play(); // 
		   
		var bookCode = $('#bookCode').val();
		var chapterCode = $('#chapterCode').val();
		//location.href="/english/EnglishLearnView.do?bookCode="+bookCode+"&chapterCode="+chapterCode+"&learnCode="+learnCode;
		
		
		media.addEventListener("playing", function(){
			//alert("재생시작");
			
			insertXApi("played");
		}, false);
		
		media.addEventListener("pause", function(){
			if (media.duration != media.currentTime) {
				//alert("일시중지");
				insertXApi("paused");
			}
		}, false);

		media.addEventListener("ended", function(){
			//alert("재생종료");
			insertXApi("complete");
		}, false);
	}
	
	
	function sendAjaxCallback(callbackId, resultData, pageInfo){
    	//  debugger;
		if(callbackId == 'callbackGetChapterList'){
  			
  			if (resultData == '0') return false;
  				
			var tabs = resultData;
			//$('#viewerList').empty();
			
			var s = "<li class='viewer-item'><a href='#' class='viewer-link'>시작</a></li>" + "<li class='viewer-item'><a href='#' class='viewer-link'>시작</a></li>";
			
			var innerHtml = "";
			
			for (var i = 0; i < tabs.length; i++) {
				
				innerHtml = innerHtml + "<li class='viewer-item'><a href='#' class='viewer-link'>" + tabs[i].chapterName + "</a></li>";
			}
			
			//document.getElementById('viewerList').innerHTML = innerHtml;
			
  	     }
  	 
   }
	
	function goOut() {
		
		var videoYn = $('#videoYn').val();
		if (videoYn == 'Y') {
			mediaPlayTimeSave();
			insertXApi("exited", "");
		}
		
		var bookCode = $('#bookCode').val();
		var chapterCode = $('#chapterCode').val();
		
		location.href="/english/EnglishDashboardView2.do?bookCode="+bookCode+"&chapterCode="+chapterCode;
	}
	
	//강의 재생시간 저장
	function mediaPlayTimeSave() {
		var bookCode = $('#bookCode').val();
		var chapterCode = $('#chapterCode').val();
		var learnCode = $('#learnCode').val();
		
		var totTime = parseInt(String(media.duration));
		var playTime = parseInt(String(media.currentTime));
		//alert(totTime);
		//alert(curTime);
		
		var requestBody = {
				'bookCode' : bookCode,
				'chapterCode' : chapterCode,
				'learnCode' : learnCode,
				'playTime' : playTime,
				'totTime' : totTime
			};
		
		sendAjaxJsonData("/comm/insertLoPlayTime.do", requestBody, "callbackInsertLoPlayTime", 'json', false);
	}
	
	function sendAjaxCallback(callbackId, resultData, pageInfo){
    	//  debugger;
		if(callbackId == 'callbackGetChapterList'){
  			
  			if (resultData == '0') return false;
  				
			var tabs = resultData;
			//$('#viewerList').empty();
			
			var s = "<li class='viewer-item'><a href='#' class='viewer-link'>시작</a></li>" + "<li class='viewer-item'><a href='#' class='viewer-link'>시작</a></li>";
			
			var innerHtml = "";
			
			for (var i = 0; i < tabs.length; i++) {
				
				innerHtml = innerHtml + "<li class='viewer-item'><a href='#' class='viewer-link'>" + tabs[i].chapterName + "</a></li>";
			}
			
			document.getElementById('viewerList').innerHTML = innerHtml;
			
  	     }
  	 
   }
	
	$(document).ready(function() {
		
		sendXapi("attempted", "강의 시작", "Activity", window.location.href, $('#learnName').val());
		
		  var videoYn = $('#videoYn').val();
		
		   //강의 진입
		   insertXApi("access", "");
		   
		   if (videoYn == 'Y') {
			   media = document.querySelector('#media');
			   media.removeAttribute('src'); // src 프로퍼티를 제거
			   media.src = $('#mp4').val(); // 다른 미디어로 소스 변경
			   media.load(); // 새로운 정보를 다시 로드

			   media.play(); // 
			   
			   
			   media.addEventListener("playing", function(){
					//alert("재생시작");
					
					insertXApi("played");
				}, false);
				
				media.addEventListener("pause", function(){
					if (media.duration != media.currentTime) {
						//alert("일시중지");
						insertXApi("paused");
					}
				}, false);

				media.addEventListener("ended", function(){
					//alert("재생종료");
					insertXApi("complete");
				}, false);
		   }
			
			
	   });
	
	//강의 플래이 xapi
	function insertXApi(verb) {
		var bookCode = $('#bookCode').val();
		var chapterCode = $('#chapterCode').val();
		var learnCode = $('#learnCode').val();
		var index = $('#index').val();
		
		//var totTime = parseInt(String(media.duration));
		//var playTime = String(media.currentTime);
		//alert(totTime);
		//alert(curTime);
		
		var objectId = "";
		var enGbDesc = "";
		var koKrDesc = "";
		
		if (verb == "access"){
			objectId = learnCode;
			enGbDesc = "complete lecture";
			koKrDesc = "강의 완료";
		}else if (verb == "complete"){
			objectId = learnCode + "|" + index;
			enGbDesc = "complete lecture";
			koKrDesc = "강의 완료";
		}else if (verb == "played") {
			objectId = learnCode + "|" + index + "|" + media.currentTime;
			enGbDesc = "played lecture";
			koKrDesc = "강의 재생";
		}else if (verb == "paused") {
			objectId = learnCode + "|" + index + "|" + media.currentTime;
			enGbDesc = "paused lecture";
			koKrDesc = "강의 일시정지";
		}else if (verb == "exited") {
			objectId = learnCode + "|" + index + "|" + media.currentTime;
			enGbDesc = "exited lecture";
			koKrDesc = "강의 이탈";
		}
			
		
		//xapiParamMap.put("objectId", "");
		//xapiParamMap.put("enGbDesc", "select period");
		//xapiParamMap.put("enUsDesc", "select period");
		//xapiParamMap.put("koKrDesc", "학기선택(1)");
		
		var requestBody = {
				'verb' : verb,
				'objectId' : objectId,
				'enGbDesc' : enGbDesc,
				'enUsDesc' : enGbDesc,
				'koKrDesc' : koKrDesc
			};
		
		sendAjaxJsonData("/comm/insertXApi.do", requestBody, "callbackInsertXApi", 'json', false);
	}
	
	
	//강의 플래이 xapi
	function insertXApi2(verb, beforePlayTime, currentPlayTime) {
		var bookCode = $('#bookCode').val();
		var chapterCode = $('#chapterCode').val();
		var learnCode = $('#learnCode').val();
		var index = $('#index').val();
		
		//var totTime = parseInt(String(media.duration));
		//var playTime = String(media.currentTime);
		//alert(totTime);
		//alert(curTime);
		
		var objectId = "";
		var enGbDesc = "";
		var koKrDesc = "";
		
		objectId = learnCode + "|" + beforePlayTime + "|" + currentPlayTime;
		enGbDesc = "skipped lecture";
		koKrDesc = "강의 건너뛰기";
		
		var requestBody = {
				'verb' : verb,
				'objectId' : objectId,
				'enGbDesc' : enGbDesc,
				'enUsDesc' : enGbDesc,
				'koKrDesc' : koKrDesc
			};
		
		sendAjaxJsonData("/comm/insertXApi.do", requestBody, "callbackInsertXApi", 'json', false);
		//media.currentTime += parseFloat(5.0);
	}
	
	/*
	media.addEventListener("play", function(){
		if (media.currentTime == 0){
			//alert("재생진입");
		}
	}, false);
	*/
	
	
	
	
	
</script>
</body>
</html>
