<%--
  Class Name : LoginUsr.jsp
  Description : Login 인증 화면
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2022.06.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp?v=1" />
    <!-- TABLE 헤드 고정 By BizAn 추가 -->
	 <style>
            #learnTable th {
            position: sticky;
            top: 0px;
        }
    </style>
    <script>
    (function($) {
    	var Defaults = $.fn.select2.amd.require('select2/defaults');  
      $.extend(Defaults.defaults, {
      	dropdownPosition: 'auto'
      });  
     	var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');  
      var _positionDropdown = AttachBody.prototype._positionDropdown;  
      AttachBody.prototype._positionDropdown = function() {  
        var $window = $(window);  
    		var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
    		var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');   
    		var newDirection = null;  
    		var offset = this.$container.offset();  
    		offset.bottom = offset.top + this.$container.outerHeight(false);		
    		var container = {
        		height: this.$container.outerHeight(false)
    		};
        
        container.top = offset.top;
        container.bottom = offset.top + container.height;

        var dropdown = {
          height: this.$dropdown.outerHeight(false)
        };

        var viewport = {
          top: $window.scrollTop(),
          bottom: $window.scrollTop() + $window.height()
        };

        var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
        var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);
        
        var css = {
          left: offset.left,
          top: container.bottom
        };

        var $offsetParent = this.$dropdownParent;

        if ($offsetParent.css('position') === 'static') {
          $offsetParent = $offsetParent.offsetParent();
        }

        var parentOffset = $offsetParent.offset();

        css.top -= parentOffset.top
        css.left -= parentOffset.left;
        
        var dropdownPositionOption = this.options.get('dropdownPosition');    
    		if (dropdownPositionOption === 'above' || dropdownPositionOption === 'below') {    
        		newDirection = dropdownPositionOption;   
        } else {    		
            if (!isCurrentlyAbove && !isCurrentlyBelow) {
          			newDirection = 'below';
        		}
        		if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
          		newDirection = 'above';
        		} else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
          		newDirection = 'below';
        		}    
        }
        if (newDirection == 'above' ||
            (isCurrentlyAbove && newDirection !== 'below')) {
          css.top = container.top - parentOffset.top - dropdown.height;
        }
        if (newDirection != null) {
          this.$dropdown
            .removeClass('select2-dropdown--below select2-dropdown--above')
            .addClass('select2-dropdown--' + newDirection);
          this.$container
            .removeClass('select2-container--below select2-container--above')
            .addClass('select2-container--' + newDirection);
        }
        this.$dropdownContainer.css(css);
      };
      
    })(window.jQuery);
    </script>
</head>

<body>
<form id="frm" name="frm" autocomplete="off">

    <input type="hidden" id="lessonCode" name="lessonCode" value="10">
	<input type="hidden" id="chapterCodeSel" name="chapterCodeSel" value="">
	
	<input type="hidden" id="contextChartLabel" name="contextChartLabel" value="${contextChartLabel}">
	<input type="hidden" id="contextChartData" name="contextChartData" value="${contextChartData}">
	<input type="hidden" id="contextChartData2" name="contextChartData2" value="${contextChartData2}">
	<input type="hidden" id="activityChartLabel" name="activityChartLabel" value="${activityChartLabel}">
	<input type="hidden" id="activityChartData" name="activityChartData" value="${activityChartData}">
	
	<input type="hidden" id="chapterName" name="chapterName" value="${fn:split(chapterList[0].chapterName, '.')[1]}">
	
	<input type="hidden" id="lesson" name="lesson" value="1">
	
	<div class="c-wrap c-wrap--large">
			<div class="dashboard">
				<div class="d-header">
					<div class="c-aligner">
						<div class="d-tab">
							<ul class="d-tab_list">
								<li class="d-tab_item"><a href="/math/MathDashboardView.do" class="d-tab_link">수학</a></li><!-- active클래스 추가 시 활성화 -->
								<li class="d-tab_item active"><a href="#" class="d-tab_link">영어</a></li>								
							</ul>
<!-- 							<ul class="d-tab_list" style="float:right;"> -->
<%-- 								<li class="d-tab_item active"><a href="/comm/finalEnglish.do?problemNo=1" class="d-tab_link" style="padding-left:20px; padding-right:20px;">영어최종진단 ${finalEnglishMessage}</a></li>								 --%>
<!-- 							</ul> -->
<!-- 							<ul class="d-tab_list" style="float:right;"> -->
<%-- 								<li class="d-tab_item active"><a href="/comm/finalMath.do?problemNo=1" class="d-tab_link" style="padding-left:20px; padding-right:20px;">수학최종진단 ${finalMathMessage}</a></li>								 --%>
<!-- 							</ul> -->
						</div>
					</div>
				</div>
				<div class="d-content">
					<div class="c-aligner">
						<a href="#" class="d-arrow d-arrow--prev d-arrow--disabled"></a><!-- d-arrow--disabled 클래스 추가 시 비활성화 스타일 적용 -->
						<div class="d-section">
							<div class="d-current">
								<div class="d-cont" style="font-size: 30px; padding:0px;">
									${message}
								</div>
								<div class="d-current_select" style="display:none;">
									<div class="d-current_select">
										<div class="c-select">
											<select name="bookCode" id="bookCode">
											    <c:forEach items="${bookList}" var="item" varStatus="status">
												<option value="${item.bookCode}" ${item.bookCode == bookCode ? 'selected':''}>${item.termGubun}학기</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="d-current_select d-current_select--large" style="display:none;">
										<div class="c-select">
											<select name="chapterCode" id="chapterCode">
												<c:forEach items="${chapterList}" var="item" varStatus="status">
												<option value="${item.chapterCode}" ${item.chapterCode == chapterCode ? 'selected':''}>${fn:split(item.chapterName, '.')[1]}</option>
												<c:if test="${status.count == 1}">
												   <script>
												       //alert("${item.chapterCode}");
												       $('#chapterCodeSel').val("${item.chapterCode}");
												   </script>
												</c:if>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								<div class="d-currentbox">
									<dl>
										<dt>총 학습시간</dt>
										<dd>
											<em id="totPlayHour">${learnInfo.totPlayHour}</em> 시간 <em id="totPlayMin">${learnInfo.totPlayMin}</em> 분
										</dd>
									</dl>
									<dl>
										<dt>강의 수강현황</dt>
										<dd>
											<em id="totPlayCnt">${learnInfo.totPlayCnt}</em> 개
										</dd>
									</dl>
									<dl>
										<dt>문제풀이 현황</dt>
										<dd>
											<em id="answerProblemCnt">${learnInfo.answerProblemCnt}</em> 개 / <em id="totProblemCnt">${learnInfo.totProblemCnt}</em> 개
										</dd>
									</dl>
								</div>
							</div>
						</div>
						
						<div class="d-section" id="inner_list">
							<div class="d-box">
								<div class="d-heading">1학기 1단원 [Where Are You From?] 학습 성취도</div>
								<div class="d-cont">
									<div class="d-tablebox">
										<table class="d-table" id="learnTable">
											<colgroup>
												<col style="width: 70px" />
												<col />
												<col style="width: 140px" />
												<col style="width: 140px" />
												<col style="width: 140px" />
											</colgroup>
											<thead>
												<tr>
													<th scope="col"><em>NO</em></th>
													<th scope="col" class="left">차시명</th>
													<th scope="col">학습수행여부</th>
													<th scope="col">맞은 문제수</th>
													<th scope="col">학습성취도</th>
												</tr>
											</thead>
											<tbody id="learnBody">
											    <c:forEach items="${learnList}" var="item" varStatus="status">
												<tr>
													<td>${status.count}</td>
													<td class="left"><a href="#" class="d-table_link">${item.longName}</a></td>
													<td>${item.jumsu == 0 || item.jumsu == null?'학습안함':'학습함'}</td>
													<td>${item.problemCount - item.wrongCount}/${item.problemCount}</td>
													<td>${item.jumsu > 0 ? item.jumsu:'-'}${item.jumsu > 0 ?'%':''}</td>
												</tr>
												</c:forEach>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						
						<div class="d-section">
							<div class="d-boxs">
								<div class="d-box">
									<div class="d-heading">학습진단분석</div>
									<div class="d-cont">
										<div class="d-analysis">
											<div class="d-analysis_heading">
												<div class="d-info">내용영역</div>
											</div>
											<div class="d-analysis_tabbox">
												<canvas id="contextChart" height="350" style="width: 100%"></canvas>
											</div>
										</div>
									</div>
								     
								</div>
								
								<div class="d-box">
									<div class="d-heading">학습활동이력 <a href="/comm/XapiDashboard.do?url=${lrsUrl2}"> [보인LRS]</a><a href="/comm/SkDashboard.do"> [서강LRS] </a></div>
									<div class="d-cont">
										<iframe src="${lrsUrl1}" style="width:100%; height:400px;"></iframe>
									</div>
								</div>
								
								<div class="d-box" style="display:none;">
									<div class="d-heading">학습진단분석</div>
									<div class="d-cont">
										<div class="d-analysis">
											<div class="d-analysis_heading">
												<div class="d-info">행동영역</div>
												<div class="d-analysis_navi">
													<lable id="contextLabel">Lesson1</lable> > <lable id="activityLabel">1차시</lable>
												</div>
											</div>
											<div class="d-analysis_cont">
											  <div class="d-analysis_tabbox">
													<canvas id="activityChart" height="330" width="330"></canvas>
													<div class="d-analysis_tab" id="learnTable2">
													    <c:forEach items="${learnList}" var="item" varStatus="status">
															<a id="a${status.count}" href="javascript:lessonClick(${status.count});" class="green">${status.count}차시</a>
														</c:forEach>
													    <!-- 
														<a id="a1" href="javascript:lessonClick(1);" class="purple  active">1차시</a> active클래스 추가 시 활성화 
														<a id="a6" href="javascript:lessonClick(6);" class="red">6차시</a>
														<a id="a2" href="javascript:lessonClick(2);" class="green">2차시</a>
														<a id="a7" href="javascript:lessonClick(7);" class="yellow">7차시</a>
														<a id="a3" href="javascript:lessonClick(3);" class="orange">3차시</a>
														<a id="a8" href="javascript:lessonClick(8);" class="deep-pink">8차시</a>
														<a id="a4" href="javascript:lessonClick(4);" class="pink">4차시</a>
														<a id="a9" href="javascript:lessonClick(9);" class="yellow-green">9차시</a>
														<a id="a5" href="javascript:lessonClick(5);" class="skyblue">5차시</a>
														 -->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a href="javascript:goView2();" class="d-arrow d-arrow--next"></a><!-- d-arrow--disabled 클래스 추가 시 비활성화 스타일 적용 -->
					</div>			
				</div>
			</div>
		</div>
 </form>
	
<form action="/login/actionLogout.do">
	<div style="text-align:center;">
		<input type="submit" value="로그아웃">
	</div><br>
</form>	
	
<script type="text/javascript">
    
	$('select').select2({
		minimumResultsForSearch: Infinity,
		dropdownPosition: 'below'
	});
	$('select').on("change", function() {
		$(this).parent().addClass('change');
	});

	// q-header
	$(window).scroll(function(){
		var scrollX = $(this).scrollLeft();
		$('.q-header').scrollLeft(scrollX,300,'linear');
	})
	
	$("#bookCode").change(function(event) {
		//sendXapi("selected", "학기 선택", "Activity", window.location.href, event.target.selectedOptions[0].text);
		requestBody = getFormDataToJson('frm');
		sendAjaxJsonData("/comm/getChapterList.do", requestBody, "callbackGetChapterList", 'json', false);
		//sendAjaxJsonData("/sps/getLeanList.do", requestBody, "callbackGetLeanList", 'json', false);
		
		//내용영역차트 업데이트
		/*
		contextChart.config.data.datasets.forEach(function(dataset) {
    		dataset.data = dataset.data.map(function() {
    			return randomScalingFactor();
    		});
    	});
		contextChart.update();
		*/
		
		sendAjaxJsonData("/comm/getContextChartList.do", requestBody, "callbackGetContextChartList", 'json', false);
		
		//학기 선택
		insertXApi("selected", "period");
	});
	
	$("#chapterCode").change(function(event) {
		//sendXapi("selected", "단원 선택", "Activity", window.location.href, event.target.selectedOptions[0].text);
		var chapterCode = $(this).val();
		//alert(chapterCode);
		$('#chapterCodeSel').val(chapterCode);
		requestBody = getFormDataToJson('frm');
		sendAjaxJsonData("/comm/getLearnList.do", requestBody, "callbackGetLearnList", 'json', false);
		sendAjaxJsonData("/comm/getLearnList.do", requestBody, "callbackGetLearnList", 'json', false);
		sendAjaxJsonData("/comm/getLearnList.do", requestBody, "callbackGetLearnList", 'json', false);
		sendAjaxJsonData("/comm/getLearnList.do", requestBody, "callbackGetLearnList", 'json', false);
		sendAjaxJsonData("/comm/getLearnList.do", requestBody, "callbackGetLearnList", 'json', false);
		sendAjaxJsonData("/comm/getLearnInfo.do", requestBody, "callbackGetLearnInfo", 'json', false);
		
		//단원 선택
		insertXApi("selected", ""); 
	});
	
	function goView2() {
		
		var bookCode = $('#bookCode').val();
		
		//이탈
		insertXApi("exited", ""); 
		
		//alert($('#chapterCodeSel').val());
		location.href="/english/EnglishDashboardView2.do?bookCode="+bookCode+"&chapterCode="+$('#chapterCodeSel').val();
		
	}
	
	function sendAjaxCallback(callbackId, resultData, pageInfo){
    	//  debugger;
    	if(callbackId == 'callbackGetListInner'){
    		debugger;
    		
    			$('#inner_list').html(resultData);
  				$('#inner_list').show();
  	    }else if(callbackId == 'callbackGetLearnList'){
  			if (resultData != '0') {
  				var tabs = resultData;
  				var targetBody = document.getElementById('learnBody');
  				var targetBody2 = document.getElementById('learnTable2');
  				
  				//$(targetBody).remove();
  				
  				//debugger;
  				
  				$( '#learnTable > tbody').empty();
  				$( '#learnTable2').empty();
  				
  				var t = 0;
  				for (var i = 0; i < tabs.length; i++) {
  					t++
  					$(targetBody).append('<tr>');
  					$(targetBody).append('<td>' + t +'</td>');
  					$(targetBody).append("<td class='left'><a href='#' class='d-table_link'>" + tabs[i].longName + "</a></td>");
  					if (tabs[i].jumsu == 0 || tabs[i].jumsu == null )
  						$(targetBody).append('<td>학습안함</td>');
  					else
  						$(targetBody).append('<td>학습함</td>');
  					
  					$(targetBody).append('<td>' + (tabs[i].problemCount - tabs[i].wrongCount)+ '/' + tabs[i].problemCount +'</td>');
  					if (tabs[i].jumsu == 0 || tabs[i].jumsu == null )
  						$(targetBody).append('<td></td>');
  					else
  						$(targetBody).append('<td>' + tabs[i].jumsu +'%</td>');
  					
  					$(targetBody).append('</tr>');
  					
  					$(targetBody2).append("<a id='a" + t + "' href='javascript:lessonClick(" + t + ");' class='green'>" + t + "차시</a>");
  				}
  				
  				lessonClick(1);
  			}
  	  }else if(callbackId == 'callbackGetLearnList2'){
  			if (resultData != '0') {
  				var tabs = resultData;
  				var targetBody2 = document.getElementById('learnTable2');
  				
  				//$(targetBody).remove();
  				
  				//debugger;
  				
  				$('#learnTable2').empty();
  				
  				var t = 0;
  				for (var i = 0; i < tabs.length; i++) {
  					t++
  					$(targetBody2).append("<a id='a" + t + "' href='javascript:lessonClick(" + t + ");' class='green'>" + t + "차시</a>");
  				}
  				
  				lessonClick(1);
  			}		
  		}else if(callbackId == 'callbackGetChapterList'){
  			
  			if (resultData == '0') return false;
  				
			var tabs = resultData;
			var targetSel = document.getElementById('chapterCode');
			$(targetSel).empty();
			//$(targetSel).append("<option value=''><spring:message code='lbl.all'/></option>");
			for (var i = 0; i < tabs.length; i++) {
				$(targetSel).append('<option value=' + tabs[i].chapterCode + '>'
										+ tabs[i].chapterName2 + '</option>');
			}
			
			requestBody = getFormDataToJson('frm');
			sendAjaxJsonData("/comm/getLearnList.do", requestBody, "callbackGetLearnList", 'json', false);
  	    
  		}else if(callbackId == 'callbackGetLearnInfo'){
 			
 			if (resultData == '0') return false;
 			var learnInfo = resultData;
 			
 			//debugger;
 			
 			if (typeof learnInfo.totPlayHour == "undefined") {
    				//alert(learnInfo.totPlayHour);
    				learnInfo.totPlayHour = 0;
    				learnInfo.totPlayMin = 0;
    				learnInfo.totPlayCnt = 0;
    				learnInfo.answerProblemCnt = 0;
    				learnInfo.totProblemCnt = 0;
    			} 
 			
 			document.getElementById('totPlayHour').innerHTML = learnInfo.totPlayHour;
 			document.getElementById('totPlayMin').innerHTML = learnInfo.totPlayMin;
 			document.getElementById('totPlayCnt').innerHTML = learnInfo.totPlayCnt;
 			document.getElementById('answerProblemCnt').innerHTML = learnInfo.answerProblemCnt;
 			document.getElementById('totProblemCnt').innerHTML = learnInfo.totProblemCnt;
 			
       }else if(callbackId == 'callbackGetActivityChartList'){
 			
	    	 //debugger;
	    	 
			if (resultData == '0') return false;
			
			var chartList = resultData;
			var chartData = [];
			
			for (var i = 0; i < chartList.length; i++) {
				chartData.push(chartList[i].resultValue);
			}
			
			activityChart.config.data.datasets.forEach(function(dataset) {
	    		dataset.data = chartData;
	    		/*
	    		dataset.data =  dataset.data.map(function() {
	    			return randomScalingFactor();
	    		});
	    		*/
	    	});
			
		    activityChart.update();
			
      }else if(callbackId == 'callbackGetContextChartList'){
			
	    	 //debugger;
	    	 
			if (resultData == '0') return false;
			
			
			var chartList = resultData;
			var chartData = [];
			var chartData2 = [];
			
			for (var i = 0; i < chartList.length; i++) {
				chartData.push(chartList[i].resultValue1);
				
				//alert(chartList[i].resultValue);
			}
			
			for (var i = 0; i < chartList.length; i++) {
				chartData2.push(chartList[i].resultValue2);
				
				//alert(chartList[i].resultValue);
			}
			
			
			var contextLABEL = null;
			
			/*
			if (chartList[0].bookCode == '0251D')
				contextLABEL = [['자연수의','혼합계산'], '약수와 배수', '규칙과 대응', '약분과 등분', ['분수의','덧셈과 뺄셈'], ['다각형의','둘레와 넓이']];
			else
				contextLABEL = [['수의 범위와','어림하기'], '분수의 곱셈', '합동과 대칭', '소수의 곱셈', '직육면체', '평균과 가능성'];
			
			
			contextChart.config.data.labels = contextLABEL;
			*/
			
			var i = 0;
			
			contextChart.config.data.datasets.forEach(function(dataset) {
				
				//debugger;
				
				
				
				if (i==0) {
					//alert('chartData => ' + chartData);
					
					dataset.data = chartData;
				}else {
					//alert('chartData2 => ' + chartData2);
					
					dataset.data =  chartData2;
				}
				
				i++;
	    		//dataset[0].data = chartData;
	    		//dataset[1].data = chartData2;
	    	    
	    		/*
	    		dataset.data =  dataset.data.map(function() {
	    			return randomScalingFactor();
	    		});
	    		*/
	    		
	    	});
			
			contextChart.update();
			
    }
  	 
   }
	
	
	
	function lessonClick(lesson){
		
		//sendXapi("selected", "차시선택", "Activity", window.location.href, $('#chapterName').val());
		
		$('#lesson').val(lesson);
		
		requestBody = {
				"lessonCode" : $('#lessonCode').val(),
				"bookCode" : $('#bookCode').val(),
				"chapterMiddle" : lesson,
				"chapterName" : $('#chapterName').val(),
				"chapterCode" : $('#chapterCodeSel').val(),
				"lesson" : lesson
		}
			
		sendAjaxJsonData("/comm/getActivityChartList.do", requestBody, "callbackGetActivityChartList", 'json', false);
		
		
		  for (let i = 1; i < 20; i++) {
			  var temp = 'a'+i;
			  
			  //alert(document.getElementById(temp).classList.item(0));
			  if (lesson == i ){
				  if (document.getElementById(temp).classList.item(0) == 'deep-pink') {
					  activityChart.config.data.datasets[0].backgroundColor = color(chartColors.deepPink).alpha(0.2).rgbString();//color(chartColors.red).alpha(0.2).rgbString();
					  activityChart.config.data.datasets[0].borderColor = chartColors.deepPink;
					  activityChart.config.data.datasets[0].pointBackgroundColor = chartColors.deepPink;
					  activityChart.config.data.datasets[0].pointBorderColor = chartColors.deepPink;
				  }else if(document.getElementById(temp).classList.item(0) == 'yellow-green'){
					  activityChart.config.data.datasets[0].backgroundColor = color(chartColors.yellowGreen).alpha(0.2).rgbString();//color(chartColors.red).alpha(0.2).rgbString();
					  activityChart.config.data.datasets[0].borderColor = chartColors.yellowGreen;
					  activityChart.config.data.datasets[0].pointBackgroundColor = chartColors.yellowGreen;
					  activityChart.config.data.datasets[0].pointBorderColor = chartColors.yellowGreen;
				  }
				  else
					  {
					  activityChart.config.data.datasets[0].backgroundColor = color(document.getElementById(temp).classList.item(0)).alpha(0.2).rgbString();//color(chartColors.red).alpha(0.2).rgbString();
					  activityChart.config.data.datasets[0].borderColor = document.getElementById(temp).classList.item(0);
					  activityChart.config.data.datasets[0].pointBackgroundColor = document.getElementById(temp).classList.item(0);
					  activityChart.config.data.datasets[0].pointBorderColor = document.getElementById(temp).classList.item(0);
				  }
				  
				  document.getElementById('activityLabel').innerHTML = lesson + '차시';
				  
				  /*
				  activityChart.config.data.datasets.forEach(function(dataset) {
			    		dataset.data = dataset.data.map(function() {
			    			return randomScalingFactor();
			    		});
			    	});
				  
				  activityChart.update();
				  //updateChart(activityChart);
				  */
				  
				  document.getElementById(temp).className += ' active';
			  }
			  else{
				  document.getElementById(temp).className = document.getElementById(temp).classList.item(0);
			  }
				  
		  }
		  
	}
	
	
    
   	function randomScalingFactor() {
   		return Math.round(Math.random() * 100);
   	}
   	
   	var chartColors = {
    		red: 'rgb(255, 99, 132)',
    		orange: 'rgb(255, 159, 64)',
    		yellow: 'rgb(255, 205, 86)',
    		green: 'rgb(75, 192, 192)',
    		blue: 'rgb(54, 162, 235)',
    		purple: 'rgb(153, 102, 255)',
    		deepPink: 'rgb(255, 20, 147)', 
    		yellowGreen: 'rgb(154, 205, 50)',
    		grey: 'rgb(201, 203, 207)'
    	};
   	var effectColors = {
   		highlight: 'rgba(255, 255, 255, 0.75)',
   		shadow: 'rgba(0, 0, 0, 0.5)',
   		innerglow: 'rgba(255, 255, 0, 0.5)',
   		outerglow: 'rgb(255, 255, 0)'
   	};
   	var color = Chart.helpers.color;
   	
   	
    //내용영역 Chart
   	var contextLABEL = ['Lesson 1', 'Lesson 2', 'Lesson 3', 'Lesson 4', 'Lesson 5', 'Lesson 6', 'Lesson 7'];
   	var contextDATA = [80,75,66,79,91,65,86];
   	var contextDATA2 = [64,76,72,67,72,88,75];
   	
	//LABELS = $('#contextChartLabel').val().split("^");
   	contextDATA = $('#contextChartData').val().split("^");
   	contextDATA2 = $('#contextChartData2').val().split("^");
   	
   	//alert("contextDATA => " + contextDATA);
   	//alert("contextDATA2 => " + contextDATA2);
    var config = {
            type: 'radar',
            data: {
              labels: contextLABEL,
              datasets: [
               {
                  	label: "말하기/듣기",
                  	backgroundColor: color(chartColors.red).alpha(0.2).rgbString(),
	      			borderColor: chartColors.red,
	      			pointBorderColor: chartColors.red,
	      			pointBackgroundColor: chartColors.red,
	      			/*
                  	data: [25.48,54.16,40.61,48.06,44.45,90.23,30.53].map(function() {
        				return randomScalingFactor();
        			}),
        			*/
        			data: contextDATA,
        			shadowOffsetX: 3,
        			shadowOffsetY: 3,
        			shadowBlur: 10,
        			shadowColor: effectColors.shadow,
        			pointRadius: 4,
        			pointBevelWidth: 2,
        			pointBevelHighlightColor: effectColors.highlight,
        			pointBevelShadowColor: effectColors.shadow,
        			pointHoverRadius: 6,
        			pointHoverBevelWidth: 3,
        			pointHoverInnerGlowWidth: 20,
        			pointHoverInnerGlowColor: effectColors.innerglow,
        			pointHoverOuterGlowWidth: 20,
        			pointHoverOuterGlowColor: effectColors.outerglow
                },
                {
                    label: "읽기/쓰기",
                    backgroundColor: color(chartColors.blue).alpha(0.2).rgbString(),
        			borderColor: chartColors.blue,
        			pointBackgroundColor: chartColors.blue,
        			pointBorderColor: chartColors.blue,
        			data: contextDATA2,
        			/*
                    data: [45.48,24.16,70.61,38.06,24.45,50.23,60.53].map(function() {
        				return randomScalingFactor();
        			}),
        			*/
        			shadowOffsetX: 3,
        			shadowOffsetY: 3,
        			shadowBlur: 10,
        			shadowColor: effectColors.shadow,
        			pointRadius: 4,
        			pointBevelWidth: 2,
        			pointBevelHighlightColor: effectColors.highlight,
        			pointBevelShadowColor: effectColors.shadow,
        			pointHoverRadius: 6,
        			pointHoverBevelWidth: 3,
        			pointHoverInnerGlowWidth: 20,
        			pointHoverInnerGlowColor: effectColors.innerglow,
        			pointHoverOuterGlowWidth: 20,
        			pointHoverOuterGlowColor: effectColors.outerglow
                  }
              ]
            },
            options: {
              responsive: false,	
              title: {
                display: true,
                text: '내용영역'
             },
             scale: {
                 min: 0,
                 max: 100,
                 stepSize: 20
             },
             scales: {
                 r: {
                   ticks: {
                      //color: 'white',
                      //backdropColor: 'black', // should render black behind the text
                   },
                   pointLabels:{
	                     /* https://www.chartjs.org/docs/latest/axes/radial/linear.html#point-label-options */
	                     //color: ['red', 'black', 'black', 'black', 'black', 'black'],
	                     color : (ctx) => {
	                    	       return ctx.index % 2 === 0 ? 'green' : 'red' 
	                     		}
	                     ,
	                     font : {
	                    	 size: 15,
	                     },
	                     //backdropPadding: 2,
	                     display: true,
	                     //padding: 10,
	                     callback: function(label){
	                         return label;
	                     },
	               },
	               /*
	               grid: {
	                   color: (ctx) => {
	                     //return ctx.chart.scales.r.ticks[ctx.index + 1].label === "12" ? "red" : "green";
	                     return ctx.index % 2 === 0 ? "red" : "green";
	                   }
	               },
	               */
                 },
             },     
     	    plugins: {
                legend: {
                    display: true,
                    align :'end',
                    labels: {
                        //color: 'rgb(255, 99, 132)'
                    }
                }
            },
            
            /*
             onClick: (evt, activeEls, chart) => {
                 const {
                     x,
                     y
                   } = evt;
                   let index = -1;

                   for (let i = 0; i < chart.scales.r._pointLabelItems.length; i++) {
                     const {
                       bottom,
                       top,
                       left,
                       right
                     } = chart.scales.r._pointLabelItems[i];

                     if (x >= left && x <= right && y >= top && y <= bottom) {
                       index = i;
                       break;
                     }
                   }

                   if (index === -1) {
                     return;
                   }

                   const clickedLabel = chart.scales.r._pointLabels[index];
                   alert(clickedLabel);
                   document.getElementById('contextLabel').innerHTML = clickedLabel;
                   //window.open(`https://www.google.com/search?q=color%20${clickedLabel}`); // Blocked in stack snipet. See fiddle
                 }
             
             */
             
             }
        };
    
    
    /////////////////////////////////////////////////////////////////////////////////////////
    //내용영역 Chart pointLabel 클릭  이벤트 처리
    $("#contextChart").click(function (evt) {
   		//debugger;
   	  	var eventLocation = getEventLocation(this,evt);
	   	const {
            x,
            y
          } = eventLocation;
          let index = -1;

          for (let i = 0; i < contextChart.scales.r._pointLabelItems.length; i++) {
            const {
              bottom,
              top,
              left,
              right
            } = contextChart.scales.r._pointLabelItems[i];
            if (x >= left && x <= right && y >= top && y <= bottom) {
              index = i;
              break;
            }
          }

          if (index === -1) {
            return;
          }

          const clickedLabel = contextChart.scales.r._pointLabels[index];
          //alert(clickedLabel);
          
          var arr = ['black', 'black', 'black', 'black', 'black', 'black', 'black'];
          if (index == 0 )  
        	  arr = ['red', 'black', 'black', 'black', 'black', 'black', 'black'];
          else if(index == '1')
        	  arr = ['black', 'red', 'black', 'black', 'black', 'black', 'black'];
          else if(index == '2')
        	  arr = ['black', 'black', 'red', 'black', 'black', 'black', 'black'];
          else if(index == '3')
        	  arr = ['black', 'black', 'black', 'red', 'black', 'black', 'black'];
          else if(index == '4')
        	  arr = ['black', 'black', 'black', 'black', 'red', 'black', 'black'];
          else if(index == '5')
        	  arr = ['black', 'black', 'black', 'black', 'black', 'red', 'black'];
          else if(index == '6')
        	  arr = ['black', 'black', 'black', 'black', 'black', 'black', 'red'];
          
          //debugger;
          
   	      document.getElementById('contextLabel').innerHTML = clickedLabel;
   	      $('#chapterName').val(clickedLabel);
   	   
   	   
   	      config.options.scales.r.pointLabels['color'] = arr;
   	   	  contextChart.render();
   	      contextChart.update();
   	      
   	   var requestBody = {
  			    'bookCode'  : $('#bookCode').val(),
  				'chapterName3' : clickedLabel
  			};
  	   
  	   	//alert(requestBody);
  	   
  		sendAjaxJsonData("/comm/getLearnList.do", requestBody, "callbackGetLearnList2", 'json', false);
          
   	});
   	
    
   	function getElementPosition(obj) {
   	  var curleft = 0, curtop = 0;
   	  if (obj.offsetParent) {
   	    do {
   	      curleft += obj.offsetLeft;
   	      curtop += obj.offsetTop;
   	    } while (obj = obj.offsetParent);
   	      return { x: curleft, y: curtop };
   	  }
   	  return undefined;
   	};

   	function getEventLocation(element,event){
   	  // Relies on the getElementPosition function.
   	  var pos = getElementPosition(element);

   	  return {
   	    x: (event.pageX - pos.x),
   	    y: (event.pageY - pos.y)
   	  };
   	};

   	function pointDistance(point1, point2) {
   	  return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
   	};
    /////////////////////////////////////////////////////////////////////////////////////////
   	
    
    //행동영역 Chart
   	var activityLABEL = ["식별력", "이해력", "적용력", "추론력", "표현력"];
   	var activityDATA = $('#activityChartData').val().split("^");
   	
    var config2 = {
            type: 'radar',
            data: {
              labels: activityLABEL,
              datasets: [
                {
                    label: "1950",
                    backgroundColor: color(chartColors.purple).alpha(0.2).rgbString(),
	      			borderColor: chartColors.purple,
	      			pointBorderColor: chartColors.purple,
	      			pointBackgroundColor: chartColors.purple,
                    data: activityDATA,
        			shadowOffsetX: 3,
        			shadowOffsetY: 3,
        			shadowBlur: 10,
        			shadowColor: effectColors.shadow,
        			pointRadius: 4,
        			pointBevelWidth: 2,
        			pointBevelHighlightColor: effectColors.highlight,
        			pointBevelShadowColor: effectColors.shadow,
        			pointHoverRadius: 6,
        			pointHoverBevelWidth: 3,
        			pointHoverInnerGlowWidth: 20,
        			pointHoverInnerGlowColor: effectColors.innerglow,
        			pointHoverOuterGlowWidth: 20,
        			pointHoverOuterGlowColor: effectColors.outerglow
                }
              ]
            },
            options: {
              responsive: false,	//false 이면 canvas 사이즈에 맞게 변경
              title: {
                display: false,
                text: 'Distribution in % of world population'
              },
              scale: {
	                 min: 0,
	                 max: 100,
	                 stepSize: 20
	             },
              plugins:{
                  legend:{
                     display:false
                  }
               }
            }
            
        };
    
    /*
    window.onload = function() {
    	var ctx = document.getElementById('contextChart').getContext('2d');
    	window.contextChart = new Chart(ctx, config);
    	
    	var ctx2 = document.getElementById('activityChart').getContext('2d');
    	window.activityChart = new Chart(ctx2, config2);
    	
    };
    */
    
    var ctx = document.getElementById('contextChart').getContext('2d');
    var ctx2 = document.getElementById('activityChart').getContext('2d');
    var contextChart = new Chart(ctx, config);
    var activityChart = new Chart(ctx2, config2);
    
    
    $(document).ready(function() {
       //sendXapi("attempted", "과목 선택", "Activity", window.location.href, "영어");    	
    	
 	   //대시보드 1 진입
 	   insertXApi("attempted", "");
 	   //과목 선택
 	   insertXApi("focused", "");
 	   //학기 선택
 	   insertXApi("selected", "period");
 	   //단원 선택
 	   insertXApi("selected", "");
 	   
 	  lessonClick(1);
 	   
    });
    
       
     // xapi 호출
   	function insertXApi (verb, gubun) {
   		var lessonCode = $('#lessonCode').val();
   		var bookCode = $('#bookCode').val();
   		var chapterCode = $('#chapterCodeSel').val();
   		var period = bookCode.substring(3,1);
   		
   		var objectId = "";
   		var enGbDesc = "";
   		var koKrDesc = "";
   		
   		if (verb == "attempted") {
   			objectId = "http://hpweb.boinit.com/english/EnglishDashboardView.do";
   			enGbDesc = "entered the dashboard1";
   			koKrDesc = "대시보드1 진입";
   		}else if (verb == "focused"){
   			objectId = lessonCode;
   			enGbDesc = "select lesson";
   			koKrDesc = "과목선택";
   		}else if (verb == "selected") {
   			if (verb == "period") {
   				objectId = period;
   	  			enGbDesc = "select period";
   	  			koKrDesc = "학기선택(" + period + ")";
   			}else{
   				objectId = chapterCode;
   	  			enGbDesc = "select chapter";
   	  			koKrDesc = "단원선택";
   			}
   		}else if (verb == "exited") {
   			objectId = "http://hpweb.boinit.com/english/EnglishDashboardView.do";
   			enGbDesc = "exited the dashboard1";
   			koKrDesc = "대시보드1 이탈";
   		}
   			
   		
   		//xapiParamMap.put("objectId", "");
   		//xapiParamMap.put("enGbDesc", "");
   		//xapiParamMap.put("enUsDesc", "");
   		//xapiParamMap.put("koKrDesc", "");
   		
   		var requestBody = {
   				'verb' : verb,
   				'objectId' : objectId,
   				'enGbDesc' : enGbDesc,
   				'enUsDesc' : enGbDesc,
   				'koKrDesc' : koKrDesc
   			};
   		
   		sendAjaxJsonData("/comm/insertXApi.do", requestBody, "callbackInsertXApi", 'json', false);
   	}
 	
         //페이지 벗어날 때
     	window.addEventListener("unload", function() {
 		      let xapiJson = new FormData(); 
 		      xapiJson.append('verb','exited');
 		      xapiJson.append('objectId','http://hpweb.boinit.com/english/EnglishDashboardView.do');
 		      xapiJson.append('enGbDesc','exited the dashboard1');
 		      xapiJson.append('enUsDesc','exited the dashboard1');
 		      xapiJson.append('koKrDesc','대시보드1 이탈');
 		      navigator.sendBeacon("/comm/insertXApi2.do", xapiJson); 
 	    }, false);
         
	</script>
</body>
</html>
