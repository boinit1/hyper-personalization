<%--
  Class Name : LoginUsr.jsp
  Description : Login 인증 화면
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2022.06.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp?v=1" />
</head>

<body>
<form method="post" id="frm" name="frm" autocomplete="off">
    <input type="hidden" id="nowPage" name="nowPage" value="${nowPage == null ? 1 : nowPage}">
	<input type="hidden" id="rowCnt" name="rowCnt" value="${rowCnt == null ? 3 : rowCnt}">
	<input type="hidden" id="bookCode" name="bookCode" value="${bookCode}">
	<input type="hidden" id="chapterCode" name="chapterCode" value="${chapterCode}">
	
	<input type="hidden" id="chasiLabel" name="chasiLabel" value="${chasiLabel}">
	<input type="hidden" id="chasiJumsu" name="chasiJumsu" value="${chasiJumsu}">
	<input type="hidden" id="chasiTime" name="chasiTime" value="${chasiTime}">
	<input type="hidden" id="chasiAddProblem" name="chasiAddProblem" value="${chasiAddProblem}">
	<input type="hidden" id="chasiExpectJumsu" name="chasiExpectJumsu" value="${chasiExpectJumsu}">
	<input type="hidden" id="chasiFinalExpectJumsu" name="chasiFinalExpectJumsu" value="${chasiFinalExpectJumsu}">
	<input type="hidden" id="chasiFinalExpectJumsuHigeYn" name="chasiFinalExpectJumsuHigeYn" value="${chasiFinalExpectJumsuHigeYn}">	
	<input type="hidden" id="chasiProblem" name="chasiProblem" value="${chasiProblem}">
	
		
	<div class="c-wrap c-wrap--large">
			<div class="dashboard">
				<div class="d-header">
					<div class="c-aligner">
						<div class="d-tab">
							<ul class="d-tab_list">
								<li class="d-tab_item"><a href="/math/MathDashboardView.do" class="d-tab_link">수학</a></li><!-- active클래스 추가 시 활성화 -->
								<li class="d-tab_item active"><a href="#" class="d-tab_link">영어</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="d-content">
					<div class="c-aligner">
						<a href="/english/EnglishDashboardView.do?bookCode=${bookCode}&chapterCode=${chapterCode}" class="d-arrow d-arrow--prev"></a><!-- d-arrow--disabled 클래스 추가 시 비활성화 스타일 적용 -->
						<div class="d-section">
							<div class="d-box">
								<div class="d-heading">학습예측</div>
								<div class="d-cont">
									<canvas id="predicChart" height="400" style="width: 100%"></canvas>
								</div>
							</div>
						</div>
						<div class="d-section">
							<div class="d-boxs">
								<div class="d-box"  id="inner_list">
								   <!-- EnglishDashboardView2Inner.jsp -->
								</div>
								
								
								<div class="d-box">
									<div class="d-heading">Quiz Bot 추천문항</div>
									<div class="d-cont">
										<div class="d-recommend">
											<div class="subject">ENGLISH AI</div>
											<ul class="list">
												<li>추천된 문제를 풀면서 배운 내용을 확인하고, 다음 단원을 준비할 수 있습니다.</li>
												<li>‘${chasiName}’ 차시의 문제를 더 공부해서 부족한 영역을 채워보아요.</li>
											</ul>
											<div class="d-recommend_button">
												<div class="c-buttons c-buttons--right"><a href="javascript:goQuiz();" class="c-button">문제 풀기</a></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a href="#" class="d-arrow d-arrow--next d-arrow--disabled"></a><!-- d-arrow--disabled 클래스 추가 시 비활성화 스타일 적용 -->
					</div>			
				</div>
			</div>
		</div>
		
		<div class="c-popup"  id="alertPopup2">
			<div class="c-popup_aligner">
				<div class="c-popup_box c-popup_box--middle">
					<div class="c-popup_txt">오늘은 더 이상 풀 문제가 없어요! <br />내일 다시 도전해주세요.</div>
					<div class="c-popup_buttons"><a href="javascript:alertClose()" class="c-popup_button">확인</a></div>
					<a href="javascript:alertClose()" class="c-popup_close">팝업 닫기</a>
				</div>
			</div>
		</div>
 </form>
	
	
	
<script type="text/javascript">
	
	function alertClose() {
		document.getElementById('alertPopup2').className = "c-popup";
	}
	
	function goQuiz(){
		var _todayProblemCnt = '${todayProblemCnt}';	
		var todayProblemCnt = parseInt(_todayProblemCnt);
		if(todayProblemCnt >= 5){
			document.getElementById('alertPopup2').className += " active";
		}else{
			location.href = "/english/EnglishProblemSolve.do?bookCode=${bookCode}&chapterCode=${chapterCode}&rowCnt=1&nowPage=1";	
		}		
	}
	
	var chartColors = {
    		red: 'rgb(255, 99, 132)',
    		black: 'rgb(0, 0, 0)',
    		orange: 'rgb(255, 159, 64)',
    		yellow: 'rgb(255, 205, 86)',
    		green: 'rgb(75, 192, 192)',
    		blue: 'rgb(54, 162, 235)',
    		purple: 'rgb(153, 102, 255)',
    		grey: 'rgb(201, 203, 207)'
    	};
   	var effectColors = {
   		highlight: 'rgba(255, 255, 255, 0.75)',
   		shadow: 'rgba(0, 0, 0, 0.5)',
   		innerglow: 'rgba(255, 255, 0, 0.5)',
   		outerglow: 'rgb(255, 255, 0)'
   	};
    
   	function randomScalingFactor() {
   		return Math.round(Math.random() * 100);
   	}
   	
   	var color = Chart.helpers.color;
   	
   	var chasiLabel = $('#chasiLabel').val().split("^");
   	var chasiJumsu = $('#chasiJumsu').val().split("^");
   	var chasiProblem = $('#chasiProblem').val().split("^");
//    	var chasiTime = $('#chasiTime').val().split("^");
   	var chasiAddProblem = $('#chasiAddProblem').val().split("^");
   	var chasiExpectJumsu = $('#chasiExpectJumsu').val().split("^");
   	var chasiFinalExpectJumsu = $('#chasiFinalExpectJumsu').val().split("^");
   	var chasiFinalExpectJumsuHigeYn = $('#chasiFinalExpectJumsuHigeYn').val();
   	
   	const data2 = {
   			labels: chasiLabel,
   			datasets: [
   				{
   					label: '학습량(문항)',
   					borderColor: 'rgb(153, 102, 255, 1)',
   		  			backgroundColor: 'rgb(153, 102, 255, 0.5)',
   		  			stack: 'Stack 0',
   		  			barPercentage: 0.5,
   		  	        barThickness: 50,
   		  	        maxBarThickness: 50,
   					fill: false,
   					data: chasiProblem, //[84,55,66,77,72],
   	    			yAxisID: 'y1',
   	    			order: 0
   				},
   				{
//   	 			label: '학습시간(분)',
   				label: '추가학습량(문항)',
   				borderColor:'rgb(255, 205, 86, 1)',
   	  			backgroundColor: 'rgb(255, 205, 86, 0.5)',
   	  			stack: 'Stack 0',
   	  			barPercentage: 0.5,
   	  	        barThickness: 50,
   	  	        maxBarThickness: 50,
   				fill: false,
   				data: chasiAddProblem, //[33, 22, 44, 55, 48],
   				yAxisID: 'y1',
   				order: 1
   				},
   				{
   					label: '내점수',
   					borderColor: chartColors.black,
   	      			pointBorderColor: chartColors.black,
   	      			pointBackgroundColor: chartColors.black,
   					fill: false,
   					borderDash: [5, 5],
   					/*
   					data: [3, 2, 3, 4].map(function() {
   	    				return randomScalingFactor();
   	    			}),
   	    			*/
   	    			data: chasiJumsu, //[60, 57, 55, 53, 55],
   	    			shadowOffsetX: 3,
   	    			shadowOffsetY: 3,
   	    			shadowBlur: 10,
   	    			shadowColor: effectColors.shadow,
   	    			pointRadius: 4,
   	    			pointBevelWidth: 2,
   	    			pointBevelHighlightColor: effectColors.highlight,
   	    			pointBevelShadowColor: effectColors.shadow,
   	    			pointHoverRadius: 10,
   	    			pointHoverBevelWidth: 3,
   	    			pointHoverInnerGlowWidth: 20,
   	    			pointHoverInnerGlowColor: effectColors.innerglow,
   	    			pointHoverOuterGlowWidth: 20,
   	    			pointHoverOuterGlowColor: effectColors.outerglow,
   					yAxisID: 'y',
   					type: 'line',
   					order: 2
   				},
   				{
   					label: '예측 점수',
   					borderColor: chartColors.green,
   					pointBorderColor: chartColors.green,
   	      			pointBackgroundColor: chartColors.green,
   					fill: false,
   					borderDash: [5, 5],
   					data: chasiExpectJumsu, //[64, 58, 52, 43, 48],
   					shadowOffsetX: 3,
   	    			shadowOffsetY: 3,
   	    			shadowBlur: 10,
   	    			shadowColor: effectColors.shadow,
   	    			pointRadius: 4,
   	    			pointBevelWidth: 2,
   	    			pointBevelHighlightColor: effectColors.highlight,
   	    			pointBevelShadowColor: effectColors.shadow,
   	    			pointHoverRadius: 10,
   	    			pointHoverBevelWidth: 3,
   	    			pointHoverInnerGlowWidth: 20,
   	    			pointHoverInnerGlowColor: effectColors.innerglow,
   	    			pointHoverOuterGlowWidth: 20,
   	    			pointHoverOuterGlowColor: effectColors.outerglow,
   					yAxisID: 'y',
   					type: 'line',
   					order: 4
   				},
   				{
   					label: 'noshow',
   					borderColor: ((chasiFinalExpectJumsuHigeYn=='Y') ? chartColors.blue : chartColors.red),
   					pointBorderColor: ((chasiFinalExpectJumsuHigeYn=='Y') ? chartColors.blue : chartColors.red),
   	      			pointBackgroundColor: ((chasiFinalExpectJumsuHigeYn=='Y') ? chartColors.blue : chartColors.red),
   					fill: false,
   					borderDash: [5, 5],
   					data: chasiFinalExpectJumsu, //[64, 58, 52, 43, 48],
   					shadowOffsetX: 3,
   	    			shadowOffsetY: 3,
   	    			shadowBlur: 10,
   	    			shadowColor: effectColors.shadow,
   	    			pointRadius: 4,
   	    			pointBevelWidth: 2,
   	    			pointBevelHighlightColor: effectColors.highlight,
   	    			pointBevelShadowColor: effectColors.shadow,
   	    			pointHoverRadius: 10,
   	    			pointHoverBevelWidth: 3,
   	    			pointHoverInnerGlowWidth: 20,
   	    			pointHoverInnerGlowColor: effectColors.innerglow,
   	    			pointHoverOuterGlowWidth: 20,
   	    			pointHoverOuterGlowColor: effectColors.outerglow,
   					yAxisID: 'y',
   					type: 'line',
   					order: 3
   				}
   			]
   		};

   		const options2 = {
   			responsive: true,
   			interaction: {
   			  mode: 'index',
   			  intersect: false,
   			},
   			tooltips: {
   			      position: 'nearest',
   			      mode: 'index',
   			      intersect: false,
   			},
   			stacked: false,	
   			plugins: {
   			  title: {
   				display: false,
   				text: 'Chart.js Combo Bar & Line Chart - Multi Axis'
   			  },
   			  legend: {
   				  font: {
   	                  size: 15
   	              },
   	              position: 'bottom' ,
   	              labels: {
   	           		generateLabels(chart) {
   	           			var data = chart.data;
   	                    if (data.labels.length && data.datasets.length) {
   	                    	return [
   	                    		{
   									"text" : "학습량",
   	                    			"fillStyle": "rgb(153, 102, 255, 1)",
   	                    			"strokeStyle" : "rgb(153, 102, 255, 1)"
   	                    		},
   	                    		{
   									"text" : "추가학습량",
   	                    			"fillStyle": "rgb(255, 205, 86, 0.5)",
   	                    			"strokeStyle" : "rgb(255, 205, 86, 0.5)"
   	                    		},
   	                    		{
   									"text" : "내점수",
   	                    			"fillStyle": "white",
   	                    			"strokeStyle" : "black",
   	                    			"lineDash" : [5, 5]
   	                    		},
   	                    		{
   									"text" : "예측점수",
   	                    			"fillStyle": "white",
   	                    			"strokeStyle" : "green",
   	                    			"lineDash" : [5, 5]
   	                    		},
   	                    	];
   	                    }
   	           		}
   	           	  }
   			  },
   			  tooltip: {
   		         callbacks: {
   		         	label: function(context) {
   						let label = context.dataset.label || '';
   						if(label == "noshow") return "";
   						if (label) {
   							label += ': ';
   						}
   						if (context.parsed.y !== null) {
   							label += context.parsed.y;
   						}
   						return label;
   		              }
   		            }
   				}
   			},
   			scales: {
   			  y: {
   				type: 'linear',
   				display: true,
   				position: 'left',
   				title: {
   	                display: true,
   	                font: {
   	                    size: 15
   	                },
   	                text: '점수'
   	            },
   			  },
   			  y1: {
   				type: 'linear',
   				display: true,
   				position: 'right',
   				ticks: {
   	                beginAtZero: true,
   	                callback: function (value, index, values) {
   	                    return value + '개';
   	                }
   	            },
   	            title: {
   	                display: true,
   	                font: {
   	                    size: 15
   	                },
   	                text: '문항풀이개수'
   	            },
   				// grid line settings
   				grid: {
   				  drawOnChartArea: false, // only want the grid lines for one axis to show up
   				},
   			  },
   			}
   			
   		  };

	
	        
	  window.onload = function() {
		  new Chart('predicChart', {
				type: 'bar',
				data: data2,
				options: options2
			});
	    }; 
	    
	
function sendAjaxCallback(callbackId, resultData, pageInfo){
			
			//debugger;
			// 배너 리스트 조회 API 호출 callback
			if(callbackId == 'callbackGetListInner'){
				if (resultData != null) {
					$('#no_result').hide();
					$('#inner_list').html(resultData);
					$('#inner_list').show();
				} else {
					$('#no_result').show();
				}
			}else if(callbackId == 'callbackSaveDisplayNo'){
				//debugger;
				if(resultData.resultCode == 1){
					Module.ui.dialog("<spring:message code='msg.success.save'/>", [$.extend({}, vex.dialog.buttons.NO, {text: "<spring:message code='btn.confirm'/>"})], function(isOk){
						$('#modalDetail').modal('hide');
					});
					//ListControl.fnSearchList("", "", "paging");
				}else{
					alert("<spring:message code='msg.error.save'/>");
				}
			}
		}
	    
	    $(document).ready(function() {   
			ListControl.fnInit();
	    });
	    
	    var ListControl = {

	    		fnInit : function() {

	    			$('#inner_list').hide();
	    			ListControl.fnSearchList("", "", "paging");
	    		}
	    		,
	    		fnSearchList : function(init, suffix, flag) {
	    			
	    			if (flag != "paging") {
	    				$('#nowPage').val('1');
	    			}

	    			
	    			var frm;
	    			if (init) {
	    				frm = init.substr(1);
	    			} else {
	    				//ListControl.fnSortUrl(suffix);
	    				requestBody = getFormDataToJson('frm');
	    			}

	    			//showLoadingImage();
	    			sendAjaxJsonData("/english/EnglishDashboardView2Inner.do", requestBody, "callbackGetListInner", 'html', true);
	    		}
	    }
	    
	    var bookCode = "";
	    var chapterCode = "";
	    var objectId = "";
	    
	    $(document).ready(function() {
	    	
	    	bookCode = $('#bookCode').val();
	   		chapterCode = $('#chapterCode').val();
	   		objectId = "http://hpweb.boinit.com/math/MathDashboardView2.do?bookCode="+bookCode+"&chapterCode="+chapterCode;
	   		
	 	   //대시보드 1 진입
	 	   insertXApi("attempted", "");
	 	   //과목 선택
	 	   insertXApi("focused", "");
	 	   //학기 선택
	 	   insertXApi("selected", "period");
	 	   //단원 선택
	 	   insertXApi("selected", "");
	 	   
	    });
	    
	     
	     // xapi 호출
	   	function insertXApi(verb, gubun) {
	   		
	   		
	   		var enGbDesc = "";
	   		var koKrDesc = "";
	   		
	   		if (verb == "attempted") {
	   			enGbDesc = "entered the dashboard2";
	   			koKrDesc = "대시보드2 진입";
	   		}else if (verb == "exited") {
	   			enGbDesc = "exited the dashboard2";
	   			koKrDesc = "대시보드2 이탈";
	   		}
	   			
	   		
	   		//xapiParamMap.put("objectId", "");
	   		//xapiParamMap.put("enGbDesc", "");
	   		//xapiParamMap.put("enUsDesc", "");
	   		//xapiParamMap.put("koKrDesc", "");
	   		
	   		var requestBody = {
	   				'verb' : verb,
	   				'objectId' : objectId,
	   				'enGbDesc' : enGbDesc,
	   				'enUsDesc' : enGbDesc,
	   				'koKrDesc' : koKrDesc
	   			};
	   		
	   		sendAjaxJsonData("/comm/insertXApi.do", requestBody, "callbackInsertXApi", 'json', false);
	   	}
	 	
	         //페이지 벗어날 때
	     	window.addEventListener("unload", function(){
	 		      let xapiJson = new FormData(); 
	 		      xapiJson.append('verb','exited');
	 		      xapiJson.append('objectId',objectId);
	 		      xapiJson.append('enGbDesc','exited the dashboard2');
	 		      xapiJson.append('enUsDesc','exited the dashboard2');
	 		      xapiJson.append('koKrDesc','대시보드2 이탈');
	 		      navigator.sendBeacon("/comm/insertXApi2.do", xapiJson); 
	 	    }, false);
</script>
</body>
</html>
