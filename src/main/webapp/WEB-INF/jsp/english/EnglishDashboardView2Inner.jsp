<%--
 * MathDashboardView2Inner.jsp
 * 
 * @author (주)윈솔텍
 * @since 2022.06.16
 * @version 1.0
 * @see 
 *
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.10.16  BizAn             최초 생성
 * </pre>
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript">
	function list(page) {
		var lastPage = ${pager.totPage };
		
		if(page > lastPage) page = lastPage; // 현재페이지가 마지막페이지보다 크면 마지막페이지 호출
		$('#nowPage').val(page);
		$('form[name="frm"]').submit();
		
		//window.scrollTo(0, 0);
	}
</script>		
							<div class="d-boxs">
								<div class="d-box">
									<div class="d-heading">추천 강의<c:if test="${ result.size() > 0}"> (전체 : ${result.size()} 건)</c:if></div>
									<div class="d-cont">
										<div class="d-listbox">
											<ul class="d-list">
											    <c:if test="${ result.size() == 0 || result == null}">
											        <li>
														<div class="value">
															<div class="txt">추천 강의가 없습니다.
															</div>
														</div>
													</li>
												</c:if>
												<c:if test="${ result.size() > 0}">
												     <c:forEach items="${result}" var="item" varStatus="status">
												        <li>
													        <div class="key">${item.shortName}</div>
															<div class="value">
																<div class="txt">${item.longName}
																</div>
																
																<!-- <div class="c-buttons"><a href="/english/EnglishLearnView.do?bookCode=${bookCode}&chapterCode=${chapterCode}&learnCode=1051D00011102&topicCode=1051D00011102_03" class="c-button">학습하기</a></div>-->
																<div class="c-buttons"><a href="/english/EnglishLearnView.do?bookCode=${bookCode}&chapterCode=${chapterCode}&learnCode=${item.learnCode}&topicCode=${item.topicCode}" class="c-button">학습하기</a></div> 
															</div>
														</li>
												     </c:forEach>
												</c:if>
											</ul>
										</div>
										<!-- 
										<div class="c-pager">
											<c:if test="${pager.curBlock > 1}">
												<a href="javascript:list('1')" class="arrow arrow--prev">이전</a>
											</c:if>
						
											<c:forEach var="page_no" begin="${pager.blockBegin}" end="${pager.blockEnd}">
												<c:choose>
													<c:when test="${page_no == pager.curPage }">
															<a href="#" id="curPage" class="num active">${page_no }</a>
													</c:when>
													<c:otherwise>
															<a href="javascript:list('${page_no }')" class="num">${page_no }</a><
													</c:otherwise>
												</c:choose>
											</c:forEach>
											
											<c:if test="${pager.curBlock < pager.blockEnd}">
												<a href="javascript:list(${pager.curPage} + 5)" class="arrow arrow--next">다음</a>
											</c:if>
										</div>
										 -->
									</div>
			
	