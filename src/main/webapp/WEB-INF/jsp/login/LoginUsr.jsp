<%--
  Class Name : LoginUsr.jsp
  Description : Login 인증 화면
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2022.06.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp?v=1" />

<script type="text/javascript">


/*
window.addEventListener("unload", () => {
    let dummy = new FormData(); 
    dummy.append('data','xxxxxxxxx'); 
    window.navigator.sendBeacon("/comm/insertXApi.do",dummy); 
  }, false);
  */
  
function actionLogin() {

    if (document.loginForm.userId.value =="" || document.loginForm.userPwd.value =="") {
    	document.getElementById('alertPopup').className += " active";
        return false;
    } else {
    	
    	var varUA = navigator.userAgent.toLowerCase(); //userAgent 값 얻기
        if ( varUA.indexOf('android') > -1) {
        	var cookieSaveYn = document.createElement("input");
            cookieSaveYn.setAttribute("type", "hidden");
            cookieSaveYn.setAttribute("name", "cookieSaveYn");
            cookieSaveYn.setAttribute("value", "Y");
            document.loginForm.appendChild(cookieSaveYn);
        } else if ( varUA.indexOf("iphone") > -1||varUA.indexOf("ipad") > -1||varUA.indexOf("ipod") > -1 ) {
        } else {
        }
        
        document.loginForm.action="/login/actionLogin.do";
        document.loginForm.submit();
    }
}

function alertClose() {

	document.getElementById('alertPopup').className = "c-popup";
}
function alertClose2() {

	document.getElementById('alertPopup2').className = "c-popup";
}

function setCookie (name, value, expires) {
    document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString();
}

function getCookie(Name) {
    var search = Name + "="
    if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
        offset = document.cookie.indexOf(search)
        if (offset != -1) { // 쿠키가 존재하면
            offset += search.length
            // set index of beginning of value
            end = document.cookie.indexOf(";", offset)
            // 쿠키 값의 마지막 위치 인덱스 번호 설정
            if (end == -1)
                end = document.cookie.length
            return unescape(document.cookie.substring(offset, end))
        }
    }
    return "";
}

function saveid(form) {
    var expdate = new Date();
    // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
    if (form.checkId.checked)
        expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30); // 30일
    else
        expdate.setTime(expdate.getTime() - 1); // 쿠키 삭제조건
    setCookie("saveid", form.id.value, expdate);
}

function getid(form) {
    form.checkId.checked = ((form.id.value = getCookie("saveid")) != "");
}

function fnInit() {
    var message = document.loginForm.message.value;
    if (message != "") {
        alert(message);
    }
    getid(document.loginForm);
}

</script>
</head>

<body>
<form id="loginForm" name="loginForm" action="/login/actionLogin2.do" method="post" onsubmit="return sendit();">
<div class="login">
			<div class="c-aligner">
				<div class="l-aligner">
					<div class="l-heading">LOGIN</div>
					<div class="l-logo"><img src="../images/l-logo.png" alt="" /></div>
					<div class="l-inputbox">
						<div class="l-input">
							<input type="text" name="userId" id="userId" class="c-input" placeholder="아이디" />
						</div>
						<div class="l-input">
							<input type="password" name="userPwd" id="userPwd" class="c-input" placeholder="비밀번호" />
						</div>
					</div>
					<div class="l-buttons"><a href="javascript:actionLogin();" class="l-button">LOGIN</a></div>
				</div>
			</div>
		</div>
		<input type="hidden" name="userNm" id="userNm" value="김동현" />
		<!-- popup -->
		<!-- 
			* 팝업 활성화 시
			- .c-popup에 active클래스 추가
			- 상단 html태그에 is-popup-open클래스 추가
		-->
		<script>
			/* 화면 확인용 - 개발 시 이 스크립트는 삭제해주세요 */ 
			//$('html').addClass('is-popup-open');
		</script>
		<div class="c-popup" id="alertPopup">
			<div class="c-popup_aligner">
				<div class="c-popup_box">
					<div class="c-popup_txt" id="popupText">아이디 또는 비밀번호를 확인해주세요</div>
					<div class="c-popup_buttons"><a href="javascript:alertClose();" class="c-popup_button">확인</a></div>
					<a href="javascript:alertClose();" class="c-popup_close">팝업 닫기</a>
				</div>
			</div>
		</div>
		
		<div class="c-popup" id="alertPopup2">
			<div class="c-popup_aligner">
				<div class="c-popup_box">
					<div class="c-popup_txt" id="popupText2">아이디 또는 비밀번호를 확인해주세요</div>
					<div class="c-popup_buttons"><a href="javascript:alertClose2();" class="c-popup_button">확인</a></div>
					<a href="javascript:alertClose2();" class="c-popup_close">팝업 닫기</a>
				</div>
			</div>
		</div>
		<!-- popup -->
		
 </form>
	
	<script>
	
	
		$(document).ready(function() {
			var autoLoginFailYn = "${autoLoginFailYn}";
			
			if(autoLoginFailYn != "Y"){
				var varUA = navigator.userAgent.toLowerCase(); //userAgent 값 얻기
		        if ( varUA.indexOf('android') > -1) {
	 	        	var autoLoginYn = document.createElement("input");
	 	        	autoLoginYn.setAttribute("type", "hidden");
	 	        	autoLoginYn.setAttribute("name", "autoLoginYn");
	 	        	autoLoginYn.setAttribute("value", "Y");
	 	            document.loginForm.appendChild(autoLoginYn);
	 	            document.loginForm.action="/login/actionLogin.do";
	 		        document.loginForm.submit();	
		        } else if ( varUA.indexOf("iphone") > -1||varUA.indexOf("ipad") > -1||varUA.indexOf("ipod") > -1 ) {
		        } else {
		        } 	 
			}
	    });
	
		history.replaceState({}, null, location.pathname);
		/*<![CDATA[*/
		var errormsg = "${message}";
		//var errormsg = "${callUrl}";
		//alert(errormsg);
		/*]]>*/
		
		if(errormsg == '0000'){
			document.getElementById("popupText2").innerHTML = '아이디 또는 비밀번호를 확인해주세요.';
			document.getElementById('alertPopup2').className += " active";
		}
		
		if(errormsg == '0001'){
			document.getElementById("popupText2").innerHTML = '로그인 또는 세션이 해제 되었습니다';
			document.getElementById('alertPopup2').className += " active";
		}

		window.onload = function() {
			if (getCookie("userId")) {
				document.loginForm.adminUserId.value = getCookie("userId");
				document.loginForm.auto_check.checked = true;
			}
		}

		function setCookie(name, value, expiredays)
		{
			var todayDate = new Date();
			todayDate.setDate(todayDate.getDate() + expiredays);
			document.cookie = name + "=" + escape(value) + "; path=/; expires="
					+ todayDate.toGMTString() + ";"
		}

		function getCookie(Name) {
			var search = Name + "=";
			if (document.cookie.length > 0) {
				offset = document.cookie.indexOf(search);
				if (offset != -1) {
					offset += search.length;
					end = document.cookie.indexOf(";", offset);
					if (end == -1)
						end = document.cookie.length;
					return unescape(document.cookie.substring(offset, end));
				}
			}
		}

		function sendit() {
			var frm = document.loginForm;

			if (!frm.userId.value || !frm.userPwd.value) {
				document.getElementById('alertPopup').className += " active";
				//frm.username.focus();
				return false;
			}

			
			if (frm.auto_check.checked == true) {
				setCookie("userId", frm.adminUserId.value, 7);
			} else {
				setCookie("userId", frm.adminUserId.value, 0);
			}

			return true;
		}


	function caps_lock(e) {
           var keyCode = 0;
           var shiftKey = false;
           keyCode = e.keyCode;
           shiftKey = e.shiftKey;
           if (((keyCode >= 65 && keyCode <= 90) && !shiftKey)
                   || ((keyCode >= 97 && keyCode <= 122) && shiftKey)) {
        	   Module.ui.dialog("<spring:message code='msg.info.oncapslock'/>",[
					$.extend({}, vex.dialog.buttons.YES, {text: "<spring:message code='btn.confirm'/>"})
					]);
             return false;
           }

           /*
           if (((keyCode >= 65 && keyCode <= 90) && !shiftKey)
                   || ((keyCode >= 97 && keyCode <= 122) && shiftKey)) {
               show_caps_lock();
               setTimeout("hide_caps_lock()", 3500);
           } else {
               hide_caps_lock();
           }
           */
    }

    function show_caps_lock() {
         $("#capslock").show();
    }

    function hide_caps_lock() {
         $("#capslock").hide();
    }
    
	</script>
	
	<script>
	 window.addEventListener("unload", function() {
	      let dummy = new FormData(); 
	      dummy.append('verb','exited');
	      dummy.append('objectId','http://hpweb.boinit.com/math/MathDashboardView.do');
	      dummy.append('enGbDesc','exited the dashboard1');
	      dummy.append('enUsDesc','exited the dashboard1');
	      dummy.append('koKrDesc','대시보드1 이탈');
	      navigator.sendBeacon("/comm/insertXApi2.do", dummy); 
	    }, false);
	</script>
</body>
</html>
