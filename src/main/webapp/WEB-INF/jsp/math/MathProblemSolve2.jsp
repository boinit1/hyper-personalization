<%--
  Class Name : MathProbleSolve2.jsp
  Description : 문제풀이 화면
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2022.06.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp?v=1" />
	<script type="text/javascript" src="/js/boinxapi/agent.quiz.js"></script>
</head>

<body>
<form id="frm" name="frm" autocomplete="off">
<div class="c-wrap">
			<div class="question">
				<div class="q-header">
					<div class="c-aligner">
						<div class="q-subject">
							<div class="recommend"><span>2022.04.14 오늘의 추천문항</span></div>
							<div class="navi">수학 5-1. 자연수의 혼합계산 > 1차시 두 양 사이의 관계를 알아볼까요 > 기본문제</div>
							<div class="q-subject_buttons">
								<a href="/math/MathDashboardView2.do" class="q-subject_button">나가기</a>
							</div>
						</div>
					</div>
				</div>
				<div class="q-content">
					<div class="c-aligner">
						<div class="question-box">
							<div class="question-cont">
								<a href="/math/MathProblemSolve.do" class="question-arrow question-arrow--prev">이전</a>
								<div class="question-key question-key--right"><!-- question-key--right 클래스 추가 시 정답 -->
									<div class="txt"><span class="num">1.</span>곱셈과 나눗셈이 섞여 있는 식에서는 앞에서부터 차례대로 계산해요.</div>
									<div class="ex"><span>예) 24÷6×2 <em class="c-correct" style="top:0;left:52px;"></em></span></div>									
								</div>
								<div class="question-listbox">
									<ul class="question-list">
										<li><a href="#" class="question-txt">48÷6×2 <span class="c-correct" style="top:55px;left:84px;"></span></a></li>
										<li><a href="#" class="question-txt">48÷(6×2) <span class="c-correct" style="top:55px;left:200px;"></span></a></li>
									</ul>
								</div>
								<div class="question-buttons question-buttons--next">
									<a href="javascript:verifyAnswer();" class="question-button">다음 문제 풀기</a>
								</div>
								<div class="q-correct">
									<div class="q-correct_key">정답</div>
									<div class="q-correct_value">
										<span>48÷6×2<em class="c-correct" style="top:5px;left:-9px;"></em></span>
										<span>48÷(6×2)<em class="c-correct" style="top:5px;left:53px;"></em></span>
									</div>
								</div>
								<a href="javascript:endProblem();" class="question-arrow question-arrow--next">다음</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="c-popup"  id="alertPopup">
				<div class="c-popup_aligner">
					<div class="c-popup_box c-popup_box--small">
						<div class="c-popup_txt">정답을 입력해주세요 !</div>
						<div class="c-popup_buttons"><a href="javascript:alertClose();" class="c-popup_button">확인</a></div>
						<a href="javascript:alertClose();" class="c-popup_close">팝업 닫기</a>
					</div>
				</div>
			</div>
			
			<div class="c-popup"  id="alertPopup2">
				<div class="c-popup_aligner">
					<div class="c-popup_box c-popup_box--middle">
						<div class="c-popup_txt">오늘은 더 이상 풀 문제가 없어요! <br />내일 다시 도전해주세요.</div>
						<div class="c-popup_buttons"><a href="javascript:alertClose2();" class="c-popup_button">확인</a></div>
						<a href="javascript:alertClose2();" class="c-popup_close">팝업 닫기</a>
					</div>
				</div>
			</div>
			
			
		</div>
 </form>
	
	
	
<script type="text/javascript">
	function verifyAnswer() {
	   	document.getElementById('alertPopup').className += " active";
	}
	
	function endProblem() {
	   	document.getElementById('alertPopup2').className += " active";
	}
	
	function alertClose() {
		document.getElementById('alertPopup').className = "c-popup";
	}
	
	function alertClose2() {
		document.getElementById('alertPopup2').className = "c-popup";
	}
</script>
</body>
</html>
