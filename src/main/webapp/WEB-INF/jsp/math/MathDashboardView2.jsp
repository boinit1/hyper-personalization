<%--
  Class Name : LoginUsr.jsp
  Description : Login 인증 화면
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2022.06.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp?v=1" />
	
	<style>
	/* other recommendations */
	.d-footer {
	  margin-top: 20px;
	  text-align: right;
	}

	.d-footer button {
	  display: inline-block;
	  padding: 0 13px 0 18px;
	  height: 38px;
	  border-radius: 38px;
	  color: #333;
	  text-align: center;
	  transition: all 0.25s;
	  font-size: 15px;
	  font-family: "NanumSquareRound", sans-serif;
	  font-weight: bold;
	}

	.d-footer button:hover {
	  background-color: #DFF6FB;
	}

	.other-reccom-sidebar {
	  opacity: 0;
	  visibility: hidden;
	  position: fixed;
	  top: 0;
	  right: 0;
	  bottom: 0;
	  left: 0;
	  width: 100vw;
	  height: 100vh;
	  background-color: rgba(0, 0, 0, 0.5);
	  overflow: hidden;
	}

	.other-reccom-sidebar .inner {
	  position: absolute;
	  top: 0;
	  right: 0;
	  transform: translateX(100%);
	  width: 400px;
	  height: 100%;
	  transition: all 0.5s;
	  overflow: auto;
	}

	.other-reccom-sidebar .inner .section {
	  position: absolute;
	  top: 0;
	  right: 0px;
	  width: 350px;
	  height: 100%;
	  background-color: #fff;
	}

	.toggle-icon {
	  opacity: 0;
	  visibility: hidden;
	  position: fixed;
	  top: 10px;
	  right: 0;
	  display: flex;
	  align-items: center;
	  justify-content: center;
	  width: 50px;
	  height: 70px;
	  border-top-left-radius: 5px;
	  border-bottom-left-radius: 5px;
	  border: 2px solid #C9EFF9;
	  border-right: none;
	  background: #F4FCFE;
	  cursor: pointer;
	  transition: all 0.5s;
	  box-shadow: inset -2px 0px 5px 0px rgba(201, 239, 249, 0.5);
	  z-index: 90;
	}

	.toggle-icon svg {
	  width: 80%;
	  height: 80%;
	}

	.toggle-icon.active {
	  opacity: 1;
	  visibility: visible;
	  right: 350px;
	}

	.toggle-icon.shrink {
	  right: 0;
	}

	.other-reccom-sidebar .header {
	  margin: 20px 10px 20px 25px;
	  padding-left: 0;
	}

	.other-reccom-sidebar .header h2 {
	  font-size: 25px;
	  font-weight: bold;
	}

	.other-reccom-sidebar .content {
	  width: 91%;
	  margin-left: 20px;
	  height: 81%;
	  overflow-y: auto;
	}

	.other-reccom-sidebar .content li {
	  width: 95%;
	  margin-bottom: 10px;
	  padding: 8px 13px;
	  border-radius: 5px;
	  border: 2px solid #C9EFF9;
	  background: #F4FCFE;
	  cursor: pointer;
	}

	.other-reccom-sidebar .content li:hover {
	  background-color: #DFF6FB;
	}

	.other-reccom-sidebar .content li .title {
	  word-break: break-all;
	  font-size: 16px;
	}

	.other-reccom-sidebar .footer {
	  padding: 0;
	  border-bottom: none;
	  margin: 20px 10px 20px 25px;
	}

	.other-reccom-sidebar .footer button {
	  width: 95%;
	  margin: 0 auto;
	  text-align: center;
	  background: #f8f8f8;
	  border: 1px solid #EDEDED;
	  border-radius: 5px;
	  padding: 15px 0;
	  color: #333;
	  transition: all 0.25s;
	}

	.other-reccom-sidebar .footer button:hover {
	  background-color: #E6E6E6;
	  border: 1px solid #D4D4D4;
	}

	.other-reccom-sidebar.active {
	  opacity: 1;
	  visibility: visible;
	}

	.other-reccom-sidebar.active .inner {
	  transform: translateX(0);
	}

	.other-reccom-sidebar.shrink .inner {
	  transform: translateX(350px) !important;
	}

	.scroll {
	  overflow-x: hidden;

	  &::-webkit-scrollbar-track {
	    border-radius: 5px;
	    background-color: #EDEDED;
	  }

	  &::-webkit-scrollbar {
	    width: 5px;
	    background-color: #EDEDED;
	  }

	  &::-webkit-scrollbar-thumb {
	    margin-bottom: 5px;
	    border-radius: 5px;
	    background-color: #DCDCDC;
	  }
	}
	</style>
</head>

<body>
<form method="post"  id="frm" name="frm" autocomplete="off">
	<input type="hidden" id="nowPage" name="nowPage" value="${nowPage == null ? 1 : nowPage}">
	<input type="hidden" id="rowCnt" name="rowCnt" value="${rowCnt == null ? 3 : rowCnt}">
	<input type="hidden" id="bookCode" name="bookCode" value="${bookCode}">
	<input type="hidden" id="chapterCode" name="chapterCode" value="${chapterCode}">
	
	<input type="hidden" id="chasiLabel" name="chasiLabel" value="${chasiLabel}">
	<input type="hidden" id="chasiJumsu" name="chasiJumsu" value="${chasiJumsu}">
	<input type="hidden" id="chasiTime" name="chasiTime" value="${chasiTime}">
	<input type="hidden" id="chasiAddProblem" name="chasiAddProblem" value="${chasiAddProblem}">
	<input type="hidden" id="chasiExpectJumsu" name="chasiExpectJumsu" value="${chasiExpectJumsu}">
	<input type="hidden" id="chasiFinalExpectJumsu" name="chasiFinalExpectJumsu" value="${chasiFinalExpectJumsu}">
	<input type="hidden" id="chasiFinalExpectJumsuHigeYn" name="chasiFinalExpectJumsuHigeYn" value="${chasiFinalExpectJumsuHigeYn}">	
	<input type="hidden" id="chasiProblem" name="chasiProblem" value="${chasiProblem}">
	<input type="hidden" id="chasiProblem2" name="chasiProblem2" value="${chasiProblem2}">
	
	
	<div class="c-wrap c-wrap--large">
			<div class="dashboard">
				<div class="d-header">
					<div class="c-aligner">
						<div class="d-tab">
							<ul class="d-tab_list">
								<li class="d-tab_item active"><a href="" class="d-tab_link">수학</a></li><!-- active클래스 추가 시 활성화 -->
								<li class="d-tab_item"><a href="/english/EnglishDashboardView.do" class="d-tab_link">영어</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="d-content">
					<div class="c-aligner">
						<a href="/math/MathDashboardView.do?bookCode=${bookCode}&chapterCode=${chapterCode}" class="d-arrow d-arrow--prev"></a><!-- d-arrow--disabled 클래스 추가 시 비활성화 스타일 적용 -->
						<div class="d-section">
							<div class="d-box">
								<div class="d-heading">학습예측</div>
								<div class="d-cont">
									<canvas id="predicChart" height="400" style="width: 100%"></canvas>
								</div>
							</div>
						</div>
						
						 
						<div class="d-section">
							<div class="d-boxs">
							   
								<div class="d-box"  id="inner_list">
								 <!-- 
									<div class="d-heading">추천 강의</div>
									<div class="d-cont">
										<div class="d-listbox">
											<ul class="d-list">
												<li>
													<div class="key">5-③ 규칙과 대응</div>
													<div class="value">
														<div class="txt">생활 속에서 대응관계를 찾아 식으로 나타내어 볼까요(1)
														</div>
														<div class="c-buttons"><a href="/math/MathLearnView.do" class="c-button">학습하기</a></div>
													</div>
												</li>
												<li>
													<div class="key">5-③ 규칙과 대응</div>
													<div class="value">
														<div class="txt">생활 속에서 대응관계를 찾아 식으로 나타내어 볼까요(1)
														</div>
														<div class="c-buttons"><a href="/math/MathLearnView.do" class="c-button">학습하기</a></div>
													</div>
												</li>
												<li>
													<div class="key">5-③ 규칙과 대응</div>
													<div class="value">
														<div class="txt">생활 속에서 대응관계를 찾아 식으로 나타내어 볼까요(1)
														</div>
														<div class="c-buttons"><a href="/math/MathLearnView.do" class="c-button">학습하기</a></div>
													</div>
												</li>
											</ul>
										</div>
										<div class="c-pager">
											<a href="#" class="arrow arrow--prev">이전</a>
											<a href="#" class="num active">1</a>
											<a href="#" class="num">2</a>
											<a href="#" class="arrow arrow--next">다음</a>
										</div>
									</div>
									-->
								 </div>
								
								<div class="d-box">
									<div class="d-heading">Quiz Bot 추천문항</div>
									<div class="d-cont">
										<div class="d-recommend">
											<div class="subject">MATH AI</div>
											<ul class="list">
												<li>추천된 문제를 풀면서 배운 내용을 확인하고, 다음 단원을 준비할 수 있습니다.</li>
												<li>‘${chasiName}’ 차시의 문제를 더 공부해서 부족한 영역을 채워보아요.</li>
											</ul>
											<div class="d-recommend_button">
												<div class="c-buttons c-buttons--right"><a href="javascript:goQuiz();" class="c-button">문제 풀기</a></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="d-footer">
							<button type="button" onclick="toggleSidebar()">다른 추천 받아보기
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							  <path d="M5.63605 7.75735L7.05026 6.34314L12.7071 12L7.05029 17.6568L5.63608 16.2426L9.87869 12L5.63605 7.75735Z" fill="currentColor" />
							  <path d="M12.7071 6.34314L11.2929 7.75735L15.5356 12L11.2929 16.2426L12.7072 17.6568L18.364 12L12.7071 6.34314Z" fill="currentColor" />
							</svg>
							</button>
						</div>
						<a href="#" class="d-arrow d-arrow--next d-arrow--disabled"></a><!-- d-arrow--disabled 클래스 추가 시 비활성화 스타일 적용 -->
					</div>			
				</div>
			</div>
		</div>
		
		<div class="c-popup"  id="alertPopup2">
			<div class="c-popup_aligner">
				<div class="c-popup_box c-popup_box--middle">
					<div class="c-popup_txt">오늘은 더 이상 풀 문제가 없어요! <br />내일 다시 도전해주세요.</div>
					<div class="c-popup_buttons"><a href="javascript:alertClose()" class="c-popup_button">확인</a></div>
					<a href="javascript:alertClose()" class="c-popup_close">팝업 닫기</a>
				</div>
			</div>
		</div>
		
		<div class="toggle-icon" onclick="toggleSidebar(true)">
		  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
		    <path d="M5.63605 7.75735L7.05026 6.34314L12.7071 12L7.05029 17.6568L5.63608 16.2426L9.87869 12L5.63605 7.75735Z" fill="currentColor" />
		    <path d="M12.7071 6.34314L11.2929 7.75735L15.5356 12L11.2929 16.2426L12.7072 17.6568L18.364 12L12.7071 6.34314Z" fill="currentColor" />
		  </svg>
		</div>
		
		<div class="other-reccom-sidebar">
		  <div class="black"></div>
		  <div class="inner">
		    <div class="section">
		      <div class="header">
		        <h2>다른 추천강의 보기</h2>
		      </div>
		      <div class="content scroll">
		        <ul>
		          <li>
		            <div class="title">덧셈과 뺄셈이 섞여있는 식을 계산해 볼까요</div>
		          </li>
		        </ul>
		      </div>
		      <div class="footer">
		        <button type="button" onclick="toggleSidebar()">나가기</button>
		      </div>
		    </div>
		  </div>
		</div>
			
 </form>
	
<script type="text/javascript">


	function alertClose() {
		document.getElementById('alertPopup2').className = "c-popup";
	}

	function goQuiz(){
		var _todayProblemCnt = '${todayProblemCnt}';	
		var todayProblemCnt = parseInt(_todayProblemCnt);
		if(todayProblemCnt >= 5){
			document.getElementById('alertPopup2').className += " active";
		}else{
			location.href = "/math/MathProblemSolve.do?bookCode=${bookCode}&chapterCode=${chapterCode}&rowCnt=1&nowPage=1";	
		}		
	}


	var chartColors = {
    		red: 'rgb(255, 99, 132)',
    		black: 'rgb(0, 0, 0)',
    		orange: 'rgb(255, 159, 64)',
    		yellow: 'rgb(255, 205, 86)',
    		green: 'rgb(75, 192, 192)',
    		blue: 'rgb(54, 162, 235)',
    		purple: 'rgb(153, 102, 255)',
    		grey: 'rgb(201, 203, 207)'
    	};
   	var effectColors = {
   		highlight: 'rgba(255, 255, 255, 0.75)',
   		shadow: 'rgba(0, 0, 0, 0.5)',
   		innerglow: 'rgba(255, 255, 0, 0.5)',
   		outerglow: 'rgb(255, 255, 0)'
   	};
    
   	function randomScalingFactor() {
   		return Math.round(Math.random() * 100);
   	}
   	
   	var color = Chart.helpers.color;
   	
   	var chasiLabel = $('#chasiLabel').val().split("^");
   	var chasiJumsu = $('#chasiJumsu').val().split("^");
   	var chasiProblem = $('#chasiProblem').val().split("^");
   	var chasiProblem2 = $('#chasiProblem2').val().split("^");
   	var chasiAddProblem = $('#chasiAddProblem').val().split("^");
   	var chasiExpectJumsu = $('#chasiExpectJumsu').val().split("^");
   	var chasiFinalExpectJumsu = $('#chasiFinalExpectJumsu').val().split("^");
   	var chasiFinalExpectJumsuHigeYn = $('#chasiFinalExpectJumsuHigeYn').val();
   	
	const data2 = {
		labels: chasiLabel,
		datasets: [
			{
				label: '학습량(문항)',
				borderColor: 'rgb(153, 102, 255, 1)',
	  			backgroundColor: 'rgb(153, 102, 255, 0.5)',
	  			stack: 'Stack 0',
	  			barPercentage: 0.5,
	  	        barThickness: 50,
	  	        maxBarThickness: 50,
				fill: false,
				data: chasiProblem, //[84,55,66,77,72],
    			yAxisID: 'y1',
    			order: 0
			},
			{
				label: 'noshow',
				borderColor: 'rgb(153, 102, 255, 0.1)',
	  			backgroundColor: 'rgb(153, 102, 255, 0.1)',
	  			stack: 'Stack 0',
	  			barPercentage: 0.5,
	  	        barThickness: 50,
	  	        maxBarThickness: 50,
				fill: false,
				data: chasiProblem2, //[84,55,66,77,72],
    			yAxisID: 'y1',
    			order: 0
			},
			{
// 			label: '학습시간(분)',
			label: 'noshow',
			borderColor:'rgb(255, 205, 86, 1)',
  			backgroundColor: 'rgb(255, 205, 86, 0.5)',
  			stack: 'Stack 0',
  			barPercentage: 0.5,
  	        barThickness: 50,
  	        maxBarThickness: 50,
			fill: false,
			data: chasiAddProblem, //[33, 22, 44, 55, 48],
			yAxisID: 'y1',
			order: 1
			},
			{
				label: '내점수',
				borderColor: chartColors.black,
      			pointBorderColor: chartColors.black,
      			pointBackgroundColor: chartColors.black,
				fill: false,
				borderDash: [5, 5],
				/*
				data: [3, 2, 3, 4].map(function() {
    				return randomScalingFactor();
    			}),
    			*/
    			data: chasiJumsu, //[60, 57, 55, 53, 55],
    			shadowOffsetX: 3,
    			shadowOffsetY: 3,
    			shadowBlur: 10,
    			shadowColor: effectColors.shadow,
    			pointRadius: 4,
    			pointBevelWidth: 2,
    			pointBevelHighlightColor: effectColors.highlight,
    			pointBevelShadowColor: effectColors.shadow,
    			pointHoverRadius: 10,
    			pointHoverBevelWidth: 3,
    			pointHoverInnerGlowWidth: 20,
    			pointHoverInnerGlowColor: effectColors.innerglow,
    			pointHoverOuterGlowWidth: 20,
    			pointHoverOuterGlowColor: effectColors.outerglow,
				yAxisID: 'y',
				type: 'line',
				order: 2
			},
			{
				label: '예측 점수',
				borderColor: chartColors.green,
				pointBorderColor: chartColors.green,
      			pointBackgroundColor: chartColors.green,
				fill: false,
				borderDash: [5, 5],
				data: chasiExpectJumsu, //[64, 58, 52, 43, 48],
				shadowOffsetX: 3,
    			shadowOffsetY: 3,
    			shadowBlur: 10,
    			shadowColor: effectColors.shadow,
    			pointRadius: 4,
    			pointBevelWidth: 2,
    			pointBevelHighlightColor: effectColors.highlight,
    			pointBevelShadowColor: effectColors.shadow,
    			pointHoverRadius: 10,
    			pointHoverBevelWidth: 3,
    			pointHoverInnerGlowWidth: 20,
    			pointHoverInnerGlowColor: effectColors.innerglow,
    			pointHoverOuterGlowWidth: 20,
    			pointHoverOuterGlowColor: effectColors.outerglow,
				yAxisID: 'y',
				type: 'line',
				order: 4
			},
			{
				label: 'noshow',
				borderColor: ((chasiFinalExpectJumsuHigeYn=='Y') ? chartColors.blue : chartColors.red),
					pointBorderColor: ((chasiFinalExpectJumsuHigeYn=='Y') ? chartColors.blue : chartColors.red),
	      			pointBackgroundColor: ((chasiFinalExpectJumsuHigeYn=='Y') ? chartColors.blue : chartColors.red),
				fill: false,
				borderDash: [5, 5],
				data: chasiFinalExpectJumsu, //[64, 58, 52, 43, 48],
				shadowOffsetX: 3,
    			shadowOffsetY: 3,
    			shadowBlur: 10,
    			shadowColor: effectColors.shadow,
    			pointRadius: 4,
    			pointBevelWidth: 2,
    			pointBevelHighlightColor: effectColors.highlight,
    			pointBevelShadowColor: effectColors.shadow,
    			pointHoverRadius: 10,
    			pointHoverBevelWidth: 3,
    			pointHoverInnerGlowWidth: 20,
    			pointHoverInnerGlowColor: effectColors.innerglow,
    			pointHoverOuterGlowWidth: 20,
    			pointHoverOuterGlowColor: effectColors.outerglow,
				yAxisID: 'y',
				type: 'line',
				order: 3
			}
		]
	};

	const options2 = {
		responsive: true,
		interaction: {
		  mode: 'index',
		  intersect: false,
		},
		tooltips: {
		      position: 'nearest',
		      mode: 'index',
		      intersect: false,
		},
		stacked: false,	
		plugins: {
		  title: {
			display: false,
			text: 'Chart.js Combo Bar & Line Chart - Multi Axis'
		  },
		  legend: {
			  font: {
                  size: 15
              },
              position: 'bottom' ,
              labels: {
           		generateLabels(chart) {
           			var data = chart.data;
                    if (data.labels.length && data.datasets.length) {
                    	return [
                    		{
								"text" : "학습량",
                    			"fillStyle": "rgb(153, 102, 255, 1)",
                    			"strokeStyle" : "rgb(153, 102, 255, 1)"
                    		},
                    		{
								"text" : "추가학습량",
                    			"fillStyle": "rgb(255, 205, 86, 0.5)",
                    			"strokeStyle" : "rgb(255, 205, 86, 0.5)"
                    		},
                    		{
								"text" : "내점수",
                    			"fillStyle": "white",
                    			"strokeStyle" : "black",
                    			"lineDash" : [5, 5]
                    		},
                    		{
								"text" : "예측점수",
                    			"fillStyle": "white",
                    			"strokeStyle" : "green",
                    			"lineDash" : [5, 5]
                    		},
                    	];
                    }
           		}
           	  }
		  },
		  tooltip: {
	         callbacks: {
	         	label: function(context) {
					let label = context.dataset.label || '';
					if(label == "noshow") return "";
					if (label) {
						label += ': ';
					}
					if (context.parsed.y !== null) {
						label += context.parsed.y;
					}
					return label;
	              }
	            }
			}
		},
		scales: {
		  y: {
			type: 'linear',
			display: true,
			position: 'left',
			title: {
                display: true,
                font: {
                    size: 15
                },
                text: '점수'
            },
		  },
		  y1: {
			type: 'linear',
			display: true,
			position: 'right',
			ticks: {
                beginAtZero: true,
                callback: function (value, index, values) {
                    return value + '개';
                }
            },
            title: {
                display: true,
                font: {
                    size: 15
                },
                text: '문항풀이개수'
            },
			// grid line settings
			grid: {
			  drawOnChartArea: false, // only want the grid lines for one axis to show up
			},
		  },
		}
		
	  };

	
	        
	  window.onload = function() {
		  new Chart('predicChart', {
				type: 'bar',
				data: data2,
				options: options2
			});
	    }; 
	    
	    function sendAjaxCallback(callbackId, resultData, pageInfo){
			
			//debugger;
			// 배너 리스트 조회 API 호출 callback
			if(callbackId == 'callbackGetListInner'){
				if (resultData != null) {
					$('#no_result').hide();
					$('#inner_list').html(resultData);
					$('#inner_list').show();
				} else {
					$('#no_result').show();
				}
			}else if(callbackId == 'callbackSaveDisplayNo'){
				//debugger;
				if(resultData.resultCode == 1){
					Module.ui.dialog("<spring:message code='msg.success.save'/>", [$.extend({}, vex.dialog.buttons.NO, {text: "<spring:message code='btn.confirm'/>"})], function(isOk){
						$('#modalDetail').modal('hide');
					});
					//ListControl.fnSearchList("", "", "paging");
				}else{
					alert("<spring:message code='msg.error.save'/>");
				}
			}
		}
	    
	    $(document).ready(function() {   
			ListControl.fnInit();
	    });
	    
	    var ListControl = {

	    		fnInit : function() {

	    			$('#inner_list').hide();
	    			ListControl.fnSearchList("", "", "paging");
	    		}
	    		,
	    		fnSearchList : function(init, suffix, flag) {
	    			
	    			if (flag != "paging") {
	    				$('#nowPage').val('1');
	    			}
	    			
	    			var frm;
	    			if (init) {
	    				frm = init.substr(1);
	    			} else {
	    				//ListControl.fnSortUrl(suffix);
	    				requestBody = getFormDataToJson('frm');
	    			}

	    			//showLoadingImage();
	    			sendAjaxJsonData("/math/MathDashboardView2Inner.do", requestBody, "callbackGetListInner", 'html', true);
	    		}
	    }
	    
	    
	    var bookCode = "";
	    var chapterCode = "";
	    var objectId = "";
	    
	    $(document).ready(function() {
	    	
	    	bookCode = $('#bookCode').val();
	   		chapterCode = $('#chapterCode').val();
	   		objectId = "http://hpweb.boinit.com/math/MathDashboardView2.do?bookCode="+bookCode+"&chapterCode="+chapterCode;
	   		
	 	   //대시보드 1 진입
	 	   insertXApi("attempted", "");
	 	   //과목 선택
	 	   insertXApi("focused", "");
	 	   //학기 선택
	 	   insertXApi("selected", "period");
	 	   //단원 선택
	 	   insertXApi("selected", "");
	 	   
	    });
	    
	     
	     // xapi 호출
	   	function insertXApi(verb, gubun) {
	   		
	   		
	   		var enGbDesc = "";
	   		var koKrDesc = "";
	   		
	   		if (verb == "attempted") {
	   			enGbDesc = "entered the dashboard2";
	   			koKrDesc = "대시보드2 진입";
	   		}else if (verb == "exited") {
	   			enGbDesc = "exited the dashboard2";
	   			koKrDesc = "대시보드2 이탈";
	   		}
	   			
	   		
	   		//xapiParamMap.put("objectId", "");
	   		//xapiParamMap.put("enGbDesc", "");
	   		//xapiParamMap.put("enUsDesc", "");
	   		//xapiParamMap.put("koKrDesc", "");
	   		
	   		var requestBody = {
	   				'verb' : verb,
	   				'objectId' : objectId,
	   				'enGbDesc' : enGbDesc,
	   				'enUsDesc' : enGbDesc,
	   				'koKrDesc' : koKrDesc
	   			};
	   		
	   		sendAjaxJsonData("/comm/insertXApi.do", requestBody, "callbackInsertXApi", 'json', false);
	   	}
	 	
	         //페이지 벗어날 때
	     	window.addEventListener("unload", function(){
	 		      let xapiJson = new FormData(); 
	 		      xapiJson.append('verb','exited');
	 		      xapiJson.append('objectId',objectId);
	 		      xapiJson.append('enGbDesc','exited the dashboard2');
	 		      xapiJson.append('enUsDesc','exited the dashboard2');
	 		      xapiJson.append('koKrDesc','대시보드2 이탈');
	 		      navigator.sendBeacon("/comm/insertXApi2.do", xapiJson); 
	 	    }, false);
	    
	         
	         
	function toggleSidebar(needShrink) {
		const bar = document.querySelector(".other-reccom-sidebar");
		const icon = document.querySelector(".toggle-icon");
		const isClosed = bar.classList.contains("active") ? true : false;
		
		if (needShrink) {
			if (isClosed) { // 축소
				bar.classList.remove("active");
				icon.classList.add("shrink");
				document.querySelector('body').style.overflow = 'auto';
			} else { // 펼치기
				bar.classList.add("active");
				icon.classList.remove("shrink");
				document.querySelector('body').style.overflow = 'hidden';
			}
			return;
		}

		icon.classList.remove("shrink");
		if (isClosed) { // 닫기
		  bar.classList.remove("active");
		  icon.classList.remove("active");
		  document.querySelector('body').style.overflow = 'auto';
		} else { // 열기
		  bar.classList.add("active");
		  icon.classList.add("active");
		  document.querySelector('body').style.overflow = 'hidden';
		}
	}
</script>
</body>
</html>
