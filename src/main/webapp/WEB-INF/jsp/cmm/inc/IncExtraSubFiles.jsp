<%--
  Class Name : IncFooter.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
 
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
	<table>
		<colgroup>
			<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%">
		</colgroup>
		<tbody>
			<!-- 003 참고자료 -->
			<tr>
				<th colspan="2">첨부파일</th>
				<td colspan="16" class="link">
					<!-- [{contentsExtraSeq=6363, extraSeq=3396, contentsExtraDiv=FILE, contentsExtraType=SUB, contentsExtraCategoryCd=003, 
					contentsExtraCategoryTitle=참고자료, contentsExtraUseType=null, contentsExtraTitle=null, contentsExtraShowFileName=1. 받아올림이 없는 (세 자리 수)+(세 자리 수).pdf, 
					contentsExtraUrl=null, contentsExtraCubeShowYn=N, contentsExtraModalShowYn=N, contentsExtraDownloadYn=N, fileId=4197a3314f54420fb6ea936a7c221599, 
					createDate=2021.03.11 21:25:32, createUser=batch, createUserName=null, updateDate=2021.03.15 12:21:36, updateUser=null, updateUserName=null}] -->
					<%
						String[] arrItems = {"000","001","002","003","004"};
						pageContext.setAttribute("arrItems", arrItems) ;
					%>
					<c:forEach var="idx" items="${arrItems}"> 
						<c:set var="item" value="${contentsExtraList003[idx]}" />
						<input type="hidden" id="contentsExtraSeq${idx}" name="contentsExtraSeq${idx}" value="${item.contentsExtraSeq}">
						<input type="hidden" id="fileId${idx}" name="fileId${idx}" value="${item.fileId}">
						<div class="resultGuideSec mt10">
							<div class="arLeft">
								<select id="contentsExtraType${idx}" name="contentsExtraType${idx}" class="wd140 contentsExtraType">
									<option value="">자료유형</option>
									<option value="SUB" ${item != null?'selected':''}>추가자료</option>
								</select>
								<select id="contentsExtraCategoryCd${idx}" name="contentsExtraCategoryCd${idx}" class="wd140" ${item != null?'':'disabled'}>
									<option value="">활용유형 선택</option>
									<c:forEach items="${contentsExtraCategoryList}" var="cata_item" varStatus="status">
										<option value="${cata_item.contentsExtraCategoryCd}" <c:if test="${item.contentsExtraCategoryCd==cata_item.contentsExtraCategoryCd}">selected</c:if>>
											${cata_item.contentsExtraCategoryTitle}</option>
									</c:forEach>
								</select>
								<span class="arLeft" <c:if test="${item == null}">style="display: none;"</c:if>>
									<label class=""></label>
									<label style="margin-right: 3px;">
								        <c:set var="sizePDF" value="${(item.fileSize / 1024 / 1024)+(0.1-((item.fileSize / 1024 / 1024)%0.1))%0.1}" />
										(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizePDF*10) - ((sizePDF*10)%1)) * (1/10)   } " /> MB)
										<strong class="label-text-blue">${item.contentsExtraShowFileName}</strong>
									</label>
									<c:if test="${item.extraPlayCnt == 0}">
									<label id="parent" style="border:none;">
										<button type="button" data-contentsExtraSeq="${item.contentsExtraSeq}" class="fileDelete" aria-label="Close"><span aria-hidden="true">&nbsp;X</span></button>
									</label>
									</c:if>
									<c:if test="${item.extraPlayCnt > 0}">
										 <span class="tRed">플레이리스트 사용중</span>
									</c:if>
								</span>
								<label <c:if test="${item == null}">style="display: none;"</c:if>></label>
								<label class="arLeft" <c:if test="${item != null}">style="display: none;"</c:if>>
									<label class=""><label class="btn-input-file" for="file_${idx}_SUB" id="">파일 찾기</label></label>
									<input type="file" id="file_${idx}_SUB" name="file_${idx}_SUB" style="display:none" accept="*">
									<!-- <input type="file" id="file_${idx}_SUB" name="file_${idx}_SUB" style="display:none" accept=".pdf"> -->
									<label style="border:none;">선택된 파일 없음</label>
								</label>
							</div>
							<div class="pageNavi arRight">
								<div class="btnArea arRight">
									<label class="chkB"><input type="checkbox" class="chk_unit" name="contentsExtraDownloadYnChk${idx}"
										<c:if test="${item.contentsExtraDownloadYn=='Y'}">checked="checked"</c:if>><span>다운로드</span></label>
								</div>
							</div>
						</div>
					</c:forEach>
					<div class="arCenter">※ 첨부파일은 최대 5개 등록 가능합니다.</div>
					<div class="arCenter">
									<span class="tRed">※ 현재 플레이리스트에서 사용 중인 첨부파일의 수정 또는 삭제를 원하시면 먼저 플레이리스트를 삭제 하고 저장한 다음, 수정 또는 삭제를 해주세요</span>
					</div>				
				</td>
			</tr>
		</tbody>
	</table>
	
	<script type="text/javascript">
		$('.contentsExtraType').on('change', function(e) {
			if($(this).val())
				$(this).next().attr('disabled', false);
			else
				$(this).next().attr('disabled', true).val("");
		});
	</script>