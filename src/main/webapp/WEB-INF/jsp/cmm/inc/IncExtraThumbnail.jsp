<%--
  Class Name : IncFooter.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
 
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
	<div class="tdInner">
		<table>
			<tbody>
				<tr>
					<td width="200px" style="border-style: none;">
						<input type="hidden" id="extraThumbnailFileId" name="extraThumbnailFileId" value="${detailInfo.extraThumbnailFileId}">
						<img id="preview-image"  src="${fileUrl}/cms/files/view/${detailInfo.extraThumbnailFileId}" class="prdImg" onerror="this.src='/img/default_img.png'"   onclick="window.open(this.src);"  style="cursor:pointer;"/>
					</td>
					<td style="border-style: none;">
						<c:if test="${!empty detailInfo.extraThumbnailFileId}">
							<label class="">
							    <label style="margin-right: 3px;">
							    <c:set var="sizeVideo" value="${(detailInfo.extraThumbnailFileSize / 1024 / 1024)+(0.1-((detailInfo.extraThumbnailFileSize / 1024 / 1024)%0.1))%0.1}" />
									(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizeVideo*10) - ((sizeVideo*10)%1)) * (1/10)   } " /> MB)
							        <strong class="label-text-blue">${detailInfo.extraThumbnailFileName}</strong>
								</label>
								<label id="" style="border:none;">
									<button type="button" data-contentsExtraSeq="" class="fileDelete" 
										onclick="javascript:$('#extraThumbnailFileId').val('');$('#preview-image').attr('src','');"><span aria-hidden="true">&nbsp;X</span></button>
								</label>
							</label>
						</c:if>
						<label class=""></label>
						<div class="arLeft" <c:if test="${!empty detailInfo.extraThumbnailFileId}">style="display: none;"</c:if>>
							<label class=""><label class="btn-input-file" for="file_TUM" id="">이미지 찾기</label></label>
							<span>${imageSizeComment }</span>
							<p>
							<input type="file" id="file_TUM" name="file_TUM" class="input-image" style="display:none" accept=".gif,.png,.jpg,.jpeg">
							<label id="List_TUM" style="border:none;">선택된 파일 없음</label>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<script type="text/javascript">
		function setInitUrl(dataObject, other){
			$("#"+ dataObject).val('');
			$("#"+ other).val('');
		}
	
		function setInitFile(dataObject, other){
			$("#"+ dataObject).next().find('.closeFile').trigger('click');
			$("#"+ other).val('');
		}
	</script>