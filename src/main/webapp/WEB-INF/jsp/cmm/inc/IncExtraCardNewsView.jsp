<%--
  Class Name : IncFooter.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
 
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
	<table>
		<colgroup>
			<col width="10%"><col width="90%">
		</colgroup>
		<tbody>
			<tr>
				<th>No</th>
				<th>이미지</th>
			</tr>
			<c:forEach items="${cardNewsList}" var="item" varStatus="status">
				<tr>
					<td class="arCenter">${status.index + 1}</td>
					<td class="arLeft">
						<img src="${fileUrl}/cms/files/view/${item.fileId}" class="prdImg" onerror="this.src='/img/default_img.png'" onclick="window.open(this.src);"  style="cursor:pointer;"/>
						<c:if test="${item.fileId != null}">
							<c:set var="sizeVideo" value="${(item.fileSize / 1024 / 1024)+(0.1-((item.fileSize / 1024 / 1024)%0.1))%0.1}" />
							(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizeVideo*10) - ((sizeVideo*10)%1)) * (1/10)   } " /> MB)
							<strong class="label-text-blue" style="cursor:pointer;" onclick="window.open('${fileUrl}/cms/files/view/${item.fileId}');">${item.originFileName}</strong>
						</c:if>
					</td>
				</tr>				 
			</c:forEach>
		</tbody>
	</table>