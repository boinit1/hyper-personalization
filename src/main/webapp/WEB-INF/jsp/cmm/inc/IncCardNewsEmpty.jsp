<%--
  Class Name : EgovMainView.jsp 
  Description : 메인화면
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
 
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<tr>
		<td>
			<input type="hidden" id="extraCardNewsGroup000" name="extraCardNewsGroup000" value="${item.extraCardNewsGroup}">
		</td>
		<td>
			<input class="inv-val" type="number" id="extraCardNewsSortNo000" name="extraCardNewsSortNo000" 
			data-no="${item.extraCardNewsSortNo}" value="${item.extraCardNewsSortNo}"
			onkeyup="saveExtraCardNewsSortNo(this, '${item.extraCardNewsSeq}', event);"
			autocomplete="off" onblur="chkChangeSortNo(this, event);">
		</td>
		<td>
			<span class="arLeft" <c:if test="${item == null}">style="display: none;"</c:if>>
				<img src="${fileUrl}/cms/files/view/${item.fileId}" class="prdImg" onerror="this.src='/img/default_img.png'"/>
				<label class="btn-input-file" onclick="javascript:downloadFilePop('${fileUrl}/cms/files/download/${item.fileId}')">다운로드</label>
			</span>
			<span class="arLeft" <c:if test="${item != null}">style="display: none;"</c:if>>
				<img id="preview-image-000"  src="" class="prdImg" onerror="this.src='/img/default_img.png'"/>
				<label class=""><label class="btn-input-file" for="file_000_CRD" id="">파일 찾기</label></label>
				<input type="file" id="file_000_CRD" name="file_000_CRD" data-preview="preview-image-000" class="input-image" style="display:none" accept=".gif,.png,.jpg,.jpeg">
				<label id="List_TUM" style="border:none;">선택된 파일 없음</label>
			</span>
		</td>	
		<td>
			<span class="btnS green"><a href="javascript:void(0)" onclick="javascript:addExtraCardNews(this);">추가</a></span>
		</td>
		<td class = "">
			<span class="btnS lineG"><a href="javascript:void(0)" onclick="javascript:delExtraCardNews(this, '${item.extraCardNewsSeq}');">삭제</a></span>
		</td>
	</tr>
	