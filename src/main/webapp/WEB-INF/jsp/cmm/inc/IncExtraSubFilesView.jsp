<%--
  Class Name : IncFooter.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
 
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
	<table>
		<colgroup>
			<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%">
		</colgroup>
		<tbody>
			<tr>
				<th colspan="2">첨부파일</th>
				<td colspan="16" class="link">
					<c:forEach items="${contentsExtraList003}" var="item" varStatus="status">
						<div class="resultGuideSec mt10">
							<div class="arLeft">
								<label class="">추가자료 : ${item.contentsExtraCategoryTitle} : </label>
								<label id="" style="border:none;">
									<c:set var="sizeHWP" value="${(item.fileSize / 1024 / 1024)+(0.1-((item.fileSize / 1024 / 1024)%0.1))%0.1}" />
										(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizeHWP*10) - ((sizeHWP*10)%1)) * (1/10)   } " /> MB)
									<strong class="label-text-blue" style="cursor:pointer;" onclick="javascript:downloadFilePop('${fileUrl}/cms/files/download/${item.fileId}')">${item.contentsExtraShowFileName}</strong>
									<span> </span>
								</label>
								<%-- <c:if test="${item.contentsExtraDownloadYn=='Y'}"> --%>
									<label class="btn-input-file" onclick="javascript:downloadFilePop('${fileUrl}/cms/files/download/${item.fileId}')">다운로드</label>
								<%-- </c:if> --%>
							</div>
							<div class="pageNavi arRight">
								<%-- <label class="chkB"><input type="checkbox" class="chk_unit" name=""
										<c:if test="${item.contentsExtraCubeShowYn=='Y'}">checked="checked"</c:if> disabled><span>스케줄큐브</span></label>
									<label class="chkB"><input type="checkbox" class="chk_unit" name=""
										<c:if test="${item.contentsExtraModalShowYn=='Y'}">checked="checked"</c:if> disabled><span>수업모달</span></label> --%>
								<label class="chkB"><input type="checkbox" class="chk_unit" name=""
									<c:if test="${item.contentsExtraDownloadYn=='Y'}">checked="checked"</c:if> disabled><span>다운로드</span></label>
							</div>
						</div>
					</c:forEach>
				</td>
			</tr>
		</tbody>
	</table>