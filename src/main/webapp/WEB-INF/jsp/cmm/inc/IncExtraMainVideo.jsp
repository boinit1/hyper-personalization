<%--
  Class Name : IncFooter.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
 
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
	<table>
		<colgroup>
			<col width="90"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
			<col width="10%"><col width="10%"><col width="10%">
		</colgroup>
		<c:forEach items="${contentsExtraList002}" var="item" varStatus="status">
			<c:set var="CONTENTS_EXTRA_SEQ" value="${item.contentsExtraSeq}"/>
			<c:set var="CONTENTS_EXTRA_DIV" value="${item.contentsExtraDiv}"/>
			<c:set var="CONTENTS_EXTRA_SHOW_FILE_NAME" value="${item.contentsExtraShowFileName}"/>
			<c:set var="CONTENTS_EXTRA_FILE_ID" value="${item.fileId}"/>
			<c:set var="CONTENTS_EXTRA_URL" value="${item.contentsExtraUrl}"/>
			<c:set var="CONTENTS_EXTRA_FILESIZE" value="${item.fileSize}"/>
			<c:set var="CONTENTS_EXTRA_DOWNLOAD_YN" value="${item.contentsExtraDownloadYn}"/>
		</c:forEach>
		<input type="hidden" id="contentsExtraSeq" name="contentsExtraSeq" value="${CONTENTS_EXTRA_SEQ}">
		<input type="hidden" id="contentsExtraFileId" name="contentsExtraFileId" value="${CONTENTS_EXTRA_FILE_ID}">
		<input type="hidden" id="contentsExtraUrlOld" name="contentsExtraUrlOld" value="${CONTENTS_EXTRA_DIV}${CONTENTS_EXTRA_URL}">
		<tbody>
			<tr>
				<td style="border:none"><input type="radio" id="" value="URL" name="contentsExtraDivRd" class="form-check-input" 
						<c:if test="${CONTENTS_EXTRA_DIV=='URL'}">checked="checked"</c:if> onclick="setInitFile('file_VID_MAI','contentsExtraUrlYOUTUBE');" ${detailInfo.extraPlayCnt > 0?"disabled":""}><span>&nbsp;링크</span>
				<td style="border:none" colspan="17"><input class="inv-val" type="text"
					id="contentsExtraUrlURL" name="contentsExtraUrlURL" value="<c:if test="${CONTENTS_EXTRA_DIV=='URL'}">${CONTENTS_EXTRA_URL }</c:if>"></td>
			</tr>
			<tr> 
				<td style="border:none"><input type="radio" id="" value="YOUTUBE" name="contentsExtraDivRd" class="form-check-input" 
						<c:if test="${CONTENTS_EXTRA_DIV=='YOUTUBE'}">checked="checked"</c:if> onclick="setInitFile('file_VID_MAI','contentsExtraUrlURL');" ${detailInfo.extraPlayCnt > 0?"disabled":""}><span>&nbsp;유튜브</span>
				<td style="border:none" colspan="9"><input class="inv-val" type="text" 
					id="contentsExtraUrlYOUTUBE" name="contentsExtraUrlYOUTUBE" value="<c:if test="${CONTENTS_EXTRA_DIV=='YOUTUBE'}">${CONTENTS_EXTRA_URL }</c:if>"></td>
				<td style="border:none" colspan="8">! Youtube URL은 아래 형식을 따름<p>예 ) https://www.youtube.com/embed/sI_nwQ7PnMM
				</td>
			</tr>
			<tr>
				<td style="border:none"><input type="radio" id="" value="FILE" name="contentsExtraDivRd" class="form-check-input" 
						<c:if test="${CONTENTS_EXTRA_DIV == null || CONTENTS_EXTRA_DIV=='FILE'}">checked="checked"</c:if> onclick="setInitUrl('contentsExtraUrlURL','contentsExtraUrlYOUTUBE');" ${detailInfo.extraPlayCnt > 0?"disabled":""}><span>&nbsp;파일</span>
				<td style="border:none" colspan="17">
					<div class="resultGuideSec">
						<div class="halfGuide70">
							<c:if test="${CONTENTS_EXTRA_DIV=='FILE'}">
								<label class="">
								    <label style="margin-right: 3px;">
								        <c:set var="sizeVideo" value="${(CONTENTS_EXTRA_FILESIZE / 1024 / 1024)+(0.1-((CONTENTS_EXTRA_FILESIZE / 1024 / 1024)%0.1))%0.1}" />
										(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizeVideo*10) - ((sizeVideo*10)%1)) * (1/10)   } " /> MB)
										<strong class="label-text-blue">${CONTENTS_EXTRA_SHOW_FILE_NAME}</strong>
									</label>
									<label id="" style="border:none;">
										<button type="button" data-contentsExtraSeq="${CONTENTS_EXTRA_SEQ}" class="fileDelete" aria-label="Close"><span aria-hidden="true">&nbsp;X</span></button>
									</label>
								</label>
							</c:if>
							<label class=""></label>
							<div class="arLeft" <c:if test="${CONTENTS_EXTRA_DIV=='FILE'}">style="display: none;"</c:if>>
								<label class=""><label class="btn-input-file" for="file_VID_MAI" id="">파일 찾기</label></label>
								<input type="file" id="file_VID_MAI" name=file_VID_MAI style="display:none" accept=".">
								<label style="border:none;">MP4등 동영상 파일, 선택된 파일 없음</label>
							</div>
						</div>
						<div class="halfGuide30 arRight">
							<label class="chkB"><input type="checkbox" id="contentsExtraDownloadYnChkVID" name="contentsExtraDownloadYnChkVID"
										<c:if test="${CONTENTS_EXTRA_DOWNLOAD_YN=='Y'}">checked="checked"</c:if>><span>다운로드허용</span></label>
						</div>
					</div>
					
				</td>
			</tr>
		</tbody>
	</table>