<%--
  Class Name : EgovMainView.jsp 
  Description : 메인화면
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
 
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
					<div class="inputTable" id="textbookLessonTemp" style="display: none;">
						<p class="arRight"><label class="chkN"><input type="checkbox" id="chk_title" class="chkTypeRadio"><span>대표교과</span></label></p>
						<table>
							<colgroup> 
								<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
								<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
								<col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%">
								<col width="10%"><col width="10%"><col width="10%">
							</colgroup>
							<tbody>
								<tr>
									<th colspan="2">국/검/인정</th>
									<td colspan="3" id="textbookCurriculumCdName"></td>
									<th colspan="2">교육과정</th>
									<td colspan="3" id="textbookCurriculumRevCdName"></td>
									<th colspan="2">학교급</th>
									<td colspan="5" id="textbookCurriculumLevelCdName"></td>
									<td rowspan="4"><button value="삭제" class="btn lineBtn textbookLessonDel" id="delTextbookLessonTemp">삭제</button></td>
								</tr>
								<tr>
									<th colspan="2">교과</th>
									<td colspan="3" id="textbookCurriculumCourseCdName"></td>
									<th colspan="2">학년</th>
									<td colspan="3" id="textbookCurriculumGradeCdName"></td>
									<th colspan="2">학기</th>
									<td colspan="5" id="textbookCurriculumTermCdName"></td>
								</tr>
								<tr>
									<th colspan="2">교과서</th>
									<td colspan="3" id="textbookTitle" ></td>
									<th colspan="2">대단원</th>
									<td colspan="3" id="textbookLessonUnitHighName2"></td>
									<th colspan="2">중단원</th>
									<td colspan="5" id="textbookLessonUnitMidName2"></td>
								</tr>
								<tr>
									<th colspan="2">소단원</th>
									<td colspan="3" id="textbookLessonUnitLowName2"></td>
									<th colspan="2">차시</th>
									<td colspan="3" id="textbookLessonPeriodName2"></td>
									<th colspan="2"></th>
									<td colspan="5"></td>
								</tr>
							</tbody>
						</table>
					<br>
					</div>
	
