<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
	<div class="tdInner">
		<table>
			<tbody>
				<tr>
					<td width="200px" style="border-style: none;">
						<input type="hidden" id="imgFile2Id" name="imgFile2Id" value="${detailInfo.imgFile2Id}">
						<c:if test="${!empty detailInfo.imgFile2Id}">
						<img id="preview-image2"  src="${fileServerUrl}${imageDir}${detailInfo.imgFile2Id}" class="prdImg" onerror="this.src='/img/default_img.png'"   onclick="window.open(this.src);"  style="cursor:pointer;"/>
						</c:if>
						<c:if test="${empty detailInfo.imgFile2Id}">
						<img id="preview-image2"  src="/img/default_img.png" class="prdImg" onerror="this.src='/img/default_img.png'"   onclick="window.open(this.src);"  style="cursor:pointer;"/>
						</c:if>
					</td>
					<td style="border-style: none;">
						<c:if test="${!empty detailInfo.imgFile2Id}">
							<label class="">
							    <label style="margin-right: 3px;">
							    <c:if test="${detailInfo.imgFile2Size > 1000000}">
							    	<c:set var="sizeVideo" value="${(detailInfo.imgFile2Size / 1024 / 1024)+(0.1-((detailInfo.imgFile2Size / 1024 / 1024)%0.1))%0.1}" />
									(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizeVideo*10) - ((sizeVideo*10)%1)) * (1/10)   } " /> MB)
								</c:if>
								<c:if test="${detailInfo.imgFile2Size > 0 && detailInfo.imgFile2Size < 1000000}">
							    	<c:set var="sizeVideo" value="${(detailInfo.imgFile2Size / 1024 )}" />
									(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizeVideo*10) - ((sizeVideo*10)%1)) * (1/10)   } " /> KB)
								</c:if>		
							        <strong id="imgFile2Nm" class="label-text-blue">${detailInfo.imgFile2Nm}</strong>
								</label>
								
								<label id="" style="border:none;">
									<button type="button"  class="fileDelete2"  aria-label="Close" data-seq="${detailInfo.imgFile2Id}"
										onclick="javascript:$('#imgFile2Id').val('');$('#preview-image2').attr('src','');"><span aria-hidden="true">&nbsp;X</span></button>
								</label>
								
								
							</label>
						</c:if>
						<label class=""></label>
						<div class="arLeft" id="" <c:if test="${!empty detailInfo.imgFile2Id}">style="display: none;"</c:if>>
							<label class="" id="imgFile2"><label class="btn-input-file" for="file_TUM2"><spring:message code='btn.search.image'/></label>
							<span><spring:message code='lbl.recommend.image'/> : <lable id="widthSize"> ${detailInfo.widthSize==null?'400':detailInfo.widthSize}</lable>px &nbsp;X &nbsp;
									    <lable id="heightSize"> ${detailInfo.heightSize==null?'300':detailInfo.heightSize}</lable>px
									    &nbsp;&nbsp;&nbsp;&nbsp; / 2MB <spring:message code='lbl.less.than'/>
									    &nbsp;&nbsp;&nbsp;&nbsp; / gif, png, jpg, jpeg</span>
							</label>
							<p>
							<input type="file" id="file_TUM2" name="file_TUM2" class="input-image2" style="display:none" accept=".gif,.png,.jpg,.jpeg">
							<label id="List_TUM2" style="border:none;"><spring:message code='lbl.noselect.file'/></label>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<script type="text/javascript">
		function setInitUrl(dataObject, other){
			$("#"+ dataObject).val('');
			$("#"+ other).val('');
		}
	
		function setInitFile(dataObject, other){
			$("#"+ dataObject).next().find('.closeFile').trigger('click');
			$("#"+ other).val('');
		}
	</script>