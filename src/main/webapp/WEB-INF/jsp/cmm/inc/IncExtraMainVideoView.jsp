<%--
  Class Name : IncFooter.jsp
  Description : 화면하단 Footer(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
 
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
	<c:forEach items="${contentsExtraList002}" var="item" varStatus="status">
		<c:set var="CONTENTS_EXTRA_SEQ" value="${item.contentsExtraSeq}"/>
		<c:set var="CONTENTS_EXTRA_DIV" value="${item.contentsExtraDiv}"/>
		<c:set var="CONTENTS_EXTRA_SHOW_FILE_NAME" value="${item.contentsExtraShowFileName}"/>
		<c:set var="CONTENTS_EXTRA_URL" value="${item.contentsExtraUrl}"/>
		<c:set var="CONTENTS_EXTRA_FILESIZE" value="${item.fileSize}"/>
		<c:set var="FILE_ID" value="${item.fileId}"/>
		<c:set var="CONTENTS_EXTRA_DOWNLOAD_YN" value="${item.contentsExtraDownloadYn}"/>
	</c:forEach>
	<div class="resultGuideSec">
		<div class="halfGuide">
			<c:if test="${CONTENTS_EXTRA_DIV=='YOUTUBE' || CONTENTS_EXTRA_DIV=='URL'}">
				<strong class="label-text-blue" style="cursor:pointer;" onclick="window.open('${CONTENTS_EXTRA_URL}');">${CONTENTS_EXTRA_URL}</strong>
			</c:if>
			<c:if test="${CONTENTS_EXTRA_DIV=='FILE'}">
				<strong class="label-text-blue" style="cursor:pointer;" onclick="viewerOpen('${viewerUrl}', '${FILE_ID}')">${CONTENTS_EXTRA_SHOW_FILE_NAME}</strong>
			</c:if>
		</div>
		<div class="halfGuide arRight">
			<c:if test="${CONTENTS_EXTRA_DIV=='YOUTUBE' || CONTENTS_EXTRA_DIV=='URL'}">
				<label style="cursor:pointer;" onclick="window.open('${CONTENTS_EXTRA_URL}');" class="btn-input-file">미리보기</label>
			</c:if>
			<c:if test="${CONTENTS_EXTRA_DIV=='FILE'}">
				<%-- <c:if test="${CONTENTS_EXTRA_DOWNLOAD_YN=='Y'}"> --%>
					<label class="btn-input-file" onclick="javascript:viewerOpen('${viewerUrl}', '${FILE_ID}')">미리보기</label>
					<label class="btn-input-file" onclick="javascript:downloadFilePop('${fileUrl}/cms/files/download/${FILE_ID}')">다운로드</label>
				<%-- </c:if> --%>
			</c:if>
		</div>
	</div>