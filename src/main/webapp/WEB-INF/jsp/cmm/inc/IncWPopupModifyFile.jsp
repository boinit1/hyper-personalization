<%--
  Class Name : IncWBoardContentFiles.jsp
  Description : 첨부파일 등록(include)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.05.16 BizAn            최초작성
 
  author   : (주)윈솔텍 
  since    : 2022.04.01
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
	<table>
		<colgroup>
			<col width="100%">
		</colgroup>
		<tbody>
			<tr>
				<td class="link">
				    <c:if test="${detailInfo.fileId != null}">
				    	<div class="resultGuideSec mt10">
				    	<input type="hidden" id="fileId_ATT" name="fileId_ATT" value="${detailInfo.fileId}">
							<div class="">
							    <label style="margin-right: 3px;">
							    <c:if test="${detailInfo.fileSize > 1000000}">
							    	<c:set var="sizeFile" value="${(detailInfo.fileSize / 1024 / 1024)+(0.1-((detailInfo.fileSize / 1024 / 1024)%0.1))%0.1}" />
									(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizeFile*10) - ((sizeFile*10)%1)) * (1/10)   } " /> MB)
								</c:if>
								<c:if test="${detailInfo.fileSize > 0 && item.fileSize < 1000000}">
							    	<c:set var="sizeFile" value="${(detailInfo.fileSize / 1024 )}" />
									(<fmt:formatNumber type="number"  pattern="0.0" value="${ ((sizeFile*10) - ((sizeFile*10)%1)) * (1/10)   } " /> KB)
								</c:if>		
							        <strong id="originFileNm" class="label-text-blue">${detailInfo.originFileNm}</strong>
								</label>
							
								<label id="" style="border:none;">
									<button type="button"  class="fileDelete2"  aria-label="Close" data-boardFileId="${detailInfo.fileId}">
										<span aria-hidden="true">&nbsp;X</span>
									</button>
								</label>
								<label class=""></label>
								<div class="arLeft" id="" <c:if test="${!empty detailInfo.fileId}">style="display: none;"</c:if>>
									<label class="" id="attachFile"><label class="btn-input-file" for="file_ATT"><spring:message code='btn.search.file'/></label>
									</label>
									<input type="file" id="file_ATT" name="file_ATT" class="btn-input-file" style="display:none" accept="*">
									<label id="List_ATT" style="border:none;"><spring:message code='lbl.noselect.file'/></label>
								</div>
							</div>
						</div>
					</c:if>
					
					<c:if test="${detailInfo.fileId == null}">
						<input type="hidden" id="fileId_ATT" name="fileId_ATT" value="${status.index}">
						<div class="resultGuideSec mt10">
							<div class="arLeft">
								<label class="arLeft">
									<label class=""><label class="btn-input-file" for="file_ATT" id="attachFile"><spring:message code='btn.search.file'/></label></label>
									<input type="file" id="file_ATT" name="file_ATT" style="display:none" accept="*">
									<label style="border:none;"><spring:message code='lbl.noselect.file'/>&nbsp;&nbsp; / 10MB <spring:message code='lbl.less.than'/></label>
								</label>
							</div>
						</div>
					</c:if>
					<!-- <div class="arCenter">※ 첨부파일은 최대 5개 등록 가능합니다.</div> -->
				</td>
			</tr>
		</tbody>
	</table>
	
	<script type="text/javascript">
		$('.contentsExtraType').on('change', function(e) {
			if($(this).val())
				$(this).next().attr('disabled', false);
			else
				$(this).next().attr('disabled', true).val("");
		});
	</script>