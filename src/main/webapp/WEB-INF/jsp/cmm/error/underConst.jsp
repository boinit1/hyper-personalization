<%--
  Class Name : EgovMainView.jsp 
  Description : 메인화면
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2021.07.15 마강현             최초작성
  2021.08.01 김동현             수정
  author   : (주)보인정보기술 
  since    : 2021.07.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp" />
	
	<script type="text/javascript">
	</script>
</head>
<body>
	<div class="comnWrap">
		<%//@include file="/WEB-INF/jsp/whome/inc/IncHeader.jsp" %>
		
		<!-- content //-->
		<div class="content">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td align="center" valign="top"><br />
			    <br />
			    <br />
			    <table width="600" border="0" cellpadding="0" cellspacing="0" background="er_images/blue_bg.jpg' />">
			      <tr>
			        <td align="center"><table width="100%" border="0" cellspacing="9" cellpadding="0">
			          <tr>
			            <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tr>
			              </tr>
			              <tr>
			                <td><br />
			                  <br /></td>
			              </tr>
			              <tr>
			                <td align="center">
			                <table width="520" border="0" cellspacing="2" cellpadding="2">
			                  <tr>
			                    <td width="74" align="center"><img src="<c:url value='/images/egovframework/com/cmm/const.png' />" width="300" height="300" alt="danger" /></td>
			                  </tr>
			                   <tr>
			                    <td width="399" align="center" class="lt_text4">준비중 입니다.</td>
			                  </tr>
			                </table>
			                  <table width="500" border="0" cellspacing="2" cellpadding="2">
			                                  </table></td>
			              </tr>
			              <tr>
			                <td><br />
			                  <br /></td>
			              </tr>
			              <tr>
			                <td align="center"><a href="#LINK" onClick="fncGoAfterErrorPage();"><img src="<c:url value='/images/egovframework/com/cmm/go_history.jpg' />" width="90" height="29" alt="go_history" /></a></td>
			              </tr>
			            </table>
			              <br /></td>
			          </tr>
			        </table></td>
			      </tr>
			    </table>
			    </td>
			  </tr>
			</table>
		</div>
	</div>
</body>
</html>
