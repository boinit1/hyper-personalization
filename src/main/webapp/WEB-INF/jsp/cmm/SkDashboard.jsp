<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp?v=1" />
</head>

<body style="">

<div class="d-section" id="inner_list">
	<div class="d-box">
		<div class="d-heading">활동비교</div>
		<div class="d-cont" style="
    margin: 0px;
    padding: 0px;
    text-align: center;
		">
			<iframe src="${url}" style="width:100%; height:600px;"></iframe>
		</div>
	</div>
</div>
	
<br>	
<div style="text-align:center;">
	<input type="submit" value="나가기" onclick="history.back();">
</div>
<br>

</body>
</html>
