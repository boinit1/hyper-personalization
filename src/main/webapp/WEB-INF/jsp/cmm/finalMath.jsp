<%--
  Class Name : MathProbleSolve.jsp
  Description : 문제풀이 화면
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2022.06.15
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="sps.utils.StringUtil"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import = "egovframework.rte.psl.dataaccess.util.EgovMap" %>
<%@ page import = "java.util.regex.Pattern" %>  
<%@ page import = "java.util.regex.Matcher" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="/WEB-INF/jsp/home/inc/IncMetaFile.jsp?v=1" />
    <script type="text/javascript" src="/js/boinxapi/agent.quiz.js"></script>
</head>
<link rel="stylesheet" id="problem_css" href="/css/problem.css?20221223">
<style>
	body, textarea, a, button {font-family:"맑은 고딕","malgun gothic","돋움",dotum,sans-serif}
	html, body {height:100%}
	body {font-size:2.05em;font-weight: 900;position:relative;margin:0} /* ;overflow:hidden */
	ul, ol, li  {list-style:none}
	body.big .pb_layout {font-size:1.25em} /* 20px */
	
	.item_header {position:relative;width:100%;margin-bottom:3px;font-size:12px;border:1px solid #aaa;border-left-width:0;border-right-width:0;overflow:auto;background-color:#E7E7E7} /* 43.75em 700px */
	.item_header div {margin:2px 5px}
	.item_header button {padding:1px 5px;font-size:12px}
	.graybg {background-color:#cccccc}
	.alert {color:red;font-size:11px}
	
	.pb_hint {background-image:url(/common/img/icon_hint.gif)}
	.pb_answer {background-image:url(/common/img/icon_answer.gif)}
	.pb_expl {background-image:url(/common/img/icon_expl.gif)}
	.pb_round {background-image:url(/common/img/round_test.gif)}
	.big .pb_round {background-image:url(/common/img/round_big2.gif)}
	.pb_layout {width:1090px;min-width:734px}
</style>
<body>
<script type="text/javascript">
	function list(page) {
		var lastPage = ${pager.totPage };
		
		if(page > lastPage) page = lastPage; // 현재페이지가 마지막페이지보다 크면 마지막페이지 호출
		$('#nowPage').val(page);
		$('form[name="frm"]').submit();
		
		//window.scrollTo(0, 0);
	}
</script>

	<div class="c-wrap">
			<div class="question">
				<div class="q-header">
					<div class="c-aligner">
						<div class="q-subject">
							<div class="recommend"><span>수학최종진단</span></div>
							
							<div class="d-tab" style="height:50px; width:800px; margin-top:10px;">
								<ul id = "problemList" class="d-tab_list" style="height:38px;">
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=1" class="d-tab_link">1</a>
									</li>
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=2" class="d-tab_link">2</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=3" class="d-tab_link">3</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=4" class="d-tab_link">4</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=5" class="d-tab_link">5</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=6" class="d-tab_link">6</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=7" class="d-tab_link">7</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=8" class="d-tab_link">8</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=9" class="d-tab_link">9</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=10" class="d-tab_link">10</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=11" class="d-tab_link">11</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=12" class="d-tab_link">12</a>
									</li>	
									<li class="d-tab_item" style="height: 30px;width: 30px;position: 0px;padding: 0px;">
										<a style="height: 22px; width: 22px; min-width: 0px; line-height: 25px" href="/comm/finalMath.do?problemNo=13" class="d-tab_link">13</a>
									</li>
								</ul>
							</div>
							<div class="q-subject_buttons" style="top:10px;">
								<a href="/math/MathDashboardView.do" class="q-subject_button" >나가기</a>
							</div>
						</div>
					</div>
				</div>
				<div class="q-content">
					<div class="c-aligner" style="width:1250px;">
						<div class="question-box">
							<div class="question-cont">
							    <c:if test="${rowCnt != nowPage}">
										<!--  <a href="/math/MathProblemSolve.do?bookCode=${bookCode}&chapterCode=${chapterCode}&rowCnt=1&nowPage=${nowPage-1}" class="question-arrow question-arrow--prev">이전</a>-->
<!-- 										<a href="javascript:history.back();" class="question-arrow question-arrow--prev">이전</a> -->
								</c:if>
								<form id="probForm" name="probForm" action="/comm/finalMath2.do" method="post">
									<input type="hidden" name="bookCode" id="bookCode" value="${result.bookCode}">
									<input type="hidden" name="chapterCode" id="chapterCode" value="${result.chapterCode}">
									<input type="hidden" name="rowCnt" id="rowCnt" value="${rowCnt}">
									<input type="hidden" name="nowPage" id="nowPage" value="${nowPage}">
									<input type="hidden" name="problemId" id="problemId" value="${result.problemId}">
									<input type="hidden" name="problemNo" id="problemNo" value="${result.problemNo}">
									<input type="hidden" name="subNo" id="subNo" value="${result.subNo}">
									<input type="hidden" name="userAnswerStr" id="userAnswerStr" value="${userAnswerStr}">
									<input type="hidden" name="startTmstmp" id="startTmstmp" value="${startTmstmp }">
								</form>
		<c:if test="${result.userAnswer != null}">
								<form id="solveForm" name="solveForm" action="/comm/insertPbProblemSolve.do" method="post">
									<input type="hidden" name="subject" id="subject" value="MATH">
									<input type="hidden" name="lessonCode" id="lessonCode" value="${result.lessonCode}">
									<input type="hidden" name="problemId" id="problemId" value="${result.problemId}">
									<input type="hidden" name="problemNo" id="problemNo" value="${result.problemNo}">
									<input type="hidden" name="difficulty" id="difficulty" value="${result.difficulty}">
									<input type="hidden" name="importance" id="importance" value="${result.importance}">
									<input type="hidden" name="problem" id="problem" value="${result.problem}">
									<input type="hidden" name="answer" id="answer" value="${result.answer}">
									<input type="hidden" name="funArea" id="funArea" value="${result.funArea}">
									<input type="hidden" name="recArea" id="recArea" value="${result.recArea}">
									<input type="hidden" name="conArea" id="conArea" value="${result.conArea}">
									<input type="hidden" name="subNo" id="subNo" value="${result.subNo}">
									<input type="hidden" name="solveValue" id="solveValue" value="${userAnswerStr}">
									<input type="hidden" name="solveResult" id="solveResult" value="">
									<input type="hidden" name="startTmstmp" id="startTmstmp" value="${startTmstmp }">
									<input type="hidden" name="chapterCode" id="chapterCode" value="${chapterCode }">
								</form>
		</c:if>
	<%@ include file="/WEB-INF/jsp/lib/libProblem.jsp" %>
								<div class="pb_layout clearfix">
									<div class="pb_wrap">
										<div class="pb_num" style="display:none;">${nowPage}.<!--  [문제] --></div>
										<div class="pb_problem <%=checkResultSty%>"><%=problem%></div>
										<div class="pb_sentence<%=sentenceSty%>"><%=sentence%></div>
										<div class="pb_example<%=examGrpSty%>">
											<ol class="pb_an_grp<%=examSty%>"><%=exampleList %></ol>
										</div>
									</div>
									
									<div id="feed_back<%=problemNum%>" class="feed_back">
									<%=explain%>
									</div>
								</div>
								<%-- 
								<div class="question-key">
									<div class="txt"><span class="num">1.</span>${ result.problem}</div>
									<div class="ex"><span>예) 24÷6×2<em class="c-correct" style="top:0;left:52px;"></em></span></div>									
								</div>
								<div class="question-listbox">
									<ul class="question-list">
										<li><a href="#" class="question-txt">${ result.sentence}</a></li>
										<li><a href="#" class="question-txt">48÷(6×2)</a></li>
									</ul>
								</div> --%>
								
								<div class="question-buttons" <c:if test="${result.userAnswer != null}">style="display: none"</c:if>>
									<a href="javascript:verifyAnswer();" class="question-button">제출</a>
								</div>
								<div <c:if test="${result.userAnswer == null}">style="display: none"</c:if>>
									<c:if test="${nowPage < totalCnt}">
										<div class="question-buttons question-buttons--next">
											<a href="javascript:next();" class="question-button">다음 문제</a>

										</div>
									</c:if>
									<div class="q-correct" style="display: block">
										<div class="q-correct_key">정답</div>
										<div class="q-correct_value">
											<!-- <span>48÷6×2<em class="c-correct" style="top:5px;left:-9px;"></em></span>
											<span>48÷(6×2)<em class="c-correct" style="top:5px;left:53px;"></em></span> -->
											<%=answer%>
										</div>
									</div>
								</div>
								<c:if test="${nowPage < totalCnt}">
<%-- 								<a href="/math/MathProblemSolve.do?bookCode=${result.bookCode}&chapterCode=${result.chapterCode}&rowCnt=1&nowPage=${nowPage+1}" class="question-arrow question-arrow--next">다음</a> --%>
								</c:if>
								
								<!-- 
								<a href="/math/MathProblemSolve2.do" class="question-arrow question-arrow--next">다음</a>
								 -->
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="c-popup"  id="alertPopup">
				<div class="c-popup_aligner">
					<div class="c-popup_box c-popup_box--small">
						<div class="c-popup_txt">정답을 입력해주세요 !</div>
						<div class="c-popup_buttons"><a href="javascript:alertClose();" class="c-popup_button">확인</a></div>
						<a href="javascript:alertClose();" class="c-popup_close">팝업 닫기</a>
					</div>
				</div>
			</div>
			
			<div class="c-popup"  id="alertPopup2">
				<div class="c-popup_aligner">
					<div class="c-popup_box c-popup_box--small">
						<div class="c-popup_txt">마지막 문제입니다 !</div>
						<div class="c-popup_buttons"><a href="javascript:alertClose();" class="c-popup_button">확인</a></div>
						<a href="javascript:alertClose();" class="c-popup_close">팝업 닫기</a>
					</div>
				</div>
			</div>
			
		</div>
	
	
	
<script type="text/javascript">

	function verifyAnswer() {
		var isEmpty = true;
		var userAnswer = "";
		$("input[name^='answer']").each(function(){
			if (this.type == 'radio' || this.type == 'checkbox'){
				if (this.checked){
					isEmpty = false;
					userAnswer += "|" + $(this).val();
				}
			}else {
				if (!$(this).val()) {
					isEmpty = true;
					return;
				}else{ 
					isEmpty = false;
					userAnswer += "|" + $(this).val();
				}
			}
		});
		if (isEmpty){
		   	document.getElementById('alertPopup').className += " active";			
		}else{

			$('#userAnswerStr').val(userAnswer.substr(1));
			sendXapiAnswer("answered", "정답 제출", "Activity", window.location.href, userAnswer.substr(1), "");
	        document.probForm.submit();
		}
	}
	
	function retry() {
		sendXapi("retried", "다시 풀기", "Activity", window.location.href, "");
		history.back();
	}
	
	function next() {
		var problemNo = parseInt($('#problemNo').val());
		var problemId = $('#problemId').val();
		
		if(problemNo == 13){
			document.getElementById('alertPopup2').className += " active";
			return;
		}else{
			location.href = "/comm/finalMath.do?problemNo=" + (problemNo+1);
		}
	}

	function insertSolve() {
    	var solveResult = '<%=solveResult %>';
    	sendXapiScore("scored", "채점", "Activity", document.referrer, (solveResult == 'X' ? 0 : 1), "");    	
		$('#solveResult').val(solveResult);
		var requestBody = getFormDataToJson('solveForm');
//         sendAjaxJsonData("/comm/insertPbProblemSolve.do", requestBody, "callbackPbProblemSolve", 'json', false);
        sendAjaxJsonData("/comm/insertFinalTest.do", requestBody, "tttt", 'json', false);
	}

	function sendAjaxCallback(callbackId, resultData, pageInfo){
    	//  debugger;
    	if(callbackId == 'tttt'){
    		var problemNo = parseInt($('#problemNo').val());
    		$('#problemList li:nth-child('+problemNo+')').addClass("active");
  	    }
	}
	
	function alertClose() {
		document.getElementById('alertPopup').className = "c-popup";
		document.getElementById('alertPopup2').className = "c-popup";
	}
	
	function goOut() {
		debugger;
		var bookCode = $('#bookCode').val();
		var chapterCode = $('#chapterCode').val();

		<c:if test="${result.userAnswer == null}">
	 		insertXApi("exited", "problem");//문제 이탈
		</c:if>
		<c:if test="${result.userAnswer != null}">
	 		insertXApi("exited", "");//문제결과 화면 이탈
		</c:if>
		
		location.href="/math/MathDashboardView2.do?bookCode="+bookCode+"&chapterCode="+chapterCode;
		
		
	}
	
	var bookCode = "";
    var chapterCode = "";
    var objectId = "";
    
    $(document).ready(function() {
    	
    	if(window.location.href.includes('ProblemSolve.do')){
			sendXapi("attempted", "문제풀이 시작", "Activity", window.location.href, '');
	   }else if(window.location.href.includes('ProblemAnswer.do')){
		   sendXapi("attempted", "문제해설 시작", "Activity", window.location.href, '');
	   }else{
		   //sendXapi("attempted", "페이지 방문", "Activity", window.location.href, '');
	   }
    	
    	bookCode = $('#bookCode').val();
   		chapterCode = $('#chapterCode').val();
   		objectId = $('#problemId').val();

   		
   		var solvedProblemNo = '${solvedProblemNo}';
   		if(solvedProblemNo.length>0){
   			var solvedProblemNos = solvedProblemNo.split(',');
   			for(var i=0 ; i<solvedProblemNos.length ; i++){
   				$('#problemList li:nth-child('+solvedProblemNos[i]+')').addClass("active");
   			}
   		}
   		
   		
		<c:if test="${result.userAnswer == null}">
			insertXApi("asked", "");//문제 출제
		</c:if>
		<c:if test="${result.userAnswer != null}">
			insertSolve();
			insertXApi("attempted", "");//문제결과 화면 진입
		</c:if>
			
			
		
			

    });
    
     
     // xapi 호출
   	function insertXApi(verb, gubun) {
   		
   		
   		var enGbDesc = "";
   		var koKrDesc = "";
   		
   		if (verb == "asked") {
   			if (gubun == "exited") {
   				enGbDesc = "skipped lecture";
   				koKrDesc = "강의 이탈";
   			}else{
   				enGbDesc = "submitted problem";
   				koKrDesc = "문제출제";
   			}
   		}else if (verb == "exited") {
   			if (gubun == "problem") {
   				enGbDesc = "exited problem page";
   	   			koKrDesc = "문제 이탈";
   			}else{
   				enGbDesc = "exited problem result page";
   				koKrDesc = "문제결과 화면 이탈";
   			}
   		}else if (verb == "attempted") {
   			enGbDesc = "entered problem result page";
   			koKrDesc = "문제결과 화면 진입";
   		}else if (verb == "scored") {
   			enGbDesc = "problem result";
   			koKrDesc = "정답 결과";
   		}
   			
   		
   		//xapiParamMap.put("objectId", "");
   		//xapiParamMap.put("enGbDesc", "");
   		//xapiParamMap.put("enUsDesc", "");
   		//xapiParamMap.put("koKrDesc", "");
   		
   		var requestBody = {
   				'verb' : verb,
   				'objectId' : objectId,
   				'enGbDesc' : enGbDesc,
   				'enUsDesc' : enGbDesc,
   				'koKrDesc' : koKrDesc
   			};
   		
   		sendAjaxJsonData("/comm/insertXApi.do", requestBody, "callbackInsertXApi", 'json', false);
   	}
 	
         //페이지 벗어날 때
     	window.addEventListener("unload", function() {
 		      let xapiJson = new FormData(); 
 		      xapiJson.append('verb','exited');
 		      xapiJson.append('objectId',objectId);
 		      xapiJson.append('enGbDesc','exited problem page');
 		      xapiJson.append('enUsDesc','exited problem page');
 		      xapiJson.append('koKrDesc','문제 이탈');
 		      navigator.sendBeacon("/comm/insertXApi2.do", xapiJson); 
 	    }, false);
</script>
</body>
</html>
