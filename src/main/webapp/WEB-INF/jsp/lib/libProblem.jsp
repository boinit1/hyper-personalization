<%--
  Class Name : MathProbleSolve.jsp
  Description : 문제풀이 화면
  Modification Information

    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성

  author   : (주)윈솔텍
  since    : 2022.06.15
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="sps.utils.StringUtil"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import = "egovframework.rte.psl.dataaccess.util.EgovMap" %>
<%@ page import = "java.util.regex.Pattern" %>  
<%@ page import = "java.util.regex.Matcher" %>  
<script>
//===========================================================
//mp3
//===========================================================
var audioPlayer = {
	src : '',
	state : '',
	initCnt : 0,
	play : function(src) {
		if (this.initCnt > 10) {
			//self.parentVm.mdCommonDef.ModalSimpleAlert("CUSTOMMSG", "음성을 정상적으로 재생하지 못했어요.<br/>학습창을 닫고 다시 이용하세요. ");
			if($("#tempAudio").length == 0) {
				var audioTag = "<audio id=\"tempAudio\" autoplay>"
							+ "<source src=\""+ src.replace(".swf", ".mp3") +"\" type=\"audio/mpeg\">"
							+ "음성을 정상적으로 재생하지 못했어요. 학습창을 닫고 다시 이용하세요."
							+ "</audio>";
				$("#ProblemBody").append(audioTag);
			}
			var x = document.getElementById("tempAudio");
			x.src = src.replace(".swf", ".mp3");
			x.play();
			x.addEventListener("ended", function(){ $(".audioFile").removeClass("on"); });
			return false;
		} else {
			if(this.state == "playing" && this.src == src.replace(".swf", ".mp3")) {
				this.pause();
			} else {
				if(typeof audio == "undefined") {
					this.createObj(src);
				} else {
					this.state = "playing";
					this.src = src.replace(".swf", ".mp3");
					audio.src = this.src;
					audio.play();
					audio.addEventListener("ended", function(){ audioPlayer.pause(); });
				}
			}
		}
	},
	pause : function() {
		//alert("ended")
		if(typeof audio != "undefined") {
			audio.pause();
		}
		$(".audioFile").removeClass("on");

		this.state = "end";
		//console.log(this.state);
	},
	createObj : function(src) {
		audio = new Audio(src);
		this.initCnt++;
		//console.log("설치횟수 :" + this.initCnt);
		this.play(src);
	}
}

function fnHintView(problemNum) {
	$("#hintTxt" + problemNum).removeClass("hide");
}

$(function() {
	$(document).on("click", ".audioBox", function() {
		$(".audioFile").removeClass("on");
		var audObj = $(this).children(".audioFile");
		var src = audObj.attr("filePath");
		if (src != "") {
			audObj.addClass("on");
			audioPlayer.play(src);
			// console.log(vm.audioPlayer.src);
			//console.log(src.replace(".swf", ".mp3"));
		}
	});
});

</script>
								<%! 
								public EgovMap data = new EgovMap();
								public EgovMap dicTemplate = new EgovMap(); 
								public EgovMap dicReg = new EgovMap();
								public String commaChr = "¿";
								public String checkResultSty = "";//q-dozen_q q-dozen_q--correct, incorrect
								public String solveResult = "O";
								
								public void setData(EgovMap d){
									data = d;
									//dicTemplate.put("swf", "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" type=\"application/x-shockwave-flash\" {size} align=\"absmiddle\"><param name=\"wmode\" value=\"transparent\"><param name=\"quality\" value=\"high\"/>"
									//		+ "<param name=\"loop\" value=\"true\"/><param name=\"movie\" value=\"{path}\"/><embed type=\"application/x-shockwave-flash\" src=\"{path}\" wmode=\"transparent\" {size}></embed></object>");
									dicTemplate.put("mp3", "<div class=\"audioBox\"><span class=\"audioFile\" filePath=\"{path}\"></span></div>");
									dicTemplate.put("img", "<img {size} src=\"{path}\" align=\"absmiddle\">");
									// 분수
									dicTemplate.put("mixedfraction", "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"ft_tlb\"><tr><td rowspan=\"3\" class=\"ft_wnum\">$1</td><td class=\"ft_nume\">$2</td></tr><tr><td class=\"ft_line\"></td></tr><tr><td class=\"ft_deno\">$3</td></tr></table>");
									dicTemplate.put("fraction", "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"ft_tlb\"><tr><td class=\"ft_nume\">$1</td></tr><tr><td class=\"ft_line\"></td></tr><tr><td class=\"ft_deno\">$2</td></tr></table>");
									// 문제 패턴별 구성
									dicTemplate.put("pbInput", "<div class=\"inlinebox\"><span class=\"pb_an_sym\">{sortsmb}</span><input type=\"text\" name=\"answer{pnum}\" value=\"{inanswer}\" size=\"{size}\" maxlength=\"50\"><span class=\"pb_an_unit\">{unit}</span></div>"); // 주관식 입력폼
									//dicTemplate.put("pbExample", "<li><b class=\"exn\"><input type=\"{type}\"{checked} name=\"answer{pnum}\" id=\"a{pnum}_{exnum}\" value=\"{exnum}\">{cnum}</b><label for=\"a{pnum}_{exnum}\">{cont}</label></li>" '// 객관식 보기
									dicTemplate.put("pbExample", "<li><label>{cont}</label><b class=\"exn\"><input type=\"{type}\"{checked} name=\"answer{pnum}\" id=\"a{pnum}_{exnum}\" value=\"{exnum}\">{cnum}</b></li>"); // 객관식 보기. 상기2 박팀장 요청으로 label 기능 사용안함
									dicTemplate.put("pbTextarea", "<textarea name=\"answer{pnum}\">{inanswer}</textarea>");
									dicTemplate.put("pbInputBox", "<input type=\"text\" size=\"{size}\" maxlength=\"50\" name=\"answer{pnum}\" value=\"{inanswer}\" class=\"pb_input\">");
									dicTemplate.put("pbInputBoxR", "<input type=\"text\" size=\"1\" maxlength=\"1\" name=\"answer{pnum}\" value=\"{inanswer}\" class=\"pb_round\">");

									dicReg.put("findObj", Pattern.compile("(\\d+(?:_\\d+)?_(p|s|ex|h|e)\\d{0,2}\\.(\\w{3}))(\\|\\d+(?:\\|\\d+)?)?")); 
									dicReg.put("findInput", Pattern.compile("\\${2}(\\d{1,2}|R)\\${2}")); 
									dicReg.put("findTag", Pattern.compile("</?[a-z]+.*?>")); 
									dicReg.put("fraction", Pattern.compile("\\(([^\\(\\)< ]+)\\/([^\\(\\)< ]+)\\)")); 
									dicReg.put("mixedfraction", Pattern.compile("\\(([^\\(\\)< ]+)\\+\\(([^\\(\\)< ]+)\\/([^\\(\\)< ]+)\\)\\)")); 
									dicReg.put("unClosedTag", Pattern.compile("<(center|font|p|div|b|u)\\b.*?>")); 
									dicReg.put("brTag", Pattern.compile("([ \\t\\r\\n]*<br/?>[ \\t]*[\\r\\n]*)")); 
									dicReg.put("alphabet", Pattern.compile("[A-Za-z0-9]")); 
								}

								public String num2circleStr(String ctype, int num){
									if ("H".equals(ctype))
										return "㉠㉡㉢㉣㉤㉥㉦㉧㉨㉩㉪㉫㉬㉭".substring(numMax(num % 15, 1) - 1, numMax(num % 15, 1));
									else if ("1".equals(ctype))
										return "①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮".substring(numMax(num % 16, 1) - 1, numMax(num % 16, 1));
									else
										return "" + num;
								}

								public int numMax(int v1, int v2){
									if (v1 > v2) 
										return v1;
									else
										return v2;
								}

								public String exampleList(){
									String template = "", buf = "", writeAnswer = "";
									int idx = 0;
									boolean userAnswerView;
									userAnswerView = data.containsKey("userAnswer");

									String answerType = (String)data.get("answerType");
									if ("T".equals(answerType)) { // 서술형
										template = ((String)dicTemplate.get("pbTextarea")).replace("{num}", "1");
										if (userAnswerView) 
											writeAnswer = ((String[])data.get("userAnswer"))[0];
										else
											writeAnswer = "";
										template = template.replace("{inanswer}", writeAnswer);
										return template;
									} else if ("B".equals(answerType)) { // 주관식
										if ( !data.containsKey("answer") || " hide".equals((String)data.get("examSty")) )
											return "";


										String sortsmb = "", unitChar = "", tSize = "", firstAnswer = "";
										int answerCnt, answerUnitCnt, fSize;
										boolean bImeActive;
										// {sortsmb} 순서 표시, {pnum} 시험지상 문제번호, {inanswer} 정답(db 또는 사용자), {unit} 단위
										String[] arrAnswer = (String)data.get("answer") == null ? null : ((String)data.get("answer")).split(",");
										answerCnt =  arrAnswer == null ? 0 : arrAnswer.length;
										String[] arrAnswerUnit = (String)data.get("answerUnit") == null ? null : ((String)data.get("answerUnit")).split("[|]");
										answerUnitCnt = arrAnswerUnit == null ? 0 : arrAnswerUnit.length;
										
										for (idx = 0; idx < answerCnt; idx++) {
											if ("N".equals((String)data.get("answerArrange"))) {
												sortsmb = (idx+1) + ")";
											} else if ("A".equals((String)data.get("answerArrange")) && idx > 0) {
												sortsmb = "→";
											} else {
												sortsmb = "";
											}
	
											if (userAnswerView) {
												writeAnswer = ((String[])data.get("userAnswer"))[idx];
											} else {
												writeAnswer = "";
											}
	
											unitChar = "";
											if (answerUnitCnt > 0) {
												if (answerUnitCnt == 1) {
													unitChar = arrAnswerUnit[0];
												} else if (answerUnitCnt >= idx) 
													unitChar = arrAnswerUnit[idx];
												
											}
											firstAnswer = arrAnswer[idx].split("^")[0];
											fSize = firstAnswer.length();
											if (fSize < 5) {
												if (answerCnt >= 4) { // 5개부터
													fSize = 3;
												} else {
													fSize = 5;
												}
											}
											tSize = fSize + "\" style=\"width:" + fSize + "em";
											// 굳이 필요 있을까 하기도... 부하걸리는 일임
											if (((Pattern)dicReg.get("alphabet")).matcher(firstAnswer).find()) {
												tSize = fSize + "\" class=\"pb_ime_inactive";
											} else {
												tSize = fSize + "\" class=\"pb_ime_active";
											}
	
											template = (String)dicTemplate.get("pbInput");
											template = template.replace("{sortsmb}", sortsmb);
											template = template.replace("{pnum}", (String)data.get("problemNum"));
											template = template.replace("{inanswer}", writeAnswer);
											template = template.replace("{size}", tSize);
											template = template.replace("{unit}", unitChar);
											if ((answerCnt >= 5 || answerUnitCnt > 0) && (idx+1) % 4 == 0) { buf = buf + "<br>";}
											buf = buf + template;
										}
										// 입력란이 4개 이상 줄바꿈 됐을때 왼쪽 정렬하려면 div로 한번더 감쌈
										//Response.Write "<li><div class=""inlinebox"">" +  buf  + "</div></li>"
										return "<li>" +  buf  + "</li>";
									} else { // 객관식
										if (!data.containsKey("example")) 
											return "";

										int ansCnt, uansCnt, ansIdx = 0;
										String temp = "";
										ansCnt = ((String)data.get("answer")).length() + 1;
										if (userAnswerView) {
											uansCnt = ((String[])data.get("userAnswer")).length;
										} else {
											uansCnt = 0;
										}
										
										String ex = (String)((ArrayList<EgovMap>)data.get("example")).get(0).get("example");
		
										for (idx = 0; idx < ((ArrayList<EgovMap>)data.get("example")).size();idx++) {
											template = (String)dicTemplate.get("pbExample");
											template = template.replace("{pnum}", (String)data.get("problemNum"));
											template = template.replace("{exnum}", "" +(idx+1));
											template = template.replace("{cnum}", num2circleStr((String)data.get("answerType"), idx+1));
	
											temp = "";
											if (userAnswerView) {
												if (uansCnt > ansIdx) {
													if ((idx+1) == Integer.valueOf(((String[])data.get("userAnswer"))[ansIdx])) {
														temp = " checked";
														ansIdx = ansIdx +1;
													}
												}
											}
											template = template.replace("{checked}", temp);
	
											if ("S".equals((String)data.get("answerMode"))) {
												temp = "radio";
											} else {
												temp = "checkbox";
											}
											template = template.replace("{type}", temp);
											temp = (String)((ArrayList<EgovMap>)data.get("example")).get(idx).get("example");
											if (temp.startsWith("#")) { temp = ""; }
											template = template.replace("{cont}", tagProc(temp));
											buf = buf + template;
										}
										return buf;
									}
								}

								public String answerProc(String ansStr){
									solveResult = "O";
									String answer, answerItem;
									String answerArrange = (String)data.get("answerArrange");
									String answerType = (String)data.get("answerType");
									ansStr = ansStr.replace("^", "<!or!>");

									boolean userAnswerView = false;
									boolean checkResult = true;
									boolean answerFirst = false;
									userAnswerView = data.containsKey("userAnswer");
									if (ansStr.contains(",")) {
										String[] ans = ansStr.split(",");
										for (int idx = 0; idx < ans.length; idx++){
											answerItem = ans[idx].replace(commaChr, ",");
											String[] temp = answerItem.split("<!or!>");
											// 첫번째 정답 마지막 동일 정답 처음이 @으로 시작한다면 정답 대신 해당 내용을 보여준다.
											if (temp[temp.length - 1].startsWith("@")) {
												answer = tagProc(temp[temp.length - 1].substring(1));
												answerFirst = true;
											}else{
												try{
													Integer.parseInt(temp[0]);
													answer = num2circleStr(answerType, Integer.parseInt(temp[0]));
												}
												catch(Exception ex){
													answer = temp[0];
												}
											}
											String styleCorrect = "";
											if (userAnswerView){
												String writeAnswer = ((String[])data.get("userAnswer"))[idx];
												for (int i = 0; i < temp.length; i++){
													if (writeAnswer.equals(temp[i]))
														styleCorrect = "c-correct";
												}
											}
											// 순서 있는 숫자
											if ("N".equals(answerArrange))
												answer = "<span class=\"pc1\">" + (idx+1) + ")</span> " + answer;
											//class=\"" + styleCorrect + "\" 
											ans[idx] = "<em style=\"top:5px;\"><span id=\"ansStr\" data-value=\"" + ansStr + "\" style=\"min-width: 108px;text-align: center;\">" + answer + "</span></em>";

											if ("".equals(styleCorrect)){
												checkResult = false;
												solveResult = "X";
											}
										}
										if (answerFirst) // 첫번째 정답 마지막 동일 정답 처음이 @으로 시작
											answer = ans[0];
										else if ("A".equals(answerArrange)) // 순서 있는 화살표
											answer = String.join(" → ", ans);
										else
											answer = String.join(", ", ans);
									} else {//답이 한 개 인 경우
										String[] temp = ansStr.split("<!or!>");
										String styleCorrect = "";
										if (userAnswerView){
											String writeAnswer = ((String[])data.get("userAnswer"))[0];
											for (int idx = 0; idx < temp.length; idx++){
												if (writeAnswer.equals(temp[idx]))
													styleCorrect = "c-correct";
											}
										}
										answer = String.join(" or ", temp);
										//class=\"" + styleCorrect + "\" 
										answer =  "<em style=\"top:5px;\"><span id=\"ansStr\" data-value=\"" + ansStr + "\" style=\"min-width: 108px;text-align: center;\">" + answer + "</span></em>";

										if ("".equals(styleCorrect)){
											checkResult = false;
											solveResult = "X";
										}
									}

									if (userAnswerView){
										checkResultSty = "q-dozen_q q-dozen_q--" + (checkResult?"correct":"incorrect");
									}else{
										checkResultSty = "";
									}
									return answer;
								}

								public String tagProc(String s){
									if (StringUtil.isEmpty(s)) 
										s = "";
									else {
										Matcher matcher;
										String addTag = "", temp = "";
										Integer findIdx = 0, findIdxBf = 0, tagIdx = 0;
										boolean userAnswerView = false;
	
										// 안닫힌 태그 찾아 정리,닫힌 태그를 찾지 못하거나 이전에 찾았던 곳과 동일하다면 닫히 않은것. 즉 열림만 중복
										// <u>...</u><u>...<u><u>...</u> 이런 경우도 있어 두번 닫게된다.
										matcher = ((Pattern)dicReg.get("unClosedTag")).matcher(s);
										addTag = "";
										temp = s;
										while (matcher.find()) { 
											findIdx = temp.indexOf("</" + matcher.group(1), matcher.start()+1);
											if (findIdx == findIdxBf || findIdx <= 0)
												addTag = "</"+ matcher.group(1) +">" + addTag;
											findIdxBf = findIdx;
											temp = temp.substring(matcher.end());
											matcher = ((Pattern)dicReg.get("unClosedTag")).matcher(temp);
										}
										if (!"".equals(addTag)) {
											s = s + addTag;
										}
										// 줄바꿈 처리
										s = ((Pattern)dicReg.get("brTag")).matcher(s).replaceAll("\r\n");
										// 분수 처리
										temp = s;
										ArrayList<String> mvList = new ArrayList();
										matcher = ((Pattern)dicReg.get("findTag")).matcher(temp);
										while (matcher.find()) { 
											mvList.add(tagIdx, matcher.group(0));
											temp = temp.replace(matcher.group(0), "#@" + tagIdx + "@");
											tagIdx = tagIdx + 1;
											matcher = ((Pattern)dicReg.get("findTag")).matcher(temp);
										}
										// 분수표현이 포함되어 있는지 체크
										matcher = ((Pattern)dicReg.get("fraction")).matcher(temp);
										if (matcher.find()) {
											temp = ((Pattern)dicReg.get("mixedfraction")).matcher(temp).replaceAll((String)dicTemplate.get("mixedfraction")); // (4+(2/3)) 형태부터 변경. (2/3) 먼저 변경하믄 안되니까
											temp = ((Pattern)dicReg.get("fraction")).matcher(temp).replaceAll((String)dicTemplate.get("fraction"));

										}
										// 포함된 태그 살리기
										tagIdx = 0;
										for (int i = 0; i < mvList.size(); i++) { 
											temp = temp.replace("#@" + i + "@", mvList.get(i));
										}
										s = temp;
										// $$5$$, $$R$$  input box 처리
										matcher = ((Pattern)dicReg.get("findInput")).matcher(s);
										if (matcher.find()) {
											Integer aIdx = 0;
											String writeAnswer = "";
											userAnswerView = data.containsKey("userAnswer");
											data.put("examSty", " hide");
											do { 
												if ("R".equals(matcher.group(1)))
													temp = (String)dicTemplate.get("pbInputBoxR");
												else{
													temp = (String)dicTemplate.get("pbInputBox");
													temp = temp.replace("{size}", matcher.group(1));
												}
												temp = temp.replace("{pnum}", (String)data.get("problemNum"));
												if (userAnswerView) {
													writeAnswer = ((String[])data.get("userAnswer"))[aIdx];
													aIdx = aIdx + 1;
												} else
													writeAnswer = "";
											
												temp = temp.replace("{inanswer}", writeAnswer);
												String m = matcher.group();
	
												s = s.substring(0, s.indexOf(m)) + temp + s.substring(s.indexOf(m) + m.length());
											}while (matcher.find());
										}
										// img, mp3 등 처리
										if (data.containsKey("lessonCode") && data.containsKey("schoolYear")){
											matcher = ((Pattern)dicReg.get("findObj")).matcher(s);
											while (matcher.find()) { 
												if ("mp3".equals(matcher.group(3)))
													temp = (String)dicTemplate.get("mp3");
												else
													temp = (String)dicTemplate.get("img");
												temp = temp.replace("{path}", (String)data.get("filePath") + (String)data.get("lesson") + "/" + (String)data.get("schoolYear")
												 + "/" + matcher.group(2) + "/" + matcher.group(1));
												if (!StringUtil.isEmpty(matcher.group(4)))
													temp = temp.replace("{size}", "width=\"" + String.join("\" height=\"", matcher.group(4).substring(1).split("\\|")) + "\"");
												else
													temp = temp.replace("{size}", "");

												if ("s".equals(matcher.group(2)) && s.equals(matcher.group()))
													temp = temp.replace(">", " class=\"pac2\">");
												s = s.replace(matcher.group(), temp);
											}
										}
									}
									return s;
								}
								
								// 보기 첫번째 항목에서 옵션 정보를 찾는다.
								// 현재 문제 정보 테이블에 넣을 공간이 없기에... ㉠:보기번호 한글원문자, ③: 보기 3단배열
								public boolean exHiddnOpt(ArrayList arrEx, String opt){
									boolean bReturn = false;
									/*if (IsArray(arrEx)) {
										Dim firstEx, optIdx
										firstEx = arrEx(0,0)
										if (InStr(firstEx, "<!--") = 1) {   '// -->
											optIdx = InStr(firstEx, opt)
											if (optIdx > 0 And optIdx < InStr(firstEx, "-->")) {
												arrEx(0,0) = Replace(firstEx, opt, "", 1, 1)
												arrEx(0,0) = Replace(arrEx(0,0), "<!---->", "", 1, 1)
												bReturn = vbTrue
											}
										}
									}*/
									return bReturn;
								}
								
								public void setGlobalVal() {
									// 지문 위치
									if (data.get("pattern") == null)
										data.put("pattern", 0);
									Integer patten = (Integer)data.get("pattern");
									switch (patten){
										case 1: case 2: case 3: case 4: case 5: case 6: case 15: case 16: case 17: case 18: case 19: case 21: case 25: case 27:
											data.put("sentenceType", "T");
									        break;
										case 7: case 8:
											data.put("sentenceType", "L");
											data.put("sentenceSty", " pb_sen_left");
									        break;
										default :
											data.put("sentenceType", "N");
											data.put("sentenceSty", " hide");
									}
	
									// 보기 형태
									switch (patten){
										case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13: case 14:
											data.put("answerType", "1");
											// H를 값으로 갖는 정의이 추가되어 따로 처리
											if (data.containsKey("example")) {
												if (exHiddnOpt((ArrayList)data.get("example"), "㉠")) {
													data.put("answerType", "H");
												}
											}
									        break;
										case 25: case 26:
											data.put("answerType", "T");
									        break;
										default :
											data.put("answerType", "B");
									}
	
									// 단/다답 (객관식만 해당)
									switch (patten){
										case 1: case 3: case 5: case 7: case 9: case 11: case 13:
											data.put("answerMode", "S");
									        break;
										case 2: case 4: case 6: case 8: case 10: case 12: case 14:
											data.put("answerMode", "M");
									}
	
									// 보기/입력폼 배열
									switch (patten){
										case 1: case 2: case 7: case 8: case 9: case 10:
											data.put("answerArrange", "1"); // 세로 (객관식)
									        break;
										case 3: case 4: case 11: case 12:
											data.put("answerArrange", "H"); // 가로
									        break;
										case 5: case 6: case 13: case 14:
											data.put("answerArrange", "2"); // 믹스
											// 3단 배열이 추가되어 패턴을 변경해야 했으나... 너무 많아서 이제 사용하지 안을 kind 컬럼을 활용...
											if (data.containsKey("example")) {
												if (exHiddnOpt((ArrayList)data.get("example"), "③")) {
													data.put("answerArrange", "3"); // 3단 배열
												}
											}
									        break;
										case 27: case 28:
											data.put("answerArrange", "T"); // 순서 있음 (주관식)
									        break;
										case 17: case  22:
											data.put("answerArrange", "N"); // 숫자 순서
									        break;
										case 18: case  23:
											data.put("answerArrange", "A"); // 화살표 순서
									        break;
										case 19: case  24:
											data.put("answerArrange", "F"); // 순서없음
									}
	
									// 보기/주관식 배열 설정 스타일
									switch (data.get("answerType").toString()){
										case "B":
											data.put("examSty", " pb_an_ca1r"); // 주관식은 오른쪽 정렬
									        break;
										case "T":
											data.put("examSty", " pb_an_ca1c"); // 서술형은 가운데 정렬
									        break;
										default : // 1, H
											switch (data.get("answerArrange").toString()){
												case "1":
													data.put("examSty", " pb_an_ca1"); // 기본 세로형
											        break;
												case "H": // 가로는 보기 수만큼
													if (data.containsKey("example")) 
														data.put("examSty", " pb_an_ca" + ((EgovMap)data.get("example")).size()+1);
													else
														data.put("examSty", " pb_an_ca5");
											        break;
												case "2": // 믹스
													data.put("examSty", " pb_an_ca2");
											        break;
												case "3": // 3단 배열
													data.put("examSty", " pb_an_ca3"); // 3단 배열
											}
									}
									// 학년 값 없을때 채움
									if (! data.containsKey("schoolYear")) {
										if (data.containsKey("learnCode")) 
											data.put("schoolYear", data.get("learnCode").toString().substring(3, 1));
										else if (data.containsKey("chapterCode")) 
											data.put("schoolYear", data.get("chapterCode").toString().substring(3, 1));
									}
								}
								%>
								
							<%				
								EgovMap data = (EgovMap)request.getAttribute("result");
								setData(data);
								setGlobalVal();

								String problem = "", sentence = "", hint = "", answer = "", explain = "", explainType = "", explainLec = "";
								String sentenceSty = "", examSty = "";
								String examGrpSty = "";
								String problemNum = (String)data.get("problemNum");
								String sentenceKind = "";
								
								Integer patten = Integer.valueOf(data.get("pattern").toString());

								if (!StringUtil.isEmpty(data.get("problem")))
									problem = tagProc((String)data.get("problem"));
								
								if (!StringUtil.isEmpty(data.get("sentenceSty"))) 
									sentenceSty = (String)data.get("sentenceSty");

								if (!StringUtil.isEmpty(data.get("sentence"))) {
									sentence = tagProc((String)data.get("sentence"));
									sentenceKind = "1";
									if (!StringUtil.isEmpty(data.get("sentenceKind"))) 
										sentenceKind = (String)data.get("sentenceKind");
									// 이미지나 플래시 단독으로 있을때는 테두리 없음
									// 공통 지문에도 테두리 없는 경우 필요하기 때문에 공통 지문 조건 제거
									//if (sentence.indexOf("pac2") <= 0 && sentenceKind != "a") 
									
									sentence = "<div class=\"pb_box\">" + sentence + "<div class=\"minw\"></div></div>";
								}
								else
									sentenceSty = " hide";

								//if (!StringUtil.isEmpty(data.get("hint"))) 
								//	hint = tagProc((String)data.get("hint"));

								if (!StringUtil.isEmpty(data.get("answer"))) {
									answer = (String)data.get("answer");
									answer = answerProc(answer);
									//answer = "<div class=\"pb_answer\">" + answer + "</div>";
									/* answer = "<div class=\"content_answer\">" + "\n" +
						                    "    <div class=\"icon review_icon\">정답</div>" + "\n" +
						                    "    <div class=\"wrap_text\">" + answer + "</div>" + "\n" +
						                    "</div>"; */
								}
								
								if (!StringUtil.isEmpty(data.get("explain"))) {
									explain = (String)data.get("explain");
									//explain = "<div class=\"pb_expl\">" + expLec + explain + "</div>";
									explain = "<div class=\"content_boxer content_review\">" + "\n" +
						                    "    <div class=\"icon review_icon\">해설</div>" + "\n" +
						                    "    <div class=\"char\"></div>" + "\n" +
						                    "    <div class=\"wrap_text\">" + "\n";
						            //explain += expLec;
						            explain += "        <div class=\"pb_expl wrap\">" + explain + "</div>" + "\n" +
						                    "    </div>" + "\n" +
						                    "</div>";

								}

								if (!StringUtil.isEmpty(data.get("examSty"))) {
									examSty = (String)data.get("examSty");
									if (patten == 7 || patten == 8) 
										examGrpSty = "_lr";
								}
								else
									examGrpSty = " hide";

								// 1,2 학년 다답 객관식 문제의 경우 힌트에 몇개의 답이 있는지 추가한다.
								// 힌트가 없는 문제는 답 수가 힌트가 된다.
								if (!StringUtil.isEmpty(data.get("answer"))) {
									String schoolYear = (String)data.get("schoolYear");
									if (schoolYear == "1" || schoolYear == "2") 
										if (patten == 1 || patten == 2 || patten == 6 || patten == 8 || patten == 10  || patten == 12  || patten == 14 ) {
											if (!StringUtil.isEmpty(data.get("hint"))) 
												data.put("hint", (String)data.get("hint") + "<br>");
											data.put("hint", (String)data.get("hint") + (((String[])data.get("answer")).length+1) + "개입니다.)");
										}
								}
								
								if (!StringUtil.isEmpty(data.get("hint"))) {
									hint = tagProc((String)data.get("hint"));
									hint = "<div id=\"hintTxt" +  problemNum  + "\" class=\"pb_hint hide\">" +  hint  + "</div>";
									// 힌트 보기
									problem = problem  +  "<span class=\"hint_btn\"> [<span onclick=\"fnHintView('" +  problemNum  + "')\">힌트</span>]</span>";
								}

								String exampleList = exampleList();
								
								if (!StringUtil.isEmpty(data.get("comSentenceCnt"))) {
							%>
								<div class="pb_layout clearfix">
									<div class="pb_sent_com_txt">[${nowPage } ~ <%=(String)data.get("comSenLastNum") %>] 다음 질문에 답하세요.</div>
									<div class="pb_sentence pb_sentence_com<%=sentenceSty%>"><%=sentence%></div>
								</div>
							<%
									sentenceSty = " hide";
								}
							%>
