function ActiveX_Object(objectID, classID, codeBase, width, height)
{
	this.objectID = objectID;
	this.classID = classID;
	this.codeBase = codeBase;
	this.width = width;
	this.height = height;
}

function ActiveX_Param(name, value)
{
	this.name = name;
	this.value = value;
}

function CreateActiveX(object, params)
{
	document.write("<OBJECT ID='" + object.objectID + "' CLASSID='" + object.classID + "' CODEBASE='" + object.codeBase + "' WIDTH='" + object.width + "' HEIGHT='" + object.height + "'>");
	if (params != null)
	{
		for (i = 0; i < params.length; i++)
			document.write("<PARAM NAME='" + params[i].name + "' VALUE='" + params[i].value + "'>");		
	}
	document.write("</OBJECT>");
}