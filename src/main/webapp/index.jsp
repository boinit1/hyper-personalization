<%--
  Class Name : index.jsp
  Description : 최초화면으로 메인화면으로 이동한다.(system)
  Modification Information
 
    수정일           수정자              수정내용
  -------    --------    ---------------------------
  2022.06.15 BizAn             최초작성
 
  author   : (주)윈솔텍 
  since    : 2022.06.15
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
<jsp:forward page="/home/mainPage.do"/>
<script type="text/javaScript">document.location.href="<c:url value='/admin/TreeMenuList.do'/>"</script>
--%>
<script type="text/javaScript">document.location.href="<c:url value='/login/LoginUsr.do'/>"</script>