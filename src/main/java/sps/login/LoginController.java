package sps.login;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.cmmn.trace.LeaveaTrace;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import sps.utils.ApiService;
import sps.utils.ParameterArrUtil;
import sps.utils.UserDetailsHelper;


/**
 * LoginController 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.15
 * @version 1.0
 * @see 
 *
 * 로그인을 처리하는 컨트롤러
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.15  BizAn                  최초 생성
 * </pre>
 */

@Controller
public class LoginController {
	
	@Resource(name="egovMessageSource")
	private EgovMessageSource  egovMessageSource;
		
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "ApiService")
	private ApiService apiService;

	@Resource(name = "LoginService")
	private LoginService loginService;
	
	/** TRACE */
	@Resource(name = "leaveaTrace")
	LeaveaTrace leaveaTrace;
    
    private final int loginFailMaxCnt = 5; 

    @RequestMapping(value = "/login/LoginUsr.do")
	public String loginUsrView(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model, HttpSession session) throws Exception {
		
    	return "login/LoginUsr";
        
	}
    
    @RequestMapping(value = "/login/actionLogin.do")
	public String actionLogin(Locale locale, @ModelAttribute("loginVO") LoginVO loginVO, @RequestParam Map<String, Object> commandMap, HttpServletResponse response, HttpServletRequest request,  ModelMap model, HttpSession session) throws Exception {
    	
		LoginVO resultVO = null;
		if(loginVO != null && "Y".equals(loginVO.getAutoLoginYn())) {    	        	
    		Cookie[] cookies = request.getCookies();
    	    if(cookies !=null){
    	        for(Cookie tempCookie : cookies){
    	            if(tempCookie.getName().equals("id")){
    	            	resultVO = new LoginVO();
    	            	resultVO.setUserId(tempCookie.getValue());
    	            	resultVO.setUserNm(TempUser.tempUserNames.get(tempCookie.getValue()));
    	            	break;
    	            }
    	        }
    	    }
		} else {
			resultVO = loginService.actionLogin(loginVO);
		}
	    
	    if(resultVO == null) { 
	    	model.addAttribute("autoLoginFailYn", "Y");
			return "login/LoginUsr";
	    }
	    
	    
	    if(loginVO != null && "Y".equals(loginVO.getCookieSaveYn())) {
	    	Cookie cookie = new Cookie("id", resultVO.getUserId());
            cookie.setMaxAge(24*60*60*15);
            response.addCookie(cookie);
	    }
	    
	    request.getSession().setAttribute("LoginVO", resultVO);
	    
	    // xAPI 호출//////////////////////////////////////////////////
	    // 로그인
  		String userId = resultVO.getUserId();
  		String userNm = resultVO.getUserNm();
  		EgovMap xapiParamMap = new EgovMap();
		xapiParamMap.put("id", userId);
		xapiParamMap.put("name", userNm);
		xapiParamMap.put("verb", "attempted");
		xapiParamMap.put("objectId", userId);
		
		xapiParamMap.put("enGbDesc", "success login");
		xapiParamMap.put("enUsDesc", "success login");
		xapiParamMap.put("koKrDesc", "로그인 성공");
		
		apiService.callXApi(xapiParamMap);
		//////////////////////////////////////////////////////////////
		
	
		return "redirect:/math/MathDashboardView.do";
  
	}
    
    @RequestMapping(value = "/login/actionLogout.do")
   	public String actionLogout(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model, HttpSession session) throws Exception {
   		
    	//무조건 로그인 세션 삭제
   		RequestContextHolder.getRequestAttributes().removeAttribute("LoginVO", RequestAttributes.SCOPE_SESSION);
   		
   		//페이징 옵션
   		ParameterArrUtil.mapValueSetModel(commandMap, model);
   		
   		Cookie[] cookies = request.getCookies() ;
   		
   		if(cookies != null){
   			for(int i=0; i < cookies.length; i++){
   				
   				// 쿠키의 유효시간을 0으로 설정하여 만료시킨다
   				cookies[i].setMaxAge(0) ;
   				
   				// 응답 헤더에 추가한다
   				response.addCookie(cookies[i]) ;
   			}
   		}
   		
   		model.addAttribute("autoLoginFailYn", "Y");
		return "login/LoginUsr";
           
   	}
}
