package sps.login;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import org.apache.ibatis.type.Alias;

import egovframework.rte.psl.dataaccess.util.EgovMap;
import sps.comm.DefaultVO;

@Getter
@Setter
@Alias("LoginVO")
public class LoginVO {
	private static final long serialVersionUID = 1L;
	private String autoLoginYn;
	private String cookieSaveYn;
	private String userId;
	private String userNo;
	private String userPwd;
	private String lastLoginDt;
	private Integer loginFailCnt;
	private String userNm;
}