package sps.login;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * LoginService 클래스 (로그인 처리)
 * 
 * @author (주)윈솔텍
 * @since 2022.06.16
 * @version 1.0
 * @see 
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.15  BizAn                  최초 생성
 * </pre>
 */
@Service("LoginService")
public class LoginService extends EgovAbstractServiceImpl {

	private final LoginDAO loginDAO;
	
	private final int loginFailMaxCnt = 5; 

	@Autowired
    public LoginService(LoginDAO loginDAO) {
        this.loginDAO = loginDAO;
    }

	
	public LoginVO actionLogin(LoginVO vo) throws Exception {
		
		if(TempUser.tempUsersPw.get(vo.getUserId()) == null) {
			return null;
		}else if(TempUser.tempUsersPw.get(vo.getUserId()).equals(vo.getUserPwd())) {
		
			LoginVO loginVO = new LoginVO();
			try 
			{
				loginVO.setUserId(vo.getUserId());
				loginVO.setUserNm(TempUser.tempUserNames.get(vo.getUserId()));				
				
				return loginVO;
				
			}catch(Exception ex) {
				ex.printStackTrace();
				return null;
			}
			
		} else {
			return null;
		}
	}
	
}