package sps.login;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.EgovAbstractMapper;
import egovframework.rte.psl.dataaccess.util.EgovMap;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * LoginDAO 클래스 (로그인 처리)
 * 
 * @author (주)윈솔텍
 * @since 2022.06.16
 * @version 1.0
 * @see 
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.16  BizAn                  최초 생성
 * </pre>
 */
@Repository
public class LoginDAO {

	@Resource(name="sqlSession")
    private final SqlSessionTemplate sqlSession;

    @Autowired
    public LoginDAO(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }
    
	public LoginVO getWUserInfo(EgovMap param) throws Exception {
		return (LoginVO) sqlSession.selectOne("wLoginMapper.getWUserInfo", param);
	}
	
}
