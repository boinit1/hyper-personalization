package sps.api;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.cmmn.trace.LeaveaTrace;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.property.EgovPropertyService;
import sps.login.LoginService;
import sps.login.LoginVO;
import sps.utils.ScriptUtil;
import sps.utils.StringUtil;


/**
 * ApiController 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.20
 * @version 1.0
 * @see 
 *
 * 자동 로그인을 처리하는 컨트롤러
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.15  BizAn                  최초 생성
 * </pre>
 */

@Controller
public class ApiController {
	
	@Resource(name="egovMessageSource")
	private EgovMessageSource  egovMessageSource;
		
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "LoginService")
	private LoginService loginService;
	
	/** TRACE */
	@Resource(name = "leaveaTrace")
	LeaveaTrace leaveaTrace;
    
	private String visangApiUrl =  EgovProperties.getProperty("visang.api.url");
	private String visangApiToken =  EgovProperties.getProperty("visang.api.token");
	
    @RequestMapping(value = "/api/spsLogin.do")
	public String spsLogin(HttpServletRequest request,  HttpServletResponse response, ModelMap model) throws Exception {
    
    	
    	/*
    	 * java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		      URL url = new URL(callUrl);
	
		      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	
		      connection.setDoInput(true);
		      connection.setDoOutput(true);
		      connection.setRequestProperty("Connection","Keep-Alive");
		      connection.setRequestProperty("Authorization", apiToken);
		      connection.setRequestProperty("Content-Type","multipart/form-data;boundary="+boundary+";charset=utf-8");
		      connection.setRequestMethod("POST");
		      DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
    	 */
    	
    	HashMap resJson = new HashMap();
    	
    	//API 토큰 체크
    	/*
		if(!loginApiToken.equals(request.getHeader("Authorization"))) {
			resJson.put("resultCode", "0001");
		    resJson.put("errorMessage", "Authorization failed.");
		    ScriptUtil.alert(response, "Authorization failed.");
		    return resJson.toString();
		}    
    	*/
    	
    	try {
    		
    		Enumeration params = request.getParameterNames();
    		String userId = "";
    		while(params.hasMoreElements()) {
    			String name = (String)params.nextElement();
    			if ("userId".equals(name)) 
    				userId = request.getParameter(name);
    		}
    		
    		if (StringUtil.isEmpty(userId)) {
    			resJson.put("resultCode", "0001");
    		    resJson.put("errorMessage", "userId 파라미터 값이 없습니다.");
    		    ScriptUtil.alert(response, "userId 파라미터 값이 없습니다.");
    		    return resJson.toString();
    		}
    		
	    	LoginVO loginVO = new LoginVO();
	    	loginVO.setUserId(userId);
	    	LoginVO resultVO = loginService.actionLogin(loginVO);
	    	request.getSession().setAttribute("LoginVO", resultVO);
			
			return "redirect:/math/MathDashboardView.do";
    	}catch (Exception e) {
    		resJson.put("resultCode", "0001");
		    resJson.put("errorMessage", "처리 중 시스템 오류가 발생하였습니다. " + e.getMessage());
		    ScriptUtil.alert(response, "처리 중 시스템 오류가 발생하였습니다. " + e.getMessage());
		    return resJson.toString();
    	}
	}
}
