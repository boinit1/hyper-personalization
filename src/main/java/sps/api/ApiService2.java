package sps.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.internal.Primitives;

//import com.nanet.apos.exception.AposException;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import twitter4j.JSONObject;

@Service("ApiService2")
public class ApiService2 extends EgovAbstractServiceImpl  {
	
	@Autowired
	Environment environment;
	
	
	private Logger LOGGER = Logger.getLogger(ApiService2.class);
	
	//비상 HOST
	private String visangApiHost = EgovProperties.getProperty("visang.api.host"); 
	private String visangApiToken = EgovProperties.getProperty("visang.api.token");
	
	//서강대 HOST
	private String seogangApiHost = EgovProperties.getProperty("seogang.api.host"); 
	private String seogangApiToken = EgovProperties.getProperty("seogang.api.token");
	
	
	
	public String callVisangApi(String methed, String requestJson) throws Exception {

		  String callUrl = visangApiHost +  methed;
		  
		  try {
	    	  java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		      URL url = new URL(callUrl);
	
		      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	
		      connection.setDoInput(true);
		      connection.setDoOutput(true);
		      connection.setUseCaches(false);
		      connection.setRequestProperty("Connection","Keep-Alive");
		      connection.setRequestProperty("Authorization", visangApiToken);
		      connection.setRequestProperty("Content-Language","ko-KR");
		      connection.setRequestProperty("Content-Type", "application/json");
		      connection.setRequestMethod("POST");

		      DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
		      dos.writeBytes(requestJson);
		      //써진 버퍼를 stream에 출력.
		      dos.flush();
	
		      //전송. 결과를 수신.
		      BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
		      String output = "";
		      String temp;
		      while ((temp = br.readLine()) != null) {
		         output += temp;
		      }
	
		      LOGGER.debug("===========================================");
		      LOGGER.debug("= " + output);
		      LOGGER.debug("===========================================");
	
		      connection.disconnect();
		      dos.close();
		      
		      return output;
	      } catch (Exception ex) {
	    	  HashMap resJson = new HashMap();
	    	  resJson.put("resultCode", "9998");
	    	  resJson.put("errorCode", "9998");
	    	  resJson.put("errorMessage", "Fileserver connection failed.");
	    	  return resJson.toString();
	      }
	}

}
