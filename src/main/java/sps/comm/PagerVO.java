package sps.comm;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PagerVO {
	public int PAGE_SCALE	= 10;
	public int BLOCK_SCALE = 10;

	private int curPage; 			// 현재페이지
	private int prevPage;			// 이전페이지
	private int nextPage;			// 다음페이지
	private int totPage;			// 전체페이지
	private int totBlock;			// 전체페이지 블록
	private int curBlock;			// 현재페이지 블록
	private int prevBlock;			// 이전페이지 블록
	private int nextBlock;			// 다음페이지 블록
	private int pageBegin;			// 시작페이지
	private int pageEnd;			// 끝페이지
	private int blockBegin;			// 시작 블록
	private int blockEnd;			// 끝 블록
	private int totCnt;				// 전체로우수	
	
	private String searchType;		// 검색어 타입
	private String keyword;			// 검색어
	private String userid;			// 사용자ID

	public PagerVO() {}
	
	public PagerVO(int pageScale, int blockScale, int count, int curPage) {
		curBlock = 1;
		this.PAGE_SCALE	= pageScale;
		this.BLOCK_SCALE = blockScale;
		this.totCnt = count;
		this.curPage = curPage;
		setTotPage(count);
		setPageRange();
		setTotBlock();
		setBlockRange();
	}
	
	public void setBlockRange() {

		curBlock = (int) Math.ceil((curPage - 1) / BLOCK_SCALE) + 1;

		blockBegin = (curBlock - 1) * BLOCK_SCALE + 1;

		blockEnd = blockBegin + BLOCK_SCALE - 1;

		if (blockEnd > totPage)
			blockEnd = totPage;

		prevPage = (curPage == 1) ? 1 : (curBlock - 1) * BLOCK_SCALE;

		nextPage = curBlock > totBlock ? (curBlock * BLOCK_SCALE) : (curBlock * BLOCK_SCALE) + 1;

		if (nextPage >= totPage)
			nextPage = totPage;
	}

	public void setPageRange() {

		pageBegin = (curPage - 1) * PAGE_SCALE + 1;

		pageEnd = pageBegin + PAGE_SCALE - 1;
	}

	public void setTotPage(int count) {

		totPage = (int) Math.ceil(count * 1.0 / PAGE_SCALE);
	}

	public void setTotBlock() {

		totBlock = (int) Math.ceil(totPage / BLOCK_SCALE);
	}

	@Override
	public String toString() {
		return "PagerVo [curPage=" + curPage + ", prevPage=" + prevPage + ", nextPage=" + nextPage + ", totPage="
				+ totPage + ", totBlock=" + totBlock + ", curBlock=" + curBlock + ", prevBlock=" + prevBlock
				+ ", nextBlock=" + nextBlock + ", pageBegin=" + pageBegin + ", pageEnd=" + pageEnd + ", blockBegin="
				+ blockBegin + ", blockEnd=" + blockEnd + ", searchType=" + searchType + ", keyword=" + keyword
				+ ", userid=" + userid + "]";
	}
	
	
}
