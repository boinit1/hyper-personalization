package sps.comm;

import lombok.Getter;
import lombok.Setter;

/**
 * WstPaging 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.15
 * @version 1.0
 * @see 
 *
 * Paging 처리 클래스
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.16  BizAn                  최초 생성
 * </pre>
 */

@Getter
@Setter
public class Paging {
	private int currentPage = 1;
	private int pagePerCount = 100;
	private int count;
	private int totalCount;
	private int maxPageCount;
	private int startRow = 0;
	private int endRow = -1;

	public Paging() {

	}

	public Paging(int currentPage, int pagePerCount, int totalCount) {
		this.currentPage = currentPage;
		this.pagePerCount = pagePerCount;
		this.totalCount = totalCount;
		this.count = 0;
		_calculate();
	}

	private void _calculate() {

		// 현제 페이지 설정이 없으면 1페이지로 고정
		if (currentPage <= 0) currentPage = 1;

		// 페이지당 개수 설정이 있어야 계산
		if (pagePerCount > 0) {

			// 전체 검색 개수와 페이지당 개수가 딱 맞아 떨어지면 나눈값 / 아니면 +1
			if ((totalCount % pagePerCount) == 0) {
				maxPageCount = (int) (totalCount / pagePerCount);
			} else {
				maxPageCount = (int) (totalCount / pagePerCount) + 1;
			}

			// 현제 노출 된 카운트는 마지막페이지일때만 계산 / 그 이외에는 페이지당 개수랑 같음
			if (maxPageCount == currentPage) {
				count = totalCount % pagePerCount;
				if(totalCount == pagePerCount) {
					count = totalCount;
				}
			} else if (maxPageCount > currentPage){
				count = pagePerCount;
			}

			// 시작 행 인덱스는 1부터 시작 (1/3 [0~2], [3,5], [6~8] )
			startRow = pagePerCount * (currentPage - 1);
			endRow = startRow + pagePerCount;

		}
		// 페이지당 개수 설정이 없으면 1페이지 전체 리스트
		else {
			currentPage = 1;
			pagePerCount = totalCount;
			count = totalCount;
			startRow = 0;
			endRow = totalCount - 1;
			maxPageCount = 1;
		}
	}
}
