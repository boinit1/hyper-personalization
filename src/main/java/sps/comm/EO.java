package sps.comm;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class EO implements Serializable {
	private static final long serialVersionUID = -6168799228959824230L;
	private int rownum;
	private String sortKey;
	private String sortType;
	private int currentPage = -1;
	private int pagePerCount = -1;
	private int page;
	private int perPage;
	private Paging paging;
}
