package sps.comm;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * DefaultVO 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.15
 * @version 1.0
 * @see 
 *
 * VO 부모 클래스
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.16  BizAn                  최초 생성
 * </pre>
 */

@Getter
@Setter
public abstract class DefaultVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int rownum;
	private String sortKey;
	private String sortType;
	private int currentPage = -1;
	private int pagePerCount = -1;
	private int page;
	private int perPage;
	private Paging paging;
	
	/** 검색조건 */
    private String searchCondition = "";

    /** 검색Keyword */
    private String searchKeyword = "";

    /** 검색사용여부 */
    private String searchUseYn = "";

    /** 현재페이지 */
    private int pageIndex = 1;

    /** 페이지갯수 */
    private int pageUnit = 10;

    /** 페이지사이즈 */
    private int pageSize = 10;

    /** firstIndex */
    private int firstIndex = 1;

    /** lastIndex */
    private int lastIndex = 1;

    /** recordCountPerPage */
    private int recordCountPerPage = 10;

    /** 검색KeywordFrom */
    private String searchKeywordFrom = "";

	/** 검색KeywordTo */
    private String searchKeywordTo = "";
}
