package sps.comm;

import egovframework.rte.psl.dataaccess.util.EgovMap;

import java.util.List;
import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * WstDAO 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.16
 * @version 1.0
 * @see 
 *
 * 데이터 액세스 공통 클래스
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.16  BizAn                  최초 생성
 * </pre>
 */

@Repository
public class DAO {

	@Resource(name="sqlSession")
    private final SqlSessionTemplate sqlSession;

    @Autowired(required=true)
    public DAO(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }
    
	
	public List<?> selectList(String query, EgovMap param) throws Exception {
		return (List<?>) sqlSession.selectList(query, param);
	}
	
	public EgovMap selectOne(String query, EgovMap param) throws Exception {
		return sqlSession.selectOne(query, param);
	}

	public Integer dmlInsert(String query, EgovMap vo) throws Exception {
		return sqlSession.insert(query, vo);
	}
	
	public Integer dmlUpdate(String query, EgovMap vo) throws Exception {
		return sqlSession.update(query, vo);
	}
	
	public Integer dmlDelete(String query, EgovMap vo) throws Exception {
		return sqlSession.delete(query, vo);
	}

}
