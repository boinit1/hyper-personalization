package sps.utils;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

//JSON 메시지 처리를 위해 추가
import java.util.HashMap;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 메시지 리소스 사용을 위한 MessageSource 인터페이스 및 ReloadableResourceBundleMessageSource 클래스의 구현체
 */

public class EgovMessageSourceExtend extends ReloadableResourceBundleMessageSource implements MessageSource {

	private ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource;

	/**
	 * getReloadableResourceBundleMessageSource() 
	 * @param reloadableResourceBundleMessageSource - resource MessageSource
	 * @return ReloadableResourceBundleMessageSource
	 */	
	public void setReloadableResourceBundleMessageSource(ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource) {
		this.reloadableResourceBundleMessageSource = reloadableResourceBundleMessageSource;
	}
	
	/**
	 * getReloadableResourceBundleMessageSource() 
	 * @return ReloadableResourceBundleMessageSource
	 */	
	public ReloadableResourceBundleMessageSource getReloadableResourceBundleMessageSource() {
		return reloadableResourceBundleMessageSource;
	}
	
	/**
	 * 정의된 메세지 조회
	 * @param code - 메세지 코드
	 * @return String
	 */	
	public String getMessage(String code) {
		return getReloadableResourceBundleMessageSource().getMessage(code, null, Locale.getDefault());
	}

	//JSON 메시지 처리를 위해 야기서부터 추가 By BizAn
	public HashMap getResultToJsonMsg(String resCd, String resMsg)
	  {
	    HashMap resJson = new HashMap();

	    resCd = resCd.replace("CMMN.", "");
	    resCd = resCd.replace("UNIQ.", "");

	    resJson.put("JSON", "Y");
	    resJson.put("resultCode", resCd);
	    resJson.put("errorMessage", resMsg);

	    return resJson;
	  }

	  public String getResCommToJsonMsg(String msgCd)
	  {
	    return getMessage("GWTY." + msgCd);
	  }

	  public HashMap getResCRUDToJsonMsg(String resCode, String divTorF, HashMap Param)
	    throws JsonProcessingException
	  {
	    String resMsg = "";
	    if (Param.get("initSetLeng") == null) Param.put("initSetLeng", "KR");
	    if (Param.get("initSrvGroup") == null) Param.put("initSrvGroup", "ADM");
	    String initSetLeng = Param.get("initSetLeng").toString();
	    String initSrvGroup = Param.get("initSrvGroup").toString();
	    String msgCd = initSetLeng + "." + initSrvGroup + "." + resCode + "." + divTorF;

	    System.out.println("msgCd === " + msgCd);

	    String workName = "";
	    if ((Param.get("workDiv") != null) && (!Param.get("workDiv").equals(""))) workName = getMessage(initSetLeng + "." + initSrvGroup + "." + Param.get("workDiv"));

	    System.out.println("workName === " + workName);

	    resMsg = getMessage(msgCd);
	    resMsg = resMsg.replaceAll("#WKND#", workName);

	    System.out.println("resMsg === " + resMsg);

	    HashMap resJson = new HashMap();
	    resJson = getResultToJsonMsg(resCode, resMsg);

	    return resJson;
	  }

	  public String getMatchMsg(String msgCd, String workDiv, String workName, String paramNm, HashMap Param)
	  {
	    String[] arrWorkDiv = workDiv.split(",");
	    String[] arrWorkName = workName.split(",");
	    String resMSG = "";

	    for (int i = 0; i < arrWorkDiv.length; i++)
	    {
	      if (Param.get(paramNm).toString().equals(arrWorkDiv[i]))
	      {
	        resMSG = getMessage(msgCd);
	        resMSG = resMSG.replaceAll("#WKND#", arrWorkName[i]);
	        break;
	      }

	    }

	    return resMSG;
	  }
	  
}
