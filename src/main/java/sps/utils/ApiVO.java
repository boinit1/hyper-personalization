package sps.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import egovframework.rte.psl.dataaccess.util.EgovMap;
import twitter4j.JSONArray;
import twitter4j.JSONObject;

import lombok.Getter;
import lombok.Setter;
import sps.utils.PageInfoVO;



/*
 * API VO 클래스
 * @author (주)윈솔텍
 * @since 2022-06-15
 * @version 1.0 
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2022-06-16  BizAn          최초 생성
 *
 * </pre>
 */




@Getter
@Setter
@SuppressWarnings("serial")
public class ApiVO  implements Serializable {

	/** API 호출 성공유무 */
	private String resultCode = "";

	/** 오류 코드 */
	private String errorCode = "";

	/** 오류 메시지 */
	private String errorMessage = "";

	/** 페이지 정보 */
	private PageInfoVO pageInfo = new PageInfoVO();

	/** 리스트 결과 */
	private List<?> list = new ArrayList();

	/** 상세 결과 */
	private Object item;
		
	
    /** 결과 데이터  */
    private JSONObject pageInfoObject;
	
    /** 결과 데이터  */
    private JSONObject resultObject;
    
    /** 결과 데이터  */
    private JSONArray resultArray;
    
    /** 결과 데이터  */
    private String resultData;
    
    /** 결과 데이터  */
    private JSONObject outputObject;
        
    /** 결과 데이터  */
    private List<EgovMap> listEgovMap = new ArrayList<EgovMap>();
    
    /** 결과 데이터  */
    private EgovMap egovMap = new EgovMap();

	/** 현재페이지 */
	private int pageIndex = 1;
    
	/** 페이지갯수 */
	private int pageUnit = 10;
    
	/** 페이지사이즈 */
	private int pageSize = 10;

	/** firstIndex */
	private int firstIndex = 1;

	/** lastIndex */
	private int lastIndex = 1;

	/** recordCountPerPage */
	private int recordCountPerPage = 10;
}

