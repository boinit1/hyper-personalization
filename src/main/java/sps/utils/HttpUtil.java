package sps.utils;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtil {
	public static Boolean isExistUrl(String url) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("HEAD");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return true;
	         } else {
	            return false;
	         }
		}catch (Exception e) {
	        e.printStackTrace();
	        return false;
		}
	}
	
	public static Boolean isExistIndexFile(String path) {
		try {
			
			File file = new File(path + "\\index.html");
	        if (file.exists()) {
	            if (file.isDirectory()) {
	                return false;
	            } else if (file.isFile()) {
	            	return true;
	            }
	        } else {
	        	return false;
	        }
	        
		}catch (Exception e) {
	        e.printStackTrace();
	        return false;
		}
		return false;
	}

}
