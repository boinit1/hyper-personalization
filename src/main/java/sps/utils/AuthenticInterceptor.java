package sps.utils;

import java.util.Enumeration;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.mvc.WebContentInterceptor;

import egovframework.com.cmm.EgovMessageSource;
import sps.login.LoginVO;
import sps.login.TempUser;

/**
 * 인증여부 체크 인터셉터
 * @author 공통서비스 개발팀 서준식
 * @since 2011.07.01
 * @version 1.0
 * @see
 *
 */

public class AuthenticInterceptor extends WebContentInterceptor {

	@Resource(name="egovMessageSource")
	private EgovMessageSource  egovMessageSource;
	
	/**
	 * 세션에 계정정보(LoginVO)가 있는지 여부로 인증 여부를 체크한다.
	 * 계정정보(LoginVO)가 없다면, 로그인 페이지로 이동한다.
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException {

		LoginVO loginVO = (LoginVO) UserDetailsHelper.getAuthenticatedUser();
		
		if (loginVO.getUserId() != null) {
			String userAgent = request.getHeader("User-Agent").toUpperCase();
			
		    if(userAgent.indexOf("MOBI") > -1) {
		     
		    	Cookie[] cookies = request.getCookies();
	    	    if(cookies !=null){
	    	        for(Cookie tempCookie : cookies){
	    	            if(tempCookie.getName().equals("id")){
	    	            	loginVO = new LoginVO();
	    	            	loginVO.setUserId(tempCookie.getValue());
	    	            	loginVO.setUserNm(TempUser.tempUserNames.get(tempCookie.getValue()));
	    	            	break;
	    	            }
	    	        }
	    	    }
		    	
		    }
		}
	    
		
		if (loginVO.getUserId() != null) {
			return true;
		} else {
			ModelAndView modelAndView = new ModelAndView("redirect:/login/LoginUsr.do");
			String callUrl = request.getRequestURL().toString() + "?";
			Enumeration enumeration = request.getParameterNames();
	        while (enumeration.hasMoreElements()) {
	            String parameterName = (String) enumeration.nextElement();
	            callUrl += parameterName + "=" + request.getParameter(parameterName) + "&";
				modelAndView.addObject("callUrl", callUrl);
	        }
			modelAndView.addObject("message", "0001");
			throw new ModelAndViewDefiningException(modelAndView);
		}
		
	}

}
