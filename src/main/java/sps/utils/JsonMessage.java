package sps.utils;

import java.util.HashMap;

public class JsonMessage {
	
	public static HashMap getJsonMessage(String resCd, String resMsg)
	  {
	    HashMap resJson = new HashMap();

	    resJson.put("JSON", "Y");
	    resJson.put("resultCode", resCd);
	    resJson.put("errorMessage", resMsg);

	    return resJson;
	  }
}
