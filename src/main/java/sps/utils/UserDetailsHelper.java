package sps.utils;

import java.util.ArrayList;
import java.util.List;

import egovframework.rte.fdl.string.EgovObjectUtil;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import sps.login.LoginVO;

/**
 * EgovUserDetails Helper 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.16
 * @version 1.0
 * @see 
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.16  BizAn                  최초 생성
 * </pre>
 */
public class UserDetailsHelper {
	
		/**
		 * 인증된 사용자객체를 VO형식으로 가져온다.
		 * @return Object - 사용자 ValueObject
		 */
		public static LoginVO getAuthenticatedUser() {
			return (LoginVO)RequestContextHolder.getRequestAttributes().getAttribute("LoginVO", RequestAttributes.SCOPE_SESSION)==null ? 
					new LoginVO() : (LoginVO) RequestContextHolder.getRequestAttributes().getAttribute("LoginVO", RequestAttributes.SCOPE_SESSION);

		}

		public static Boolean isAuthenticated() {
			LoginVO LoginVO = (LoginVO) RequestContextHolder.getRequestAttributes().getAttribute("LoginVO", RequestAttributes.SCOPE_SESSION);
			if (EgovObjectUtil.isNull(LoginVO) || LoginVO.getUserId() == null) {
				return Boolean.FALSE;
			}
			return Boolean.TRUE;
		}
}
