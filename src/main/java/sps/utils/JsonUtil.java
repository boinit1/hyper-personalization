package sps.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import egovframework.rte.psl.dataaccess.util.EgovMap;

public class JsonUtil {
	public static String toJsonArray(List<EgovMap> eGovMapList) {
		JSONArray array = new JSONArray();
		for(int i = 0; i < eGovMapList.size(); i++) {
		    EgovMap m = eGovMapList.get(i);
		    JSONObject obj = new JSONObject();
		    
		    m.forEach((key, value)
				    -> obj.put(key, StringUtil.isNullToString(value)));
		    
		    array.add(obj);
		}
		
		return array.toJSONString();
	}
	
	public static JSONObject defalutJsonObject(EgovMap param) {
		String uuid = UUID.randomUUID().toString();
		
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("id",uuid);
		jsonObject.put("timestamp",sdf.format(date));
		
		
		return jsonObject;
	}
	
}
