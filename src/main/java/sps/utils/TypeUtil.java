package sps.utils;

import java.text.DecimalFormat;

public class TypeUtil {
	private final static DecimalFormat decimalformat = new DecimalFormat();

	public static String toStr(Object value) {
		if (value == null) {
			return "";
		}
		return String.valueOf(value);
	}

	public static String toStr(boolean value) {
		return String.valueOf(value);
	}

	public static String toStr(char value) {
		return String.valueOf(value);
	}

	public static String toStr(char[] value) {
		return String.valueOf(value);
	}

	public static String toStr(int value) {
		return String.valueOf(value);
	}

	public static String toStr(long value) {
		return String.valueOf(value);
	}

	public static String toStr(float value) {
		return String.valueOf(value);
	}

	public static String toStr(double value) {
		return String.valueOf(value);
	}

	public static int toInt(Object value) {
		return toInt(toStr(value));
	}

	public static int toInt(String value) {
		if (value == null) {
			value = "";
		}
		try {
			return decimalformat.parse(value).intValue();
		} catch (Exception e) {
			return 0;
		}
	}

	public static int toInt(long value) {
		return new Long(value).intValue();
	}

	public static int toInt(float value) {
		return new Float(value).intValue();
	}

	public static int toInt(double value) {
		return new Double(value).intValue();
	}

	public static long toLong(Object value) {
		return toLong(toStr(value));
	}

	public static long toLong(String value) {
		if (value == null) {
			value = "";
		}
		try {
			return decimalformat.parse(value).longValue();
		} catch (Exception e) {
			return 0L;
		}
	}

	public static long toLong(int value) {
		return new Integer(value).longValue();
	}

	public static long toLong(float value) {
		return new Float(value).longValue();
	}

	public static long toLong(double value) {
		return new Double(value).longValue();
	}

	public static float toFloat(Object value) {
		return toFloat(toStr(value));
	}

	public static float toFloat(String value) {
		if (value == null) {
			value = "";
		}
		try {
			return decimalformat.parse(value).floatValue();
		} catch (Exception e) {
			return 0f;
		}
	}

	public static float toFloat(int value) {
		return new Integer(value).floatValue();
	}

	public static float toFloat(long value) {
		return new Long(value).floatValue();
	}

	public static float toFloat(double value) {
		return new Double(value).floatValue();
	}

	public static double toDouble(Object value) {
		return toDouble(toStr(value));
	}

	public static double toDouble(String value) {
		if (value == null) {
			value = "";
		}
		try {
			return decimalformat.parse(value).doubleValue();
		} catch (Exception e) {
			return 0.0;
		}
	}

	public static double toDouble(int value) {
		return new Integer(value).intValue();
	}

	public static double toDouble(long value) {
		return new Long(value).doubleValue();
	}

	public static double toDouble(float value) {
		return new Float(value).doubleValue();
	}
}
