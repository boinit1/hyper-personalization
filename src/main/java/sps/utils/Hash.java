package sps.utils;

import java.security.MessageDigest;

/**
 * Hash 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.15
 * @version 1.0
 * @see 
 *
 * SHA-256 해시값 생성
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.16  BizAn                  최초 생성
 * </pre>
 */
public class Hash {
	
	public Hash() {
	}
	
	//해시값 리턴
	public static String getHash(String input){		
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");	        
			//Applies sha256 to our input, 
			byte[] hash = digest.digest(input.getBytes("UTF-8"));	        
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
	}	
			
}