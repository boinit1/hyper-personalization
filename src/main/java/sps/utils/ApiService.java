package sps.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.internal.Primitives;

import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import twitter4j.JSONArray;
import twitter4j.JSONException;
import twitter4j.JSONObject;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

/*
 * 외부시스템  JSON API 
 * @author (주)윈솔텍
 * @since 2022-06-15
 * @version 1.0 
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2022-06-16  BizAn          최초 생성
 *
 * </pre>
 */


@Service("ApiService")
public class ApiService extends EgovAbstractServiceImpl {

	//<!-- [BTGenStart:class] --> 

	private static final Logger LOGGER = LoggerFactory.getLogger(ApiService.class);

	//웹 URL
	private String webMainHost = EgovProperties.getProperty("web.main.host"); 
		
	//비상 HOST
	private String visangApiHost = EgovProperties.getProperty("visang.api.host"); 
	private String visangApiToken = EgovProperties.getProperty("visang.api.token");
	
	//서강대 HOST
	private String seogangApiHost = EgovProperties.getProperty("seogang.api.host"); 
	private String seogangApiToken = EgovProperties.getProperty("seogang.api.token");
	
	//xAPI HOST
	private String xapiApiHost = EgovProperties.getProperty("xapi.api.host"); 
	private String xapiApiToken = EgovProperties.getProperty("xapi.api.token");
	private String xapiVerbHost = EgovProperties.getProperty("xapi.verb.host");
	
	static HashMap codeMap = new HashMap();
	
	
	
    static String CRLF = "\r\n"; 
    static String twoHyphens = "--"; 
    static String boundary = "*****mgd*****"; 
	
    /**
	 * 비상 API 호출.
	 * @param jsonParam
	 * @throws Exception
	 */
    
    public void callXApi(EgovMap param) throws Exception {
		
		String jsonParam = generateXapiJsonFormat(param);
    	
    	callApi2(xapiApiHost, xapiApiToken, jsonParam);
		
	}

	public ApiVO callListVisangApi(String apiUrl, String jsonParam) throws Exception {
		
		apiUrl = visangApiHost + apiUrl;
		String output = callApi(apiUrl, visangApiToken, jsonParam);
		
		if (output == null || output.equals("")) return null;
		
		ApiVO apiVO = jsonParseToListEgovMap(output);
		if (!apiVO.getResultCode().equals("200"))
			apiVO = null;
			//throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		
		return apiVO;
		
	}
	
	public ApiVO callListSeogangApi(String apiUrl, String jsonParam) throws Exception {
		
		apiUrl = seogangApiHost + apiUrl;
		String output = callApi(apiUrl, seogangApiToken, jsonParam);
		
		if (output == null || output.equals("")) return null;
		
		ApiVO apiVO = jsonParseToListEgovMap2(output);
		if (!apiVO.getResultCode().equals("200"))
			apiVO = null;
			//throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		
		return apiVO;
		
	}
	
	//비상 learnAchieveList
	private ApiVO jsonParseToListEgovMap(String output) throws Exception {
		
		ApiVO apiVO = new ApiVO();
		Gson gson = new Gson();
		
		JSONObject outObj = new JSONObject(output);
		apiVO.setOutputObject(outObj);
		apiVO.setResultCode(outObj.has("status") && !outObj.isNull("status")  ? outObj.getString("status") : null);
		
		
		JSONObject outObj2 = null;
		if (outObj.has("data") && !outObj.isNull("data")) {
			outObj2 = new JSONObject(outObj.getString("data"));
		
			//총합습시간, 강의수강현황 문제풀이 현황
			EgovMap egovMap2 = new EgovMap();
			egovMap2.put("memberProblemTime", outObj2.getInt("memberProblemTime"));
			egovMap2.put("memberLectureCount", outObj2.getInt("memberLectureCount"));
			egovMap2.put("memberProblemCount", outObj2.getInt("memberProblemCount"));
			egovMap2.put("problemTotalCount", outObj2.getInt("problemTotalCount"));
			apiVO.setEgovMap(egovMap2);
			/////////////////////////////////////
		}
		
		apiVO.setResultArray(outObj2.has("learnAchieveList") && !outObj2.isNull("learnAchieveList") ? outObj2.getJSONArray("learnAchieveList") : null);
		
		//System.out.println(apiVO.getResultArray().toString());
		
		//JsonArray를 List HashMap 형으로 변환
		ObjectMapper mapper = new ObjectMapper();
		List<HashMap<String, Object>> dtlList;
		dtlList = mapper.readValue(apiVO.getResultArray().toString(), new TypeReference<List<Map<String, Object>>>(){});

		//List HashMap 을 List EgovMap 로 변환
		
		List<EgovMap> listMap = new ArrayList<EgovMap>();
		for(int i=0; i<dtlList.size(); i++) {
			EgovMap egovMap = new EgovMap();
			for(Map.Entry entry : dtlList.get(i).entrySet()){
				egovMap.put(entry.getKey(), StringUtil.isNullToString(entry.getValue())); 
			}
			listMap.add(egovMap);
		}
		
		//apiVO.setList(dtlList);
		apiVO.setListEgovMap(listMap);
		
		return apiVO;
	}
	
	//서강대 api/recommend/topic_code API
	private ApiVO jsonParseToListEgovMap2(String output) throws Exception {
		
		ApiVO apiVO = new ApiVO();
		Gson gson = new Gson();
		
		JSONObject outObj = new JSONObject(output);
		apiVO.setOutputObject(outObj);
		apiVO.setResultCode(outObj.has("topic_codes") ? "200" : null);
		
		/*
		JSONObject outObj2 = null;
		if (outObj.has("topic_codes") && !outObj.isNull("topic_codes")) {
			outObj2 = new JSONObject(outObj.getString("topic_codes"));
		}
		*/
		
		apiVO.setResultArray(outObj.has("topic_codes") && !outObj.isNull("topic_codes") ? outObj.getJSONArray("topic_codes") : null);
		
		//System.out.println(apiVO.getResultArray().toString());
		
		//JsonArray를 List HashMap 형으로 변환
		ObjectMapper mapper = new ObjectMapper();
		List<HashMap<String, Object>> dtlList;
		dtlList = mapper.readValue(apiVO.getResultArray().toString(), new TypeReference<List<Map<String, Object>>>(){});

		//List HashMap 을 List EgovMap 로 변환
		
		List<EgovMap> listMap = new ArrayList<EgovMap>();
		for(int i=0; i<dtlList.size(); i++) {
			EgovMap egovMap = new EgovMap();
			for(Map.Entry entry : dtlList.get(i).entrySet()){
				egovMap.put(entry.getKey(), StringUtil.isNullToString(entry.getValue())); 
			}
			listMap.add(egovMap);
		}
		
		//apiVO.setList(dtlList);
		apiVO.setListEgovMap(listMap);
		
		return apiVO;
	}
	
	public void callApi2(String apiUrl, String token, String jsonParam)  {
		
		try {
			
			String callUrl = apiUrl;
			LOGGER.debug("API URL:" + callUrl);
			LOGGER.debug("API TOKEN:" + token);
			LOGGER.debug("API PARAM:" + jsonParam);
			
			//java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
			
			
	         
			URL url = new URL(callUrl);
			//String protocol = url.getProtocol();
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			//connection.setRequestProperty("Content-Type", "application/json; utf-8");
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Authorization", token);
			
			/*
			// Set Hostname verification
			((HttpsURLConnection) connection).setHostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					// Ignore host name verification. It always returns true.
					return true;
				}
			});
			
			// SSL setting
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, null, null); // No validation for now
			((HttpsURLConnection) connection).setSSLSocketFactory(context.getSocketFactory());
			
			// Connect to host
			connection.connect();
			connection.setInstanceFollowRedirects(true);
			*/		
	
			OutputStream os = connection.getOutputStream();
	        os.write(jsonParam.getBytes("UTF-8"));
	        os.flush();
	         /*
			try(OutputStream os = connection.getOutputStream()) {
			    byte[] input = jsonParam.getBytes("utf-8");
			    os.write(input, 0, input.length);			
			}
			*/
			/*
			OutputStream os = connection.getOutputStream();
			os.write(jsonParm.getBytes());
			os.flush();
			*/

			int status = connection.getResponseCode();
			LOGGER.debug("통신 결과 : " + status);
			if (status != HttpURLConnection.HTTP_OK) {
				LOGGER.error("API 통신 오류 : [" + status + "] " + callUrl + " 연결 실패!");
				throw new Exception("API 통신 오류 : [" + status + "] " + callUrl + " 연결 실패!");
			}
	
			BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
			String output = "";
			String temp;
			while ((temp = br.readLine()) != null) {
				output += temp;
			}
	
			LOGGER.debug("===========================================");
			LOGGER.debug("= " + output);
			LOGGER.debug("===========================================");
	
			connection.disconnect();
	
			
		} catch (Exception e) {
			//e.printStackTrace();
			LOGGER.error("API 통신 오류 :" + e.getMessage());
			
		}
		
		
	}
	
    
	public String callApi(String apiUrl, String token, String jsonParam) throws Exception {
		
		try {
			
			String callUrl = apiUrl;
			LOGGER.debug("API URL:" + callUrl);
			LOGGER.debug("API PARAM:" + jsonParam);
			
			
			//jsonParam = URLEncoder.encode(jsonParam, "UTF-8");
			
			java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
			URL url = new URL(callUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Content-Type", "application/json");
			//connection.setRequestProperty("Content-Type", "application/json; utf-8");
			//connection.setRequestProperty("Authorization", token);
			connection.setRequestProperty("x-api-key", token);
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
	
			try(OutputStream os = connection.getOutputStream()) {
			    byte[] input = jsonParam.getBytes("utf-8");
			    os.write(input, 0, input.length);			
			}
			
			/*
			OutputStream os = connection.getOutputStream();
			os.write(jsonParm.getBytes());
			os.flush();
			*/

			int status = connection.getResponseCode();
			LOGGER.debug("통신 결과 : " + status);
			if (status != HttpURLConnection.HTTP_OK) {
				LOGGER.error("API 통신 오류 : [" + status + "] " + callUrl + " 연결 실패!");
				throw new Exception("API 통신 오류 : [" + status + "] " + callUrl + " 연결 실패!");
			}
	
			BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
			String output = "";
			String temp;
			while ((temp = br.readLine()) != null) {
				output += temp;
			}
	
			LOGGER.debug("===========================================");
			LOGGER.debug("= " + output);
			LOGGER.debug("===========================================");
	
			connection.disconnect();
	
			return output;
		} catch (Exception e) {
			//e.printStackTrace();
			LOGGER.error("API 통신 오류 :" + e.getMessage());
			throw new Exception("API 통신 오류 :" + e.getMessage());
			
		}
		
	}
	/**
	 * API List형을 조회한다.
	 * @param banner
	 * @throws Exception
	 */
	public ApiVO callApiList(String apiUrl, String param, Class cl) throws Exception {
		
		String output = callApi(apiUrl, "", param);
		
		ApiVO apiVO = jsonParseApiList(output, cl);
		
		if (!apiVO.getResultCode().equals("success"))
			throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		
		
		return apiVO;
		
	}

	/**
	 * API Item형을 조회한다.
	 * @param banner
	 * @throws Exception
	 */
	public ApiVO callApiItem(String apiUrl, String param, Class cl) throws Exception {
		
		String output = callApi(apiUrl, "", param);
		
		ApiVO apiVO = jsonParseApi(output, cl);
		
		if (!apiVO.getResultCode().equals("success"))
			throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		
		return apiVO;
		
	}


	/**
	 * API Item형을 조회한다.
	 * @param banner
	 * @throws Exception
	 */
	public ApiVO callApiItem(String apiUrl, String param) throws Exception {
		
		String output = callApi(apiUrl, "", param);
		
		ApiVO apiVO = jsonParseApi(output);
		
		//if (!apiVO.getResultCode().equals("success"))
		//	throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		
		return apiVO;
		
	}



	/**
	 * API Item형을 조회한다.
	 * @param banner
	 * @throws Exception
	 */
	public ApiVO callApiString(String apiUrl, String param) throws Exception {
		
		String output = callApi(apiUrl, "", param);
		
		ApiVO apiVO = jsonParseApiString(output);
		
		//if (!apiVO.getResultCode().equals("success"))
		//	throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		
		return apiVO;
		
	}

	
	/**
	 * JSONObject 형을 조회한다.
	 * @param banner
	 * @throws Exception
	 */
	public JSONObject callApiItemNoCheck(String apiUrl, String param) throws Exception {
		
		String output = callApi(apiUrl, "", param);
		
		JSONObject outObj = new JSONObject(output);
	
	
		return outObj;
		
	}

	

	/**
	 * API 를 저장(호출)한다.
	 * @param banner
	 * @throws Exception
	 */
	public ApiVO callApiSave(String apiUrl, String param, Class cl) throws Exception {
		
		String output = callApi(apiUrl, "", param);
		
		ApiVO apiVO = jsonParseApiSave(output, cl);
		
		if (!apiVO.getResultCode().equals("success"))
			throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		
		return apiVO;
		
	}

	
	/**
	 * API 를 호출한다.
	 * @param banner
	 * @throws Exception
	 */
	public ApiVO callApiSave(String apiUrl, String param) throws Exception {
		
		String output = callApi(apiUrl, "", param);
		
		ApiVO apiVO = jsonParseApiSave(output);
		
		if (!apiVO.getResultCode().equals("success"))
			throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		
		return apiVO;
		
	}


	/**
	 * API Code을 조회한다.
	 * @param banner
	 * @throws Exception
	 */
 	public List<?> callApiCode(String code) throws Exception {
 		
		

		List<HashMap> list = (List) codeMap.get(code);
		if (list != null) {
			LOGGER.debug("CACHE CODE 호출 : " + code);
			return list;
		}
		
 		
 		
 		
 		JSONObject pObj = new JSONObject();
		pObj.put("codeGroup", code);
		
		ApiVO apiVO = callApiList("/tms/code/list", pObj.toString(), HashMap.class);


		if (!apiVO.getResultCode().equals("success"))
			throw new Exception("API Error : [" + apiVO.getErrorCode() + "] " + apiVO.getErrorMessage());
		

		List<HashMap> codeList = new ArrayList();
		for (int i=0; i<apiVO.getList().size(); i++) {
			HashMap map = (HashMap) apiVO.getList().get(i);
			HashMap newMap = new HashMap();
			if (map.containsKey("code"))
				newMap.put("value", map.get("code"));
			if (map.containsKey("codeName"))
				newMap.put("text", map.get("codeName"));
			codeList.add(newMap);
		}
		
		codeMap.put(code, codeList);
		
		LOGGER.debug("API CODE 호출 : " + code);
		return codeList;
 		
 	}

 	
	public String callApi2(String apiUrl, String param) throws Exception {
		
		try {
			//String body = "{\"tbId2\" : \"56\"}";
			String apiHost = "";
			String apiToken = "";
			apiHost = apiUrl;
			apiToken = apiToken;
			
			String callUrl = apiHost + apiUrl;
			LOGGER.debug("API URL:" + callUrl);
			LOGGER.debug("API PARAM:" + param);
			
			java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
			URL url = new URL(callUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Authorization", apiToken);
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
	
			
			OutputStream os = connection.getOutputStream();
			os.write(param.getBytes());
			os.flush();

			int status = connection.getResponseCode();
			LOGGER.debug("통신 결과 : " + status);
			if (status != HttpURLConnection.HTTP_OK) {
				LOGGER.error("API 통신 오류 : [" + status + "] " + callUrl + " 연결 실패!");
				throw new Exception("API 통신 오류 : [" + status + "] " + callUrl + " 연결 실패!");
			}
	
			BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
			String output = "";
			String temp;
			while ((temp = br.readLine()) != null) {
				output += temp;
			}
	
			LOGGER.debug("===========================================");
			LOGGER.debug("= " + output);
			LOGGER.debug("===========================================");
	
			connection.disconnect();
	
			return output;
		} catch (Exception e) {
			//e.printStackTrace();
			LOGGER.error("API 통신 오류 :" + e.getMessage());
			throw new Exception("API 통신 오류 :" + e.getMessage());
			
		}
		
	}

	
	private ApiVO jsonParseApi(String output, Class cl) throws Exception {
		ApiVO apiVO = new ApiVO();
		Gson gson = new Gson();
		
		JSONObject outObj = new JSONObject(output);
		apiVO.setOutputObject(outObj);
		apiVO.setResultCode(outObj.has("resultCode") && !outObj.isNull("resultCode")  ? outObj.getString("resultCode") : null);
		apiVO.setErrorCode(outObj.has("errorCode") && !outObj.isNull("errorCode") ? outObj.getString("errorCode") : null);
		apiVO.setErrorMessage(outObj.has("errorMessage") && !outObj.isNull("errorMessage") ? outObj.getString("errorMessage") : null);
		
		apiVO.setPageInfoObject(outObj.has("pageInfo") && !outObj.isNull("pageInfo") ? outObj.getJSONObject("pageInfo") : null);
		apiVO.setResultObject(outObj.has("resultData") && !outObj.isNull("resultData") ? outObj.getJSONObject("resultData") : null);


		// 페이징 정보
		if (apiVO.getPageInfoObject() != null)
			apiVO.setPageInfo(convJsonToObject(apiVO.getPageInfoObject().toString(), PageInfoVO.class));
		
		
		if (apiVO.getResultObject() != null) {
			Object obj = convJsonToObject(apiVO.getResultObject().toString(), cl);
			apiVO.setItem(obj);
		}
		
		return apiVO;
	}
	
	
	
	private ApiVO jsonParseApi(String output) throws Exception {
		ApiVO apiVO = new ApiVO();
		Gson gson = new Gson();
		
		JSONObject outObj = new JSONObject(output);
		apiVO.setOutputObject(outObj);
		apiVO.setResultCode(outObj.has("resultCode") && !outObj.isNull("resultCode")  ? outObj.getString("resultCode") : null);
		apiVO.setErrorCode(outObj.has("errorCode") && !outObj.isNull("errorCode") ? outObj.getString("errorCode") : null);
		apiVO.setErrorMessage(outObj.has("errorMessage") && !outObj.isNull("errorMessage") ? outObj.getString("errorMessage") : null);
		
		apiVO.setPageInfoObject(outObj.has("pageInfo") && !outObj.isNull("pageInfo") ? outObj.getJSONObject("pageInfo") : null);
		apiVO.setResultObject(outObj.has("resultData") && !outObj.isNull("resultData") ? outObj.getJSONObject("resultData") : null);


		// 페이징 정보
		if (apiVO.getPageInfoObject() != null)
			apiVO.setPageInfo(convJsonToObject(apiVO.getPageInfoObject().toString(), PageInfoVO.class));
		
		
		
		return apiVO;
	}
	
	
	private ApiVO jsonParseApiString(String output) throws Exception {
		ApiVO apiVO = new ApiVO();
		Gson gson = new Gson();
		
		JSONObject outObj = new JSONObject(output);
		apiVO.setOutputObject(outObj);
		apiVO.setResultCode(outObj.has("resultCode") && !outObj.isNull("resultCode")  ? outObj.getString("resultCode") : null);
		apiVO.setErrorCode(outObj.has("errorCode") && !outObj.isNull("errorCode") ? outObj.getString("errorCode") : null);
		apiVO.setErrorMessage(outObj.has("errorMessage") && !outObj.isNull("errorMessage") ? outObj.getString("errorMessage") : null);
		
		apiVO.setPageInfoObject(outObj.has("pageInfo") && !outObj.isNull("pageInfo") ? outObj.getJSONObject("pageInfo") : null);
		apiVO.setResultData(outObj.has("resultData") && !outObj.isNull("resultData") ? outObj.getString("resultData") : null);


		// 페이징 정보
		if (apiVO.getPageInfoObject() != null)
			apiVO.setPageInfo(convJsonToObject(apiVO.getPageInfoObject().toString(), PageInfoVO.class));
		
		
		
		return apiVO;
	}
	
	
	
	
	private ApiVO jsonParseApiList(String output, Class cl) throws Exception {
		ApiVO apiVO = new ApiVO();
		Gson gson = new Gson();
		
		JSONObject outObj = new JSONObject(output);
		apiVO.setOutputObject(outObj);
		apiVO.setResultCode(outObj.has("resultCode") && !outObj.isNull("resultCode")  ? outObj.getString("resultCode") : null);
		apiVO.setErrorCode(outObj.has("errorCode") && !outObj.isNull("errorCode") ? outObj.getString("errorCode") : null);
		apiVO.setErrorMessage(outObj.has("errorMessage") && !outObj.isNull("errorMessage") ? outObj.getString("errorMessage") : null);
		
		apiVO.setPageInfoObject(outObj.has("pageInfo") && !outObj.isNull("pageInfo") ? outObj.getJSONObject("pageInfo") : null);
		apiVO.setResultArray(outObj.has("resultData") && !outObj.isNull("resultData") ? outObj.getJSONArray("resultData") : null);

		Boolean test = outObj.isNull("resultData");
		
		// 페이징 정보
		if (apiVO.getPageInfoObject() != null)
			apiVO.setPageInfo(convJsonToObject(apiVO.getPageInfoObject().toString(), PageInfoVO.class));
		
		ArrayList list =  new ArrayList();

		for (int i=0; apiVO.getResultArray() != null && i<apiVO.getResultArray().length(); i++) {

			if (!apiVO.getResultArray().isNull(i)) {
				JSONObject jobj = apiVO.getResultArray().getJSONObject(i);
				Object item = convJsonToObject(jobj.toString(), cl);
				list.add(item);
			}
		}
		apiVO.setList(list);

		return apiVO;
	}
	
	

	private ApiVO jsonParseApiSave(String output, Class cl) throws Exception {
		ApiVO apiVO = new ApiVO();
		Gson gson = new Gson();
		
		JSONObject outObj = new JSONObject(output);
		apiVO.setOutputObject(outObj);
		apiVO.setResultCode(outObj.has("resultCode") && !outObj.isNull("resultCode")  ? outObj.getString("resultCode") : null);
		apiVO.setErrorCode(outObj.has("errorCode") && !outObj.isNull("errorCode") ? outObj.getString("errorCode") : null);
		apiVO.setErrorMessage(outObj.has("errorMessage") && !outObj.isNull("errorMessage") ? outObj.getString("errorMessage") : null);
		
		apiVO.setPageInfoObject(outObj.has("pageInfo") && !outObj.isNull("pageInfo") ? outObj.getJSONObject("pageInfo") : null);
		apiVO.setResultObject(outObj.has("resultData") && !outObj.isNull("resultData") ? outObj.getJSONObject("resultData") : null);


		// 페이징 정보
		if (apiVO.getPageInfoObject() != null)
			apiVO.setPageInfo(convJsonToObject(apiVO.getPageInfoObject().toString(), PageInfoVO.class));
		

		
		if (apiVO.getResultObject() != null) {
			Object obj = convJsonToObject(apiVO.getResultObject().toString(), cl);
			apiVO.setItem(obj);
		}
		
		return apiVO;
	}
	

 
	private ApiVO jsonParseApiSave(String output) throws Exception {
		ApiVO apiVO = new ApiVO();
		Gson gson = new Gson();
		
		JSONObject outObj = new JSONObject(output);
		apiVO.setOutputObject(outObj);
		apiVO.setResultCode(outObj.has("resultCode") && !outObj.isNull("resultCode")  ? outObj.getString("resultCode") : null);
		apiVO.setErrorCode(outObj.has("errorCode") && !outObj.isNull("errorCode") ? outObj.getString("errorCode") : null);
		apiVO.setErrorMessage(outObj.has("errorMessage") && !outObj.isNull("errorMessage") ? outObj.getString("errorMessage") : null);
		
		apiVO.setPageInfoObject(outObj.has("pageInfo") && !outObj.isNull("pageInfo") ? outObj.getJSONObject("pageInfo") : null);
		//apiVO.setResultObject(outObj.has("resultData") && !outObj.isNull("resultData") ? outObj.getJSONObject("resultData") : null);


		// 페이징 정보
		if (apiVO.getPageInfoObject() != null)
			apiVO.setPageInfo(convJsonToObject(apiVO.getPageInfoObject().toString(), PageInfoVO.class));
		
		
		
		return apiVO;
	}
	


 

 

	

    public String EnCodeType(String sParam, String sType1, String sType2)
	{
    	try {
    		return new String(sParam.getBytes(sType1),sType2);
    	}
    	catch(Exception e) {
    		return sParam;
    	}
    }

    
    
    /**
     * Json(String)형태의 데이타를 VO에 매핑시킨다. vo에 없는 값은 무시한다. 
     * @param fieldName - name of file field
     * @param fieldValue - file name
     * @param type - mime type
     * @param fileInputStream - stream of bytes that get sent up
     */
    public <T> T convJsonToObject_(String jsonStr, Class<T> classOfT)
    {
    	
    	Gson gson = new Gson();
    	Object object = gson.fromJson(jsonStr, (Type)classOfT);
    	return Primitives.wrap(classOfT).cast(object);
    	
    }
    
    /**
     * Json(String)형태의 데이타를 VO에 매핑시킨다. vo에 없는 값은 오류를 발생시킨다.
     * @param fieldName - name of file field
     * @param fieldValue - file name
     * @param type - mime type
     * @param fileInputStream - stream of bytes that get sent up
     */
    public <T> T convJsonToObject(String jsonStr, Class<T> classOfT) throws JsonParseException, JsonMappingException, IOException
    {
    	ObjectMapper mapper = new ObjectMapper();



    	Object object = mapper.readValue(jsonStr, classOfT);
    	return (T) Primitives.wrap(classOfT).cast(object);
    	
    }
    
    
    /**
     * Json(String)형태의 데이타를 VO에 매핑시킨다. vo에 없는 값은 오류를 발생시킨다.
     * @param fieldName - name of file field
     * @param fieldValue - file name
     * @param type - mime type
     * @param fileInputStream - stream of bytes that get sent up
     * @throws JSONException 
     */
    public ArrayList convJsonToList(String jsonStr, Class cl) throws JsonParseException, JsonMappingException, IOException, JSONException
    {
		ArrayList list =  new ArrayList();
	
		JSONArray outObj = new JSONArray(jsonStr);
		for (int i=0; outObj != null && i<outObj.length(); i++) {
	
			if (!outObj.isNull(i)) {
				JSONObject jobj = outObj.getJSONObject(i);
				Object item = convJsonToObject(jobj.toString(), cl);
				list.add(item);
			}
		}
		return list;
    }

    
	


	public String getUrl(String sUrl) {
		
		StringBuffer outBuf = new StringBuffer();
		try {
			// URL클래스의 생성자로 주소를 넘겨준다.
			URL u = new URL( sUrl ); 
			// 해당 주소의 페이지로 접속을 하고, 단일 HTTP 접속을 하기위해 캐스트한다.
			HttpURLConnection  huc = (HttpURLConnection) u.openConnection();
			// POST방식으로 요청한다.( 기본값은 GET )
			huc.setRequestMethod("GET");
				// InputStream으로 서버로 부터 응답 헤더와 메시지를 읽어들이겠다는 옵션을 정의한다.
			huc.setDoInput(true);
				// OutputStream으로 POST 데이터를 넘겨주겠다는 옵션을 정의한다.
			huc.setDoOutput(true);
				// 요청 헤더를 정의한다.( 원래 Content-Length값을 넘겨주어야하는데 넘겨주지 않아도 되는것이 이상하다. )
			huc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
				// 새로운 OutputStream에 요청할 OutputStream을 넣는다.
			

	        BufferedReader inFile = new BufferedReader(new InputStreamReader(new BufferedInputStream(huc.getInputStream()), "UTF-8"));
	        String sLine = null;
	        while( (sLine = inFile.readLine()) != null )
	        {
	        	outBuf.append(sLine);
	        }
	        inFile.close();
	        huc.disconnect();
		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        return outBuf.toString();
	}
	
	
	


	public String getUrlPost(String sUrl, String param) {
		
		StringBuffer outBuf = new StringBuffer();
		try {
			// URL클래스의 생성자로 주소를 넘겨준다.
			URL u = new URL( sUrl ); 
			// 해당 주소의 페이지로 접속을 하고, 단일 HTTP 접속을 하기위해 캐스트한다.
			HttpURLConnection  huc = (HttpURLConnection) u.openConnection();
			// POST방식으로 요청한다.( 기본값은 GET )
			huc.setRequestMethod("POST");
				// InputStream으로 서버로 부터 응답 헤더와 메시지를 읽어들이겠다는 옵션을 정의한다.
			huc.setDoInput(true);
				// OutputStream으로 POST 데이터를 넘겨주겠다는 옵션을 정의한다.
			huc.setDoOutput(true);
				// 요청 헤더를 정의한다.( 원래 Content-Length값을 넘겨주어야하는데 넘겨주지 않아도 되는것이 이상하다. )
			huc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
				// 새로운 OutputStream에 요청할 OutputStream을 넣는다.
			
			OutputStream os = huc.getOutputStream();
			os.write(param.getBytes());
			os.flush();

			int status = huc.getResponseCode();
			if (status != 200) {
				return ""+status;
			}
			
	        BufferedReader inFile = new BufferedReader(new InputStreamReader(new BufferedInputStream(huc.getInputStream()), "UTF-8"));
	        String sLine = null;
	        while( (sLine = inFile.readLine()) != null )
	        {
	        	outBuf.append(sLine);
	        }
	        inFile.close();
	        huc.disconnect();
		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        return outBuf.toString();
	}
	//<!-- [BTGenEnd:class] -->
	
	public String generateXapiJsonFormat(EgovMap param) throws Exception {
		
			String uuid = UUID.randomUUID().toString();
			
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
			
			String name, id, verb, objectId = "";
			String enGbDesc, enUsDesc, koKrDesc = "";
			
			name = (String)param.get("name");
			id = (String)param.get("id");
			verb = (String)param.get("verb");
			objectId = (String)param.get("objectId");
			//objectId = webMainHost + objectId;
			enGbDesc = (String)param.get("enGbDesc");
			enUsDesc = (String)param.get("enUsDesc");
			koKrDesc = (String)param.get("koKrDesc");
			
			
			
			//actor Json
			JSONObject jsonObjectActor = new JSONObject();
	//		jsonObjectActor.put("name", name);
	//		jsonObjectActor.put("mbox", "");
	//		jsonObjectActor.put("id", id);
			// TODO: 아래 임시로 mailto... 
			jsonObjectActor.put("name", name);
			jsonObjectActor.put("id", id);
			jsonObjectActor.put("mbox", "mailto:"+id+"@adlnet.gov");
			
			//verbDisplay Json
			JSONObject jsonObjectVerbDisplay = new JSONObject();
			jsonObjectVerbDisplay.put("en-GB", verb);
			jsonObjectVerbDisplay.put("en-US", verb);
					
			//verb Json
			JSONObject jsonObjectVerb = new JSONObject();
			jsonObjectVerb.put("id", xapiVerbHost + "/" + verb);
			jsonObjectVerb.put("display", jsonObjectVerbDisplay);
			
			//objectDefinitionActor Json
			JSONObject jsonObjectDefinitionActor = new JSONObject();
			jsonObjectDefinitionActor.put("name", name);
			jsonObjectDefinitionActor.put("mbox", "");
			jsonObjectDefinitionActor.put("id", id);
			
			//objectDefinitionDescription Json
			JSONObject jsonObjectDefinitionDescription = new JSONObject();
			jsonObjectDefinitionDescription.put("en-GB", enGbDesc);
			jsonObjectDefinitionDescription.put("en-US", enUsDesc);
			jsonObjectDefinitionDescription.put("ko-KR", koKrDesc);
			
			
			//objectDefinition Json
			JSONObject jsonObjectDefinition = new JSONObject();
			jsonObjectDefinition.put("actor", jsonObjectDefinitionActor);
			jsonObjectDefinition.put("description", jsonObjectDefinitionDescription);
			
			
			//object Json
			JSONObject jsonObjectObject = new JSONObject();
			jsonObjectObject.put("objectType", "Activity");
			jsonObjectObject.put("id", objectId);
			jsonObjectObject.put("definition", jsonObjectDefinition);
					
			
			//main Json
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", uuid);
			jsonObject.put("timestamp", sdf.format(date));
			jsonObject.put("actor", jsonObjectActor);
			jsonObject.put("verb", jsonObjectVerb);
			jsonObject.put("object", jsonObjectObject);
			
			return jsonObject.toString();
		
		
	}
}

