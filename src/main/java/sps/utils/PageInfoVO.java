package sps.utils;


import java.io.Serializable;


import lombok.Getter;
import lombok.Setter;
import twitter4j.JSONArray;


/*
 * 페이지 VO 클래스
 * @author (주)윈솔텍
 * @since 2022-06-15
 * @version 1.0 
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2022-06-16  BizAn          최초 생성
 *
 * </pre>
 */


@Getter
@Setter
@SuppressWarnings("serial")
public class PageInfoVO  implements Serializable {

	/** 데이터 개수 */
	private int count = 0;
	
	/** 전체	데이터 개수 */
	private int totalCount = 0;
	
	/** 최대 페이지 개수 */
	private int maxPageCount = 0;
	
	/** 현재 페이지페이지 */
	private int currentPage = 1;

	/** 페이지당 개수 */
	private int pagePerCount = 10;
	
	private JSONArray getResultData = null;
	
}