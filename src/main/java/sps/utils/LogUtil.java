package sps.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import egovframework.com.cmm.service.EgovProperties;

/**
 * WstLogUtil 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.16
 * @version 1.0
 * @see 
 *
 * 자체 로그파일 생성
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.16  BizAn                  최초 생성
 * </pre>
 */

public class LogUtil {
	
	private static String logDir =  EgovProperties.getProperty("wcms.log.dir");
	
	public LogUtil () {
		  //Path relativePath = Paths.get("");
	      //curPath = relativePath.toAbsolutePath().toString();
		  //FileCtl.makeDir(curPath);
	}
	
    public static synchronized void writeDebugLog(String sLog)
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
        String sToday = formatter.format(cal.getTime()); //for log-time
        String sDate = formatter2.format(cal.getTime()); //for file-name

        String sFileName = logDir + sDate +"_Debug.Log";
        sLog = "[" + sToday + "]:" + sLog;

        try
        {
        	makeDir(logDir);
        	
            BufferedWriter bw = new BufferedWriter(new FileWriter(sFileName, true));
            bw.write(sLog);
            bw.newLine();
            bw.close();
        }
        catch(IOException ie)
        {
            ie.printStackTrace();
        }
    }
    
    public static synchronized void writeInfoLog(String sLog)
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
        String sToday = formatter.format(cal.getTime()); //for log-time
        String sDate = formatter2.format(cal.getTime()); //for file-name

        
        String sFileName = logDir + sDate +"_Info.Log";
        sLog = "[" + sToday + "]:" + sLog;

        try
        {
        	makeDir(logDir);
        	
            BufferedWriter bw = new BufferedWriter(new FileWriter(sFileName, true));
            bw.write(sLog);
            bw.newLine();
            bw.close();
        }
        catch(IOException ie)
        {
            ie.printStackTrace();
        }
    }
    
    public static synchronized void writeErrorLog(String sLog)
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
        String sToday = formatter.format(cal.getTime()); //for log-time
        String sDate = formatter2.format(cal.getTime()); //for file-name

        String sFileName = logDir + sDate +"_Error.Log";
        sLog = "[" + sToday + "]:" + sLog;

        try
        {
        	makeDir(logDir);
        	
            BufferedWriter bw = new BufferedWriter(new FileWriter(sFileName, true));
            bw.write(sLog);
            bw.newLine();
            bw.close();
        }
        catch(IOException ie)
        {
            ie.printStackTrace();
        }
    }
    
    public static void makeDir(String folderPath) {
		try{
		        File makeFolder = new File(folderPath);
		        if(!makeFolder.exists())   makeFolder.mkdir(); 
		 } catch (Exception e) {
			   	e.printStackTrace();
		 }
   }
}