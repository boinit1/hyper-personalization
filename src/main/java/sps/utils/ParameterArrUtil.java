package sps.utils;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

/**
 * ParameterArrUtil 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.15
 * @version 1.0
 * @see 
 *
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.16  BizAn                  최초 생성
 * </pre>
 */
public class ParameterArrUtil
{
  private Logger logger = Logger.getLogger(getClass());

  String dvSimb = "<;;>";

  public void mapValueLogger(HashMap map) {
    Iterator iterator = map.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = (Map.Entry)iterator.next();
      this.logger.info("hmap=======[[" + entry.getKey() + "]]===hmap value========[[" + entry.getValue() + "   ]]");
    }
  }

  public static void mapValueSetModel(Map<String, Object> map, ModelMap model) {
    Iterator iterator = map.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = (Map.Entry)iterator.next();
      model.put((String) entry.getKey(), entry.getValue());
    }
  }

  public HashMap mapSetting(Map parameterMap, HttpServletRequest request)
  {
    HashMap map = new HashMap();
    Iterator requestParameterIter = parameterMap.keySet().iterator();
    int tmp = 0;
    while (requestParameterIter.hasNext()) {
	    Object key = requestParameterIter.next();
	    String[] value = (String[])parameterMap.get(key);

      tmp = value.length;

      //if (tmp != 1) {
        for (int i = 0; i < value.length; i++) {
	      map.put(key, StringUtils.defaultIfEmpty(value[i], ""));

	    }
      //}
    }

    Enumeration enu = request.getParameterNames();
    String name = "";
    String arr_value = "";

    while (enu.hasMoreElements()) {
      name = (String)enu.nextElement();

      String[] values = request.getParameterValues(name);

      tmp = values.length;

      if (tmp != 1) {
        for (int i = 0; i < values.length; i++)
        {
          if (arr_value.equals(""))
            arr_value = values[i];
          else {
            arr_value = arr_value + this.dvSimb + values[i];
          }
        }

        map.put(name, arr_value);
        arr_value = "";
      }

    }

    return map;
  }

  public static Map getParamValue(HttpServletRequest request)
  {
    Map retMap = new HashMap();
    Enumeration paramenum = request.getParameterNames();
    while (paramenum.hasMoreElements()) {
      String key = (String)paramenum.nextElement();
      String[] paramValues = request.getParameterValues(key);
      if ((paramValues != null) && (paramValues.length > 1)) {
        retMap.put(key, paramValues);
      }
      else {
        retMap.put(key, paramValues[0]);
      }
    }
    return retMap;
  }

  public String[] getArrayParamValue(HashMap param, String paramName)
  {
    String[] paramValues = param.get(paramName).toString().split(this.dvSimb);

    return paramValues;
  }

  public String makeArrayParamValue(String[] arrValues)
  {
    String strParam = "0";

    for (int i = 1; i < arrValues.length; i++)
    {
      if (arrValues[i] != null)
      {
        if (strParam.equals(""))
          strParam = arrValues[i];
        else {
          strParam = strParam + this.dvSimb + arrValues[i];
        }
      }
    }

    return strParam;
  }

  public HashMap checkParam(HashMap param, HashMap chkMap)
  {
    String paramName = (String) chkMap.get("paramName");
    String paramType = (String) chkMap.get("paramType");
    String paramReq = (String) chkMap.get("paramReq");

    String[] arrParamNm = paramName.split(",");
    String[] arrParamReq = paramReq.split(",");
    String[] arrParamType = paramType.split(",");

    HashMap resParam = new HashMap();
    resParam.put("JSON", "Y");
    resParam.put("resCd", "0000");
    resParam.put("errDiv", "TRUE");
    resParam.put("errNM", "TRUE");

    for (int i = 0; i < arrParamNm.length; i++)
    {
      resParam = checkParamReqDefYN(arrParamNm[i], arrParamReq[i], param);
      System.out.println("arrParamNm[i] === " + arrParamNm[i]);
      if (resParam.get("resCd").toString().equals("0001"))
      {
        resParam.put("resMsg", "파마라메터 오류입니다.");
        break;
      }

      resParam = checkParamReqValYN(arrParamNm[i], arrParamReq[i], param);
      if (resParam.get("resCd").toString().equals("0001"))
      {
        resParam.put("resMsg", "파마라메터 오류입니다.");
        break;
      }

      resParam = checkParamValType(arrParamNm[i], arrParamType[i], param);
      if (resParam.get("resCd").toString().equals("0001"))
      {
        resParam.put("resMsg", "파마라메터 오류입니다.");
        break;
      }
    }

    return resParam;
  }

  public HashMap checkParamReqDefYN(String paramName, String paramReq, HashMap param)
  {
    HashMap resParam = new HashMap();
    resParam.put("resCd", "0000");
    resParam.put("errDiv", "TRUE");
    resParam.put("errNM", "TRUE");

    if (paramReq.equals("Y"))
    {
      if (param.get(paramName) == null)
      {
        resParam.put("resCd", "0001");
        resParam.put("errDiv", "paramName");
        resParam.put("errNM", paramName);
      }
    }

    return resParam;
  }

  public HashMap checkParamReqValYN(String paramName, String paramReq, HashMap param)
  {
    HashMap resParam = new HashMap();
    resParam.put("resCd", "0000");
    resParam.put("errDiv", "TRUE");
    resParam.put("errNM", "TRUE");

    if (paramReq.equals("Y"))
    {
      if (param.get(paramName).toString().equals(""))
      {
        resParam.put("resCd", "0001");
        resParam.put("errDiv", "VALUE");
        resParam.put("errNM", paramName);
      }
    }

    return resParam;
  }

  public HashMap checkParamValType(String paramName, String paramType, HashMap param)
  {
    HashMap resParam = new HashMap();
    resParam.put("resCd", "0000");
    resParam.put("errDiv", "TRUE");
    resParam.put("errNM", "TRUE");

    String paramVal = "";
    if (param.get(paramName) != null) paramVal = param.get(paramName).toString();
    String resCd = "0000";

    System.out.println("checkParamValType paramVal === " + paramVal);

    if (paramType.equals("NUM"))
    {
      if (!isValidNum(paramVal)) resCd = "0001";
    }
    else if (paramType.equals("EML"))
    {
      if (!isValidEmail(paramVal)) resCd = "0001";
    }
    else if (paramType.equals("PH"))
    {
      if (!isValidPhNum(paramVal)) resCd = "0001";
    }
    else if (paramType.equals("CP"))
    {
      if (!isValidCellpNum(paramVal)) resCd = "0001";

    }

    if (resCd.equals("0001"))
    {
      resParam.put("resCd", "0001");
      resParam.put("errDiv", "paramType");
      resParam.put("errNM", paramName + "(" + paramType + ")");
    }

    System.out.println("checkParamValType resParam === " + resParam);

    return resParam;
  }

  public String chkParam(String paramName, HashMap param)
  {
    String strParam = paramName;
    String[] arrParam = paramName.split(",");

    for (int i = 0; i < arrParam.length; i++)
    {
      if (param.get(arrParam[i]) == null)
      {
        strParam = arrParam[i];
        break;
      }

      if (param.get(arrParam[i]).toString().equals(""))
      {
        strParam = arrParam[i];
        break;
      }

    }

    return strParam;
  }

  public String getINvalueStr(ArrayList<HashMap> resArr, String INName)
  {
    String tmpCONT_ID = "";

    for (int i = 0; i < resArr.size(); i++)
    {
      if (i == 0)
        tmpCONT_ID = ((HashMap)resArr.get(i)).get(INName).toString();
      else {
        tmpCONT_ID = tmpCONT_ID + "','" + ((HashMap)resArr.get(i)).get(INName).toString();
      }
    }
    return tmpCONT_ID;
  }

  public boolean isValidNum(String str)
  {
    if (str.equals(""))
    {
      return false;
    }

    for (int i = 0; i < str.length(); i++) {
      char check = str.charAt(i);
      if ((check < '0') || (check > ':'))
      {
        return false;
      }
    }

    return true;
  }

  public boolean isValidEmail(String email)
  {
    boolean err = false;
    String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
    Pattern p = Pattern.compile(regex);
    Matcher m = p.matcher(email);
    if (m.matches()) {
      err = true;
    }
    return err;
  }

  public boolean isValidPhNum(String phnum) {
    boolean err = false;
    String regex = "^\\d{2,3}-\\d{3,4}-\\d{4}$";
    Pattern p = Pattern.compile(regex);
    Matcher m = p.matcher(phnum);
    if (m.matches()) {
      err = true;
    }
    return err;
  }

  public boolean isValidCellpNum(String phnum)
  {
    boolean err = false;
    String regex = "^01(?:0|1[6-9])-(?:\\d{3}|\\d{4})-\\d{4}$";
    Pattern p = Pattern.compile(regex);
    Matcher m = p.matcher(phnum);
    if (m.matches()) {
      err = true;
    }
    return err;
  }

  public String rmvFLSplitSim(String str)
  {
    if (str.indexOf(this.dvSimb) > -1) str = str.substring(this.dvSimb.length(), str.toString().length() - this.dvSimb.length());

    return str;
  }
}