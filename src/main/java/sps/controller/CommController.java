package sps.controller;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.cmmn.trace.LeaveaTrace;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

import sps.comm.PagerVO;
import sps.login.LoginVO;
import sps.service.SpsService;
import sps.service.TempFinalProblem;
import sps.service.TempProblem;
import sps.service.XapiService;
import sps.utils.ApiService;
import sps.utils.ApiVO;
import sps.utils.JsonMessage;
import sps.utils.ParameterArrUtil;
import sps.utils.StringUtil;
import sps.utils.TypeUtil;
import sps.utils.UserDetailsHelper;

/**
 * CommController 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.15
 * @version 1.0
 * @see
 *
 * 
 * 
 *      <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.15  BizAn                  최초 생성
 *      </pre>
 */

@Controller
public class CommController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommController.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource egovMessageSource;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "ApiService")
	private ApiService apiService;

	/** TRACE */
	@Resource(name = "leaveaTrace")
	LeaveaTrace leaveaTrace;

	@Resource(name = "SpsService")
	private SpsService spsService;
	
	@Resource(name = "XapiService")
	private XapiService xapiService;

	private String resJson = "";
	private HashMap<String, Object> resJsonHash = null;
	private String resCd = "0000";

	@RequestMapping("/comm/getChapterList.do")
	@ResponseBody
	public HashMap<String, Object> getChapterList(@RequestBody HashMap<String, Object> map, HttpServletRequest request,
			ModelMap model) {

		resJsonHash = null;

		try {
			EgovMap paramMap = new EgovMap();
			map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));

			List<EgovMap> chapterList = null;

			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));

			chapterList = (List<EgovMap>) spsService.getChapterList(paramMap);
			if (chapterList == null) {
				resJsonHash.put("resultData", "0");
				return resJsonHash;
			}

			if (resJsonHash == null) {
				resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
			}

			if (chapterList.size() == 0)
				resJsonHash.put("resultData", "0");
			else {
				resJsonHash.put("resultData", chapterList);
			}

			/*
			 * //xapi 호출///////////////////////////////////////////////////// //학기 선택
			 * LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser(); String userId =
			 * loginVO.getUserId(); String userNm = loginVO.getUserNm(); EgovMap
			 * xapiParamMap = new EgovMap(); xapiParamMap.put("id", userId);
			 * xapiParamMap.put("name", userNm); xapiParamMap.put("verb", "selected");
			 * 
			 * if (paramMap.get("lessonCode") == null) xapiParamMap.put("objectId",
			 * paramMap.get("learnCode").toString().substring(0,2)); else
			 * xapiParamMap.put("objectId", paramMap.get("lessonCode").toString());
			 * 
			 * xapiParamMap.put("enGbDesc", "select period"); xapiParamMap.put("enUsDesc",
			 * "select period");
			 * 
			 * if (paramMap.get("bookCode").toString().substring(3,4).equals("1"))
			 * xapiParamMap.put("koKrDesc", "학기선택(1)"); else xapiParamMap.put("koKrDesc",
			 * "학기선택(2)");
			 * 
			 * apiService.callXApi(xapiParamMap);
			 * //////////////////////////////////////////////////////////////
			 */

		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return resJsonHash;
	}

	@RequestMapping("/comm/getLearnList.do")
	@ResponseBody
	public HashMap<String, Object> getLearnList(@RequestBody HashMap<String, Object> map, HttpServletRequest request,
			ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();

		ApiVO apiVO = null;

		resJsonHash = null;
		EgovMap paramMap = new EgovMap();
		map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));

		paramMap.put("memberId", userId);

		List<EgovMap> learnList = null;

		try {

			if (map.get("chapterName2") != null) {
				String chapterName = TypeUtil.toStr(map.get("chapterName2"));
				chapterName = chapterName.replace("[", "");
				chapterName = chapterName.replace("]", "");
				paramMap.put("chapterName", chapterName);

				EgovMap chapterInfo = spsService.getChapterInfo(paramMap);
				if (chapterInfo != null)
					paramMap.put("chapterCode", chapterInfo.get("chapterCode"));
			}

			if (map.get("chapterName3") != null) {
				String chapterMiddle = TypeUtil.toStr(map.get("chapterName3"));
				chapterMiddle = chapterMiddle.substring(chapterMiddle.length() - 1);
				paramMap.put("chapterMiddle", chapterMiddle);

				EgovMap chapterInfo = spsService.getChapterInfo(paramMap);
				if (chapterInfo != null)
					paramMap.put("chapterCode", chapterInfo.get("chapterCode"));
			}

			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
			// learnList = (List<EgovMap>) spsService.getLearnList(paramMap);

			apiVO = spsService.getLearnListApiVO(paramMap);

			if (apiVO == null) {
				resJsonHash.put("resultData", "0");
				return resJsonHash;
			}
			;
			learnList = apiVO.getListEgovMap();

			if (resJsonHash == null) {
				resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
			}

			LOGGER.debug("learnList:" + learnList);

			if (learnList.size() == 0)
				resJsonHash.put("resultData", "0");
			else
				resJsonHash.put("resultData", learnList);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return resJsonHash;
	}

	@RequestMapping("/comm/getContextChartList.do")
	@ResponseBody
	public HashMap<String, Object> getContextChartList(@RequestBody HashMap<String, Object> map,
			HttpServletRequest request, ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();

		resJsonHash = null;
		EgovMap paramMap = new EgovMap();
		map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));

		paramMap.put("userId", userId);

		List<EgovMap> resultList = null;

		try {
			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
			resultList = (List<EgovMap>) spsService.getContextChartList(paramMap);

			if (resultList == null) {
				resJsonHash.put("resultData", "0");
				return resJsonHash;
			}

			if (resultList.size() == 0)
				resJsonHash.put("resultData", "0");
			else {
				resJsonHash.put("resultData", resultList);
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return resJsonHash;
	}

	@RequestMapping("/comm/getActivityChartList.do")
	@ResponseBody
	public HashMap<String, Object> getActivityChartList(@RequestBody HashMap<String, Object> map,
			HttpServletRequest request, ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();

		resJsonHash = null;
		EgovMap paramMap = new EgovMap();
		map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));
		paramMap.put("userId", userId);

		List<EgovMap> resultList = null;

		boolean isZeroData = false;
		try {
			ApiVO apiVO = null;
			EgovMap tempParamMap = new EgovMap();
			tempParamMap.put("memberId", userId);
			tempParamMap.put("chapterCode", paramMap.get("chapterCode"));
			apiVO = spsService.getLearnListApiVO(tempParamMap);
			List<EgovMap> learnList = null;
			learnList = apiVO.getListEgovMap();
			
			if(learnList != null && learnList.size()>TypeUtil.toInt(paramMap.get("lesson"))) {
				EgovMap temp = learnList.get(TypeUtil.toInt(paramMap.get("lesson"))-1 % learnList.size());
				if(TypeUtil.toInt(temp.get("jumsu")) <=0 ) {
					isZeroData = true;
				}
				
			}
		} catch (Exception e) {
			// 그냥 넘어감
		}
		
		
		String chapterCode = "";
		if ("0251D".equals((String) paramMap.get("bookCode")))
			chapterCode = "0251D000100";
		else if ("0252D".equals((String) paramMap.get("bookCode")))
			chapterCode = "0252D000100";
		else if ("1051D".equals((String) paramMap.get("bookCode")))
			chapterCode = "1051D000000";
		else
			chapterCode = "1052D000000";

		try {

			
			
			
			
			
			EgovMap chapterInfo = spsService.getChapterInfo(paramMap);

			if (chapterInfo != null)
				chapterCode = (String) chapterInfo.get("chapterCode");

			paramMap.put("chapterCode", chapterCode);

			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
			resultList = (List<EgovMap>) spsService.getActivityChartList(paramMap);

			if(isZeroData) {
				for(EgovMap result : resultList) {
					result.put("resultValue", 0);
				}
			}
			
			
			if (resJsonHash == null) {
				resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
			}

			if (resultList.size() == 0)
				resJsonHash.put("resultData", "0");
			else
				resJsonHash.put("resultData", resultList);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return resJsonHash;
	}

	@RequestMapping("/comm/getLearnInfo.do")
	@ResponseBody
	public HashMap<String, Object> getTotPlayTime(@RequestBody HashMap<String, Object> map, HttpServletRequest request,
			ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();

		resJsonHash = null;
		EgovMap paramMap = new EgovMap();
		map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));

		paramMap.put("userId", userId);
		paramMap.put("memberId", userId);

		ApiVO apiVO = null;
		EgovMap learnInfo = new EgovMap();

		long totPlaySeconds = 0;

		try {
			long totPlayHour = 0;
			long totPlayMin = 0;
			long totPlayCnt = 0;

			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));

			/*
			 * EgovMap resultMap = (EgovMap) spsService.getTotPlayTime(paramMap);
			 * 
			 * if (resultMap != null) { totPlaySeconds =
			 * TypeUtil.toLong(resultMap.get("totPlayTime")); totPlayCnt =
			 * TypeUtil.toLong(resultMap.get("totPlayCnt")); }
			 * 
			 * totPlayHour = totPlaySeconds/(60*60); totPlayMin =
			 * (totPlaySeconds%(60*60))/60;;
			 * 
			 * learnInfo.put("totPlayHour", totPlayHour); learnInfo.put("totPlayMin",
			 * totPlayMin); learnInfo.put("totPlayCnt", totPlayCnt);
			 * 
			 * resultMap = (EgovMap) spsService.getTotProblemAnswer(paramMap);
			 * 
			 * if (resultMap == null) { learnInfo.put("answerProblemCnt", 0);
			 * learnInfo.put("totProblemCnt", 0); }else{ learnInfo.put("answerProblemCnt",
			 * resultMap.get("answerProblemCnt")); learnInfo.put("totProblemCnt",
			 * resultMap.get("totProblemCnt")); }
			 */

			apiVO = spsService.getLearnListApiVO(paramMap);

			totPlaySeconds = TypeUtil.toLong(apiVO.getEgovMap().get("memberProblemTime"));
			totPlayCnt = TypeUtil.toLong(apiVO.getEgovMap().get("memberLectureCount"));

			totPlayHour = totPlaySeconds / (60 * 60);
			totPlayMin = (totPlaySeconds % (60 * 60)) / 60;

			learnInfo.put("totPlayHour", totPlayHour);
			learnInfo.put("totPlayMin", totPlayMin);
			learnInfo.put("totPlayCnt", totPlayCnt);
			learnInfo.put("answerProblemCnt", TypeUtil.toLong(apiVO.getEgovMap().get("memberProblemCount")));
			learnInfo.put("totProblemCnt", TypeUtil.toLong(apiVO.getEgovMap().get("problemTotalCount")));

			LOGGER.debug("learnInfo:" + learnInfo);

			if (resJsonHash == null) {
				resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
			}

			resJsonHash.put("resultData", learnInfo);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return resJsonHash;
	}

	// LO_PLAY_TIME 입력
	@RequestMapping("/comm/insertLoPlayTime.do")
	@ResponseBody
	public HashMap<String, Object> insertLoPlayTime(@RequestBody HashMap<String, Object> map,
			HttpServletRequest request, ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();

		resJsonHash = null;
		try {
			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));

			if (userId == null) {
				resCd = "0001";
				resJsonHash = JsonMessage.getJsonMessage(resCd,
						egovMessageSource.getMessage("msg.error.nosession").toString());
				return resJsonHash;
			}

			EgovMap paramMap = new EgovMap();
			map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));

			paramMap.put("userId", userId);

			spsService.insertLoPlayTime(paramMap);

			paramMap.put("resultCode", 1);

			if (resJsonHash == null) {
				resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
			}

			resJsonHash.put("resultData", paramMap);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return resJsonHash;
	}

	// PB_PROBLEM_SOLVE 입력
	@RequestMapping("/comm/insertPbProblemSolve.do")
	@ResponseBody
	public HashMap<String, Object> insertPbProblemSolve(@RequestBody HashMap<String, Object> map,
			HttpServletRequest request, ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();

		resJsonHash = null;
		try {
			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));

			if (userId == null) {
				resCd = "0001";
				resJsonHash = JsonMessage.getJsonMessage(resCd,
						egovMessageSource.getMessage("msg.error.nosession").toString());
				return resJsonHash;
			}

			EgovMap paramMap = new EgovMap();
			map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));

			paramMap.put("memId", userId);

			// 예시 입력
			/*
			 * paramMap.clear(); paramMap.put("lessonCode", "02"); paramMap.put("problemId",
			 * "150302"); paramMap.put("difficulty", 'L'); paramMap.put("importance", 3);
			 * paramMap.put("problem", "계산해 보세요."); paramMap.put("answer", "55");
			 * paramMap.put("funArea", ""); paramMap.put("recArea", "");
			 * paramMap.put("conArea", ""); paramMap.put("solveValue", "55");
			 * paramMap.put("solveResult", 'O');
			 */

			// 문제풀이 종료시간
			Date date = new Date();
			SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String endTmstmp = sdformat.format(date);
			paramMap.put("endTmstmp", endTmstmp);

			// solveTime(문제푼시간 단위 초) 계산 ((endTmstmp - startTmstmp)/1000)
			Date format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String) paramMap.get("startTmstmp"));
			Date format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String) paramMap.get("endTmstmp"));
			long solveTime = (format2.getTime() - format1.getTime()) / 1000; // 초 차이
			paramMap.put("solveTime", solveTime);

			paramMap.put("hintYn", 'N');
			///////////////////////////////////////////////////////////////

			spsService.insertPbProblemSolve(paramMap);

			paramMap.put("resultCode", 1);
			resJsonHash.put("resultData", paramMap);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return resJsonHash;
	}

	// PB_PROBLEM_ANSWER 입력(사용하지 않음)
	@RequestMapping("/comm/insertPbProblemAnswer.do")
	@ResponseBody
	public HashMap<String, Object> insertPbProblemAnswer(@RequestBody HashMap<String, Object> map,
			HttpServletRequest request, ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();

		resJsonHash = null;
		try {
			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));

			if (userId == null) {
				resCd = "0001";
				resJsonHash = JsonMessage.getJsonMessage(resCd,
						egovMessageSource.getMessage("msg.error.nosession").toString());
				return resJsonHash;
			}

			EgovMap paramMap = new EgovMap();
			map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));

			paramMap.put("userId", userId);

			spsService.insertPbProblemAnswer(paramMap);

			paramMap.put("resultCode", 1);
			resJsonHash.put("resultData", paramMap);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return resJsonHash;
	}

	// 동영상 PLAY XAPI 입력
	@RequestMapping("/comm/insertXApi.do")
	@ResponseBody
	public HashMap<String, Object> insertXApi(@RequestBody HashMap<String, Object> map, HttpServletRequest request,
			ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();
		String userNm = loginVO.getUserNm();

		resJsonHash = null;
		try {
			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));

			if (userId == null) {
				resCd = "0001";
				resJsonHash = JsonMessage.getJsonMessage(resCd,
						egovMessageSource.getMessage("msg.error.nosession").toString());
				return resJsonHash;
			}

			EgovMap xapiParamMap = new EgovMap();
			map.forEach((key, value) -> xapiParamMap.put(key, StringUtil.isNullToString(value)));

			xapiParamMap.put("id", userId);
			xapiParamMap.put("name", userNm);
			// xapiParamMap.put("verb", "selected");
			// xapiParamMap.put("objectId", "");
			// xapiParamMap.put("enGbDesc", "select period");
			// xapiParamMap.put("enUsDesc", "select period");
			// xapiParamMap.put("koKrDesc", "학기선택(1)");

			apiService.callXApi(xapiParamMap);

			xapiParamMap.put("resultCode", 1);

			if (resJsonHash == null) {
				resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));
				resJsonHash.put("resultData", xapiParamMap);
			} else
				resJsonHash.put("resultData", xapiParamMap);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return resJsonHash;
	}

	// 동영상 PLAY XAPI 입력
	@RequestMapping("/comm/insertXApi2.do")
	public void insertXApi2(HttpServletRequest request) {

		System.out.println(request.getContentType());
	}
	
	@RequestMapping("/comm/sendXapi.do")
	@ResponseBody
	public String sendApi(@RequestBody HashMap<String, Object> map, HttpServletRequest request, ModelMap model) {
		System.out.println(">>>sendApi" + map.toString());
		return xapiService.send(map);
	}
	
	 @RequestMapping(value = "/comm/XapiDashboard.do")
   	public String mathDashboardView2(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		 model.addAttribute("url", request.getParameter("url"));
		 return "cmm/XapiDashboard";
   	}
	 
	 @RequestMapping(value = "/comm/SkDashboard.do")
   	public String SkDashboard(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		 LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		 String userId = loginVO.getUserId();
		 String mailto = "mailto:" + userId + "@sogang4edtech.com";
		 String url = "https://test.sogang4edtech.com/lrs/admin/#/learner4student/" + URLDecoder.decode(mailto, "UTF-8");
		 
		 model.addAttribute("url", url);
		 return "cmm/SkDashboard";
   	}
	 
	 @RequestMapping(value = "/comm/finalMath.do")
   	public String finalMath(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		 
		 /* 세션 정보 */
  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		String problemNo = (String)request.getParameter("problemNo");
  		String problemId = TempFinalProblem.tempMath.get(problemNo);
  		
		EgovMap paramMap = new EgovMap();
	
		commandMap.put("bookCode", "0251E");
		commandMap.put("chapterCode", "0251E000100");
		commandMap.put("rowCnt", "1");
		commandMap.put("nowPage", "1");
		
		paramMap.put("memberId", userId);
		paramMap.put("chapterCode", commandMap.get("chapterCode"));		
		
    	List<EgovMap> problemList = null;
		
    	EgovMap testParam = new EgovMap();
		testParam.put("memId", userId);
		testParam.put("subject", "MATH");
		testParam.put("problemNo", problemNo);
		List<EgovMap> test = (List<EgovMap>)spsService.getFinalTest(testParam);
		if(test != null && test.size() > 0) {
			commandMap.put("userAnswerStr", test.get(0).get("answer"));
			commandMap.put("problemId", problemId);
			return finalMath2(request, response, commandMap, model);
		}
		
		testParam.put("problemNo", null);
		test = (List<EgovMap>)spsService.getFinalTest(testParam);
		String solvedProblemNo = "";
		if(test != null && test.size() > 0) {
			for(EgovMap item : test) {
				solvedProblemNo += item.get("problemNo")+",";
			}
		}		
		if(solvedProblemNo.length() > 0) {
			solvedProblemNo = solvedProblemNo.substring(0, solvedProblemNo.length()-1);
		}		
		model.put("solvedProblemNo", solvedProblemNo);
    	
		try {
			paramMap = new EgovMap();

			paramMap.put("lessonCode", "02");
			paramMap.put("rowCnt", 1);
			paramMap.put("nowPage", 1);
			paramMap.put("sort", "PROBLEM_ID");
			paramMap.put("order", "ASC");

			paramMap.put("problemId", problemId);
			problemList = (List<EgovMap>) spsService.getProblemList2(paramMap);
			
			if(problemList.size() <= 0) {
				paramMap.remove("includeLearnCodeList");
				problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
				if(problemList.size() <= 0) {
					paramMap.remove("chapterCode");
					problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
				}
			}
			
			int totalCnt = 0;
			if(problemList.size() > 0) {
		    	EgovMap problemInfo = problemList.get(0);
		    	
		    	problemInfo.put("problemNo", problemNo);
		    	
		    	if (problemInfo.get("sentence") != null) {
			    	String sentence = (String)problemInfo.get("sentence");
			    	sentence = sentence.replace(".swf", ".mp3");
			    	sentence = sentence.replace(".JPG", ".jpg");
			    	sentence = sentence.replace(".GIF", ".gif");
			    	sentence = sentence.replace(".PNG", ".png");
			    	problemInfo.put("sentence", sentence);
		    	}
		    	problemInfo.put("problemNum", commandMap.get("nowPage"));
		    	problemInfo.put("filePath", "/problem_new/");
		    	problemInfo.put("lesson", "02");
				//if("Y".equals(problemInfo.get("exampleInd"))) {
					paramMap.putIfAbsent("problemId", problemInfo.get("problemId"));
					paramMap.putIfAbsent("subNo", problemInfo.get("subNo"));
					problemInfo.put("example", spsService.getExampleList(paramMap));
				//}
				//if (commandMap.get("userAnswerStr") != null)
				//	problemInfo.put("userAnswer", ((String)commandMap.get("userAnswerStr")).split("[|]"));//사용자 입력 답안
				problemInfo.put("bookCode", commandMap.get("bookCode"));
				model.put("result", problemInfo);
				totalCnt = Long.valueOf(String.valueOf(problemInfo.get("totalCnt"))).intValue();
			}
			
			PagerVO pager = new PagerVO(1, 100, 1, 1);
			model.put("pager", pager);
			model.put("totalCnt", 200);
  			//문제풀이 시작시간 
  			Date date = new Date();
  	        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  	        String startTmstmp = sdformat.format(date);
  	        model.put("startTmstmp", startTmstmp);
			ParameterArrUtil.mapValueSetModel(commandMap, model);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "cmm/finalMath";
   	}
	 
	@RequestMapping(value = "/comm/finalMath2.do")
	public String finalMath2(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		
		 /* 세션 정보 */
  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		String problemNo = (String)request.getParameter("problemNo");
  		
		EgovMap testParam = new EgovMap();
		testParam.put("memId", userId);
		testParam.put("subject", "MATH");
		List<EgovMap> test = (List<EgovMap>)spsService.getFinalTest(testParam);
		String solvedProblemNo = "";
		if(test != null && test.size() > 0) {
			for(EgovMap item : test) {
				solvedProblemNo += item.get("problemNo")+",";
			}
		}		
		if(solvedProblemNo.length() > 0) {
			solvedProblemNo = solvedProblemNo.substring(0, solvedProblemNo.length()-1);
		}		
		model.put("solvedProblemNo", solvedProblemNo);
		
		List<EgovMap> problemList = null;
		
		try {
			EgovMap paramMap = new EgovMap();

			/*paramMap.put("lessonCode", "02");
			paramMap.put("rowCnt", commandMap.get("rowCnt"));
			paramMap.put("nowPage", commandMap.get("nowPage"));
			paramMap.put("sort", "PROBLEM_ID");
			paramMap.put("order", "ASC");*/
			paramMap.put("sortOnly", "Y");
			//테스트용
			paramMap.put("problemId", commandMap.get("problemId"));
			paramMap.put("subNo", commandMap.get("subNo"));
			
//			problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
			problemList = (List<EgovMap>) spsService.getProblemList2(paramMap);
			int totalCnt = 0;
			if(problemList.size() > 0) {
		    	EgovMap problemInfo = problemList.get(0);
		    	
		    	problemInfo.put("problemNo", request.getParameter("problemNo"));
		    	
		    	if (problemInfo.get("sentence") != null) {
			    	String sentence = (String)problemInfo.get("sentence");
			    	sentence = sentence.replace(".swf", ".mp3");
			    	sentence = sentence.replace(".JPG", ".jpg");
			    	sentence = sentence.replace(".GIF", ".gif");
			    	sentence = sentence.replace(".PNG", ".png");
			    	problemInfo.put("sentence", sentence);
		    	}
		    	problemInfo.put("problemNum", commandMap.get("nowPage"));
		    	problemInfo.put("filePath", "/problem_new/");
		    	problemInfo.put("lesson", "02");
				//if("Y".equals(problemInfo.get("exampleInd"))) {
					paramMap.putIfAbsent("problemId", problemInfo.get("problemId"));
					paramMap.putIfAbsent("subNo", problemInfo.get("subNo"));
					problemInfo.put("example", spsService.getExampleList(paramMap));
				//}
				if (commandMap.get("userAnswerStr") != null)
					problemInfo.put("userAnswer", ((String)commandMap.get("userAnswerStr")).split("[|]"));//사용자 입력 답안
				problemInfo.put("bookCode", commandMap.get("bookCode"));
				model.put("result", problemInfo);
				totalCnt = Long.valueOf(String.valueOf(problemInfo.get("totalCnt"))).intValue();
			}
			
			PagerVO pager = new PagerVO(Integer.parseInt((String)commandMap.get("rowCnt")), 100, totalCnt, Integer.parseInt((String)commandMap.get("nowPage")));
			model.put("pager", pager);
			model.put("totalCnt", 200);
			ParameterArrUtil.mapValueSetModel(commandMap, model);
			
			LOGGER.debug("result:" + model.get("result"));
			LOGGER.debug("rowCnt:" + model.get("rowCnt"));
			LOGGER.debug("nowPage:" + model.get("nowPage"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "cmm/finalMath"; 
	}
	
	
	 
	@RequestMapping(value = "/comm/finalEnglish.do")
   	public String finalEnglish(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		 
		 /* 세션 정보 */
  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		String problemNo = (String)request.getParameter("problemNo");
  		String problemId = TempFinalProblem.tempEnglish.get(problemNo);
  		
		EgovMap paramMap = new EgovMap();
	
		commandMap.put("bookCode", "0251E");
		commandMap.put("chapterCode", "0251E000100");
		commandMap.put("rowCnt", "1");
		commandMap.put("nowPage", "1");
		
		paramMap.put("memberId", userId);
		paramMap.put("chapterCode", commandMap.get("chapterCode"));		
		
    	List<EgovMap> problemList = null;
		
    	EgovMap testParam = new EgovMap();
		testParam.put("memId", userId);
		testParam.put("subject", "ENGLISH");
		testParam.put("problemNo", problemNo);
		List<EgovMap> test = (List<EgovMap>)spsService.getFinalTest(testParam);
		if(test != null && test.size() > 0) {
			commandMap.put("userAnswerStr", test.get(0).get("answer"));
			commandMap.put("problemId", problemId);
			return finalEnglish2(request, response, commandMap, model);
		}
		
		testParam.put("problemNo", null);
		test = (List<EgovMap>)spsService.getFinalTest(testParam);
		String solvedProblemNo = "";
		if(test != null && test.size() > 0) {
			for(EgovMap item : test) {
				solvedProblemNo += item.get("problemNo")+",";
			}
		}		
		if(solvedProblemNo.length() > 0) {
			solvedProblemNo = solvedProblemNo.substring(0, solvedProblemNo.length()-1);
		}		
		model.put("solvedProblemNo", solvedProblemNo);
    	
		try {
			paramMap = new EgovMap();

			paramMap.put("lessonCode", "02");
			paramMap.put("rowCnt", 1);
			paramMap.put("nowPage", 1);
			paramMap.put("sort", "PROBLEM_ID");
			paramMap.put("order", "ASC");

			paramMap.put("problemId", problemId);
			problemList = (List<EgovMap>) spsService.getProblemList2(paramMap);
			
			if(problemList.size() <= 0) {
				paramMap.remove("includeLearnCodeList");
				problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
				if(problemList.size() <= 0) {
					paramMap.remove("chapterCode");
					problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
				}
			}
			
			int totalCnt = 0;
			if(problemList.size() > 0) {
		    	EgovMap problemInfo = problemList.get(0);
		    	
		    	problemInfo.put("problemNo", problemNo);
		    	
		    	if (problemInfo.get("sentence") != null) {
			    	String sentence = (String)problemInfo.get("sentence");
			    	sentence = sentence.replace(".swf", ".mp3");
			    	sentence = sentence.replace(".JPG", ".jpg");
			    	sentence = sentence.replace(".GIF", ".gif");
			    	sentence = sentence.replace(".PNG", ".png");
			    	problemInfo.put("sentence", sentence);
		    	}
		    	problemInfo.put("problemNum", commandMap.get("nowPage"));
		    	problemInfo.put("filePath", "/problem_new/");
		    	problemInfo.put("lesson", "02");
				//if("Y".equals(problemInfo.get("exampleInd"))) {
					paramMap.putIfAbsent("problemId", problemInfo.get("problemId"));
					paramMap.putIfAbsent("subNo", problemInfo.get("subNo"));
					problemInfo.put("example", spsService.getExampleList(paramMap));
				//}
				//if (commandMap.get("userAnswerStr") != null)
				//	problemInfo.put("userAnswer", ((String)commandMap.get("userAnswerStr")).split("[|]"));//사용자 입력 답안
				problemInfo.put("bookCode", commandMap.get("bookCode"));
				model.put("result", problemInfo);
				totalCnt = Long.valueOf(String.valueOf(problemInfo.get("totalCnt"))).intValue();
			}
			
			PagerVO pager = new PagerVO(1, 100, 1, 1);
			model.put("pager", pager);
			model.put("totalCnt", 200);
  			//문제풀이 시작시간 
  			Date date = new Date();
  	        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  	        String startTmstmp = sdformat.format(date);
  	        model.put("startTmstmp", startTmstmp);
			ParameterArrUtil.mapValueSetModel(commandMap, model);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "cmm/finalEnglish";
   	}
	
	@RequestMapping(value = "/comm/finalEnglish2.do")
   	public String finalEnglish2(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		 
		 /* 세션 정보 */
  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		String problemNo = (String)request.getParameter("problemNo");
  		
		EgovMap testParam = new EgovMap();
		testParam.put("memId", userId);
		testParam.put("subject", "ENGLISH");
		List<EgovMap> test = (List<EgovMap>)spsService.getFinalTest(testParam);
		String solvedProblemNo = "";
		if(test != null && test.size() > 0) {
			for(EgovMap item : test) {
				solvedProblemNo += item.get("problemNo")+",";
			}
		}		
		if(solvedProblemNo.length() > 0) {
			solvedProblemNo = solvedProblemNo.substring(0, solvedProblemNo.length()-1);
		}		
		model.put("solvedProblemNo", solvedProblemNo);
		
		List<EgovMap> problemList = null;
		
		try {
			EgovMap paramMap = new EgovMap();

			/*paramMap.put("lessonCode", "02");
			paramMap.put("rowCnt", commandMap.get("rowCnt"));
			paramMap.put("nowPage", commandMap.get("nowPage"));
			paramMap.put("sort", "PROBLEM_ID");
			paramMap.put("order", "ASC");*/
			paramMap.put("sortOnly", "Y");
			//테스트용
			paramMap.put("problemId", commandMap.get("problemId"));
			paramMap.put("subNo", commandMap.get("subNo"));
			
//			problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
			problemList = (List<EgovMap>) spsService.getProblemList2(paramMap);
			int totalCnt = 0;
			if(problemList.size() > 0) {
		    	EgovMap problemInfo = problemList.get(0);
		    	
		    	problemInfo.put("problemNo", request.getParameter("problemNo"));
		    	
		    	if (problemInfo.get("sentence") != null) {
			    	String sentence = (String)problemInfo.get("sentence");
			    	sentence = sentence.replace(".swf", ".mp3");
			    	sentence = sentence.replace(".JPG", ".jpg");
			    	sentence = sentence.replace(".GIF", ".gif");
			    	sentence = sentence.replace(".PNG", ".png");
			    	problemInfo.put("sentence", sentence);
		    	}
		    	problemInfo.put("problemNum", commandMap.get("nowPage"));
		    	problemInfo.put("filePath", "/problem_new/");
		    	problemInfo.put("lesson", "02");
				//if("Y".equals(problemInfo.get("exampleInd"))) {
					paramMap.putIfAbsent("problemId", problemInfo.get("problemId"));
					paramMap.putIfAbsent("subNo", problemInfo.get("subNo"));
					problemInfo.put("example", spsService.getExampleList(paramMap));
				//}
				if (commandMap.get("userAnswerStr") != null)
					problemInfo.put("userAnswer", ((String)commandMap.get("userAnswerStr")).split("[|]"));//사용자 입력 답안
				problemInfo.put("bookCode", commandMap.get("bookCode"));
				model.put("result", problemInfo);
				totalCnt = Long.valueOf(String.valueOf(problemInfo.get("totalCnt"))).intValue();
			}
			
			PagerVO pager = new PagerVO(Integer.parseInt((String)commandMap.get("rowCnt")), 100, totalCnt, Integer.parseInt((String)commandMap.get("nowPage")));
			model.put("pager", pager);
			model.put("totalCnt", 200);
			ParameterArrUtil.mapValueSetModel(commandMap, model);
			
			LOGGER.debug("result:" + model.get("result"));
			LOGGER.debug("rowCnt:" + model.get("rowCnt"));
			LOGGER.debug("nowPage:" + model.get("nowPage"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		 return "cmm/finalEnglish";
   	}
	 
	@RequestMapping("/comm/insertFinalTest.do")
	@ResponseBody
	public HashMap<String, Object> insertFinalTest(@RequestBody HashMap<String, Object> map, HttpServletRequest request,
			ModelMap model) {

		/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();

		resJsonHash = null;
		try {
			resJsonHash = JsonMessage.getJsonMessage(resCd, egovMessageSource.getMessage("msg.success.common"));

			if (userId == null) {
				resCd = "0001";
				resJsonHash = JsonMessage.getJsonMessage(resCd,
						egovMessageSource.getMessage("msg.error.nosession").toString());
				return resJsonHash;
			}

			EgovMap paramMap = new EgovMap();
			map.forEach((key, value) -> paramMap.put(key, StringUtil.isNullToString(value)));

			paramMap.put("memId", userId);
			paramMap.put("result", paramMap.get("solveResult"));
			paramMap.put("answer", paramMap.get("solveValue"));
			List<EgovMap> test = (List<EgovMap>)spsService.getFinalTest(paramMap);
			if(test != null && test.size() > 0) {
				
			}else {
				spsService.insertFinalTest(paramMap);
			}

			resJsonHash.put("resultData", paramMap);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				resJsonHash = JsonMessage.getJsonMessage("0001", egovMessageSource.getMessage("msg.error.system"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return resJsonHash;
	}
}
