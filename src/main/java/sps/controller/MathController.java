package sps.controller;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.trace.LeaveaTrace;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;


import sps.comm.PagerVO;
import sps.login.LoginVO;
import sps.service.SpsService;
import sps.service.TempLearn;
import sps.service.TempProblem;
import sps.utils.ApiService;
import sps.utils.ApiVO;
import sps.utils.JsonMessage;
import sps.utils.ParameterArrUtil;
import sps.utils.StringUtil;
import sps.utils.TypeUtil;
import sps.utils.UserDetailsHelper;
import sps.utils.HttpUtil;

/**
 * MathController 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.06.15
 * @version 1.0
 * @see 
 *
 * 
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.15  BizAn                  최초 생성
 * </pre>
 */

@Controller
public class MathController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MathController.class);
	
	@Resource(name="egovMessageSource")
	private EgovMessageSource  egovMessageSource;
		
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	//비디오 HOST
  	private String videoMainHost = EgovProperties.getProperty("video.main.host"); 
	
	@Resource(name = "ApiService")
	private ApiService apiService;

	/** TRACE */
	@Resource(name = "leaveaTrace")
	LeaveaTrace leaveaTrace;
	
	@Resource(name = "SpsService")
	private SpsService spsService;
    
	
	private String resJson = "";
	private HashMap resJsonHash = null;
	private String resCd = "0000";
	
	
    @RequestMapping(value = "/math/MathDashboardView.do")
	public String mathDashboardView(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		
    	/* 세션 정보 */
  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		
  		// 최종진단 정보...
  		EgovMap testParam = new EgovMap();
		testParam.put("memId", userId);
		testParam.put("subject", "MATH");
		List<EgovMap> test = (List<EgovMap>)spsService.getFinalTest(testParam);
		if(test != null && test.size() > 0) {
			if(test.size() >= 13) {
				int count = 0;
				for(EgovMap item : test) {
					if("O".equals(item.get("result"))) {
						count++;
					}			
				}				
				model.put("finalMathMessage", "(진단완료 : " + ((int)((float)count / 13 * 100)) + "점)");
			}else {
				model.put("finalMathMessage", "(진단중 : " + test.size() + "/13)");
			}			
		}else {
			model.put("finalMathMessage", "(미진단)");
		}
		
		
		testParam.put("subject", "ENGLISH");
		test = (List<EgovMap>)spsService.getFinalTest(testParam);
		String solvedProblemNo = "";
		if(test != null && test.size() > 0) {
			if(test.size() >= 30) {
				int count = 0;
				for(EgovMap item : test) {
					if("O".equals(item.get("result"))) {
						count++;
					}			
				}				
				model.put("finalEnglishMessage", "(진단완료 : " + ((int)((float)count / 30 * 100)) + "점)");
			}else {
				model.put("finalEnglishMessage", "(진단중 : " + test.size() + "/30)");
			}			
		}else {
			model.put("finalEnglishMessage", "(미진단)");
		}
    	
    	EgovMap paramMap = new EgovMap();
    	EgovMap learnInfo = new EgovMap();
		List<EgovMap> bookList = null;
		List<EgovMap> chapterList = null;
		List<EgovMap> learnList = null;
		
		List<EgovMap> contextChartList = null;
		List<EgovMap> activityChartList = null;
		
		ApiVO apiVO = null;
		
		long totPlaySeconds = 0;
		String contextChartLabel = "";
		String contextChartData = "";
		String activityChartLabel = "";
		String activityChartData = "";
		String bookCode = "0251E";
		
		if (commandMap.get("bookCode") != null)
			bookCode = (String)commandMap.get("bookCode");
		
		try {
			
			long totPlayHour = 0;
			long totPlayMin = 0;
			long totPlayCnt = 0;
			
			paramMap.put("lessonCode", "02");
			bookList = (List<EgovMap>) spsService.getBookList(paramMap);
			
			paramMap.put("bookCode", bookCode);
			
			paramMap.put("memberId", userId);
			
			chapterList = (List<EgovMap>) spsService.getChapterList(paramMap);
			if (chapterList.size() > 0) {
				paramMap.put("chapterCode", (String)chapterList.get(0).get("chapterCode"));
				model.put("chapterCode", chapterList.get(0).get("chapterCode"));
				//learnList = (List<EgovMap>) spsService.getLearnList(paramMap);
				
				if (commandMap.get("chapterCode") != null) {
					paramMap.put("chapterCode",commandMap.get("chapterCode"));
					model.put("chapterCode",commandMap.get("chapterCode"));
				}
				
				apiVO = spsService.getLearnListApiVO(paramMap);
				learnList = apiVO.getListEgovMap();
				
				paramMap.put("userId", userId);
				
				totPlaySeconds = TypeUtil.toLong(apiVO.getEgovMap().get("memberProblemTime"));
				totPlayCnt = TypeUtil.toLong(apiVO.getEgovMap().get("memberLectureCount"));
				/*
				EgovMap resultMap = (EgovMap) spsService.getTotPlayTime(paramMap);
				if (resultMap != null) {
					totPlaySeconds = TypeUtil.toLong(resultMap.get("totPlayTime"));
					totPlayCnt = TypeUtil.toLong(resultMap.get("totPlayCnt"));
				}
				*/
				
				
				
				/*
				int chapterLen = chapterList.size();
				for (int i=0;i<chapterLen;i++) {
					chartLabel += (String)chapterList.get(i).get("chapterName2") + "^" ;
					chartData += TypeUtil.toStr(chapterList.get(i).get("problemCnt")) + "^" ;
				}
				*/
				
			}
			
			//내용영역 차트정보
			paramMap.put("userId", userId);
			contextChartList = (List<EgovMap>) spsService.getContextChartList(paramMap);
			if (contextChartList.size() > 0) {
				int chapterLen = contextChartList.size();
				for (int i=0;i<chapterLen;i++) {
					contextChartLabel += (String)contextChartList.get(i).get("chapterName2") + "^" ;
					contextChartData += TypeUtil.toStr(contextChartList.get(i).get("resultValue")) + "^" ;
				}
			}
			
			
			//행동영역 차트정보
			activityChartList = (List<EgovMap>) spsService.getActivityChartList(paramMap);
			if (activityChartList.size() > 0) {
				int chapterLen = activityChartList.size();
				for (int i=0;i<chapterLen;i++) {
					activityChartLabel += (String)activityChartList.get(i).get("codeName") + "^" ;
					activityChartData += TypeUtil.toStr(activityChartList.get(i).get("resultValue")) + "^" ;
				}
			}
			
			totPlayHour = totPlaySeconds/(60*60);
			totPlayMin = (totPlaySeconds%(60*60))/60;
			//long second = totPlaySeconds%60;
			
			learnInfo.put("totPlayHour", totPlayHour);
			learnInfo.put("totPlayMin", totPlayMin);
			learnInfo.put("totPlayCnt", totPlayCnt);
			learnInfo.put("answerProblemCnt", TypeUtil.toLong(apiVO.getEgovMap().get("memberProblemCount")));
			learnInfo.put("totProblemCnt", TypeUtil.toLong(apiVO.getEgovMap().get("problemTotalCount")));
			
			/*
			EgovMap resultMap = (EgovMap) spsService.getTotProblemAnswer(paramMap);
			
			if (resultMap == null) {
				learnInfo.put("answerProblemCnt", 0);
				learnInfo.put("totProblemCnt", 0);
			}else{
				learnInfo.put("answerProblemCnt", TypeUtil.toLong(resultMap.get("answerProblemCnt")));
				learnInfo.put("totProblemCnt", TypeUtil.toLong(resultMap.get("totProblemCnt")));
			}
			*/
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		model.put("bookList", bookList);
		model.put("chapterList", chapterList);
		model.put("learnList", learnList);
		model.put("learnInfo", learnInfo);
		model.put("contextChartLabel", contextChartLabel.substring(0, contextChartLabel.length()-1));
		model.put("contextChartData", contextChartData.substring(0, contextChartData.length()-1));
		model.put("activityChartLabel", activityChartLabel.substring(0, activityChartLabel.length()-1));
		model.put("activityChartData", activityChartData.substring(0, activityChartData.length()-1));
		
		EgovMap lrsUrls = new EgovMap();
		paramMap.put("memId", userId);
		lrsUrls = spsService.getLrsUrl(paramMap);
		if(lrsUrls != null) {
			model.put("lrsUrl1", lrsUrls.get("url1"));
			model.put("lrsUrl2", lrsUrls.get("url2"));
		}else {
			paramMap.put("memId", "_unknown");
			lrsUrls = spsService.getLrsUrl(paramMap);
			model.put("lrsUrl1", lrsUrls.get("url1"));
			model.put("lrsUrl2", lrsUrls.get("url2"));
		}		
		model.put("message", spsService.getMessage());	
		
		ParameterArrUtil.mapValueSetModel(commandMap, model);
		
		/*
		//xapi 호출/////////////////////////////////////////////////////
		//대시보드 1페이지
		EgovMap xapiParamMap = new EgovMap();
		String userNm = loginVO.getUserNm();
		
		xapiParamMap.put("id", userId);
		xapiParamMap.put("name", userNm);
		xapiParamMap.put("verb", "attempted");
		xapiParamMap.put("objectId", "/math/MathDashboardView.do");
		xapiParamMap.put("enGbDesc", "entered the dashboard1");
		xapiParamMap.put("enUsDesc", "entered the dashboard1");
		xapiParamMap.put("koKrDesc", "대시보드1");
		
		apiService.callXApi(xapiParamMap);
		
		//과목 선택
		xapiParamMap.put("id", userId);
		xapiParamMap.put("name", userNm);
		xapiParamMap.put("verb", "focused");
		xapiParamMap.put("objectId", "02");
		xapiParamMap.put("enGbDesc", "select lesson");
		xapiParamMap.put("enUsDesc", "select lesson");
		xapiParamMap.put("koKrDesc", "과목선택");
		
		apiService.callXApi(xapiParamMap);
		//////////////////////////////////////////////////////////////
		*/
				
		return "math/MathDashboardView";
  	
	}
    
    
    
    
    
    @RequestMapping(value = "/math/MathDashboardView2.do")
	public String mathDashboardView2(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		
    	/* 세션 정보 */
  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		
  		EgovMap sibal = new EgovMap();					
  		sibal.put("memId", userId);
  		sibal.put("todayYn", "Y");
  		sibal.put("lessonCode", "02");
		EgovMap _problemCntinfo = spsService.getPbProblemSolveCntInfo(sibal);			
		int todayProblemCnt =  TypeUtil.toInt(_problemCntinfo.get("problemCnt"));
		model.put("todayProblemCnt", todayProblemCnt);
  		
    	//페이징 옵션
		ParameterArrUtil.mapValueSetModel(commandMap, model);
		
		String chasiLabel = "";
		String chasiJumsu = "";
		String chasiProblem = "";
		String chasiProblem2 = "";		
//		String chasiTime = "";
		String chasiAddProblem = "";
		String chasiExpectJumsu = "";
		String chasiFinalExpectJumsu = "";
		String chasiFinalExpectJumsuHigeYn = "N";
		int finalExpectJumsu = 0;
		
		int allCorrectProblemCnt = 0;
		int allIncorrectProblemCnt = 0;
		int weekChasiJumsu = 100;
		String weekChasiLearnCode = "";
		
		int nextJumsu = 0;
		int nextJumsuTemp1 = 0;
		int nextJumsuCntTemp1 = 0;
		int nextJumsuTemp2 = 0;
		int nextJumsuCntTemp2 = 0;
		
		try {
			List<EgovMap> learnList = null;
			ApiVO apiVO = null;
			EgovMap paramMap = new EgovMap();
			
			paramMap.put("memberId", userId);
			paramMap.put("chapterCode", commandMap.get("chapterCode"));
			
		
			apiVO = spsService.getLearnListApiVO(paramMap);
			learnList = apiVO.getListEgovMap();
			
			int chasi = 0;
			
			if (learnList != null && learnList.size() > 0) {
				int len = learnList.size();
				for (int i=0;i<len;i++) {
//					if(TypeUtil.toInt(learnList.get(i).get("problemCount")) <= 0) {
//						continue;
//					}
//					if("N".equals(TypeUtil.toStr(learnList.get(i).get("matchYn")))) {
//						continue;
//					}					
					
					chasi++;
					
					EgovMap ttt = new EgovMap();					
					ttt.put("memId", userId);
					ttt.put("learnCode", learnList.get(i).get("learnCode"));
					EgovMap problemCntinfo = spsService.getPbProblemSolveCntInfo(ttt);
					
					// TODO: 쿼리로 가져오기
					int correctProblemCnt =  TypeUtil.toInt(problemCntinfo.get("crrectProblemCnt"));
					int incorrectProblemCnt = TypeUtil.toInt(problemCntinfo.get("incorrectProblemCnt"));
					int addProblemCnt = TypeUtil.toInt(problemCntinfo.get("problemCnt"));
					
					allCorrectProblemCnt += correctProblemCnt;
					allIncorrectProblemCnt += incorrectProblemCnt;
					
					int addJumsu = (correctProblemCnt * 3) + (incorrectProblemCnt * -3);					
//					int expectJumsu = (Integer.parseInt(TypeUtil.toStr(learnList.get(i).get("jumsu")))) + addJumsu;			
					
					chasiLabel += TypeUtil.toStr(chasi) +"차시" + "^" ;
					chasiJumsu += TypeUtil.toStr(learnList.get(i).get("jumsu")) + "^" ;
					chasiProblem += TypeUtil.toStr(learnList.get(i).get("problemCount")) + "^" ;
					
					
					// 초기 예측 점수
					int originJumsu = Integer.parseInt(TypeUtil.toStr(learnList.get(i).get("jumsu")));
					int startExpectJumsu = Integer.parseInt(TypeUtil.toStr(learnList.get(i).get("jumsu")));
					if(len > 3) {
						
						// 1차시 (1차시 학습 성취도 * 0.5) + (2차시 학습 성취도 * 0.5)
						if(i == 0 ) {
							float j1 = Integer.parseInt(TypeUtil.toStr(learnList.get(i).get("jumsu")));
							float j2 = Integer.parseInt(TypeUtil.toStr(learnList.get(i+1).get("jumsu")));
							float jr =	(j1 * 0.5f) + (j2 * 0.5f);
							startExpectJumsu = Math.round(jr);
						}
						// 마지막 차시 // (1차시 학습 성취도 * 0.5) + (2차시 학습 성취도 * 0.5)
						else if(i == len-1) {
							float j1 = Integer.parseInt(TypeUtil.toStr(learnList.get(i-1).get("jumsu")));
							float j2 = Integer.parseInt(TypeUtil.toStr(learnList.get(i).get("jumsu")));
							float jr =	(j1 * 0.5f) + (j2 * 0.5f);
							startExpectJumsu = Math.round(jr);
						}
						// (1차시 성취도 * 0.5) + (직전 차시 성취도 * 0.3) + (직후 차시 성취도 * 0.2)
						else {
							float j1 = Integer.parseInt(TypeUtil.toStr(learnList.get(0).get("jumsu")));
							float j2 = Integer.parseInt(TypeUtil.toStr(learnList.get(i).get("jumsu")));
							float j3 = Integer.parseInt(TypeUtil.toStr(learnList.get(i+1).get("jumsu")));
							float jr =	(j1 * 0.5f) + (j2 * 0.3f) + (j3 * 0.2f);
							startExpectJumsu = Math.round(jr);
						}
						
					}
					
					// [{ln(맞은 개수+1) - ln(틀린 개수+1)}*120-기존 성취도]/EXP 영찬 확인
					int expectJumsu = startExpectJumsu;
					int expectJumsuAdd = 0;	
					expectJumsuAdd = (int) ((((Math.log(correctProblemCnt+1) - Math.log(incorrectProblemCnt+1)) * (120 - startExpectJumsu))) / 2.718f);					
					expectJumsu += expectJumsuAdd; 
					
//					System.out.println(TypeUtil.toStr(chasi) +"차시=========");
//					System.out.println("비상 성취도 : " + originJumsu);
//					System.out.println("성취도 : " + startExpectJumsu);
//					System.out.println("초기예측 : " + startExpectJumsu);					
//					System.out.println("맞은개수 : " + correctProblemCnt );
//					System.out.println("ln(맞은개수+1) : " + (Math.log(correctProblemCnt+1)) );
//					System.out.println("틀린개수 : " + incorrectProblemCnt );
//					System.out.println("ln(틀린개수+1) : " + (Math.log(incorrectProblemCnt+1)) );
//					System.out.println("(ln(맞은개수+1) - ln(틀린개수+1) * (120 - 초기예측)) : " + ((((Math.log(correctProblemCnt+1) - Math.log(incorrectProblemCnt+1)) * (120 - startExpectJumsu))) / 2.718f) );
					
					
					// Max(0, Min(100, Min(기존 성취도, 초기 예측점수) + 추가 학습량에 따른 예측 점수)) // 영찬 확인	
					expectJumsu = (expectJumsu >= 100) ? 100 : expectJumsu;
					expectJumsu = (expectJumsu < 0) ? 0 : expectJumsu;
							
					chasiExpectJumsu += TypeUtil.toStr(expectJumsu) + "^";
					
					if(i==0 || originJumsu != 0) {
						if(weekChasiJumsu >= expectJumsu) {
							weekChasiJumsu = expectJumsu;
							weekChasiLearnCode = TypeUtil.toStr(learnList.get(i).get("learnCode"));
						}	
					}	
					
					if(i < len/3) {
						nextJumsuTemp1 += expectJumsu;
						nextJumsuCntTemp1++;
					}else {
						nextJumsuTemp2 += expectJumsu;
						nextJumsuCntTemp2++;
					}
					
					chasiAddProblem += addProblemCnt + "^" ;
					finalExpectJumsu = expectJumsu;
					if(i < len-1) chasiFinalExpectJumsu += "null^";
					chasiProblem2 += "null^";
				}
			}
			
			// 다음차시 계산			
			nextJumsu = (int) (nextJumsuTemp1/nextJumsuCntTemp1*0.3 + nextJumsuTemp2/nextJumsuCntTemp2*0.7) ;
			nextJumsu = (nextJumsu > 100) ? 100 : nextJumsu;
			
			
//			System.out.println("다음차시=========");
//			System.out.println("예측점수 : " + nextJumsu);								
			
			
			if(nextJumsu >= finalExpectJumsu) chasiFinalExpectJumsuHigeYn = "Y";
			
			chasiLabel += "다음차시 ";
			chasiJumsu += "";
			chasiProblem += "";
			chasiProblem2 += "10^";
			chasiAddProblem += "";
			chasiExpectJumsu += nextJumsu + "^";
			chasiFinalExpectJumsu += finalExpectJumsu + "^" + nextJumsu + "^";
			
			model.put("chasiLabel", chasiLabel.substring(0, chasiLabel.length()-1));
			model.put("chasiJumsu", chasiJumsu.substring(0, chasiJumsu.length()-1));
			model.put("chasiProblem", chasiProblem.substring(0, chasiProblem.length()-1));
			model.put("chasiProblem2", chasiProblem2.substring(0, chasiProblem2.length()-1));
			model.put("chasiAddProblem", chasiAddProblem.substring(0, chasiAddProblem.length()-1));
			model.put("chasiExpectJumsu", chasiExpectJumsu.substring(0, chasiExpectJumsu.length()-1));
			model.put("chasiFinalExpectJumsu", chasiFinalExpectJumsu.substring(0, chasiFinalExpectJumsu.length()-1));
			model.put("chasiFinalExpectJumsuHigeYn", chasiFinalExpectJumsuHigeYn);			
			model.put("learnList", learnList);
		
			
			LOGGER.debug("chasiLabel:" + model.get("chasiLabel"));
			LOGGER.debug("chasiJumsu:" + model.get("chasiJumsu"));
			LOGGER.debug("chasiProblem:" + model.get("chasiProblem"));
			LOGGER.debug("chasiTime:" + model.get("chasiTime"));
			LOGGER.debug("learnList:" + model.get("learnList"));
			
			//귀즈로봇 취약한 단원 가져오기
			paramMap.put("userId", userId);
			paramMap.put("bookCode", commandMap.get("bookCode"));
			EgovMap info = spsService.getRobotQuizChapterInfo(paramMap);
			model.put("chapterName", info.get("chapterName2"));
			
			LOGGER.debug("chapterName:" + model.get("chapterName"));
			
			//귀즈로봇 취약한 차시 가져오기
			paramMap.put("chapterCode", info.get("chapterCode"));
			apiVO = spsService.getLearnListApiVO(paramMap);
			learnList = apiVO.getListEgovMap();
			
			model.put("chasiName", spsService.getLearnName(weekChasiLearnCode));			
			
		/*
		//xapi 호출/////////////////////////////////////////////////////
		//대시보드 2페이지
		EgovMap xapiParamMap = new EgovMap();
		String userNm = loginVO.getUserNm();
		
		xapiParamMap.put("id", userId);
		xapiParamMap.put("name", userNm);
		xapiParamMap.put("verb", "attempted");
		xapiParamMap.put("objectId", "/math/MathDashboardView2.do");
		xapiParamMap.put("enGbDesc", "entered the dashboard2");
		xapiParamMap.put("enUsDesc", "entered the dashboard2");
		xapiParamMap.put("koKrDesc", "대시보드2 진입");
		apiService.callXApi(xapiParamMap);
		
		//대시보드 1 이탈
		xapiParamMap.put("id", userId);
		xapiParamMap.put("name", userNm);
		xapiParamMap.put("verb", "exited");
		xapiParamMap.put("objectId", "/math/MathDashboardView.do");
		xapiParamMap.put("enGbDesc", "entered the dashboard1");
		xapiParamMap.put("enUsDesc", "entered the dashboard1");
		xapiParamMap.put("koKrDesc", "대시보드1 이탈");
		apiService.callXApi(xapiParamMap);
		//////////////////////////////////////////////////////////////
		*/		
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "math/MathDashboardView2";
	}
    
    @RequestMapping(value = "/math/MathDashboardView2Inner.do")
	public String MathDashboardView2Inner(@RequestBody HashMap<String, Object> map, HttpServletRequest request, ModelMap model) throws Exception {

    	/* 세션 정보 */
  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		  		
		List<EgovMap> learnList = null;
		
		try {
			
			EgovMap paramMap = new EgovMap();
			map.forEach((key, value)
			    -> paramMap.put(key, StringUtil.isNullToString(value)));
			
			paramMap.put("mem_id", userId);
			paramMap.put("subject", "수학");
			
			//paramMap.put("sort", "LEARN_CODE");
			//paramMap.put("order", "ASC");
			
			//paramMap.put("chapterCode", "0251D000200");
			
			
			//learnList = (List<EgovMap>) spsService.getLearnListPaging(paramMap);
//			learnList = (List<EgovMap>) spsService.getLearnList2Paging(paramMap); // 김동현
			learnList = (List<EgovMap>) spsService.getLearnList3Paging(paramMap); // 서강대
			
			if(learnList != null && learnList.size() > 0) {
				String weekLearnCode = (String)learnList.get(0).get("topicCode");
				weekLearnCode = weekLearnCode.split("_")[0];
				request.getSession().removeAttribute("weekLearnCode");
				request.getSession().setAttribute("weekLearnCode", weekLearnCode);
			}
			/*
			int totalCnt = 0;
			if(learnList.size() > 0) {
				model.put("result", learnList);
				totalCnt = Long.valueOf(String.valueOf(learnList.get(0).get("totalCnt"))).intValue();
			}
			
			PagerVO pager = new PagerVO(Integer.parseInt((String)map.get("rowCnt")), 5, totalCnt, Integer.parseInt((String)map.get("nowPage")));
			model.put("pager", pager);
			*/
			model.put("result", learnList);
			
			model.put("bookCode", paramMap.get("bookCode"));
			model.put("chapterCode", paramMap.get("chapterCode"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "/math/MathDashboardView2Inner";

	}
    
    @RequestMapping(value = "/math/MathLearnView.do")
	public String mathLearnView(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		
    	
    	/* 세션 정보 */
  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		
  		
    	List<EgovMap> learnList = null;
		
		try {
			EgovMap paramMap = new EgovMap();
			
			paramMap.put("mem_id", userId);
			paramMap.put("subject", "수학");
			
			paramMap.put("bookCode", commandMap.get("bookCode"));
			paramMap.put("chapterCode", commandMap.get("chapterCode"));
			paramMap.put("sortOnly", "Y");
			paramMap.put("sort", "LEARN_CODE");
			paramMap.put("order", "ASC");
			
			//learnList = (List<EgovMap>) spsService.getLearnList(paramMap);
			learnList = (List<EgovMap>) spsService.getLearnList2Paging(paramMap); // 김동현
//			learnList = (List<EgovMap>) spsService.getLearnList3Paging(paramMap); // 서강대
			
			if(learnList != null && learnList.size() > 0) {
				String weekLearnCode = (String)learnList.get(0).get("topicCode");
				weekLearnCode = weekLearnCode.split("_")[0];
				request.getSession().removeAttribute("weekLearnCode");
				request.getSession().setAttribute("weekLearnCode", weekLearnCode);
			}
			model.put("learnList", learnList);
			model.put("memId", userId);
			
			String s = commandMap.get("topicCode").toString();
		    String[] temp = s.split("_");
		    String videoFileName = temp[0];
		    
		    if(!commandMap.get("learnCode").toString().equals(videoFileName)) {
		    	commandMap.put("learnCode", videoFileName);
		    }
		    
		    if(temp.length >= 2) {
		    	if(temp[1].length() <= 1) {
		    		temp[1] = "0" + temp[1];
		    	}
		    	videoFileName += "_" + temp[1]; 
		    }
			
		    String learnName = spsService.getLearnName((String)commandMap.get("learnCode")) + "-" + spsService.getTopicName(videoFileName);
		    model.put("learnName", learnName);
		    
		    //학습 콘텐츠가 동영상인지 index.html 인지 체크
		    String host = "";
		    String url = "";
		    String path = "";
		    Boolean isIndexHtml = false;
		    if (videoMainHost.contains("http") || videoMainHost.contains("HTTP")) {
		    	url = videoMainHost + '/' + commandMap.get("learnCode").toString() + "/" +
						videoFileName + "/index.html?mem_id="+userId + "&topic_name=" + learnName;
		    	path = "C:\\\\hp\\workspace\\hpd\\src\\main\\webapp\\media\\" + commandMap.get("learnCode").toString() + "\\" + videoFileName;
//		    	isIndexHtml = HttpUtil.isExistUrl(url);
		    	isIndexHtml = HttpUtil.isExistIndexFile(path);
		    	
		    } else {
		    	host = request.getScheme() + "://" + request.getServerName() + ':' + request.getServerPort();
		    	host = "http://localhost:8081";
		    	url = host + videoMainHost + '/' + commandMap.get("learnCode").toString() + "/" +
						videoFileName + "/index.html?mem_id="+userId + "&topic_name=" + learnName;
		    	path = "C:\\\\hp\\workspace\\hpd\\src\\main\\webapp\\media\\" + commandMap.get("learnCode").toString() + "\\" + videoFileName;
//		    	isIndexHtml = HttpUtil.isExistUrl(url);
		    	isIndexHtml = HttpUtil.isExistIndexFile(path);
		    }
		    
		    if (isIndexHtml) {
		    	model.put("videoYn", "N");
		    	model.put("mp4", videoMainHost + '/' + commandMap.get("learnCode").toString() + "/" +
						videoFileName + "/index.html?mem_id="+userId + "&topic_name=" + learnName);
		    }else {
		    	model.put("videoYn", "Y");
		    	model.put("mp4", videoMainHost + '/' + commandMap.get("learnCode").toString() + "/" +
						videoFileName + ".mp4?mem_id="+userId + "&topic_name=" + learnName);
		    }
		    
			ParameterArrUtil.mapValueSetModel(commandMap, model);
			
			
			EgovMap playLearnParam = new EgovMap();
			playLearnParam.put("memId", userId);
			playLearnParam.put("topicType", (isIndexHtml?"HTML":"VIDEO"));
			playLearnParam.put("bookCode", commandMap.get("bookCode"));
			playLearnParam.put("chapterCode", commandMap.get("chapterCode"));
			playLearnParam.put("learnCode", commandMap.get("learnCode"));
			playLearnParam.put("topicCode", videoFileName);
			spsService.insertLoPlayLearn(playLearnParam);
			
			/*
			//xapi 호출/////////////////////////////////////////////////////
			// 강의 진입
			// 세션 정보 
   	  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
   	  		String userId = loginVO.getUserId();
   	  		String userNm = loginVO.getUserNm();
   	  		EgovMap xapiParamMap = new EgovMap();
   			xapiParamMap.put("id", userId);
   			xapiParamMap.put("name", userNm);
   			xapiParamMap.put("verb", "access");
   			xapiParamMap.put("objectId", commandMap.get("learnCode").toString());
   			xapiParamMap.put("enGbDesc", "access lecture");
   			xapiParamMap.put("enUsDesc", "access lecture");
   			xapiParamMap.put("koKrDesc", "강의진입");
   			
   			apiService.callXApi(xapiParamMap);
   			//////////////////////////////////////////////////////////////
   			*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    			
		return "math/MathLearnView";
	}
    
    
    
    
    @RequestMapping(value = "/math/MathProblemSolve.do")
	public String MathProblemSolve(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {

    	/* 세션 정보 */
    	LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		
  		EgovMap sibal = new EgovMap();					
  		sibal.put("memId", userId);
  		sibal.put("todayYn", "Y");
  		sibal.put("lessonCode", "02");
		EgovMap _problemCntinfo = spsService.getPbProblemSolveCntInfo(sibal);			
		int todayProblemCnt =  TypeUtil.toInt(_problemCntinfo.get("problemCnt"));
		model.put("todayProblemCnt", todayProblemCnt);
		
    	// 틀린문제만 가져오기위해 틀린 learn code 가져오기..
  		String includeLearnCodeList = "";
  		String allLearnCodeList = "";
  		List<EgovMap> learnList = null;
		ApiVO apiVO = null;
		EgovMap paramMap = new EgovMap();
		
		paramMap.put("memberId", userId);
		paramMap.put("chapterCode", commandMap.get("chapterCode"));		
	
		apiVO = spsService.getLearnListApiVO(paramMap);
		learnList = apiVO.getListEgovMap();
		
  		apiVO = spsService.getLearnListApiVO(paramMap);
		learnList = apiVO.getListEgovMap();		
		if (learnList != null && learnList.size() > 0) {
			int len = learnList.size();
			for (int i=0;i<len;i++) {
				int jumsu = Integer.parseInt(TypeUtil.toStr(learnList.get(i).get("jumsu")));
				if(jumsu < 100) {
					includeLearnCodeList += TypeUtil.toStr(learnList.get(i).get("learnCode")) + ",";
				}
				allLearnCodeList += TypeUtil.toStr(learnList.get(i).get("learnCode")) + ",";
			}
		}
		
    	List<EgovMap> problemList = null;
		
		try {
			paramMap = new EgovMap();

			paramMap.put("lessonCode", "02");
			paramMap.put("rowCnt", commandMap.get("rowCnt"));
			paramMap.put("nowPage", commandMap.get("nowPage"));
			paramMap.put("sort", "PROBLEM_ID");
			paramMap.put("order", "ASC");
			//테스트용
			paramMap.put("problemId", commandMap.get("problemId"));
			paramMap.put("subNo", commandMap.get("subNo"));
			paramMap.put("chapterCode", commandMap.get("chapterCode"));
			if(!"".equals(includeLearnCodeList)) {
				includeLearnCodeList = includeLearnCodeList.substring(0, includeLearnCodeList.length()-1);
				String[] arr = includeLearnCodeList.split(",");
				paramMap.put("includeLearnCodeList", arr);
			}else if(!"".equals(allLearnCodeList)) {
				allLearnCodeList = allLearnCodeList.substring(0, allLearnCodeList.length()-1);
				String[] arr = allLearnCodeList.split(",");
				paramMap.put("includeLearnCodeList", arr);
			}else{
				// 전체 ..
			}
			
//			problemList = (List<EgovMap>) spsService.getProblemList(paramMap);		
			
			String problemIdList = "";
			String weekLearnCode = (String)request.getSession().getAttribute("weekLearnCode");		
			weekLearnCode = null;			
			for( Map.Entry<String, String> elem : TempProblem.tempMathProblemLearnCode.entrySet() ){
				
				if(weekLearnCode != null && !"".equals("")) {
					if(weekLearnCode.equals(elem.getValue())) {
						problemIdList += elem.getKey() + ",";
					}
				}else {
					problemIdList += elem.getKey() + ",";
				}		
	        }
			
			problemIdList = problemIdList.substring(0, problemIdList.length()-1);
			String[] problemIdArrary = problemIdList.split(",");
			int radomIndex = (int)((Math.random()*10000) % (problemIdArrary.length));
			paramMap.put("problemId", problemIdArrary[radomIndex]);
			problemList = (List<EgovMap>) spsService.getProblemList2(paramMap);
			
			if(problemList.size() <= 0) {
				paramMap.remove("includeLearnCodeList");
				problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
				if(problemList.size() <= 0) {
					paramMap.remove("chapterCode");
					problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
				}
			}
			
			int totalCnt = 0;
			if(problemList.size() > 0) {
		    	EgovMap problemInfo = problemList.get(0);
		    	if (problemInfo.get("sentence") != null) {
			    	String sentence = (String)problemInfo.get("sentence");
			    	sentence = sentence.replace(".swf", ".mp3");
			    	sentence = sentence.replace(".JPG", ".jpg");
			    	sentence = sentence.replace(".GIF", ".gif");
			    	sentence = sentence.replace(".PNG", ".png");
			    	problemInfo.put("sentence", sentence);
		    	}
		    	problemInfo.put("problemNum", commandMap.get("nowPage"));
		    	problemInfo.put("filePath", "/problem_new/");
		    	problemInfo.put("lesson", "02");
				//if("Y".equals(problemInfo.get("exampleInd"))) {
					paramMap.putIfAbsent("problemId", problemInfo.get("problemId"));
					paramMap.putIfAbsent("subNo", problemInfo.get("subNo"));
					problemInfo.put("example", spsService.getExampleList(paramMap));
				//}
				//if (commandMap.get("userAnswerStr") != null)
				//	problemInfo.put("userAnswer", ((String)commandMap.get("userAnswerStr")).split("[|]"));//사용자 입력 답안
				problemInfo.put("bookCode", commandMap.get("bookCode"));
				model.put("result", problemInfo);
				totalCnt = Long.valueOf(String.valueOf(problemInfo.get("totalCnt"))).intValue();
			}
			
			PagerVO pager = new PagerVO(Integer.parseInt((String)commandMap.get("rowCnt")), 100, totalCnt, Integer.parseInt((String)commandMap.get("nowPage")));
			model.put("pager", pager);
			model.put("totalCnt", 200);
  			//문제풀이 시작시간 
  			Date date = new Date();
  	        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  	        String startTmstmp = sdformat.format(date);
  	        model.put("startTmstmp", startTmstmp);
			ParameterArrUtil.mapValueSetModel(commandMap, model);
			
			
			LOGGER.debug("result:" + model.get("result"));
			LOGGER.debug("rowCnt:" + model.get("rowCnt"));
			LOGGER.debug("nowPage:" + model.get("nowPage"));
			
			/*
			//xapi 호출/////////////////////////////////////////////////////
   			//문항
   			
   			LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
   	  		String userId = loginVO.getUserId();
   	  		String userNm = loginVO.getUserNm();
   	  		EgovMap xapiParamMap = new EgovMap();
   			xapiParamMap.put("id", userId);
   			xapiParamMap.put("name", userNm);
   			xapiParamMap.put("verb", "aasked");
   			xapiParamMap.put("objectId", commandMap.get("problemId"));
   			xapiParamMap.put("enGbDesc", "skipped lecture");
   			xapiParamMap.put("enUsDesc", "skipped lecture");
   			xapiParamMap.put("koKrDesc", "강의 이탈");
   			
   			apiService.callXApi(xapiParamMap);
   			//////////////////////////////////////////////////////////////
   			*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return "math/MathProblemSolve";
	}

    @RequestMapping(value = "/math/MathProblemAnswer.do")
	public String MathProblemAnswer(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {

    	LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
  		String userId = loginVO.getUserId();
  		
  		EgovMap sibal = new EgovMap();					
  		sibal.put("memId", userId);
  		sibal.put("todayYn", "Y");
  		sibal.put("lessonCode", "02");
		EgovMap _problemCntinfo = spsService.getPbProblemSolveCntInfo(sibal);			
		int todayProblemCnt =  TypeUtil.toInt(_problemCntinfo.get("problemCnt"));
		model.put("todayProblemCnt", todayProblemCnt);
    	
    	List<EgovMap> problemList = null;
		
		try {
			EgovMap paramMap = new EgovMap();

			/*paramMap.put("lessonCode", "02");
			paramMap.put("rowCnt", commandMap.get("rowCnt"));
			paramMap.put("nowPage", commandMap.get("nowPage"));
			paramMap.put("sort", "PROBLEM_ID");
			paramMap.put("order", "ASC");*/
			paramMap.put("sortOnly", "Y");
			//테스트용
			paramMap.put("problemId", commandMap.get("problemId"));
			paramMap.put("subNo", commandMap.get("subNo"));
			
//			problemList = (List<EgovMap>) spsService.getProblemList(paramMap);
			problemList = (List<EgovMap>) spsService.getProblemList2(paramMap);
			int totalCnt = 0;
			if(problemList.size() > 0) {
		    	EgovMap problemInfo = problemList.get(0);
		    	if (problemInfo.get("sentence") != null) {
			    	String sentence = (String)problemInfo.get("sentence");
			    	sentence = sentence.replace(".swf", ".mp3");
			    	sentence = sentence.replace(".JPG", ".jpg");
			    	sentence = sentence.replace(".GIF", ".gif");
			    	sentence = sentence.replace(".PNG", ".png");
			    	problemInfo.put("sentence", sentence);
		    	}
		    	problemInfo.put("problemNum", commandMap.get("nowPage"));
		    	problemInfo.put("filePath", "/problem_new/");
		    	problemInfo.put("lesson", "02");
				//if("Y".equals(problemInfo.get("exampleInd"))) {
					paramMap.putIfAbsent("problemId", problemInfo.get("problemId"));
					paramMap.putIfAbsent("subNo", problemInfo.get("subNo"));
					problemInfo.put("example", spsService.getExampleList(paramMap));
				//}
				if (commandMap.get("userAnswerStr") != null)
					problemInfo.put("userAnswer", ((String)commandMap.get("userAnswerStr")).split("[|]"));//사용자 입력 답안
				problemInfo.put("bookCode", commandMap.get("bookCode"));
				model.put("result", problemInfo);
				totalCnt = Long.valueOf(String.valueOf(problemInfo.get("totalCnt"))).intValue();
			}
			
			PagerVO pager = new PagerVO(Integer.parseInt((String)commandMap.get("rowCnt")), 100, totalCnt, Integer.parseInt((String)commandMap.get("nowPage")));
			model.put("pager", pager);
			model.put("totalCnt", 200);
			ParameterArrUtil.mapValueSetModel(commandMap, model);
			
			
			LOGGER.debug("result:" + model.get("result"));
			LOGGER.debug("rowCnt:" + model.get("rowCnt"));
			LOGGER.debug("nowPage:" + model.get("nowPage"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return "math/MathProblemSolve";
	}
    
    @RequestMapping(value = "/math/MathProblemSolve2.do")
	public String MathProblemSolve2(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {
		
		
    	/*
    		//xapi 호출/////////////////////////////////////////////////////
			//문항
			
			
	  		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
	  		String userId = loginVO.getUserId();
	  		String userNm = loginVO.getUserNm();
	  		EgovMap xapiParamMap = new EgovMap();
			xapiParamMap.put("id", userId);
			xapiParamMap.put("name", userNm);
			xapiParamMap.put("verb", "asked");
			xapiParamMap.put("objectId", commandMap.get("problemId"));
			xapiParamMap.put("enGbDesc", "submitted problem");
			xapiParamMap.put("enUsDesc", "submitted problem");
			xapiParamMap.put("koKrDesc", "문제 출제");
			
			apiService.callXApi(xapiParamMap);
			//////////////////////////////////////////////////////////////
			*/
    	
		return "math/MathProblemSolve2";
	}
    
}


