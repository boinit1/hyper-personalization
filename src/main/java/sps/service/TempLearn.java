package sps.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TempLearn {	
	final static List<String> tempMathTopic  = new ArrayList<String>() {{
//		0251E00011105
//		add("0251E00011105_01");
		add("0251E00011105_02");
//		0251E00011106
//		add("0251E00011106_01");
		add("0251E00011106_02");
		add("0251E00011106_03");
//		add("0251E00011106_04");
//		0251E00011107
//		add("0251E00011107_01");
		add("0251E00011107_02");
		add("0251E00011107_03");
//		add("0251E00011107_04");
//		0251E00011108
//		add("0251E00011108_01");
		add("0251E00011108_02");
		add("0251E00011108_03");
//		add("0251E00011108_04");
//		0251E00011109
//		add("0251E00011109_01");
		add("0251E00011109_02");
		add("0251E00011109_03");
//		add("0251E00011109_04");
//		0251E00011110
//		add("0251E00011110_01");
		add("0251E00011110_02");
		add("0251E00011110_03");
//		add("0251E00011110_04");
//		0251E00011111
//		add("0251E00011111_01");
		add("0251E00011111_02");
		add("0251E00011111_03");
	}};
	
	final static List<String> tempEnglishTopic = new ArrayList<String>() {{
//		1051E00011101
//		add("1051E00011101_01");
//		add("1051E00011101_02");// 오류
		add("1051E00011101_03");
		add("1051E00011101_04");
		add("1051E00011101_05");
//		1051E00011102
//		add("1051E00011102_01");
//		add("1051E00011102_02");// 오류
		add("1051E00011102_03");
		add("1051E00011102_04");
		add("1051E00011102_05");
//		1051E00011103
//		add("1051E00011103_01"); // 오류
		add("1051E00011103_02");
		add("1051E00011103_03");
//	1051E00011104
		add("1051E00011104_01");
		add("1051E00011104_02");
		

		
////		1051E00021101
//		add("1051E00021101_01");
//		add("1051E00021101_02");
//		add("1051E00021101_03");
//		add("1051E00021101_04");
//		add("1051E00021101_05");
////		1051E00021102
//		add("1051E00021102_01");
//		add("1051E00021102_02");
//		add("1051E00021102_03");
//		add("1051E00021102_04");
//		add("1051E00021102_05");
////		1051E00021103
//		add("1051E00021103_01");
//		add("1051E00021103_03");
////		1051E00021104
//		add("1051E00021104_02");
	}};
	
	final static Map<String, String> tempTopicNames = new HashMap<String, String>() {{
		put("0251E00011105_01", "단원 열기");
		put("0251E00011105_02", "단원 흐름 알기");
		put("0251E00011106_01", "시작하기");
		put("0251E00011106_02", "개념 학습1");
		put("0251E00011106_03", "개념 학습2");
		put("0251E00011106_04", "수학 정리");
		put("0251E00011107_01", "시작하기");
		put("0251E00011107_02", "개념 학습1");
		put("0251E00011107_03", "개념 학습2");
		put("0251E00011107_04", "수학 정리");
		put("0251E00011108_01", "시작하기");
		put("0251E00011108_02", "개념 학습1");
		put("0251E00011108_03", "개념 학습2");
		put("0251E00011108_04", "수학 정리");
		put("0251E00011109_01", "시작하기");
		put("0251E00011109_02", "개념 학습1");
		put("0251E00011109_03", "개념 학습2");
		put("0251E00011109_04", "수학 정리");
		put("0251E00011110_01", "시작하기");
		put("0251E00011110_02", "개념 학습1");
		put("0251E00011110_03", "개념 학습2");
		put("0251E00011110_04", "수학 정리");
		put("0251E00011111_01", "시작하기");
		put("0251E00011111_02", "개념 완성");
		put("0251E00011111_03", "실력 쑥쑥");
		
		put("1051E00011101_01", "Intro");
		put("1051E00011101_02", "Preview");
		put("1051E00011101_03", "Dialog");
		put("1051E00011101_04", "Role Play");
		put("1051E00011101_05", "Activity");
		
		put("1051E00011102_01", "Intro");
		put("1051E00011102_02", "Preview");
		put("1051E00011102_03", "Dialog");
		put("1051E00011102_04", "Role Play");
		put("1051E00011102_05", "Activity");
		
		put("1051E00011103_01", "Let's Read");
		put("1051E00011103_02", "Lecture");
		put("1051E00011103_03", "Let's Write");
		
		put("1051E00011104_01", "Let's Chant");
		put("1051E00011104_02", "Quiz Show");
	}};
	
	final static Map<String, String> tempLearnNames = new HashMap<String, String>() {{
		put("0251E00011105", "1단원 준비하기");
		put("0251E00011106", "덧셈과 뺄셈이 섞여 있는 식을 계산해 볼까요");
		put("0251E00011107", "곱셈과 나눗셈이 섞여 있는 식을 계산해 볼까요");
		put("0251E00011108", "덧셈, 뺄셈, 곱셈이 섞여 있는 식을 계산해 볼까요");
		put("0251E00011109", "덧셈, 뺄셈, 나눗셈이 섞여 있는 식을 계산해 볼까요");
		put("0251E00011110", "덧셈, 뺄셈, 곱셈, 나눗셈이 섞여 있는 식을 계산해 볼까요");
		put("0251E00011111", "1단원 정리하기");
	
		put("1051E00011101", "때에 맞는 인사말 하기 / 안부 묻고 답하기, 질문 되묻기");
		put("1051E00011102", "출신 국가 묻고 답하기 / 이름의 철자 묻고 답하기");
		put("1051E00011103", "‘나’를 소개하는 글을 읽고 이해하기");
		put("1051E00011104", "챈트와 퀴즈로 1단원 정리하기");
	}};
}
