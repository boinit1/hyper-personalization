package sps.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import sps.login.LoginVO;
import sps.utils.UserDetailsHelper;
import twitter4j.JSONObject;

@Service("XapiService")
public class XapiService extends EgovAbstractServiceImpl {
	
	public String send(Map<String, Object> data) {
		
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userId = loginVO.getUserId();
		String userNm = loginVO.getUserNm();
		
		return sendApi(convertBoinStatements(data, userId, userNm));
	}	
	
	public static String convertBoinStatements(Map<String, Object> boidXapiData, String id, String name) {
		Map<String, Object> data = new HashMap<String, Object>();
		
		data.put("verb", boidXapiData.get("verb"));
		data.put("definition", boidXapiData.get("definition"));
		
		data.put("actor.name", name);
		data.put("actor.account.name", id);
		data.put("actor.account.homePage", "http://hpweb.boinit.com");
		
//		data.put("object.id", "http://hpweb.boinit.com/math/MathProblemSolve.do?bookCode=0251D&chapterCode=0251D000100&rowCnt=1&nowPage=1");
//		data.put("object.objectType", "Activity");
		
		data.put("object.id", boidXapiData.get("objectId"));
		data.put("object.objectType", boidXapiData.get("objectType"));
		data.put("object.definition.description.en-US", boidXapiData.get("description"));
		
		data.put("result.score.raw", boidXapiData.get("score"));
		
		data.put("result.response", boidXapiData.get("response"));
		
		data.put("result.extensions.https://w3id.org/xapi/video/extensions/time", boidXapiData.get("time"));
		data.put("result.extensions.https://w3id.org/xapi/video/extensions/time-from", boidXapiData.get("timeFrom"));
		data.put("result.extensions.https://w3id.org/xapi/video/extensions/time-to", boidXapiData.get("timeTo"));
		
		
		try {
			return generateStatements(data);
		} catch (Exception e) {
			return "";
		}
	}
	
	public static String generateStatements(Map<String, Object> data) throws Exception {
		
		String verbHost = "http://adlnet.gov/expapi/verbs";
		String verb = (String)data.get("verb");
		String definition = (String)data.get("definition");
		
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));		
		
		
		JSONObject statements = new JSONObject();
		
		//actor Json
		JSONObject _actor = new JSONObject();
		_actor.put("objectType", "Agent");
		_actor.put("name", data.get("actor.name"));
			JSONObject _actorAccount = new JSONObject();
			_actor.put("account", _actorAccount);
			_actorAccount.put("homePage", data.get("actor.account.homePage"));
			_actorAccount.put("name", data.get("actor.account.name"));
		statements.put("actor", _actor);
		
		//verb Json
		JSONObject _verb = new JSONObject();
		_verb.put("id", verbHost + "/" + verb);
			JSONObject _verbDisplay = new JSONObject();
			_verb.put("display", _verbDisplay);
			_verbDisplay.put("en-US", definition);
		statements.put("verb", _verb);
		
		//object Json
		JSONObject _object = new JSONObject();
		_object.put("id", data.get("object.id"));
		_object.put("objectType", data.get("object.objectType"));		
//		if(data.get("object.objectType") != null && "Activity".equals(data.get("object.objectType"))){
//			JSONObject _object = new JSONObject();
//		}		
		if(data.get("object.definition.description.en-US") != null && !"".equals(data.get("object.definition.description.en-US"))){
			JSONObject _objectDefinition = new JSONObject();
			_object.put("definition", _objectDefinition);
			
			JSONObject _objectDefinitionDescription = new JSONObject();
			_objectDefinition.put("description", _objectDefinitionDescription);
			
			_objectDefinitionDescription.put("en-US", data.get("object.definition.description.en-US"));
		}		
		statements.put("object", _object);
					
		//result Json
		if(data.get("result.score.raw") != null && !"".equals(data.get("result.score.raw"))){
			JSONObject _result = new JSONObject();
				JSONObject _resultScore = new JSONObject();
				_result.put("score", _resultScore);
					JSONObject _resultScoreRaw = new JSONObject();
					_resultScore.put("raw", data.get("result.score.raw"));	
			statements.put("result", _result);
		}else if(data.get("result.response") != null && !"".equals(data.get("result.response"))) {
			JSONObject _result = new JSONObject();
				JSONObject _resultScore = new JSONObject();
				_result.put("response", data.get("result.response"));
			statements.put("result", _result);
		}
		
		if(data.get("result.extensions.https://w3id.org/xapi/video/extensions/time") != null && !"".equals(data.get("result.extensions.https://w3id.org/xapi/video/extensions/time"))
				|| data.get("result.extensions.https://w3id.org/xapi/video/extensions/time-from") != null && !"".equals(data.get("result.extensions.https://w3id.org/xapi/video/extensions/time-from"))
				|| data.get("result.extensions.https://w3id.org/xapi/video/extensions/time-to") != null && !"".equals(data.get("result.extensions.https://w3id.org/xapi/video/extensions/time-to"))){
			JSONObject _result = new JSONObject();
				JSONObject _resultExtensions = new JSONObject();
				_result.put("extensions", _resultExtensions);
				_resultExtensions.put("https://w3id.org/xapi/video/extensions/time", data.get("result.extensions.https://w3id.org/xapi/video/extensions/time"));
				_resultExtensions.put("https://w3id.org/xapi/video/extensions/time-from", data.get("result.extensions.https://w3id.org/xapi/video/extensions/time-from"));
				_resultExtensions.put("https://w3id.org/xapi/video/extensions/time-to", data.get("result.extensions.https://w3id.org/xapi/video/extensions/time-to"));
			statements.put("result", _result);
		}
		
		return statements.toString();
	}
	
	public static String sendApi(String jsonString){
		
		String result = "";
		
    	try {
//            URL url = new URL("http://hplrs.boinit.com/hp-active/xapi/statements");
            URL url = new URL("http://localhost:8888/hp-active/xapi/statements");
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            String auth= "Basic " + new sun.misc.BASE64Encoder().encode(("hp" + ":" + "hp").getBytes());	
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("X-Experience-API-Version", "1.0.0");
            conn.setRequestProperty ("Authorization", auth);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", " application/json");
            conn.setDoOutput(true);

            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream(),"UTF-8");
            osw.write(jsonString);
            osw.flush();
            osw.close();
            
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
            if (conn.getResponseCode() != 200) {
                System.out.println("Failed: HTTP error code : " + conn.getResponseCode());
            	throw new RuntimeException("Failed: HTTP error code : " + conn.getResponseCode());
            } else {
                System.out.println("발송 성공");
            }
            
            String line = null;
            while((line = br.readLine()) != null){
            	result += line;
            }
            br.close();           
            conn.disconnect();
            
            
            return result.replace("[", "").replace("]","").replaceAll("\"", ""); 
            
        } catch (IOException e) {
            System.out.println("RestCall Fail : " + e.getMessage());
            return result; 
        }
    }
	
	
	public static void main(String args[]) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("verb", "scored");
		data.put("definition", "Example Activity");
		
		data.put("actor.name", "김동현");
		data.put("actor.account.name", "dhkim");
		data.put("actor.account.homePage", "http://hpweb.boinit.com");
		
//		data.put("object.id", "http://hpweb.boinit.com/math/MathProblemSolve.do?bookCode=0251D&chapterCode=0251D000100&rowCnt=1&nowPage=1");
//		data.put("object.objectType", "Activity");
		
		data.put("object.id", "83478b56-f1c9-4575-8e11-a05ecccc13d0");
		data.put("object.objectType", "StatementRef");
		
		data.put("result.score.raw", 1);
		
		data.put("result.extensions.https://w3id.org/xapi/video/extensions/time", 1);
//		data.put("result.extensions.https://w3id.org/xapi/video/extensions/time-from", 1);
		data.put("result.extensions.https://w3id.org/xapi/video/extensions/time-to", 2);
		
		try {
			System.out.println(sendApi(generateStatements(data)));
			;
		} catch (Exception e) {
			System.out.println("");
		}
		
		
	}
	
}