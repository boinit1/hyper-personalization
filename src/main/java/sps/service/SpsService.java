package sps.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import twitter4j.JSONObject;

import sps.comm.DAO;
import sps.login.LoginVO;
import sps.utils.ApiService;
import sps.utils.ApiVO;
import sps.utils.TypeUtil;
import sps.utils.UserDetailsHelper;


/**
 * WClient 서비스 클래스
 * 
 * @author (주)윈솔텍
 * @since 2022.04.01
 * @version 1.0
 * @see 
 *
 * 
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2022.06.15  BizAn                  최초 생성
 * </pre>
 */
@Service("SpsService")
public class SpsService extends EgovAbstractServiceImpl {

	private final DAO dao;
	private final String defaultMapper = "spsMapper";
	
	@Resource(name = "ApiService")
	private ApiService apiService;

    @Autowired
    public SpsService(DAO dao) {
        this.dao = dao;
    }

    
    //비디오 HOST
  	private String videoMainHost = EgovProperties.getProperty("video.main.host"); 
  	
    //과목 리스트
    public List<?> getLessonList(EgovMap param) throws Exception {
  		return dao.selectList(defaultMapper + ".getLessonList", param);
  	}	
    
    //교제 리스트
    public List<?> getBookList(EgovMap param) throws Exception {
    	
    	HashMap hMap = new HashMap(param);
		JSONObject jsonObject =  new JSONObject(hMap);
		String jsonParm = jsonObject.toString();
		
		//return apiService.callListVisangApi("/lesson/getBookCodeList", jsonParm);
		
    	return dao.selectList(defaultMapper + ".getBookList", param);
  		
  	}
    
    
    //단원 리스트
    public List<?> getChapterList(EgovMap param) throws Exception {
    	
    	
    	/*
		String temp = (String)param.get("bookCode");
		String [] sArr = temp.split(",");
		
		JSONObject jsonObject =  new JSONObject();
		jsonObject.put("bookCodeList", sArr);
		String jsonParm = jsonObject.toString();
		*/
		//return apiService.callListVisangApi("/lesson/getChapterList", jsonParm);
		
  		return dao.selectList(defaultMapper + ".getChapterList", param);
  		
  	}
    
    //다원 정보
    public EgovMap getChapterInfo(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getChapterInfo", param);
  	}
    
    //차시 리스트
    public List<?> getLearnList(EgovMap param) throws Exception {
    	
    	return dao.selectList(defaultMapper + ".getLearnList", param);
  	}
    
    public ApiVO getLearnListApiVO(EgovMap param) throws Exception {
    	
    	HashMap hMap = new HashMap(param);
		JSONObject jsonObject =  new JSONObject(hMap);
		String jsonParm = jsonObject.toString();
		
		ApiVO vo = apiService.callListVisangApi("/lesson/getLearnAchieveList", jsonParm);
		
		if(vo != null) {
			List<EgovMap> learnList = vo.getListEgovMap();
			
			for(int i=learnList.size()-1 ; i>=0 ; i--) {
				EgovMap tempParam =  new EgovMap();
				tempParam.put("learnCode",(String)learnList.get(i).get("learnCode"));
			    EgovMap temp = dao.selectOne(defaultMapper + ".getLearnInfo", tempParam);
			    if(temp != null) {
			    	learnList.get(i).put("longName", temp.get("studyMemo"));
			    	learnList.get(i).put("matchYn", "Y");
			    }else {
			    	learnList.remove(i);
			    }
			}
			
			// 문제 개수 0인거 0점 처리.. 및 이상한거 제거				
			for(int i=learnList.size()-1 ; i>=0 ; i--) {
//				if("N".equals(learnList.get(i).get("matchYn"))) {
//					learnList.remove(i);
//				}	
//				else if(TypeUtil.toInt(learnList.get(i).get("problemCount")) <= 0) {
//					learnList.get(i).put("jumsu", "0");
//				}
				
//				if(TypeUtil.toInt(learnList.get(i).get("problemCount")) <= 0) {
//					learnList.get(i).put("jumsu", "0");
//				}
				
				if(learnList.get(i).get("jumsu") == null || "".equals(learnList.get(i).get("jumsu"))) {
					learnList.get(i).put("jumsu", "0");
				}
			}
		}
		
		return vo;
		
  	}
    
    //차시 리스트(페이징)
    public List<?> getLearnListPaging(EgovMap param) throws Exception {
  		return dao.selectList(defaultMapper + ".getLearnListPaging", param);
  	}
    
    
    // 임시
    public static  List<EgovMap> getTopics(String chapterCode) {
    	
    	String[] topics = new String[3];
    	
    	if(chapterCode.contains("0251E")) {
    		List temp = TempLearn.tempMathTopic;
    		Collections.shuffle(temp);    		
    		topics[0] = (String) temp.get(0); 
    		topics[1] = (String) temp.get(1); 
    		topics[2] = (String) temp.get(2);
    		
    	}else if(chapterCode.contains("1051")) {
    		List temp = TempLearn.tempEnglishTopic;    	
    		Collections.shuffle(temp);    		
    		topics[0] = (String) temp.get(0); 
    		topics[1] = (String) temp.get(1); 
    		topics[2] = (String) temp.get(2);
    	}
    	
    	List<EgovMap> learnList = new ArrayList<EgovMap>();
    	for(String topic : topics) {
    		EgovMap temp = new EgovMap();
    		temp.put("topic_code", topic);
    		temp.put("priority", "0.3");
    		learnList.add(temp);
    	}
    
    	return learnList;    	
    }
    
    // 임시
    public static  String getTopicName(String topicCode) {
    	
    	topicCode = topicCode.replace("_video", "");
    	return TempLearn.tempTopicNames.get(topicCode);    	
    }
    
    // 임시
    public static  String getLearnName(String learnCode) {
    	
    	return TempLearn.tempLearnNames.get(learnCode);    	
    }
    
    
    public List<?> getLearnList2Paging(EgovMap param) throws Exception {
    	
    	/*
    	HashMap hMap = new HashMap(param);
		JSONObject jsonObject =  new JSONObject(hMap);
		String jsonParm = jsonObject.toString();
		*/
		
		JSONObject jsonObject =  new JSONObject();
		jsonObject.put("mem_id", param.get("memId"));
		jsonObject.put("subject", param.get("subject"));
		
		List<EgovMap> learnList = null;
		
		learnList = getTopics((String)param.get("chapterCode"));
		if(learnList != null) {
			// 하드코딩 .... 
		}else {
			ApiVO apiVO = apiService.callListSeogangApi("/api/recommend/topic_code", jsonObject.toString());		
			if (apiVO == null) return null;	
			learnList = apiVO.getListEgovMap();
		}
		
		
		String codeList = "";
		
		for(int i = 0; i < learnList.size(); i++) {
			EgovMap m = learnList.get(i);
		    String s = (String)m.get("topicCode");
		    String[] temp = s.split("_");
		    codeList = codeList + temp[0] + ",";
		    String videoFileName = temp[0];
		    if(temp.length >= 2) {
		    	if(temp[1].length() <= 1) {
		    		temp[1] = "0" + temp[1];
		    	}
		    	videoFileName += "_" + temp[1]; 
		    }
		    
		    param.put("learnCode",temp[0]);
		    EgovMap l = dao.selectOne(defaultMapper + ".getLearnInfo", param);
		    
		    learnList.get(i).put("shortName", getLearnName((String)param.get("learnCode")));
		    learnList.get(i).put("longName", getTopicName(videoFileName));
		    learnList.get(i).put("learnCode", param.get("learnCode"));
		    learnList.get(i).put("bookCode", param.get("bookCode"));
		    learnList.get(i).put("chapterCode", param.get("chapterCode"));
		    String mp4 = videoMainHost + '/' + param.get("learnCode") + '/' + videoFileName + ".mp4"; 
		    learnList.get(i).put("mp4", mp4);
		    
		}
		/*
		codeList = codeList.substring(0, codeList.length() - 1);
		String[] arr = codeList.split(",");
		param.put("codeList", Arrays.asList(arr));
		
		return dao.selectList(defaultMapper + ".getLearnList2Paging", param);
		*/
		return learnList;
  	}
    
    // 서강대
    public List<?> getLearnList3Paging(EgovMap param) throws Exception {
    	
    	/*
    	HashMap hMap = new HashMap(param);
		JSONObject jsonObject =  new JSONObject(hMap);
		String jsonParm = jsonObject.toString();
		*/
		
		JSONObject jsonObject =  new JSONObject();
		jsonObject.put("mem_id", param.get("memId"));
		jsonObject.put("subject", param.get("subject"));
		
		List<EgovMap> learnList = null;

		ApiVO apiVO = apiService.callListSeogangApi("/api/recommend/topic_code", jsonObject.toString());		
		if (apiVO == null) return null;	
		learnList = apiVO.getListEgovMap();		
		
		String codeList = "";
		
		for(int i = 0; i < learnList.size(); i++) {
			EgovMap m = learnList.get(i);
		    String s = (String)m.get("topicCode");
		    String[] temp = s.split("_");
		    codeList = codeList + temp[0] + ",";
		    String videoFileName = temp[0];
		    if(temp.length >= 2) {
		    	if(temp[1].length() <= 1) {
		    		temp[1] = "0" + temp[1];
		    	}
		    	videoFileName += "_" + temp[1]; 
		    }
		    
		    param.put("learnCode",temp[0]);
		    EgovMap l = dao.selectOne(defaultMapper + ".getLearnInfo", param);
		    
		    learnList.get(i).put("shortName", getLearnName((String)param.get("learnCode")));
		    learnList.get(i).put("longName", getTopicName(videoFileName));
		    learnList.get(i).put("learnCode", param.get("learnCode"));
		    learnList.get(i).put("bookCode", param.get("bookCode"));
		    learnList.get(i).put("chapterCode", param.get("chapterCode"));
		    String mp4 = videoMainHost + '/' + param.get("learnCode") + '/' + videoFileName + ".mp4"; 
		    learnList.get(i).put("mp4", mp4);
		    
		}
		/*
		codeList = codeList.substring(0, codeList.length() - 1);
		String[] arr = codeList.split(",");
		param.put("codeList", Arrays.asList(arr));
		
		return dao.selectList(defaultMapper + ".getLearnList2Paging", param);
		*/
		return learnList;
  	}
    
    public EgovMap getLearnInfo(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getLearnInfo", param);
  	}   

    //문제 리스트
    public List<?> getProblemList(EgovMap param) throws Exception {
  		return dao.selectList(defaultMapper + ".getProblemList", param);
  	}
    
    public List<?> getProblemList2(EgovMap param) throws Exception {
  		return dao.selectList(defaultMapper + ".getProblemList2", param);
  	}
    

    //지문 정보
    public EgovMap getSentenceInfo(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getSentenceInfo", param);
  	}
    
    //보기 리스트
    public List<?> getExampleList(EgovMap param) throws Exception {
  		return dao.selectList(defaultMapper + ".getExampleList", param);
  	}

    //내용영역 Chart Data 리스트
    public List<?> getContextChartList(EgovMap param) throws Exception {
    	return dao.selectList(defaultMapper + ".getContextChartList", param); //가상데이터
  		//return dao.selectList(defaultMapper + ".getContextChartList2", param);  //실데이터
  	}
    
    //행동영역 Chart Data 리스트
    public List<?> getActivityChartList(EgovMap param) throws Exception {
  		return dao.selectList(defaultMapper + ".getActivityChartList", param); //가상데이터
  		
  		//return dao.selectList(defaultMapper + ".getActivityChartList2", param); //실데이터
  	}
    
    
    //로봇퀴즈 취약 단원명 가져조회
    public EgovMap getRobotQuizChapterInfo(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getRobotQuizChapterInfo", param); //가상데이터
  		//return dao.selectOne(defaultMapper + ".getRobotQuizChapterInfo2", param); //리얼데이터
  	}
    
    //문제 정보
    public EgovMap getProblemInfo(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getProblemInfo", param);
  	}
    
    //단원 총합습시간 조회
    public EgovMap getTotPlayTime(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getTotPlayTime", param);
  	}
    
    //단원 푼 문항수 조회
    public EgovMap getTotProblemAnswer(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getTotProblemAnswer", param);
  	}
    
    //LO_PLAY_TIME 입력
    public void insertLoPlayTime(EgovMap param) throws Exception {
    	if("".equals(param.get("totTime"))) {
    		param.remove("totTime");
    	}
    	dao.dmlInsert(defaultMapper + ".insertLoPlayTime", param);
	}
    
    
    //PB_PROBLEM_SOLVE 입력
    public void insertPbProblemSolve(EgovMap param) throws Exception {
    	
    	// 틀린문제만 가져오기위해 틀린 learn code 가져오기..
  		String includeLearnCodeList = "";
//  		String allLearnCodeList = "";
  		List<EgovMap> learnList = null;
		ApiVO apiVO = null;
		EgovMap paramMap = new EgovMap();
		
		paramMap.put("memberId", param.get("memId"));
		paramMap.put("chapterCode", param.get("chapterCode"));		
			
		String learnCode = TempProblem.tempProblemLearnCode.get(param.get("problemId"));
		param.put("chapterCode", param.get("chapterCode"));
		param.put("learnCode", learnCode);
    	
    	dao.dmlInsert(defaultMapper + ".insertPbProblemSolve", param);
	}
    
    //LO_PLAY_LEARN 입력(사용하지 않음)
    public void insertLoPlayLearn(EgovMap param) throws Exception {
    	dao.dmlInsert(defaultMapper + ".insertLoPlayLearn", param);
	}
    
    //PB_PROBLEM_ANSWER 입력(사용하지 않음)
    public void insertPbProblemAnswer(EgovMap param) throws Exception {
    	dao.dmlInsert(defaultMapper + ".insertPbProblemAnswer", param);
	}
    
    //지문 정보
    public EgovMap getPbProblemSolveCntInfo(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getPbProblemSolveCntInfo", param);
  	} 
    
  // 대시보드 url
    public EgovMap getLrsUrl(EgovMap param) throws Exception {
  		return dao.selectOne(defaultMapper + ".getLrsUrl", param);
  	} 
    
    public String getMessage() {
    	/* 세션 정보 */
		LoginVO loginVO = UserDetailsHelper.getAuthenticatedUser();
		String userName = loginVO.getUserNm();
		String day = "4";
		
		List tempList = TempMessage.tempMessage;
	
		Collections.shuffle(tempList);    		
		String message = (String) tempList.get(0);
		message = message.replaceAll("#name#", userName).replaceAll("#day#", day);
    	return message;
    }
    
    // 최종시험...
    public void insertFinalTest(EgovMap param) throws Exception {
    	dao.dmlInsert(defaultMapper + ".insertFinalTest", param);
	}
    
    //지문 정보
    public List<?> getFinalTest(EgovMap param) throws Exception {
  		return dao.selectList(defaultMapper + ".getFinalTest", param);
  	} 
 }