package sps.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TempMessage {	
	final static List<String> tempMessage  = new ArrayList<String>() {{
		add("추천 문제가 기다리고 있어요");
		add("추천 강의와 문항을 학습해보아요");
		add("추천 강의를 통해 약점을 보완해 보아요");
		add("하루에 5분이라도 공부 습관을 길러보아요");
		add("시작부터 끝까지 응원할께요");
		add("학습은 꾸준하게 하는게 중요해요!! ");
		add("'중요한 것은 꺾이지 않는 마음'");
		add("계속해서 꾸준하게 학습하는게 중요해요");
		add("Always do your best");
		add("잘하고 있어요!! 계속 학습해보아요");
		add("문제를 풀면서 배운 개념을 적용해보아요");
		add("문제를 풀면서 자신감을 길러 보아요");		
	}};
}
