package sps.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TempFinalProblem {	
public final static Map<String, String> tempEnglish = new HashMap<String, String>() {{
		
		put("1", "238179");
		put("2", "238180");
		put("3", "238181");
		put("4", "238182");
		put("5", "238183");
		put("6", "238184");
		put("7", "238185");
		put("8", "238186");
		put("9", "238187");
		put("10", "238188");
		put("11", "238189");
		put("12", "238190");
		put("13", "238191");
		put("14", "238192");
		put("15", "238193");
		put("16", "238194");
		put("17", "238195");
		put("18", "238196");
		put("19", "238197");
		put("20", "238198");
		put("21", "238199");
		put("22", "238200");
		put("23", "238201");
		put("24", "238202");
		put("25", "238203");
		put("26", "238204");
		put("27", "238205");
		put("28", "238206");
		put("29", "238207");
		put("30", "238208");
	}};
	
	public final static Map<String, String> tempMath = new HashMap<String, String>() {{
		
		put("1", "233845");
		put("2", "233849");
		put("3", "233846");
		put("4", "233847");
		put("5", "233848");
		put("6", "233855");
		put("7", "233857");
		put("8", "233858");
		put("9", "233861");
		put("10", "233864");
		put("11", "233863");
		put("12", "233865");
		put("13", "233867");
	}};
	
}
